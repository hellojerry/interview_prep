"""
Given an integer array with all positive numbers
and no duplicates, find the number of possible
combinations that add up to a given target.

ex: [1,2,3], target=4
res: 7

[1,1,1,1]
[1,1,2]
[1,2,1]
[1,3]
[2,1,1]
[2,2]
[3,1]
Note that different sequences are counted as different combinations.
"""

def combination_sum_bad(arr,target,candidate_arr,out):
	"""
	So this is another dynamic programming problem.
	Let's try the recursive version first.
	1. termination points: target == 0: success
	2. target < 0: failure
	The complexity on this thing is insane and will
	nuke the laptop. DONT RUN IT!
	"""
	if target == 0:
		return True
	if target < 0:
		return False
	for item in arr:
		## we're very close to a solution  here.
		## The tricky thing is that we're getting
		## false positives and I'm not sure how to filter them out
		tmp = [item] + candidate_arr.copy()
		if combination_sum(arr,target-item,tmp,out):
			if target - item == 0:
				out.append(tmp)
	return out

def combination_sum(target,arr,arr_idx, matrix):
	"""
	Dynamic programming problems... let's see. 
	need a matrix, and then a way of determining an end point.
	We also need a loop in here, or it needs to take the characteristic
	of a loop.
	"""
	if target == 0:
		## i think we want to return 1 here...
		return 1
	if target < 0 or arr_idx < 0:
		return 0
	current_value = arr[arr_idx]
	if matrix[target][arr_idx] == -1:
		print(target,arr_idx,current_value)
		with_target = combination_sum(target-current_value,arr,arr_idx,matrix)
		without_target = combination_sum(target,arr,arr_idx-1,matrix)
		matrix[target][arr_idx] = with_target+without_target

	return matrix[target][arr_idx]

def kickoff(arr,target):
	arrlen = len(arr)
	matrix = [[-1]*arrlen for i in range(target+1)]
	res = combination_sum(target,arr,arrlen-1,matrix)
	print(matrix)
	return res


if __name__ == "__main__":
	arr = [1,2,3]
	target = 4
	out = []
	#res = combination_sum(arr,target,[],out)
	res = kickoff(arr,target)
	print(res)	
