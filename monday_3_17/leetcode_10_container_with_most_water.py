"""
given an array of integers representing
heights, find the maximum water trapped in 
the array.
"""

def max_area(arr):
	optimal_area = 0
	arrlen = len(arr)
	left_ptr,right_ptr = 0,arrlen-1
	while left_ptr < right_ptr:
		dist = right_ptr - left_ptr
		lheight,rheight = arr[left_ptr], arr[right_ptr]
		area = min(lheight,rheight)*dist
		optimal_area = max(area,optimal_area)
		if lheight > rheight:
			right_ptr -= 1
		else:
			left_ptr += 1
	return optimal_area
