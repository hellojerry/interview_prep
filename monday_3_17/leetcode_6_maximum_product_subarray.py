"""
Given an array of integers,
find the contiguous subarray within the array
which has the largest product.

ex: [2,3,-2,4]
output: 6 (2,3)
ex: [-2,0,-1]
out: 0
"""

def array_permutations(arr):
	"""
	Brute force would be to enumerate
	every possible composition and subcomposition
	so [2], [2*3], [2*3*-2], [2*3*-2*4],
	then [3], [3*-2], [3*-2*4]
	then [-2], [-2*4]
	If we keep track of the maximum and minimum...
	Let's think about this some more
	If the element is less than zero, we want to
	swap the current minimum and maximum values (
	because the sign is inverted). From there,
	we compare the element by itself to the current max *el,
	current min*el, and get the greedy max product.
	"""
	arrlen = len(arr)
	if arrlen == 0:
		return 0
	if arrlen == 1:
		return arr[0]
	current_max,current_min = arr[0], arr[0]
	memo_max = arr[0]
	for i in range(1,arrlen):
		el = arr[i]
		if el < 0:
			current_min,current_max = current_max,current_min
		current_max = max(el, current_max*el)
		current_min = min(el, current_min*el)
		memo_max = max(current_max,memo_max)
	print(current_min,current_max)
	return max(current_max,memo_max)


if __name__ == "__main__":
	a1 = [2,3,-2,4]
	a2 = [-2,0,1]
	res = array_permutations(a1)
	print(res)
