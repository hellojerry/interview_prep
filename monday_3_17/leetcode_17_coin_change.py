"""
You are given coins of different denominations
and a total amount. Write a function to 
compute the fewest number of coins you'd need
to make the change. You have an infinite number
of each coin. if the combination is not possible,
return -1
"""

#def coin_change(amount,coin_idx,coins,matrix):
#	"""
#	So with these sorts of things it's a matter of finding the address on the matrix
#	and then a remaining amount, and taking the better of two choices.
#	In this case, "better" is fewer.
#	"""
#	## I believe this is our end point. either that or amount zeroing out.
#	if amount < 0 or coin_idx < 0:
#		return -1
#	if amount == 0:
#		return 0
#
#	cval = coins[coin_idx]
#	if matrix[amount][coin_idx] == -1:
#		with_current = coin_change(amount-cval, coin_idx,coins,matrix)
#		without_current = coin_change(amount,coin_idx-1,coins,matrix)
#		if with_current < 0 and without_current < 0:
#			matrix[amount][coin_idx] = -1
#		elif with_current < 0:
#			matrix[amount][coin_idx] = 1+ without_current
#
#		elif without_current < 0:
#			matrix[amount][coin_idx] = 1+ with_current
#
#		else:
#			matrix[amount][coin_idx] = 1+ min(with_current,without_current)
#	return matrix[amount][coin_idx]

#def coin_change(remaining,coin_idx,coins,memo_arr):
#
#	if remaining < 0 or coin_idx < 0:
#		return -1
#	if remaining == 0:
#		return 0
#	cval = coins[coin_idx]
#	if memo_arr[remaining] == -1:
#		with_current = coin_change(remaining-cval,coin_idx,coins,memo_arr)
#		without_current = coin_change(remaining,coin_idx-1,coins,memo_arr)
#		if with_current < 0 and without_current < 0:
#			memo_arr[remaining] = -1
#		elif with_current < 0:
#			memo_arr[remaining] = 1 + without_current
#		elif without_current < 0:
#			memo_arr[remaining] = 1 + with_current
#		else:	
#			memo_arr[remaining] = 1+min(with_current,without_current)
#
#	return memo_arr[remaining]

def coin_change_slow(amount,coins, ref):
	"""
	Could we do this simply recursively? This works.
	The problem is that even with 25 and [1,2,5] it
	takes a couple seconds to solve.
	"""
	if amount < 0:
		return -1
	if amount == 0:
		return ref[0]
	ref[0] += 1
	answers = []
	for item in coins:
		sub_res = coin_change_slow(amount-item,coins,ref.copy())
		if sub_res >= 0:
			answers.append(sub_res)
	if len(answers) > 0 and min(answers) >= 0:
		return min(answers)
	return -1
def coin_change(amount,coins,arr):
	"""
	I had it right with the simple recursion answer.
	This is faster from a complexity standpoint.
	we start with candidate amounts,
	and check each coin value vs the candidate amount.
	If it's equal to or less than the candidate amount,
	we find the index on the memo array for the candidate amount
	MINUS the currently tested coin. If the memo value
	is legal, we set the memo array to the tested coin 
	entry + 1.
	"""
	clen = len(coins)
	for i in range(1,amount+1):
		for j in range(clen):
			if coins[j] <= i:
				tmp = arr[i - coins[j]]
				candidate = tmp + 1
				if tmp != float("inf") and candidate < arr[i]:
					arr[i] = candidate
	if arr[amount] == float("inf"):
		return -1
	return arr[amount]

def coin_change_kickoff(amount,coins):
	"""
	This is a dynamic programming problem.
	we need to make a matrix.
	The function needs to proceed until finished.
	"""
	memo_arr = [float("inf") for i in range(amount+1)]
	memo_arr[0] = 0
	res = coin_change(amount,coins,memo_arr)
	print(memo_arr)
	return res

	

if __name__ == "__main__":
	coins = [1,2,5]
	amt = 11
	ref = [0]
	res = coin_change_slow(amt,coins,ref)
	assert(res == 3)
	r2 = coin_change_kickoff(amt,coins)
	print(r2)
	print("^^r2")
	coins = [2]
	amt = 3
	ref = [0]
	res = coin_change_slow(amt,coins,ref)
	assert(res == -1)
	r2 = coin_change_kickoff(amt,coins)
	print(r2)
	coins = [1,2,5]
	amt = 25
	ref = [0]
#	res = coin_change_slow(amt,coins,ref)
#	assert(res == 5)
	r2 = coin_change_kickoff(amt,coins)
	print(r2)
