"""
There are N courses you have to take,
labeled from 0 to N-1
Some courses have prerequisites, i.e.
in order to take course 0, you need to take
course 1 first, and it's annotated this way

[0,1]

Determine whether it's possible to finish all courses

ex:

n=2, prereq = [[1,0]]
res = true

n = 2, prereq = [[1,0],[0,1]]
res = false (circular dependency)
"""

VISITING = 0
VISITED = 1
UNVISITED = 2

def dependency(val,visited,mappings,out):
	if visited[val] == VISITING:
		return False
	elif visited[val] == VISITED:
		return True
	else:
		visited[val] = VISITING
		for course in mappings[val]:
			if not dependency(course,visited,mappings,out):
				return False
		visited[val] = VISITED
		out.insert(0,val)
		return True
		


if __name__ == "__main__":
	## my answer is fine, but there's some bug i'm
	## not seeing on the leetcode copy.

	mappings = {
		0:[],
		1:[0,3],
		2:[0],
		3:[1,2]
	}
	visited = {i:UNVISITED for i in range(4)}
	out = []
	res = True
	for key in visited:
		if not dependency(key,visited,mappings,out):
			res = False
	print(res)


#	mappings = {
#		1:[0],
#		0:[]
#	}
#	visited = {
#		1: UNVISITED,
#		0: UNVISITED
#	}
#	out = []
#	res = dependency(1,visited,mappings,out)
#	print(out)
#	print(res)
#	mappings = {
#		1:[0],
#		0:[1]
#	}
#	visited = {
#		1: UNVISITED,
#		0: UNVISITED
#	}
#	out = []
#	res = dependency(1,visited,mappings,out)
#	print(res)
#	mappings = {
#		0:[1,2],
#		1:[2],
#		2: []
#	}
#	visited = {i:UNVISITED for i in range(3)}
#	out = []
#	res = dependency(0,visited,mappings,out)
#	print(res,out)
