"""
Given an array of non-negative integers,
you are initially positioned at the first
index of the array.
Each element in the array represents your maximum
jump length in that position.
Determine if you are able to reach the last index.

ex: [2,3,1,1,4]
res: true
explanation: jump 1 step from index 0->1, then
3 steps to the last index.

ex: [3,2,1,0,4]
res: False
explanation: you will always arrive at index 3
no matter what. its maximum jump length is 0,
which makes it impossible to reach the final index.
"""

def jump_game_2(arr):
	"""
	I feel like i'm banging my head against the wall
	doing these matrix things over and over again,
	but I don't quite see how other things can work.
	Let's try the recursive stuff first and see what happens
	OK, so the recursive solution works, but it's
	bad from a complexity perspective.
	"""
	arrlen = len(arr)
	if arrlen <= 1:
		return True
	el = arr[0]
	if el == 0:
		return False
	for i in range(el,0,-1):
		if jump_game_2(arr[i:]):
			return True
	return False
		

def jump_game():
	"""
	So this is a dynamic programming problem...
	We can't sort the array...
	We can make a matrix of remaining steps, right?
	And if the current value >= remaining steps,
	we've got a hit. The greedy approach wasn't 
	fast enough.
	"""	


if __name__ == "__main__":
	res = jump_game_2([2,3,1,1,4])
	print(res)
	print(jump_game_2([3,2,1,0,4]))
	print(jump_game_2([0]))
