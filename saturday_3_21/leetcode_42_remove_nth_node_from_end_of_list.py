"""
Given a linnked list, 
remove the n-th node from the end of a list
and return its head.

ex: 1->2->3->4->5, N=2
out: 1->2->3->4->5
Given: N is always valid
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val

def remove_nth_node_from_end_of_list(head,N):
	"""
	So the brute force thing here is to push 
	the linked list to a new array, count from the back, 
	pop the element, and swap the affected pointers.
	This would be o(n) for space and o(n) for complexity

	Another route to take with this is to iterate
	through the entire linked list to get its total
	number of elements, and then do a second pass, counting
	until the target element is reached, and skipping target
	element in order to swap the pointer in. This is O(2N)
	worst case.
	"""
	cur = head
	total = 0
	while cur:
		cur = cur.next_
		total += 1
	if total == N:
		return head.next_
	cur = head
	prior = None
	next_ = cur.next_
	while total > N:
		next_ = cur.next_
		prior = cur
		cur = next_
		total -= 1
	next_ = cur.next_
	prior.next_ = next_
	return head

if __name__ == "__main__":
	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	head = remove_nth_node_from_end_of_list(n1,2)
	out = []
	while head:
		out.append(head)
		head = head.next_
	print(out)
	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	head = remove_nth_node_from_end_of_list(n1,5)
	out = []
	while head:
		out.append(head)
		head = head.next_
	print(out)
