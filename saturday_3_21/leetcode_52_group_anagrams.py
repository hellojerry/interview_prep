"""
Given an array of strings,
group anagrams together.

ex: ["eat","tea","tan","ate","nat","bat"]
out = [
	["ate","eat","tea"],
	["nat","tan"],
	["bat"]
]
"""

def group_anagrams(arr):
	"""
	There are two heuristics I can 
	use to simplify things.
	First, anagrams will always have matching lengths.
	Second, we can determine which words
	are anagrams inside of a set with matching lengths
	by sorting each input string and matching it against
	a sorted prior value.
	What i'm thinking is iterating over the array,
	sorting each individual string, and checking its
	existence in a hashmap. If it exists in the hashmap,
	push the unsorted version out to a result set
	for that mapping.
	how is this a medium? lmfao
	"""
	## the dict would be a string key,
	## than the known values for results in there.
	## at the end, we can transform this to an array
	## of arrays.
	mappings = {}
	for item in arr:
		as_sorted = "".join(sorted(item))
		if not mappings.get(as_sorted):
			mappings[as_sorted] = [item]
		else:
			mappings[as_sorted].append(item)
	out = []
	for k,v in mappings.items():
		out.append(v)
	return out


if __name__ == "__main__":
	arr = ["eat","tea","tan","ate","nat","bat"]
	expected = [
		["ate","eat","tea"],
		["nat","tan"],
		["bat"]
	]
	res = group_anagrams(arr)
	print(res)
