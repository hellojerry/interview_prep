"""
Given a 2d board and a word,
find if the word exists in the grid.
The word can be constructed from letters
of sequentially adjacent words,
where adjacent means horizontally or vertically
adjacent. The same letter cannot be repeated.

arr = [
	[A,B,C,E]
	[S,F,C,S]
	[A,D,E,E]
]
w1 = "ABCCED"
expected_1 = True
w2 = "SEE"
expected_2 = True
w3 = "ABCB"
expected_3 = False
"""

def word_search(matrix,row,col,target_word, cache):
	"""
	So let's plan this out. This looks
	like a recursive sort of problem to me.
	We take a given letter at an index, compare it
	with the lead letter, if they match, compare
	the next letter in each direction recursively until
	a match is found. We can use a temporary array to 
	store used indices

	"""
	rows=  len(matrix)
	cols = len(matrix[0])
	max_col,max_row = cols-1,rows-1
	if len(target_word) == 0:
		return True
	if not (0 <= row <= max_row) or not (0 <= col <= max_col):
		return False
	if (row,col) in cache:
		return False
	current_letter = matrix[row][col]
	if target_word[0] == current_letter:
		cache.add((row,col))
		reduced = target_word[1:]
		## left
		if word_search(matrix,row,col-1,reduced,cache):
			return True
		if word_search(matrix,row,col+1,reduced,cache):
			return True
		if word_search(matrix,row-1,col,reduced,cache):
			return True
		if word_search(matrix,row+1,col,reduced,cache):
			return True
		cache.remove((row,col))	
	return False

def kickoff(matrix,target_word):
	rows = len(matrix)
	cols = len(matrix[0])
	for r in range(rows):
		for c in range(cols):
			cache = set()
			if word_search(matrix,r,c,target_word,cache):
				return True
	return False

if __name__ == "__main__":
	
	arr = [
		["A","B","C","E"],
		["S","F","C","S"],
		["A","D","E","E"],
	]
	w1 = "ABCCED"
	expected_1 = True
	res2 = kickoff(arr,w1)
	print(res2)
	w2 = "SEE"
	expected_2 = True
	res2 = kickoff(arr,w2)
	print(res2)
	w3 = "ABCB"
	expected_3 = False
	res3 = kickoff(arr,w3)
	print(res3)
