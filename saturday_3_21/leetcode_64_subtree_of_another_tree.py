"""
Given two non-empty binary trees
S and T, check whether T is a subtree
of S. The tree S could also be considered
a subtree of itself.

ex:
	3		4
      4    5          1   2
    1  2
(true)

	3		3
     4    5          4    5
(true)

	3		4
     4     5         1    2
   1   2
      0
(false)
"""

def is_same_tree(left,right):
	if not left and not right:
		return True
	if (left and not right) or (right and not left):
		return False
	lqueue = [left]
	rqueue = [right]
	while lqueue and rqueue:
		cur_left,cur_right = lqueue.pop(0),rqueue.pop(0)
		if cur_left.val != cur_right.val:
			return False
		if (cur_left.left and not cur_right.left):
			return False
		if (not cur_left.left and cur_right.left):
			return False
		if (cur_left.right and not cur_right.right):
			return False
		if (not cur_left.right and cur_right.right):
			return False
		if cur_left.left:
			lqueue.append(cur_left.left)
		if cur_left.right:
			lqueue.append(cur_left.right)
		if cur_right.left:
			rqueue.append(cur_right.left)
		if cur_right.right:
			rqueue.append(cur_right.right)
	return True

def is_subtree(left,right):
	"""
	So we want to ensure that RIGHT
	is a subtree of LEFT.
	Here is my strategy:
	1. we do a BFS on the left tree.
	2. working through the values on the left tree,
	if we run into a value that equals the root of right,
	we call another function, "is_same_tree". if that's true,
	we return true. 
	otherwise, we continue until we've exhausted the queue.
	"""

	queue = [left]
	while queue:
		cur = queue.pop(0)
		if cur.val == right.val:
			if is_same_tree(cur,right):
				return True
		if cur.left:
			queue.append(cur.left)
		if cur.right:
			queue.append(cur.right)
	return False
