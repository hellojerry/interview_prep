"""
Given an MxN matrix, if an element is 
set to zero, set its entire row AND column
to zero, in-place.

ex:
arr = [
	[1,1,1],
	[1,0,1],
	[1,1,1]
]
out = [
	[1,0,1],
	[0,0,0],
	[1,0,1]
]

arr = [
	[0,1,2,0],
	[3,4,5,2],
	[1,3,1,5]
]
out = [
	[0,0,0,0],
	[0,4,5,0],
	[0,3,1,0]
]
"""

def set_zeroes(m):
	"""
	My thought here is to keep two sets representing 
	rows and columns containing zeroes.
	We iterate over rows and columns, pushing to the sets
	when a zero is found.
	After completion, we iterate over rows, zeroing
	all tripped rows, and then the same for the columns.

	We could speed this up by starting out with the legal rows
	and columns (all of them), and then removing values from each
	... i think.	
	"""
	rows = len(m)
	cols = len(m[0])
	target_rows,target_cols = set(),set()
	for r in range(rows):
		for c in range(cols):
			if m[r][c] == 0:
				target_rows.add(r)
				target_cols.add(c)
	for row in target_rows:
		for el in range(cols):
			m[row][el] = 0
	for col in target_cols:
		for el in range(rows):
			m[el][col] = 0

if __name__ == "__main__":
	arr = [
		[1,1,1],
		[1,0,1],
		[1,1,1]
	]
	set_zeroes(arr)
	out = [
		[1,0,1],
		[0,0,0],
		[1,0,1]
	]
	assert(arr == out)
	
	arr = [
		[0,1,2,0],
		[3,4,5,2],
		[1,3,1,5]
	]
	set_zeroes(arr)
	out = [
		[0,0,0,0],
		[0,4,5,0],
		[0,3,1,0]
	]
	assert(arr == out)
