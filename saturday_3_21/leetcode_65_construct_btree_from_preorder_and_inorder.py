"""
Given a preorder and inorder
traversal of a tree,
construct a binary tree

ex:
preorder: [3,9,20,15,7]
inorder: [9,3,15,20,7]

out:
	3
     9     20
        15     7
"""
class TreeNode(object):

	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.val

	def __repr__(self):
		return "TreeNode(%d)" % self.val
def build_preorder(root,out):
	if not root:
		return 
	out.append(root.val)
	build_preorder(root.left,out)
	build_preorder(root.right,out)

def build_inorder(root,out):
	if not root:
		return
	build_inorder(root.left,out)
	out.append(root.val)
	build_inorder(root.right,out)
	

def reconstruct_preorder_inorder(preorder,inorder):
	"""
	Now that the setup is done, the strategy.
	We know that the root element of the tree is the first
	element of the preorder. From there,
	we can find the location of the root at the inorder.
	Everything to the left of the inorder index corresponds
	to the left side of the preorder tree.
	Everything to the right is on the right side of the preorder tree.
	We can gradually build by removing elements of the array
	"""
	if not preorder:
		return None
	root_val = preorder[0]
	root = TreeNode(root_val)
	root_idx_inorder = inorder.index(root_val)
	left_subtree_inorder = inorder[:root_idx_inorder]
	right_subtree_inorder = inorder[root_idx_inorder+1:]
	left_subtree_preorder = preorder[1:len(left_subtree_inorder)+1]
	right_subtree_preorder = preorder[len(left_subtree_inorder)+1:]
	root.left = reconstruct_preorder_inorder(left_subtree_preorder,left_subtree_inorder)
	root.right = reconstruct_preorder_inorder(right_subtree_preorder,right_subtree_inorder)
	return root

if __name__ == "__main__":
	preorder = [3,9,20,15,7]
	inorder = [9,3,15,20,7]
	out_root = reconstruct_preorder_inorder(preorder,inorder)
	print(out_root)
	out_1,out_2 = [],[]
	as_preorder = build_preorder(out_root,out_1)
	print(out_1 == preorder)
	print(out_1)
	as_inorder = build_inorder(out_root,out_2)
	print(out_2 == inorder)
	print(out_2)
