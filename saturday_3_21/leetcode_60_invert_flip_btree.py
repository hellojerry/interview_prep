"""
Invert a binary tree

ex:
	4
     2     7
 1    3  6   9

out:
	4
     7     2
   9  6   3  1
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def preorder(root,out):
	if not root:
		return
	out.append(root)
	preorder(root.left,out)
	preorder(root.right,out)


def flip_btree(root):
	"""
	Let's think about this. 
	My first thought is to do a BFS.
	We keep track of the level in the output array
	and swap the pointers or some shit. I can't think.
	The other option is a DFS where we swap root.left and root.right recursively.
	Let's try that first actually
	"""
	if not root:
		return None
	root.left,root.right = flip_btree(root.right),flip_btree(root.left)
	return root

if __name__ == "__main__":
	n1 = TreeNode(1)
	n3 = TreeNode(3)
	n2 = TreeNode(2,n1,n3)
	n6 = TreeNode(6)
	n9 = TreeNode(9)
	n7 = TreeNode(7,n6,n9)
	root = TreeNode(4,n2,n7)
	out = []
	expected = preorder(root,out)
	res = flip_btree(root)
	out2 = []
	preorder(root,out2)
	print(out,out2)
