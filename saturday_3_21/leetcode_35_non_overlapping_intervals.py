"""
Given a collection of intervals
find the minimum number of intervals
you need to remove to make the rest
of the intervals non-overlapping

ex: [[1,2],[2,3],[3,4],[1,3]]
out: 1
explanation: you can remove [1,3]
and nothing else overlaps

ex: [[1,2],[1,2],[1,2]]
out: 2
you need to remove [1,2] twice
"""

def remove_overlaps_2(arr):
	"""
	So the problem statement implies that
	the data won't come in in a sorted manner,
	so we're going to want to account for that.
	Next thing... let's consider a plan of attack.
	I think this is just a matter of sorting things
	This isn't quite working.
	"""
	arr.sort(key=lambda x: (x[0],x[1]))
	print(arr)
	if len(arr) <= 1:
		return 0
	out = [arr[0]]
	to_remove = 0
	for item in arr[1:]:
		if item[0] < out[-1][1]:
			to_remove += 1
		else:
			out.append(item)
	print(out)
	return to_remove

def remove_overlaps(arr):
	"""
	So attacking from one angle isn't quite right, but i'm
	not sure exactly why. logically, we'd want to remove all
	but the smallest distance for a given starting with X. I think we've done
	that. I am not sure where i'm missing something
	"""
	arr.sort(key = lambda x: (x[1]))
	out = [arr[0]]
	to_remove = 0
	for item in arr[1:]:
		if item[0] == out[-1][0]:
			to_remove += 1
			continue
		if item[0] < out[-1][1]:
			to_remove += 1
			continue
		out.append(item)
	return to_remove

if __name__ == "__main__":
	a1 = [[1,2],[2,3],[3,4],[1,3]]
	res = remove_overlaps(a1)
	assert(res == 1)
	a2 = [[1,2],[1,2],[1,2]]
	res = remove_overlaps(a2)
	assert(res == 2)
	a3 = [[1,100],[11,22],[1,11],[2,12]]
	res = remove_overlaps(a3)
	assert(res == 2)
