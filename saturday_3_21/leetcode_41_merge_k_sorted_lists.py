"""
Merge K sorted lists and return it as
one sorted list. Analyze and describe its complexity.

ex:
arr = [1->4->5,
	1->3->4,
	2->6
]

out = 1->1->2->3->4->4->5->6
"""

class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val
def merge_k_sorted_lists(arr):
	"""
	Let's figure out a strategy with this.
	When merging two lists, it was a matter of adding 
	the lowest value to the tail out out. I think the principle
	Is the same here. The difference comes in the mechanics of
	rolling the pointers forward inside the input list, and in
	the selection of the lowest values.
	1. investigate min() behavior when None is a value -> throws error
	From a top-level perspective, the process is:
		1. find smallest value, initialize that as out_head, 
		move its pointer forward in input array
		2. kick off a while loop, finding minimum value in
		the array, using that as the next, resetting cur, and rolling
		minimum value's pointer
		3. when a given input array node's next is none, we pop it from
		the array 
	Notes for later: can we use some sort of sorting on the array nodes to reduce complexity?
	Answer: if we make this a heap based on the nodes, we can improve complexity a bit.
	My solution uses constant space
	"""
	## handle stupid null input stuff
	arr = [k for k in arr if k]
	if not arr:
		return None
	if len(arr) == 1:
		return arr[0]
	as_integers = [k.val for k in arr]
	initial_start = min(as_integers)
	initial_idx = as_integers.index(initial_start)
	out_head,out_cur = arr[initial_idx],arr[initial_idx]
	## roll the first pointer forward, and delete if it's none
	arr[initial_idx] = arr[initial_idx].next_
	if arr[initial_idx] is None:
		arr.pop(initial_idx)
	while len(arr) > 1:
		as_integers = [k.val for k in arr]
		target_val = min(as_integers)
		target_idx = as_integers.index(target_val)
		to_add = arr[target_idx]
		out_cur.next_ = to_add
		to_add_next = to_add.next_
		arr[target_idx] = to_add_next
		if arr[target_idx] is None:
			arr.pop(target_idx)
		out_cur = out_cur.next_
	out_cur.next_ = arr[0]
	return out_head

if __name__ == "__main__":
	n5a = Node(5)
	n4a = Node(4,n5a)
	n1a = Node(1,n4a)

	n4b = Node(4)
	n3b = Node(3,n4b)
	n1b = Node(1,n3b)

	n6c = Node(6)
	n2c = Node(2,n6c)
	arr = [n1a,n1b,n2c]
	out_head = merge_k_sorted_lists(arr)
	out = []
	while out_head:
		out.append(out_head)
		out_head = out_head.next_
	print(out)
