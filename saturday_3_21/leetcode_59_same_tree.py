"""
Given two binary trees,
write a function to check if they are the same or not.
Two binary trees are considered the same if they are
structurally identical and the nodes have the same values.

ex
	1		1
      2   3	       2  3
(true)

	1		1
      2                    2
(false)

	1		1
     2     1          1    2
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def is_same_tree(left,right):
	"""
	So, if we do a BFS on each, we should
	be able to have matching values when going through 
	the queue
	"""
	if not left and not right:
		return True
	if (left and not right) or (right and not left):
		return False
	lqueue = [left]
	rqueue = [right]
	while lqueue and rqueue:
		left_cur = lqueue.pop(0)
		right_cur = rqueue.pop(0)
		if left_cur.val != right_cur.val:
			return False
		if (not left_cur.left and right_cur.left):
			return False
		if (left_cur.left and not right_cur.left):
			return False
		if (not left_cur.right and right_cur.right):
			return False
		if (left_cur.right and not right_cur.right):
			return False

	if not lqueue and not rqueue:
		return True
	return False
if __name__ == "__main__":
	n2a = TreeNode(2)
	n3a = TreeNode(3)
	n1a = TreeNode(1,n2a,n3a)

	n2b = TreeNode(2)
	n3b = TreeNode(3)
	n1b = TreeNode(1,n2b,n3b)
	print(is_same_tree(n1a,n1b))
	n2a = TreeNode(2)
	n1a = TreeNode(1,n2a)
	n2b = TreeNode(2)
	print(n2b == None)
	n1b = TreeNode(1,None,n2b)
	print(is_same_tree(n1a,n1b))
