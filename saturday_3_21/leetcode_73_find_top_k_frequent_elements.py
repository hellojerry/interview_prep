"""
fiven a non-empty array of integers,
return the K most frequent elements.

a1 = [1,1,1,2,2,3]
k1 = 2
expected1 = [1,2]

a2 = [1]
k2 = [1]
expected2 = [1]
"""
import heapq
def top_k_frequent_elements(arr,K):
	frequencies = {}
	for k in arr:
		if not frequencies.get(k):
			frequencies[k] = 1
		else:
			frequencies[k] += 1
	heap = []
	for k,v in frequencies.items():
		heapq.heappush(heap, (-v,k))
	out = []
	print(K)
	while K > 0:
		out.append(heapq.heappop(heap)[1])
		K -= 1
	print(out)
	return out

if __name__ == "__main__":
	a1 = [1,1,1,2,2,3]
	k1 = 2
	expected1 = [1,2]
	r1 = top_k_frequent_elements(a1,k1)
	print(r1 == expected1)
	
	a2 = [1]
	k2 = 1
	expected2 = [1]
	r2 = top_k_frequent_elements(a2,k2)
	print(r2 == expected2)
