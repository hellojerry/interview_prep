"""
Implement a prefix tree.
Needs teh following methods:

insert:
search:
startswith:

ex:
my_trie = Trie()
trie.insert("apple")
r1 = trie.search("apple")
print(r1 == True)
r2 = trie.search("app")
print(r2 == False)
r3 = trie.startsWith("app")
print(r3 == True)
trie.insert("app")
r4 = trie.search("app")
print(r4 == True)
"""
from collections import defaultdict

class PrefixTree(object):

	def __init__(self):
		self.mappings = {}
		self.terminator = "__END__"

	def insert(self, word):
		current = self.mappings
		for letter in word:
			current = current.setdefault(letter, {})
		current[self.terminator] = self.terminator
	def search(self,word):
		current = self.mappings
		for letter in word:
			val = current.get(letter)
			if not val:
				return False
			current = val
		if not current.get(self.terminator):
			return False
		return True
	def startswith(self,word):
		current = self.mappings
		for letter in word:
			val = current.get(letter)
			if not val:
				return False
			current = val
		return True


if __name__ == "__main__":
	trie = PrefixTree()
	trie.insert("apple")
	print(trie.mappings)
	r1 = trie.search("apple")
	print(r1 == True)
	r2 = trie.search("app")
	print(r2 == False)
	r3 = trie.startswith("app")
	print(r3 == True)
	trie.insert("app")
	r4 = trie.search("app")
	print(r4 == True)
