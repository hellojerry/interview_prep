"""
Merge two sorted linked lists
and return it as a new list.
The new list should be made by splicing 
together the nodes of the first two lists.

ex: 1->2->4, 1->3->4
out: 1->1->2->3->4->4
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val

def merge_two_sorted_lists(n1,n2):
	"""
	What is the strategy to take here?
	First, as a matter of sanity, we'll want
	to make sure that n1's head is the lower
	of the two values.
	"""
	"""
	Until we reach the tail
	of one of the lists, we have no knowledge of 
	which value should be next in the output list, beyond
	the presently visible pointer in each list. 
	We should take the head of each list and retain pointers
	to both. We make the smaller value the head out "output",
	move that LL forward one node, and then kick off a while loop.
	In the while loop, we compare left.val to right.val. 
	If they're equal, attach both values to the output list and move
	both forward
	otherwise, attach the lesser value to the output list, and move it forward.
	break the while loop when we hit an empty linked pointer on one or both values.
	from that point, attach the head of the non-empty list to the tail of the output list.
	"""
	### edge case checking
	if not n1 and not n2:
		return n1
	if not n1:
		return n2
	if not n2:
		return n1
	if n1.val > n2.val:
		n1,n2 = n2,n1
	out_head = n1
	out_cur = n1
	## another edge case
	if not n1.next_:
		out_head.next_ = n2
		return out_head
	left_head, right_head = n1,n2
	left_cur = left_head.next_
	right_cur = right_head
	while left_cur and right_cur:
		l_next = left_cur.next_
		r_next = right_cur.next_
		if right_cur.val < left_cur.val:
			out_cur.next_ = right_cur
			right_cur = r_next
		else:
			out_cur.next_ = left_cur
			left_cur = l_next
		out_cur = out_cur.next_
	if left_cur:
		out_cur.next_ = left_cur
	if right_cur:
		out_cur.next_ = right_cur
	return out_head


if __name__ == "__main__":
	n4l = Node(4)
	n2l = Node(2,n4l)
	n1l = Node(1,n2l)

	n4r = Node(4)
	n3r = Node(3,n4r)
	n1r = Node(1,n3r)
	out_head = merge_two_sorted_lists(n1l,n1r)
	out = []
	while out_head:
		out.append(out_head.val)
		out_head = out_head.next_
	print(out)

	n5l = Node(5)
	n4l = Node(4,n5l)
	n4l2 = Node(4,n4l)
	n2l = Node(2,n4l2)
	n1l = Node(1,n2l)

	n4r = Node(4)
	n3r = Node(3,n4r)
	n1r = Node(2,n3r)
	out_head = merge_two_sorted_lists(n1r,n1l)
	out = []
	while out_head:
		out.append(out_head.val)
		out_head = out_head.next_
	print(out)
