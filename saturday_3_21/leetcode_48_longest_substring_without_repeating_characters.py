"""
Given a string, find
the length of the longest substring
without repeating characters.

ex: abcabcbb
res: 3 (abc)

ex: bbbbb
res: 1 (b)

ex: pwwkew
res: 3 (wke)
"""

def find(my_str,char):
	strlen = len(my_str)
	ptr = 0
	while ptr < strlen:
		el = my_str[ptr]
		if el == char:
			return ptr
		ptr += 1
	return -1

def longest_substring(my_str):
	"""
	This looks like it's a greedy algorithm.
	I think what we can do here is push values to a set
	and retain a candidate substring.
	if a new character is inside the set, we want to
	remove up until and including the occurrence of
	the repeated character inside the set, continuing
	until we reach the end (removing all other values in the
	prefix from the candidate set)
	"""
	strlen = len(my_str)
	if strlen == 0:
		return 0
	if strlen == 1:
		return 1
	candidate = my_str[0]
	max_len = 1
	ptr = 1
	while ptr < strlen:
		el = my_str[ptr]
		idx = find(candidate,el)
		if idx >= 0:
			candidate = candidate[idx+1:] + el
		else:
			candidate += el
		max_len = max(len(candidate),max_len)
		ptr += 1
	return max_len

if __name__ == "__main__":
	ex = "abcabcbb"
	expected = 3
	res = longest_substring(ex)
	print(res, res==expected)
	
	ex = "bbbbb"
	expected = 1
	res = longest_substring(ex)
	print(res, res==expected)
	
	ex = "pwwkew"
	expected = 3
	res = longest_substring(ex)
	print(res, res==expected)
