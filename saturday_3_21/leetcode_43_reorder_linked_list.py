"""
Given a singly linked list..

L0->l1->l2...->L(n-1)->l(n)

reorder it to:

L0->L(N)->l1->l(n-1)...

ex: 1->2->3->4
out: 1->4->2->3

ex: 1->2->3->4->5
out: 1->5->2->4->3
note: don't modify the node values,
just the pointers.
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val
def reorder_list(head):
	"""
	I really have to take a dump.
	Ok, so this is interesting.
	Free associating: left and right pointers,
	alternating, and then reverse the right pointers
	before zipping in? so:
	1,3
	2,4 
	-> 
	1,3 & 4,2
	then
	1->4->3->2

	1,3,5
	2,4
	then
	1,3,5 & 4,2
	then 
	1,4,3,2,5

	or, just moving the right in after a reverse?
	1,3 & 4,2 -> 1,4,2,3
	1,3,5 & 4,2 -> 1->4->2->3->5
	No, left and right + reverse and zip won't work.
	brute force: push values to a separate array,
	swapping N with l1... no.
	brute force, push values to a separate array,
	move tail to arr[1], then tail to arr[3], until ptr >= arrlen-2
	1->2->3->4
	1->4->2->3

	1->2->3->4->5
	1->5->2->3->4 (ptr=1)
	1->5->2->4->3 (ptr=3)
	This solution works, but it's o(N)
	for space, and it requires two passes over the linked list.
	I think we can get it down to a single pass
	"""
	cur = head
	arr = []
	while cur:
		arr.append(cur)
		cur = cur.next_
	arrlen = len(arr)
	if len(arr) < 3:
		return head
	ptr = 1
	#print(arr)
	while ptr <= arrlen - 2:
		tail = arr.pop()
		arr.insert(ptr,tail)
		ptr += 2
	ptr = 0
	#print(arr)
	for i in range(1,arrlen):
		el = arr[i]
		arr[ptr].next_ = el
		ptr += 1
	arr[-1].next_ = None
	return arr[0]

def reverse_ll(head):

	cur = head.next_
	prev = head
	prev.next_ = None
	while cur:
		next_ = cur.next_
		cur.next_ = prev
		prev = cur
		cur = next_
	return prev
		

def reorder_list_2(head):
	"""
	I think i might've been going in the right direction
	by reversing the second half. we get the total amount,
	reverse the second half, and then ...
	1->2->3 and 4->5
	then:
	1->2->3 and 5->4
	then interweave 1->5->2->4->3

	1->2 and 3->4
	then
	1->2 and 4->3
	then interweave 1->4->2->3
	So, mechanically:
		1. traverse entire LL, getting the total
		2. start back at the head, iterate until 
			total/2 is reached (or total/2 + 1 if odd)
		3. reverse at the found location, setting
			result of reversal as right_head
		4. while left and right:
			interweave or a finite state thing 
			or someething - worry about this in a bit 
		we can speed this up by doing the slow-fast pointer thing
		and getting the reverse that way.
	"""
	total = 0
	cur = head
	while cur:
		total += 1
		cur = cur.next_
	target = int(total/2) if total % 2 == 0 else int(total/2) + 1
	cur = head
	ptr = 0
	prior = None
	while ptr < target:
		prior = cur
		cur = cur.next_
		ptr += 1
	#print("node after target acquisition",cur)
	## ntoe: might be introducing a bug here by not clearing
	## out the next on the final before target...
	prior.next_ = None
	reversed_head = reverse_ll(cur)
	left_head,right_head = head, reversed_head
	left_cur,right_cur = left_head,right_head
	### now.. uhh..stumbling on mechanics here
	left_cur = left_cur.next_
	out = left_head
	out.next_ = None
	even = False
	while left_cur and right_cur:
		if even:
			l_next = left_cur.next_
			out.next_ = left_cur
			out = out.next_
			left_cur = l_next
		else:
			r_next = right_cur.next_
			out.next_ = right_cur
			out = out.next_
			right_cur = r_next
		even = not even
	while right_cur:
		out.next_ = right_cur
		out = out.next_
		right_cur = right_cur.next_
	while left_cur:
		out.next_ = left_cur
		out = out.next_
		left_cur = left_cur.next_
	return left_head


if __name__ == "__main__":
	#e4 = Node(4)
	#e3 = Node(3,e4)
	#e2 = Node(2,e3)
	#e1 = Node(1,e2)
	#head = reverse_ll(e1)
	#out = []
	#while head:
	#	out.append(head)
	#	head = head.next_
	#print("out",out)

	#o5 = Node(5)
	#o4 = Node(4,o5)
	#o3 = Node(3,o4)
	#o2 = Node(2,o3)
	#o1 = Node(1,o2)
	#reorder_list_2(o1)
	#head = reorder_list(o1)
	#out = []
	#while head:
	#	out.append(head)
	#	head = head.next_
	#print(out)
	e4 = Node(4)
	e3 = Node(3,e4)
	e2 = Node(2,e3)
	e1 = Node(1,e2)
	head = reorder_list_2(e1)
	out = []
	while head:
		out.append(head)
		head = head.next_
	print("out",out)
	o5 = Node(5)
	o4 = Node(4,o5)
	o3 = Node(3,o4)
	o2 = Node(2,o3)
	o1 = Node(1,o2)
	head = reorder_list_2(o1)
	out = []
	while head:
		out.append(head)
		head = head.next_
	print(out)
