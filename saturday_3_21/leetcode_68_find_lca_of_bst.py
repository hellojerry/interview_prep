"""
Given a BST, find the lowest common ancestor
of two given nodes in the BST

ex:
		6
	 2         8
      0    4     7   9
         3   5
left = 2, right = 8
out = 6
left = 3, right = 4
out = 4
left = 2, right=  4
out = 2
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def find_lca(root,left,right):
	"""
	Having trouble thinking. Might be
	best to try this again tomorrow.
	Welp, it worked lol
	"""
	if left.val <= root.val <= right.val:
		return root
	if left.val > root.val:
		return find_lca(root.right,left,right)
	return find_lca(root.left,left,right)

if __name__ == "__main__":
	n3 = TreeNode(3)
	n5 = TreeNode(5)
	n4 = TreeNode(4,n3,n5)
	n0 = TreeNode(0)
	n2 = TreeNode(2,n0,n4)
	n7 = TreeNode(7)
	n9 = TreeNode(9)
	n8 = TreeNode(8,n7,n9)
	root = TreeNode(6,n2,n8)
	r1 = find_lca(root,n2,n8)
	r2 = find_lca(root,n3,n4)
	r3 = find_lca(root,n2,n4)
	print(r1.val == 6)
	print(r2.val == 4)
	print(r3.val == 2)
