"""
Given a matrix of m x n elements
return the elements of the matrix
in spiral order.

ex:
arr = [
	[1,2,3],
	[4,5,6],
	[7,8,9]
]
out = [1,2,3,6,9,8,7,4,5]

arr = [
	[1,2,3,4],
	[5,6,7,8],
	[9,10,11,12]
]

out = [1,2,3,4,8,12,11,10,9,5,6,7]
"""
RIGHT = 0
DOWN = 1
LEFT = 2
UP = 3

DIRECTIONS = {
	RIGHT:DOWN,
	DOWN:LEFT,
	LEFT:UP,
	UP:RIGHT
}


def spiral_matrix(matrix,r,c,direction,out):
	"""
	So let's think about this a bit.
	first it's (0,0),(0,1)... 0(maxcol)
	then it's (1,maxcol).. (maxrow,maxcol)
	then (maxrow,maxcol-1)...(maxrow,0)
	then (maxrow-1,0).. (minrow+1,0)
	then moves right, rinse and repeat.
	Assuming None is not going to be part of the array,
	I can keep track of the state of the direction, and 
	i'll know when to do a state change based on the next element
	being illegal due to boundaries, or illegal due to the next
	element in the current state progression being none.
	We might want to do this recursively to make things simpler.
	Let's slow down. We need to plan this a bit more.
	How do we finalize the result? We know we've completed
	the spiral when going forward with the current state is ineligible,
	AND going forward with the current state after that is ineligible. 
	"""

	m = matrix
	rows,cols = len(m),len(m[0])
	min_row,max_row = 0, rows-1
	min_col,max_col = 0, cols-1
	current_val = m[r][c]
	out.append(current_val)
	m[r][c] = None
	## there's got to be a way to write this with less code.
	if direction == RIGHT:
		if c < max_col:
			if matrix[r][c+1] is None:
				if matrix[r+1][c] is None:
					return out
				else:
					return spiral_matrix(
						m,r+1,c,DOWN,out)
			else:
				return spiral_matrix(
					m,r,c+1,RIGHT,out)
				
		else:
			if matrix[r+1][c] is None:
				return out
			else:
				return spiral_matrix(m,
					r+1,c,DOWN,out)
	elif direction == DOWN:
		if r < max_row:
			if matrix[r+1][c] is None:
				if matrix[r][c-1] is None:
					return out
				else:
					return spiral_matrix(
						m,r,c-1,LEFT,out)
			else:
				return spiral_matrix(
					m,r+1,c,DOWN,out)
		else:
			if matrix[r][c-1] is None:
				return out
			else:
				return spiral_matrix(
					m,r,c-1,LEFT,out)

	elif direction == LEFT:
		if c > min_col:
			if matrix[r][c-1] is None:
				if matrix[r-1][c] is None:
					return out
				else:
					return spiral_matrix(
					m,r-1,c,UP,out)
			else:
				return spiral_matrix(
					m,r,c-1,LEFT,out)
		else:
			if matrix[r-1][c] is None:
				return out
			else:
				return spiral_matrix(
				m,r-1,c,UP,out)
	else:
		if r > min_row:
			if matrix[r-1][c] is None:
				if matrix[r][c+1] is None:
					return out
				else:
					return spiral_matrix(
					m,r,c+1,RIGHT,out)
		else:
			if m[r][c+1] is None:
				return out
			else:
				return spiral_matrix(
					m,r+1,c,RIGHT,out)

def spiral_2(m,r,c,direction,out):
	"""
	Prior implementation is too ugly.
	"""	

	


if __name__ == "__main__":
	arr = [
		[1,2,3],
		[4,5,6],
		[7,8,9]
	]
	acc = []
	res = spiral_matrix(arr,0,0,RIGHT,acc)
	out = [1,2,3,6,9,8,7,4,5]
	print(res == out)
	arr = [
		[1,2,3,4],
		[5,6,7,8],
		[9,10,11,12]
	]
	acc = []	
	out = [1,2,3,4,8,12,11,10,9,5,6,7]
	res = spiral_matrix(arr,0,0,RIGHT,acc)
	print(res == out)
