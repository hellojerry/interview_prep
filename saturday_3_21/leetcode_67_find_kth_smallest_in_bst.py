"""
Given a valid BST,
find the kth smallest element in the tree and return it

	3
     1     4
       2
K = 1
expected = 1

	5
     3 	   6
   2   4
1
K = 3
expected = 3
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def find_kth_smallest_node(root,K_ref):
	"""
	Free associating:
	- i could brute force enumerate the entire tree
	into a sorted array, and then count up to K and return it
	Otherwise, I feel like I need to do an inorder search,
	decrementing a reference passed in until that reference is 0,
	and returning once I hit zero.
	doing kth largest is pretty much the same here,
	but it's a reverse inorder search
	"""
	if not root:
		return None
	out = find_kth_smallest_node(root.left,K_ref)
	if out:
		return out
	K_ref[0] -= 1
	if K_ref[0] == 0:
		return root
	return find_kth_smallest_node(root.right,K_ref)
	



if __name__ == "__main__":
	n2 = TreeNode(2)
	n1 = TreeNode(1,None,n2)
	n4 = TreeNode(4)
	root = TreeNode(3,n1,n4)
	res = find_kth_smallest_node(root,[1])
	print(res.val == 1)
	r2 = find_kth_smallest_node(root,[4])
	print(r2 == 4)
	n1 = TreeNode(1)
	n2 = TreeNode(2,n1)
	n4 = TreeNode(4)
	n3 = TreeNode(3,n2,n4)
	n6 = TreeNode(6)
	root = TreeNode(5,n3,n6)
	res = find_kth_smallest_node(root,[3])
	print(res.val == 3)
