# Leetcode Prep

## Monday 3/9

1. Leetcode 1: two sum - DONE
2. Leetcode 2: max profit, buy and sell a stock once - DONE
3. Leetcode 3: array contains dupe - DONE
4. Leetcode 4: product of array except self - DONE
5. Leetcode 5: maximum contiguous subarray - DONE
6. Leetcode 7: minimum in rotated sorted array - DONE
7. Leetcode 8: search in rotated sorted array - DONE
8. Leetcode 9: 3 sum - DONE
9. Leetcode 12: number of 1 bits - DONE
10. Leetcode 15: reverse bits in 32 bit integer - DONE
11. Leetcode 38: Reverse a Linked List - DONE
12. Leetcode 61: Btree Maximum Path Sum - DONE
13. Leetcode 54: Valid Palindrome - DONE
14. Leetcode 53: Valid Parentheses - DONE
15. Leetcode 66: Validate BST - DONE
16. Leetcode 51: Valid Anagram - DONE
17. Leetcode 39: Detect a cycle in linked list - DONE
18. Leetcode 14: missing number - DONE


## Monday 3/17

1. Leetcode 10: container with most water - DONE
2. Leetcode 6: maximum product subarray - DONE
3. Leetcode 17: Coin change - DONE
4. Leetcode 26: Course Schedule - DONE

## Tuesday 3/18

1. Leetcode 24: Jump Game - DONE
2. Leetcode 33: Insert Interval - DONE
3. Leetcode 34: Merge Intervals - DONE
4. Leetcode 46: Rotate Image - DONE
5. Leetcode 62: BTree Level order Traversal - DONE
6. Leetcode 58: Maximum Depth of Btree - DONE
7. Leetcode 16: climbing stairs - DONE
8. Leetcode 55: Longest Palindromic Substring - DONE

## Saturday 3/21

Ok, it's crunch time. We need to finish the remaining
questions on this list, and then redo the 45 or so review questions,
before doing the final interview on thursday with servicenow.
Additionally, it may be worthwhile to work through 60 project euler questions.

1. Leetcode 40: Merge Two Sorted Lists - DONE
2. Leetcode 41: Merge K Sorted Lists - DONE
3. Leetcode 42: Remove Nth node from end of list - DONE
4. Leetcode 43: Reorder List - DONE
5. Leetcode 44: Set Matrix Zeroes - DONE
6. Leetcode 45: Spiral Matrix - DONE
7. Leetcode 47: Word Search - DONE
8. Leetcode 59: Same Tree - DONE
9. Leetcode 64: Subtree of another tree - DONE
10. Leetcode 65: Construct Btree from preorder and inorder traversal - DONE
11. Leetcode 52: Group Anagrams - DONE
12. Leetcode 48: Longest Substring without repeating Characters - DONE
13. Leetcode 73: Top K Frequent Elements - DONE
14. Leetcode 69: Implement Trie - DONE
15. Leetcode 67: Kth smallest element in BST - DONE
16. Leetcode 60: Invert/Flip Btree - DONE
17. Leetcode 68: Lowest Common Ancestor of BST - DONE
18. Leetcode 35: Non-overlapping Intervals - DONE

After doing these questions, work through the "leetcode explore"
on the trie stuff. I didn't see a single thing like this in EPI.

### Sunday 3/22

1. Leetcode 18: Longest increasing subsequence - DONE
2. Leetcode 20: Combination Sum - DONE
3. Leetcode 21: House Robber - DONE
4. Leetcode 22: House Robber II - DONE
5. Leetcode 23: Unique Paths - DONE
6. Leetcode 75: Longest Common Subsequence - DONE

### Monday 3/23

1. Leetcode 19: Word Break Problem - DONE (memorize this)
2. Leetcode 76: Decode Ways - DONE
3. Leetcode 30: Alien Dictionary (Premium) - DONE
4. Leetcode 50: Minimum Window Substring - DONE (review this... ugh)

### Tuesday 3/24
1. Leetcode 27: Pacific Atlantic Water Flow - DONE
2. Leetcode 63: Serialize and Deserialize Btree
3. Leetcode 74: Find Median From Data Stream
6. Leetcode 28: Number of Islands
7. Leetcode 29: Longest Consecutive Sequence
8. Leetcode 31: Graph Valid Tree (Premium)
9. Leetcode 32: Number of Connected Components in an Undirected Graph (Premium)
10. Leetcode 25: Clone Graph
11. Leetcode 70: Add and Search Word
12. Leetcode 71: Word Search 2
13. Leetcode 36: Meeting Rooms (Premium)
14. Leetcode 37: Meeting Rooms 2 (Premium)
16. Leetcode 57: Encode/Decode Strings (Premium)
17. Leetcode 49: Longest Repeating Character Replacement

### Wednesday 3/25

Make sure to do the other interval questions in the prep.

1. spring boot tutorial
2. spring stuff on flashcards
3. do one of every problem type
4. bit manipulation flashcards
5. dig up questions for servicenow online and practice all of them (entry 6->?)
6. Leetcode 77: Add Two Numbers - DONE
7. Leetcode 1: Two Sum - DONE
8. Leetcode 55: Longest Palindromic Substring - DONE
9. Leetcode 53: Valid Parentheses - DONE
10. Leetcode 34: Merge Intervals - DONE
11. Leetcode 78: Binary Tree Zigzag level order traversal
12. Leetcode 51: Valid Anagram - DONE
13. Leetcode 14: missing number - DONE
14. Leetcode 79: Design Hashmap
15. Leetcode 80: Transpose Matrix - DONE
16. Leetcode 81: Minimum add to make parenthesis valid
17. Leetcode 56: Palindromic Substrings - DONE
18. Leetcode 13: counting bits - DONE
19. Leetcode 11: sum of two integers - DONE

### Thursday

1. zigzag traversal
2. work through one of every one of the basics
3. minimum window
4. palindomic substring (longest)
5. flashcards for matrix flipping

### Friday
1. Leetcode 63: Serialize and Deserialize Btree
2. Leetcode 74: Find Median From Data Stream
3. Leetcode 28: Number of Islands
4. Leetcode 29: Longest Consecutive Sequence
5. Leetcode 31: Graph Valid Tree (Premium)
6. Leetcode 32: Number of Connected Components in an Undirected Graph (Premium)
7. Leetcode 25: Clone Graph
8. Leetcode 70: Add and Search Word
9. Leetcode 71: Word Search 2
10. Leetcode 36: Meeting Rooms (Premium)
11. Leetcode 37: Meeting Rooms 2 (Premium)
12. Leetcode 57: Encode/Decode Strings (Premium)
13. Leetcode 49: Longest Repeating Character Replacement

### Array
3. Leetcode 1: two sum - DONE
4. Leetcode 2: max profit, buy and sell a stock once - DONE
5. Leetcode 3: array contains dupe - DONE
6. Leetcode 4: product of array except self - DONE
7. Leetcode 5: maximum contiguous subarray - DONE
8. Leetcode 6: maximum product subarray - DONE
9. Leetcode 7: minimum in rotated sorted array - DONE
10. Leetcode 8: search in rotated sorted array - DONE
11. Leetcode 9: 3 sum - DONE
12. Leetcode 10: container with most water - DONE
### Binary
13. Leetcode 11: sum of two integers - DONE
14. Leetcode 12: number of 1 bits - DONE
15. Leetcode 13: counting bits - DONE
16. Leetcode 14: missing number - DONE
17. Leetcode 15: reverse bits in 32 bit integer - DONE
### Dynamic Programming
18. Leetcode 16: climbing stairs - DONE
19. Leetcode 17: Coin chainge - DONE
20. Leetcode 18: Longest increasing subsequence - DONE
21. Leetcode 19: Word Break Problem - DONE
22. Leetcode 20: Combination Sum - DONE
23. Leetcode 21: House Robber - DONE
24. Leetcode 22: House Robber II - DONE
25. Leetcode 23: Unique Paths - DONE
26. Leetcode 24: Jump Game - DONE
27. Leetcode 75: Longest Common Subsequence - DONE
28. Leetcode 76: Decode Ways - DONE
### Graph
28. Leetcode 25: Clone Graph
29. Leetcode 26: Course Schedule - DONE
30. Leetcode 27: Pacific Atlantic Water Flow - DONE
31. Leetcode 28: Number of Islands
32. Leetcode 29: Longest Consecutive Sequence
33. Leetcode 30: Alien Dictionary (Premium) - DONE
34. Leetcode 31: Graph Valid Tree (Premium)
35. Leetcode 32: Number of Connected Components in an Undirected Graph (Premium)
### Interval
36. Leetcode 33: Insert Interval - DONE
37. Leetcode 34: Merge Intervals - DONE
38. Leetcode 35: Non-overlapping Intervals - DONE
39. Leetcode 36: Meeting Rooms (Premium)
40. Leetcode 37: Meeting Rooms 2 (Premium)
### Linked List
41. Leetcode 38: Reverse a Linked List - DONE
42. Leetcode 39: Detect a cycle in linked list - DONE
43. Leetcode 40: Merge Two Sorted Lists - DONE
44. Leetcode 41: Merge K Sorted Lists - DONE
45. Leetcode 42: Remove Nth node from end of list - DONE
46. Leetcode 43: Reorder List - DONE
47. Leetcode 77: Add Two Numbers
### Matrix
47. Leetcode 44: Set Matrix Zeroes - DONE
48. Leetcode 45: Spiral Matrix - DONE
49. Leetcode 46: Rotate Image - DONE
50. Leetcode 47: Word Search - DONE
### String
51. Leetcode 48: Longest Substring without repeating Characters - DONE
52. Leetcode 49: Longest Repeating Character Replacement
53. Leetcode 50: Minimum Window Substring - DONE
54. Leetcode 51: Valid Anagram - DONE
55. Leetcode 52: Group Anagrams - DONE
56. Leetcode 53: Valid Parentheses - DONE
57. Leetcode 54: Valid Palindrome - DONE
58. Leetcode 55: Longest Palindromic Substring - DONE
59. Leetcode 56: Palindromic Substrings - DONE
60. Leetcode 57: Encode/Decode Strings (Premium)
### Tree
61. Leetcode 58: Maximum Depth of Btree - DONE
62. Leetcode 59: Same Tree - DONE
63. Leetcode 60: Invert/Flip Btree - DONE
64. Leetcode 61: Btree Maximum Path Sum - DONE
65. Leetcode 62: BTree Level order Traversal - DONE
66. Leetcode 63: Serialize and Deserialize Btree
67. Leetcode 64: Subtree of another tree - DONE
68. Leetcode 65: Construct Btree from preorder and inorder traversal - DONE
69. Leetcode 66: Validate BST - DONE
70. Leetcode 67: Kth smallest element in BST - DONE
71. Leetcode 68: Lowest Common Ancestor of BST - DONE
72. Leetcode 69: Implement Trie - DONE
73. Leetcode 70: Add and Search Word
74. Leetcode 71: Word Search 2

### Heap
75. Leetcode 72: Merge K Sorted Lists - DONE
76. Leetcode 73: Top K Frequent Elements - DONE
77. Leetcode 74: Find Median From Data Stream
