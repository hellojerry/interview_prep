"""
Consider the following two rules that are to be applied
to an array of characters.

1) replace each "a" by two "d"'s.
2) delete each entry containing a "b"

Write a program which takes as input an array of characters,
and removes each "b" and replaces each "a" by two "d"'s.
Specifically, along with the array, you are provided
an integer-valued size. Size denotes the number of entries
of the array that the operation is applied to.
You do not have to worry about preserving subsequent entries.
For example, if the array is [a,b,a,c,], and the size is four,
you can return [d,d,d,d,c] (the operation runs on c, but does not
do anything).
"""

def edit_array_old(arr,size):
	"""
	So this solution works, but the problem
	is that the inserts and pops are each o(N)
	complexity - the array has to get shifted
	around
	"""
	ptr = 0
	while size > 0:
		el = arr[ptr]
		if el == "a":
			arr[ptr] = "d"
			arr.insert(ptr, "d")
			ptr += 2
		elif el == "b":
			arr.pop(ptr)
		else:
			ptr += 1

		size -= 1
	return arr

def edit_array(arr,size):
	"""
	Book's implementation doesn't seem to work...
	"""
	write_idx, a_count = 0,0
	for i in range(size):
		if arr[i] != "b":
			arr[write_idx] = arr[i]
			write_idx += 1
		if arr[i] == "a":
			a_count += 1
	cur_idx = write_idx - 1
	write_idx += a_count - 1
	final_size = write_idx + 1
	while cur_idx >= 0:
		if arr[cur_idx] == "a":
			arr[(write_idx -1):(write_idx+1)] = "dd"
			write_idx -= 2
		else:
			arr[write_idx] = arr[cur_idx]
			write_idx -= 1
		cur_idx -= 1
	return arr

if __name__ == "__main__":
	arr = ["a","b","a","c"]
	size = 4
	res = edit_array(arr,size)
	expected = ["d","d","d","d","c"]
	assert(res == expected)
	arr = ["a","a","a"]
	size = 2
	res = edit_array(arr,size)
	assert(res == ["d","d","d","d","a"])
	arr = ["b","b","a","b"]
	size = 3
	res = edit_array(arr,size)
	assert(res == ["d","d","b"])

