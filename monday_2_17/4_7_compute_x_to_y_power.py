"""
Write a problam that takes a double X and an integer Y and returns x**y.
You can ignore overflow and underflow.
"""

####
"""
This is one of those bit shifting problems.
We know that if (x & (x-1)) -= 0, that means that X is
an exponent of two.
"""

"""
I have no idea what this is asking honestly. This is built 
in to the language.
"""

def power(x,y):
	result, power = 1.0, y
	if y < 0:
		power, x = -power, 1.0/x
	## the idea with this is that we check the 
	## least significant bit. If it's a 1,
	## we multiply the result times the current X
	## value. Then, we square X, and shift the power
	## one bit right, until we don't have any more
	## bits left in power
	while power:
		print("power: %s, x: %s, result: %s" % (str(power),str(x),str(result)))
		print("power as binary: %s" % (bin(power)[2:]))
		if power & 1:
			result *= x
		x, power = x*x, power >> 1
	return result

if __name__ == "__main__":
	res = power(1.9,15)
	print(res)
	print(1.9**15)
	assert(res == 1.9**15)
