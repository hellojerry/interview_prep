"""
You are given a list of projects and a list of dependencies (which is
a list of pairs of projects, where the second project is dependent on the first project).
All of a project's dependencies must be built before the project is. Find a build that will
allow the projects to be built. If there is no valid build order, return an error.

Projects: a,b,c,d,e,f
Dependendies: (a,d),(f,b),(b,d),(f,a),(d,c)
"""


UNVISITED = 0
VISITING = 1
VISITED = 2

def topological_sort(element, adjacency_list,visited,arr):
	if visited[element] == VISITING:
		return False
	elif visited[element] == VISITED:
		return True
	else:
		visited[element] = VISITING
		for node in adjacency_list[element]:
			if topological_sort(node,
				adjacency_list, visited, 
				arr) == False:
				return False

		arr.insert(0,element)
		visited[element] = VISITED
		return True




if __name__ == "__main__":
	adjacency_list = {
		"f":["b","a"],
		"a":["d"],
		"b":["d"],
		"d":["c"],
		"e":[],
		"c":[]
	}
	visited = {
		"a": UNVISITED,
		"b": UNVISITED,
		"c": UNVISITED,
		"d": UNVISITED,
		"e": UNVISITED,
		"f": UNVISITED,
	}
	out_arr = []
	results = []
	for element in visited:
		res = topological_sort(element,adjacency_list,visited,out_arr)
		results.append(res)
	assert(all(results) == True)
	print(out_arr)
