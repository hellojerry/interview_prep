"""
It is natural to apply graph models and algorithms
to spatial problems. Consider a black and white
digitized image of a maze. White pixels represent open
areas and black spaces are walls.

There are two special white pixels: one is designated
the entrance and the other is the exit. The goal
in this problem is to fina a way of getting from the
entrance to the exit.

Given a 2D array of black and white entries
representing a maze with designated entry and exit 
points, find a path from the entrance to the exit
if one exists.

Hint: model the maze as a graph.
"""

############################
"""
First step, lets make a small enough maze that
this makes sense. 1 represents white, 2 represents
entry or exit, 0 represents black.
"""

class Point(object):

	def __init__(self,row,col,status):
		assert(type(status) == str)
		assert(type(row) == int)
		assert(type(col) == int)
		self.row = row
		self.col = col
		self.status = status
		self.neighbors = []

	def __str__(self):
		return "Point(%d,%d,%s)" % (self.row,self.col,self.status)

	def __repr__(self):
		return "Point(%d,%d,%s)" % (self.row,self.col,self.status)

maze_raw = [
	[0,1,1,1],
	[0,0,1,0],
	[1,1,1,0],
	[1,1,1,1]
]

def generate_object_maze(maze):
	object_maze = []

	rows = len(maze)
	cols = len(maze[0])
	col_limit,row_limit = rows-1,cols-1
	for i in range(rows):
		object_maze.append([0]*cols)
	for row in range(len(maze)):
		for col in range(len(maze[0])):
			print(row,col)
			target_el = maze[row][col]
			new_point = Point(row,col,"BLACK")
			if target_el == 1:
				new_point.status = "WHITE"
			elif target_el == 2:
				new_point.status = "EXIT"
			elif target_el == 3:
				new_point.status = "ENTRY"

			object_maze[row][col] = new_point
	print(object_maze)
	return object_maze

"""
This book has a bizarre way of solving this problem.
Check the current row and column: if it's outside of the boundaries,
or if it's not white, return false. 

If it's the final point, and the final point is white,
return True


Otherwise, set the current point to black,
then check in the four directions.
If any of the directions work, append the current
row to the path, then append everything generated
in the valid derived path to the result, and return True

If none of this works return False.

"""


def find_path(maze, current_row,current_col,
		final_row,final_col,output_arr):
	min_col,min_row = 0,0
	max_col,max_row = len(maze[0]) - 1,len(maze) - 1

	## first, get rid of the illegal states
	if current_row > max_row or current_row < min_row:
		return False
	if current_col > max_col or current_col < min_col:
		return False

	current_point_val = maze[current_row][current_col]
	if current_point_val == 0:
		## if the current 
		return False
	if current_row == final_row and (
		current_col == final_col) and (
			current_point_val == 1):
		output_arr.append((current_row,current_col))
		return True
	maze[current_row][current_col] = 0
	path_left, path_right,path_up,path_down = [],[],[],[]
	left_res = find_path(maze,current_row,current_col-1,
			final_row,final_col,path_left)
	right_res = find_path(maze,current_row,current_col+1,
			final_row,final_col,path_right)
	up_res = find_path(maze,current_row-1,current_col,
			final_row,final_col,path_up)
	down_res = find_path(maze,current_row+1,current_col,
			final_row,final_col,path_down)
	pairings = [(left_res,path_left),(right_res,path_right),
		(up_res,path_up),(down_res,path_down)]
	for res, path in pairings:
		if res:
			output_arr.append((current_row,current_col))
			for item in path:
				output_arr.append(item)
			return True
	return False	
	

if __name__ == "__main__":
	maze_raw = [
		[0,1,1,1],
		[0,0,1,0],
		[1,1,1,0],
		[1,1,1,1]
	]
	out_arr = []
	res = find_path(maze_raw,3,0,0,3,out_arr)
	print(res)
	print(out_arr)
