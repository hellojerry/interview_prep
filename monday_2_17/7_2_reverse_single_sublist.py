"""
Write a program which takes a singly linked list L
and two integers S and F as arguments, and reverses
the order of the nodes from the Sth node to the Fth
node, inclusive. The ordering starts at 1, i.e. the
head node is 1. Do not allocate additional nodes.


ex:

	1->2->3->4->5->6->7
args: 3,7

result:
	1->2->7->6->5->4->3
"""

class Node(object):

	def __init__(self, val, next_=None):
		assert(type(val) == int)
		if next_ is not None:
			assert(type(next_) == Node)
		self.val = val
		self.next_ = next_

	def __str__(self):
		return "Node(%d)" % self.val

	def __repr__(self):
		return "Node(%d)" % self.val


def reverse_single_sublist(head,S,F):
	"""
	Could I bubble-sort this?
	Iterate up until S, then swap S with S+1,
	then rinse and repeat until reaching F.

	That won't work.
	The cheap thing to do here is push the target
	section into an array, reverse the array, and
	swap the pointers, but that's not in the spirit
	of the question.

	1. iterate up until S.
	2. for F - S iterations:
		get prior
		get current
		get next
		current.next = next.next
		next.next = prior.next
		prior.next = next
	"""
	cur_idx = 1
	cur = head
	while cur_idx < S-1:
		cur = cur.next_
		cur_idx += 1
	prior = cur
	cur = prior.next_
	for i in range(F-S):
		next_ = cur.next_
		cur.next_ = next_.next_
		next_.next_ = prior.next_
		prior.next_ = next_
	return head

		


		

if __name__ == "__main__":
	n7 = Node(7)
	n6 = Node(6,n7)
	n5 = Node(5,n6)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	print([n1,n2,n3,n4,n5,n6,n7])
	res = reverse_single_sublist(n1,3,7)
	cur = res
	out_arr = []
	while cur:
		out_arr.append(cur)
		cur = cur.next_
	assert(out_arr == [n1,n2,n7,n6,n5,n4,n3])

