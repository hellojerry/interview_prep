"""
Write a program that takes the head of
a singly linked list and returns None
if there is no cycle, and the node at the start 
of the cycle if there is a cycle.
"""


class Node(object):

	def __init__(self,val,next_=None):
		assert(next_ is None or type(next_) == Node)
		assert(type(val) == int)
		self.val = val
		self.next_ = next_

	def __str__(self):
		return "Node(%d)" % self.val

	def __repr__(self):
		return "Node(%d)" % self.val

def get_cycle_in_ll(head):

	slow = head
	fast = None
	if slow.next_:
		fast = slow.next_
	while slow and fast:
		if slow is fast:
			return slow
		slow = slow.next_
		if not fast.next_:
			return None
		if not fast.next_.next_:
			return None
		fast = fast.next_.next_
	return None

if __name__ == "__main__":

	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	n5.next_ = n1
	res = get_cycle_in_ll(n1)
	assert(res is n5)

	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	res = get_cycle_in_ll(n1)
	assert(res is None)
