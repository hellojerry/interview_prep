"""
Given a binary tree, return an array consisting
of the keys at the same level.
Keys should appear in the order of the corresponding
nodes' depths, breaking ties from left to right


ex:

			314
		6 		6
	271	   561	      2    271
     28     0         3         1       28   
 		    17	     401  257
                            641

The result for the above tree should be:

[[314],[6,6],[271,561,2,271],[28,0,3,1,28],[17,401,257][641]]

Hint: first think about this problem using a pair of queues.
"""

class TreeNode(object):

	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.val

	def __repr__(self):
		return "TreeNode(%d)" % self.val

def get_btree_nodes(root):
	"""
	I don't quite see how we'd need a pair 
	of queues on this. Let's write out a BFS
	as a refresher.
	Ok, so it's going in order as I'd expected.

	ok, the only part left is making sure
	that the arrays match. 

	"""

	current_level = 0
	queue = [(root,current_level)]
	out = [[]]
	prior_level = 0
	while len(queue) > 0:
		node,current_level = queue.pop(0)
		if current_level == prior_level:
			out[-1].append(node.val)
		else:
			out.append([node.val])
		prior_level = current_level
		current_level += 1
		if node.left:
			queue.append((node.left,current_level))
		if node.right:
			queue.append((node.right,current_level))
	return out

if __name__ == "__main__":
	bottom_641 = TreeNode(641)
	bottom_401 = TreeNode(401,bottom_641)
	bottom_257 = TreeNode(257)
	middle_1 = TreeNode(1,bottom_401,bottom_257)
	bottom_17 = TreeNode(17)
	middle_3 = TreeNode(3, bottom_17)
	middle_left_28 = TreeNode(28)
	middle_0 = TreeNode(0)
	top_left_271 = TreeNode(271,middle_left_28,middle_0)
	middle_right_28 = TreeNode(28)
	top_561 = TreeNode(561,None,middle_3)
	top_2 = TreeNode(2,None,middle_1)
	top_right_271 = TreeNode(271,None,middle_right_28)
	left_6 = TreeNode(6,top_left_271,top_561)
	right_6 = TreeNode(6,top_2,top_right_271)
	root = TreeNode(314,left_6,right_6)
	res = get_btree_nodes(root)
	expected =  [[314],[6,6],[271,561,2,271],[28,0,3,1,28],[17,401,257],[641]]
	assert(res == expected)
