"""
In the decimal number system, the position
of a digit is used to signify the power of 10
that digit is to be multiplied with... (this is autistic).
For example, "314" denotes 100x3 + 1x10 + 4x1.
The base b number system generalizes the decimal number system.

The string "A(k-1)a(k-2)...A(1)A(0) where 0 <= a(i) < b
denotes in base b the integer... blah blah blah.

In non retard terms this means, for example, in base 3

210 in base 3 is

(2 x 9) + (1x3) + 0 == 21

212

(2x (3**2)) + (1 x (3**1)) + (2 x (3**0)) == 23


Write a program that performs base conversion. The input is a string S,
an integer B, and another integer b2. The string represents an integer in base b1.
The output should be a string representing an integer in base b2.

"""


def base_conversion(my_str, initial_base,expected_base):
	"""
	First part is converting to base 10.
	We can take the 
	"""
	is_negative = False
	if my_str.startswith("-"):
		is_negative = True
		my_str = my_str[1:]
	as_base_10 = 0
	ct = 0
	for i in range(len(my_str)-1,-1,-1):
		as_base_10 += int(my_str[i]) * (initial_base**ct)
		ct += 1

	out = ""
	while as_base_10:
		digit, remainder = divmod(as_base_10,expected_base)
		out = str(remainder) + out
		as_base_10 = digit
	if is_negative:
		return "-" + out
	return out


if __name__ == "__main__":
	initial_base = 10
	input_string = "21"
	expected_base = 3
	res = base_conversion(input_string,initial_base,expected_base)
	assert(res == "210")
	initial_base = 12
	input_string = "595"
	expected_base = 3
	res = base_conversion(input_string,initial_base,expected_base)
	assert(int(res,expected_base) == int(input_string,initial_base))
	initial_base = 9
	input_string = "-83"
	expected_base = 4
	res = base_conversion(input_string,initial_base,expected_base)
	assert(int(res,expected_base) == int(input_string,initial_base))
	
