"""

Implement an algorithm that takes as input an array of distinct
elements and a size, and returns a subset of the given size of the array elements.
All subsets should be equally likely. Return the result in input array itself.
"""

import random

def random_sampling(size,arr):
	arrlen = len(arr)
	arr_boundary = arrlen - 1
	for i in range(size):
		r = random.randint(i,arr_boundary)
		arr[r],arr[i] = arr[i],arr[r]

	return arr[:size]

if __name__ == "__main__":


	res = random_sampling(4,[i for i in range(20)])
	print(res)
