"""
A 2D array can be written as a sequence in several orders - 
the most natural ones being row-by-row or column-by-column.
In this problem we explore the problem of writing the 2D
array in spiral order. For example:

arr1 = [
	[1,2,3],
	[4,5,6],
	[7,8,9]
]

The spiral ordering for arr1 is [1,2,3,6,9,8,7,4,5]

arr2 = [
	[1,2,3,4],
	[5,6,7,8],
	[9,10,11,12],
	[13,14,15,16]
]

The spiral ordering for arr2 is [1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10]

Write a program which takes an NxN 2D array and returns the spiral ordering of 
the array.

Hint: use case analysis and divide and conquer.
"""

DIRECTION_UP = 0
DIRECTION_DOWN = 1
DIRECTION_LEFT = 2
DIRECTION_RIGHT = 3

DIRECTION_MAPPINGS = {
	DIRECTION_RIGHT:DIRECTION_DOWN,
	DIRECTION_DOWN:DIRECTION_LEFT,
	DIRECTION_LEFT:DIRECTION_UP,
	DIRECTION_UP:DIRECTION_RIGHT
}

VISITED = -1

def is_corner(row,col,direction,matrix):
	rows, cols = len(matrix), len(matrix[0])
	max_row,max_col = rows - 1, cols - 1
	if direction == DIRECTION_RIGHT:
		if col == max_col or matrix[row][col+1] == VISITED:
			return True
		return False
	elif direction == DIRECTION_LEFT:
		if col == 0 or matrix[row][col-1] == VISITED:
			return True
		return False
	elif direction == DIRECTION_DOWN:
		if row == max_row or matrix[row+1][col] == VISITED:
			return True
		return False
	else:
		if row == max_row or matrix[row-1][col] == VISITED:
			return True
		return False
def is_complete(row,col,matrix):
	rows,cols = len(matrix), len(matrix[0])
	max_row,max_col = rows - 1, cols - 1
	(left_complete,right_complete,
		up_complete,down_complete) = [False]*4
	if row == max_row or matrix[row+1][col] == VISITED:
		down_complete = True
	if row == 0 or matrix[row-1][col] == VISITED:
		up_complete = True
	if col == max_col or matrix[row][col+1] == VISITED:
		right_complete = True
	if col == 0 or matrix[row][col-1] == VISITED:
		left_complete = True
	if all([left_complete,right_complete,up_complete,
		down_complete]):
		return True
	return False

			


def get_spiral_ordering(row,col,direction,matrix,output_arr):
	"""
	If we test that we're in a corner, that will
	ensure that we reverse the direction of things.
	How do we define a corner? That's done.
	How do we know when we're done? Let's get the main piece done first
	"""
	output_arr.append(matrix[row][col])
	matrix[row][col] = VISITED
	if is_complete(row,col,matrix):
		return output_arr
	if is_corner(row,col,direction,matrix):
		direction = DIRECTION_MAPPINGS[direction]
	if direction == DIRECTION_RIGHT:
		get_spiral_ordering(row,col+1,
			direction,matrix,output_arr)
	elif direction == DIRECTION_LEFT:
		get_spiral_ordering(row,col-1,
			direction,matrix,output_arr)
	elif direction == DIRECTION_DOWN:
		get_spiral_ordering(row+1,col,
			direction,matrix,output_arr)
	else:
		get_spiral_ordering(row-1,col,
			direction,matrix,output_arr)
	return output_arr


if __name__ == "__main__":
	arr1 = [
		[1,2,3],
		[4,5,6],
		[7,8,9]
	]
	output_arr = []
	res = get_spiral_ordering(0,0,DIRECTION_RIGHT,arr1,output_arr)
	assert(res == [1,2,3,6,9,8,7,4,5])
	arr2 = [
		[1,2,3,4],
		[5,6,7,8],
		[9,10,11,12],
		[13,14,15,16]
	]
	output_arr = []
	res = get_spiral_ordering(0,0,DIRECTION_RIGHT,arr2,output_arr)
	assert(res == [1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10])
