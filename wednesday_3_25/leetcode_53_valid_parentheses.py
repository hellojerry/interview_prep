"""
Given a string containing just the characters
(,),{,},[,]
determine if the input string is valid.

a1 = "()"
e1 = True

a2 = "()[]{}"
e2 = True

a3 = "([)]"
e3 = False

a4 = "([)]"
e4 = False

a5 = "{[]}"
e5 = True
"""
def valid(S):
	"""
	Starting the problem at 1:31. Let's try
	and have this done by 1:45pm.
	This is a stack problem. We want a mapping
	for the matching values (i.e. "{" with "}").
	We push values to the stack as we se them,
	and for each value following it, check if the 
	most recent value on the stack matches.
	if it does, continue, otherwise exit with False
	"""
	mappings = {
		"}":"{",
		")":"(",
		"]":"["
	}
	stack = []
	for el in S:
		if el not in mappings:
			stack.append(el)
		else:
			if len(stack) == 0:
				return False
			prior = stack.pop()
			if mappings[el] != prior:
				return False


	return len(stack) == 0

if __name__ == "__main__":
	a1 = "()"
	e1 = True
	r1 = valid(a1)
	assert(r1 == e1)
	print("!!")
	a2 = "()[]{}"
	e2 = True
	r2 = valid(a2)
	assert(r2 == e2)	
	
	a3 = "([)]"
	e3 = False
	r3 = valid(a3)
	assert(r3 == e3)	
	
	a4 = "([)]"
	e4 = False
	r4 = valid(a4)
	assert(r4 == e4)	
	
	a5 = "{[]}"
	e5 = True
	r5 = valid(a5)
	assert(r5 == e5)	
