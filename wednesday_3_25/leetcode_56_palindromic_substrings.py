"""
Given a string, your task is to count
how many palindromic substrings are in the string
Substrings with different start indices or end indices
are counted as different substrings.

a1 = "abc"
e1 = 3

a2 = "aaa"
e2 = 6
("a","a","a","aa","aa","aaa")
"""

def palindromic_substrings(S):
	"""
	So this one is a bit trickier.
	Let's try to knock it out by 5pm.
	After that, one more leetcode, then we're 
	going to make a simple API with spring boot,
	then go to bed.
	My first thought is to check the length of the string.
	if it's zero or 1, that's the number.
	From here, it gets a bit tricker. This problem seems
	like it could be recursive.
	My gut instinct here is to move a right pointer forward
	and test palindromicity, until the end is reached. Then
	we move the left pointer forward, rinse and repeat.
	This isn't quite n^2 but it's close. We can use a sliding window on
	this, because we don't need to test individual values.
	"""

	strlen = len(S)
	if strlen <= 1:
		return strlen
	ct = strlen
	l,r = 0,0
	while l < strlen-1:
		lval = S[l]
		r = l+1
		while r < strlen:
			cand = S[l:r+1]
			if cand == cand[::-1]:
				ct += 1
			r += 1
		l += 1
	return ct

if __name__ == "__main__":
	a1 = "abc"
	e1 = 3
	r1 = palindromic_substrings(a1)
	print(r1,r1 == e1)
	a2 = "aaa"
	e2 = 6
	r2 = palindromic_substrings(a2)
	print(r2,r2 == e2)
