"""
You are given two non-empty linked lists
representing two non-negative integers.
The digits are stored in reverse order
and each of their nodes contain a single digit.
Add the two numbers and return it as a linked list.
You may assume the two numbers do not contain
any leading zero, except the number zero itself.

ex:

2->4->3 + 5->6->4

out: 7->0->8
explanation: 342 + 465 = 807
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val

def add_two(left_h,right_h):
	"""
	Ok, let's think about this.
	Each element in the linked list
	represents the tailing element.
	So in the example above, the addition
	goes
	4+3 -> 7
	6+4 = 10 -> 0
	5+2+1(cf) -> 8
	So for this, I iterate over the linked lists
	and keep track of a carryforward.
	One thing to note is that instead of a general total,
	we've got to return a linked list
	"""
	cur_left,cur_right = left_h,right_h
	cf = 0
	out_head,out_cur = None,None
	## handle when both exist
	while cur_left and cur_right:
		lval,rval = cur_left.val,cur_right.val
		new_val = cf + lval + rval
		cf,new_val = divmod(new_val,10)
		print(cf,new_val)
		new_node = Node(new_val)
		if out_head is None:
			out_head,out_cur = new_node,new_node
		else:
			out_cur.next_ = new_node
			out_cur = new_node
		cur_left,cur_right = cur_left.next_,cur_right.next_
	## left straggler
	while cur_left:
		new_val = cf + cur_left.val
		cf,new_val = divmod(new_val,10)
		new_node = Node(new_val)
		cur_left = cur_left.next_
		out_cur.next_ = new_node
		out_cur = out_cur.next_
	## right straggler
	while cur_right:
		new_val = cf + cur_right.val
		cf,new_val = divmod(new_val,10)
		new_node = Node(new_val)
		cur_right = cur_right.next_
		out_cur.next_ = new_node
		out_cur = out_cur.next_
	if cf > 0:
		out_cur.next_ = Node(cf)
	return out_head
if __name__ == "__main__":
	l3 = Node(3)
	l4 = Node(4,l3)
	l2 = Node(2,l4)

	r4 = Node(4)
	r6 = Node(6,r4)
	r5 = Node(5,r6)
	out = add_two(l2,r5)
	res = []
	while out:
		res.append(out)
		out = out.next_
	print(res)

	l1,r1 = Node(5),Node(5)
	out = add_two(l1,r1)
	res = []
	while out:
		res.append(out)
		out = out.next_
	print(res)
	l8 = Node(8)
	l1 = Node(1,l8)
	r0 = Node(0)
	out = add_two(l1,r0)
	res = []
	while out:
		res.append(out)
		out = out.next_
	print(res)
	l1 = Node(1)
	ra = Node(9)
	rb = Node(9,ra)
	out = add_two(l1,rb)
	res = []
	while out:
		res.append(out)
		out = out.next_
	print(res)
