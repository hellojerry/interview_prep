"""
Given an array of integers, return
indices of the two numbers such
that they add up to a specific target.
You may assume that each input has
exactly one solution, and you may not
use the same element twice

a1 = [2,7,11,15]
t1 = 9
e1 = [0,1]
"""

def two_sum(arr,target):
	"""
	I supposed I could make a hashmap of these.
	Then I iterate over the array, subtracting
	the current value, checking existence for
	the difference, if it exists we have an answer
	I could speed this up and make it a single pass though.
	"""
	mappings = {}
	for ind,val in enumerate(arr):
		mappings[val] = ind
	for i in range(len(arr)):
		el = arr[i]
		diff = target - el
		exists = mappings.get(diff)
		if exists is not None:
			return [i,exists]
	return []

def two_sum(arr,target):
	mappings = {}
	for ind,val in enumerate(arr):
		diff = target - val
		exists = mappings.get(diff)
		if exists is not None:
			return [ind,exists]
		mappings[val] = ind

if __name__ == "__main__":
	a1 = [2,7,11,15]
	t1 = 9
	e1 = [0,1]
	res = two_sum(a1,t1)
	print(res)
