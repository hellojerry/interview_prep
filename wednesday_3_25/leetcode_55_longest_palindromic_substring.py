"""
Given a string S, find the longest palindromic
substring in S.

s1= "babad"
e1 = "bab"

s2 = "cbbd"
e2 = "bb"
"""

def longest(S):
	"""
	Assuming the length of the string is 
	gte 1, we have a minimum length of 1
	Let's think of how we can attack this.
	Let's try and solve this by 1:35pm.
	First thought: move right pointer forward
	until the end, memoizing palindromes if they exist,
	then move left pointer forward to end.
	Second thought: start at far left and far right.
	incrementally move inwards.
	Third thought: is this a greedy algorithm?
	Fourth thought: can we manage anything with recursion here?
	Let's see the consequences of idea 1.
	babad
	0:2 -> false
	0:3-> true
	0:4 -> false (baba)
	0:5 -> false (babad)
	1:3 -> ab (false)
	1:4 -> aba (true, but not bigger)
	1:5 -> abad (false)
	Ok, what if we made the sliding window bigger every time we found 
	a palindrome?
	"""
	strlen = len(S)
	if strlen == 0:
		return ""
	if strlen == 1:
		return 1
	cur_max = S[0]
	left_ptr,right_ptr = 0,0
	while left_ptr < strlen and left_ptr + len(cur_max) < strlen+1:
		right_ptr = left_ptr + len(cur_max)
		while right_ptr < strlen:
			cand = S[left_ptr:right_ptr+1]
			if cand == cand[::-1] and len(cand) > len(cur_max):
				cur_max = cand
			right_ptr += 1
		left_ptr += 1
	return cur_max

if __name__ == "__main__":	
	s1= "babad"
	e1 = "bab"
	r1 = longest(s1)
	print(r1)	
	s2 = "cbbd"
	e2 = "bb"
	r2 = longest(s2)
	print(r2)
	s3 = "abaccccddddd"
	print(longest(s3))
