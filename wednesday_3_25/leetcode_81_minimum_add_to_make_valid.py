"""
Given a string S of "(" and ")"
parentheses,
return the minimum number of parentheses
we must add to make the resulting string valid.

a1 = "())"
e1 = 1

a2 = "((("
e2 = 3

a3 = "()"
e3 = 0

a4 = "()))(("
e4 = 4
"""

def min_amount(S):
	"""
	I think we can knock this out in five minutes.
	Let's try and get this done by 4:15.
	Here's my thought process:
		1. if stack is empty, push value to stack.
		2. otherwise, if val == ), and stack[-1] == (,
			pop, else, append to stack
		return stack length
	"""
	if len(S) == 0:
		return 0
	stack = []
	for i in S:
		if len(stack) == 0:
			stack.append(i)
			continue
		if i == ")" and stack[-1] == "(":
			stack.pop()
		else:
			stack.append(i)


	return len(stack)

if __name__ == "__main__":
	a1 = "())"
	e1 = 1
	r1 = min_amount(a1)
	assert(r1 == e1)	
	a2 = "((("
	e2 = 3
	r2 = min_amount(a2)
	assert(r2 == e2)	
	
	a3 = "()"
	e3 = 0
	r3 = min_amount(a3)
	assert(r3 == e3)	
	
	a4 = "()))(("
	e4 = 4
	r4 = min_amount(a4)
	assert(r4 == e4)	
