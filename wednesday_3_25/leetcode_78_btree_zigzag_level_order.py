"""
Given a binary tree,
return the zigzag level order traversal
of its nodes values. (from left to right,
then right to left for the next level,
alternating)

ex:
	3
    9       20
	  15   7

expected = [
	[3],
	[20,9],
	[15,7]
]
"""

class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val
def zigzag(root):
	"""
	So a level-order traversal means pushing things
	to a queue, then popping from the queue
	and appending left and right children to the queue.
	every popped value goes to an "out" array.
	I think we can have this finished by 2:15. 
	The strategy on this is to use the queue, 
	and inside the queue, when pushing to it, indicate
	whether to go left or right for the next one.
	This is also requiring that we have the levels inside
	of sublists. We're close, just need to handle the sublist
	functionality.
	"""	
	if not root:
		return []
	queue = [(root,0)]
	out = []
	while queue:
		cur,level = queue.pop(0)
		if len(out) == 0:
			out.append([cur.val,level])
		else:
			if out[-1][-1] != level:
				prior_level = out[-1].pop()
				if prior_level % 2 != 0:
					out[-1] = out[-1][::-1]
				
				out.append([cur.val,level])
			else:
				out[-1].insert(
					-1,cur.val)
		if cur.left:
			queue.append([cur.left,level+1])
		if cur.right:
			queue.append([
				cur.right,level+1])
	out[-1].pop()
#	for i in range(1,len(out)):
#		if i % 2 != 0:
#			out[i] = out[i][::-1]
	return out



if __name__ == "__main__":
	n15 = TreeNode(15)
	n7 = TreeNode(7)
	n20 = TreeNode(20,n15,n7)
	n9 = TreeNode(9)
	n3 = TreeNode(3,n9,n20)
	res = zigzag(n3)
	e1 = [[3],[20,9],[15,7]]
	print(res == e1)
#	assert(res == e1)

	#"""
	#	1
	#    2      3
        # 4       5
        #"""
	n4 = TreeNode(4)
	n5 = TreeNode(5)
	n2 = TreeNode(2,n4)
	n3 = TreeNode(3,n5)
	n1 = TreeNode(1,n2,n3)
	e2 = [[1,],[3,2],[4,5]]
	res = zigzag(n1)
	print(res == e2)
