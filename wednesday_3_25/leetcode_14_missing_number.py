"""
Given an array containing N distinct numbers
from o-N, find the missing one

a1 = [3,0,1]
e2 = 2
a2 = [9,6,4,2,3,5,7,0,1]
e2 = 8
"""
def missing(arr):
	"""
	I could sort this, and then iterate over a range
	and return the missing one.
	I ciuld make a set range and compare the set
	i could sort and find the one where it's not equal
	to prior -1. Let's try to finish this by 3:20
	"""
	cands = set(range(len(arr)+1))
	return (cands - set(arr)).pop()

def missing(arr):
	as_set = set(arr)
	for i in range(len(arr)+1):
		if i not in as_set:
			return i

if __name__ == "__main__":
	a1 = [3,0,1]
	e1 = 2
	r1 = missing(a1)
	print(r1)
	print(r1 == e1)
	a2 = [9,6,4,2,3,5,7,0,1]
	e2 = 8
	r2 = missing(a2)
	print(r2 == e2)
