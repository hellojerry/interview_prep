"""
Given a collection of intervals, merge
all overlapping intervals

a1 = [[1,3],[2,6],[8,10],[15,18]]
e1 = [[1,6],[8,10],[15,18]]

a2 = [[1,4],[4,5]]
e2 = [[1,5]]
"""

def merge_intervals(arr):
	"""
	Let's see if we can get this handled by 2pm.
	This looks like a pretty simple thing.
	My thought is that I can do this in a single pass.
	Assuming the arr is longer than 1, iterate from idx 1
	until the end, comparing the first value of EL
	to out[-1][-1]. if it's less than out[-1][-1],
	i further check and compare EL[1] to out[-1][-1].
	if EL[-1][-1] less than out[-1][-1], i continue,
	otherwise i make out[-1][-1] = el[1].
	if el[0] gt out[-1][-1], i append.
	I can't remember if these things come in sorted, so i'm
	going to do a sort here first.
	"""
	arrlen = len(arr)
	if arrlen <= 1:
		return arr
	arr.sort(key=lambda x:x[0])## sort by the leading element
	out = [arr[0]]
	for item in arr[1:]:
		if item[0] <= out[-1][-1]:
			if item[1] < out[-1][-1]:
				continue
			else:
				out[-1][-1] = item[1]
		else:
			out.append(item)
	return out

def merge_intervals(arr):
	"""
	Still have nine minutes to go. Let's try
	and do this in-place
	"""
	arrlen = len(arr)
	if arrlen <= 1:
		return arr
	arr.sort(key=lambda x:x[0])## sort by the leading element
	ptr = 1
	while ptr < arrlen:
		el = arr[ptr]
		prior = arr[ptr-1]
		if el[0] <= prior[1]:
			if el[1] < prior[1]:
				arr.pop(ptr)
			else:
				prior[1] = el[1]
				arr.pop(ptr)
		else:
			ptr += 1
		arrlen = len(arr)
	return arr

if __name__ == "__main__":
	a1 = [[1,3],[2,6],[8,10],[15,18]]
	e1 = [[1,6],[8,10],[15,18]]
	r1 = merge_intervals(a1)
	assert(r1 == e1)	
	a2 = [[1,4],[4,5]]
	e2 = [[1,5]]
	r2 = merge_intervals(a2)
	assert(r2 == e2)	
