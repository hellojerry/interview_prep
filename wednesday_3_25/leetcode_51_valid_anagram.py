"""
given two strings,
determine if they're anagrams of one another

a1,b1 = "anagram","nagaram"
e1 = True

a2,b2 = "car","rat"
e2 = True
"""
def is_anagram(a,b):
	return sorted(a) == sorted(b)

if __name__ == "__main__":
	
	a1,b1 = "anagram","nagaram"
	e1 = True
	assert(is_anagram(a1,b1) == e1)	
	a2,b2 = "car","rat"
	e2 = False
	assert(is_anagram(a2,b2) == e2)	
