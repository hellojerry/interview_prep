"""
Given a matrix A, return its transpose.
This means that its flipped over its main
diagonal

a1 = [
	[1,2,3],
	[4,5,6],
	[7,8,9]
]
e1 = [
	[1,4,7],
	[2,5,8],
	[3,6,9]
]
a2 = [
	[1,2,3],
	[4,5,6]
]
e2 = [
	[1,4],
	[2,5],
	[3,6]
]
"""
from pprint import pprint
def transpose(m):
	"""
	Let's try to get this done by 3:45.
	So If I understand this correctly,
	[0,1] -> [1,0],
	[0,2] -> [2,0]
	[1,2] -> [2,1]
	just for curiosity's sake i wanna test a few things
	So i'm going to start with even sides to make this 
	a bit easier to understand.A "Diagonal",
	assuming one is not at a corner,
	means [r+1][c-1]
	"""
	return [list(z) for z in zip(*m)]

	

if __name__ == "__main__":
	a1 = [
		[1,2,3],
		[4,5,6],
		[7,8,9]
	]
	a1 = transpose(a1)
	e1 = [
		[1,4,7],
		[2,5,8],
		[3,6,9]
	]
	print(a1,a1 == e1)
	a2 = [
		[1,2,3],
		[4,5,6]
	]
	a2 = transpose(a2)
	e2 = [
		[1,4],
		[2,5],
		[3,6]
	]
	print(a2)
