"""

Calculate the sum of two integers A and B,
but you can't use + and -

a1,b1 = 1,2
e1 = 3

a2,b2 = -2,3
e2 = 1
"""

def add(A,B):
	carry = 0
	mask = 0xFFFFFFFF
	while B & mask != 0:
		carry = (A&B) << 1
		A = A^B
		B = carry
	return A&mask if B > mask else A

if __name__ == "__main__":

	a1,b1 = 1,2
	e1 = 3
	r1 = add(a1,b1)
	print(r1,r1 == e1)	
	a2,b2 = -2,3
	e2 = 1
	r2 = add(a2,b2)
	print(r2,r2 == e2)	
