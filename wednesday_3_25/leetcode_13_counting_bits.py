"""
Given a non-negative integer, for every number 
in the range 0 <= i <= num, count the number
of 1s in their binary representation 
and return them as an array.

a1 = 2
e1 = [0,1,1]
a2 = 5
e2 = [0,1,1,2,1,2]
"""

def count_1s(num):
	ct = 0
	while num:
		if num & 1:
			ct += 1
		num >>= 1
	return ct

def count_bits_brute(num):
	"""
	So the brute force approach
	with this is to iterate from
	1 to n, counting the bits.
	I don't think that this is
	in the spirit of the question.
	"""
	out = []
	for i in range(num+1):
		out.append(count_1s(i))
	return out

def count_bits(num):
	"""
	Let's think about some of the 
	properties of binary. if it's a power
	of 2, that means there's exactly 
	1 live bit in the number. Is there
	any way we can derive the number of bits
	from the prior number without that
	while loop in count_1s?
	every odd number has 1 more bit than the prior number.
	Is there any simple pattern with evens?
	1,2,2,3,2,3,3,4,
	There is a subtle interval in there. let's
	print out the evens and see if there's something
	obvious.
	So my solution was actualyl one of leetcodes solutions,
	but leetcode's fucking retarded
	"""
	if num == 0:
		return 0
	out = [0 for i in range(num+1)]
	out[1] = 1
	for i in range(2,num+1):
		if i & i-1 == 0:
			out[i] = 1
			continue
		if i % 2 == 0:
			out[i] = out[i&(i-1)] + 1
			##previous version
			## out[i] = count_1s(i)
		else:
			out[i] = out[i-1] +1
	return out
if __name__ == "__main__":
	a1 = 2
	e1 = [0,1,1]
	r1 = count_bits(a1)
	print(r1, r1 == e1)
	a2 = 5
	e2 = [0,1,1,2,1,2]
	r2 = count_bits(a2)
	print(r2, r2 == e2)
	a3 = 8
	print(count_bits(a3))
