"""
Solve the knapsack problem.

ex:
knapsack size: 5oz
available items:
	geode1: $60, weight: 5oz
	geode2: $50, weight: 3oz
	geode3: $70, weight: 4oz
	geode4: $30, weight: 2oz

solution:
	$80, (geode2 & geode4)
"""

class Geode(object):

	def __init__(self,name, weight,value):
		for i in [value,weight]:
			assert(type(i) == int and i > 0)
		assert(type(name) == str and len(name) > 0)
		self.value = value
		self.name = name
		self.weight = weight

	def __str__(self):
		return "Geode(%s,weight:%d,value:%d)" % (self.name,self.weight,self.value)
	def __repr__(self):
		return "Geode(%s,weight:%d,value:%d)" % (self.name,self.weight,self.value)

def knapsack(matrix,item_array,item_idx,remaining_weight):

	if item_idx < 0:
		return 0
	if matrix[item_idx][remaining_weight] == -1:
		"""
		compute it WITH and WITHOUT the item
		"""
		without_item = knapsack(matrix,item_array,item_idx-1,remaining_weight)
		with_item = 0
		item_weight = item_array[item_idx].weight
		item_value = item_array[item_idx].value
		if item_weight <= remaining_weight:
			with_item = item_value + knapsack(matrix,item_array,item_idx-1,remaining_weight-item_weight)
		matrix[item_idx][remaining_weight] = max(with_item,without_item)
	return matrix[item_idx][remaining_weight]



def kickoff(item_arr,remaining_weight):
	"""
	Step 1: set up matrix with len(items) rows and remaining_weight+1 columns,
	all marked at -1 to indicate not being visited.
	The matrix will 
	"""
	arrlen = len(item_arr)
	matrix = [[-1]*(remaining_weight+1) for i in range(arrlen)]
	res = knapsack(matrix, item_arr,arrlen-1,remaining_weight)
	return res
if __name__ == "__main__":
	obj_arr = [
		Geode("G1",5,60),
		Geode("G2",3,50),
		Geode("G3",4,70),
		Geode("G4",2,30)
	]
	print(obj_arr)

	res = kickoff(obj_arr,5)
	assert(res == 80)
	
