

def is_legal(matrix,row,col,target_value):
	rows = len(matrix)
	cols = len(matrix[0])

	remaining_values = set(range(1,10))
	for c in range(cols):
		el = matrix[row][c]
		remaining_values -= {el}
	if target_value not in remaining_values:
		return False

	for r in range(rows):
		el = matrix[r][col]
		remaining_values -= {el}
	if target_value not in remaining_values:
		return False
	target_rows,target_cols = None,None
	if 0 <= row < 3:
		target_rows = (0,3)
	elif 3 <= row < 6:
		target_rows = (3,6)
	else:
		target_rows = (6,9)
	if 0 <= col < 3:
		target_cols = (0,3)
	elif 3 <= col < 6:
		target_cols = (3,6)
	else:
		target_cols = (6,9)
	for r in range(target_rows[0],target_rows[1]):
		for c in range(target_cols[0],target_cols[1]):
			el = matrix[r][c]
			remaining_values -= {el}
	return target_value in remaining_values	

def solve_sudoku(matrix):
	rows = len(matrix)
	cols = len(matrix[0])
	nonzero_elements = 0
	for row in range(rows):
		for col in range(cols):
			if matrix[row][col] == 0:
				for i in range(1,10):
					if is_legal(matrix,
						row,col,i):
						matrix[row][col] = i
						if solve_sudoku(
							matrix):
							return True
						matrix[row][col] = 0
				return False 


			else:
				nonzero_elements += 1

	return nonzero_elements == rows*cols

if __name__ == "__main__":
	arr_legal = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	solve_sudoku(arr_legal)
	print(arr_legal)
	for row in arr_legal:
		print(row)
	expected = [ 
		[5, 3, 4, 6, 7, 8, 9, 1, 2],
		[6, 7, 2, 1, 9, 5, 3, 4, 8],
		[1, 9, 8, 3, 4, 2, 5, 6, 7],
		[8, 5, 9, 7, 6, 1, 4, 2, 3],
		[4, 2, 6, 8, 5, 3, 7, 9, 1],
		[7, 1, 3, 9, 2, 4, 8, 5, 6],
		[9, 6, 1, 5, 3, 7, 2, 8, 4],
		[2, 8, 7, 4, 1, 9, 6, 3, 5],
		[3, 4, 5, 2, 8, 6, 1, 7, 9]
	]
	assert(arr_legal == expected)
