"""
Check if a linked list is a palindrome
1->2->3->3->2->1 true
1->2->3->2->1 true
1->2->3->3->1 false
1->2->3->3->4->1 false
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val
def reverse_ll(head):

	cur = head
	previous = None
	while cur:
		next_ = cur.next_
		cur.next_ = previous
		cur = next_
	return previous


def is_ll_palindrome(head):
	"""
	So the strategy with this is
	do a fast and a short one, then reverse the slow one(since that
	would be the head of the fast one). then compare the slow to the head.
	I know how to do this from here, it's going to be tedious writing this all up.
	"""

if __name__ == "__main__":
	n1r = Node(1)
	n2r = Node(2,n1b)
	n3r = Node(3,n2b)
	n3l = Node(3,n3r)
	n2l = Node(2,n3l)
	n1l = Node(1,n2l)
	
