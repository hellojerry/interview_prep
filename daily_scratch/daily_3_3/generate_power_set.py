"""

Generate a power set.

the power set of {1,2} is {},{1},{2},{1,2}
the power set of {1,2,3} is {},{1},{2},{3},{1,2},{2,3},{3,1},{1,2,3}
"""

def recurse(value,acc):

	acc_len = len(acc)
	if acc_len == 0:
		acc.append(set())
		acc.append({value})
		return acc
	else:
		copied = acc.copy()
		for item in copied:
			to_add = item.copy()
			to_add.add(value)
			acc.append(to_add)
		return acc

def generate_power_set(my_set,acc):
	"""
	With this sort of problem, we can start with a base case -> acc is empty.
	if acc is empty, we add an empty set, along with one value from the set.
	if acc is not empty, we copy the acc, and add a single value to each new copied element
	in order to generate the next set.
	"""
	for value in my_set:
		acc = recurse(value,acc)
	return acc

if __name__ == "__main__":
	res = generate_power_set({1,2,3},[])
	print(res)


