
"""
Solve the knapsack problem.

ex:
knapsack size: 5oz
available items:
	geode1: $60, weight: 5oz
	geode2: $50, weight: 3oz
	geode3: $70, weight: 4oz
	geode4: $30, weight: 2oz

solution:
	$80, (geode2 & geode4)
"""

class Geode(object):

	def __init__(self,name,value,weight):
		for i in [value,weight]:
			assert(type(i) == int and i > 0)
		assert(type(name) == str and len(name) > 0)
		self.name = name
		self.value = value
		self.weight = weight
	def __str__(self):
		return "Geode(%s, weight:%d, value:%d)" % (self.name,self.weight,self.value)
	def __repr__(self):
		return "Geode(%s, weight:%d, value:%d)" % (self.name,self.weight,self.value)


def knapsack(matrix,items,K,remaining_weight):
	"""
	There are a couple of ways to do this.
	Let's try the matrix route. From what I remember,
	we want a matrix of total weight+1 columns, and len(items) rows.
	K represents the index in the items array.
	if the index is out of the items bound, return 0
	if the matrix at K with the remaining index isn't marked,
	we want to compute the max value with and without the weight,
	and set the matrix at K equal to the best price.
	Finally, we return the matrix at the item index with the remaining weight.
		
	"""
	if K < 0:
		return 0
	if matrix[K][remaining_weight] == -1:
		without_item = knapsack(matrix,items,K-1,remaining_weight)
		with_item = 0
		item_weight = items[K].weight
		item_value = items[K].value
		if remaining_weight >= item_weight:
			with_item = item_value + knapsack(matrix,items,K-1, remaining_weight - item_weight)
		matrix[K][remaining_weight] = max(with_item,without_item)
	return matrix[K][remaining_weight]	



def kickoff(items,total_weight):
	matrix = [[-1]*(total_weight+1) for item in items]
	res = knapsack_problem(matrix,weight,len(items),items)
