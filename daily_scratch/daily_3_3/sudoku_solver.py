"""
Solve sudoku
"""



def is_valid(matrix):
	"""
	So the first part of this is a validity check.
	If the item is duplicated in the row, no go.
	If the item is duplicated in the column, no go.
	If the item is in the section, no go.
	"""
	rows = len(matrix)
	cols = len(matrix[0])


	for row in range(rows):
		remaining_values = set(range(1,10))
		for col in range(cols):
			el = matrix[row][col]
			if el != 0 and el not in remaining_values:
				return False
			remaining_values -= {el}

	for col in range(cols):
		remaining_values = set(range(1,10))
		for row in range(rows):
			el = matrix[row][col]
			if el != 0 and el not in remaining_values:
				return False
			remaining_values -= {el}
	row_sections = [(0,3),(3,6),(6,9)]
	col_sections = row_sections.copy()
	for row_section in row_sections:
		for col_section in col_sections:
			remaining_values = set(range(1,10))
			for i in range(row_section[0],row_section[1]):
				for j in range(col_section[0],col_section[1]):
					el = matrix[i][j]
					if el != 0 and el not in remaining_values:
						return False
					remaining_values -= {el}
	return True


def solve_sudoku(matrix):
	"""
	So I can't remember if i wanted a secondary matrix
	for this or not.
	Let's 
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	nonzero_rows = 0
	for row in range(rows):
		for col in range(cols):
			if matrix[row][col] == 0:
				for i in range(1,10):
					matrix[row][col] = i
					if is_valid(matrix) and (
						solve_sudoku(matrix)):
						return True
					else:
						matrix[row][col] = 0
				print(matrix)
				return False
			else:
				nonzero_rows += 1
	return nonzero_rows == 81


if __name__ == "__main__":
	arr_legal = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	solve_sudoku(arr_legal)
	print(arr_legal)
	print(is_valid(arr_legal))
	for row in arr_legal:
		print(row)
	expected = [ 
		[5, 3, 4, 6, 7, 8, 9, 1, 2],
		[6, 7, 2, 1, 9, 5, 3, 4, 8],
		[1, 9, 8, 3, 4, 2, 5, 6, 7],
		[8, 5, 9, 7, 6, 1, 4, 2, 3],
		[4, 2, 6, 8, 5, 3, 7, 9, 1],
		[7, 1, 3, 9, 2, 4, 8, 5, 6],
		[9, 6, 1, 5, 3, 7, 2, 8, 4],
		[2, 8, 7, 4, 1, 9, 6, 3, 5],
		[3, 4, 5, 2, 8, 6, 1, 7, 9]
	]
	assert(arr_legal == expected)
