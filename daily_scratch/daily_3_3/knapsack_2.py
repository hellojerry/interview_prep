


def knapsack(matrix,item_array,item_idx,remaining_weight):

	## exit if we've exhausted the array
	if item_idx < 0:
		return 0
	## check and see if the combination hasn't been computed.
	## if it hasn't, get the top value of the knapsack with the item
	## and without the item
	if matrix[item_idx][remaining_weight] == -1:
		item_weight = item_array[item_idx].weight
		item_value = item_array[item_idx].value
		without_item = knapsack(matrix,item_array,item_idx-1,remaining_weight)
		with_item = 0
		if item_weight <= remaining_weight:
			with_item = item_value + knapsack(matrix,item_array,item_idx-1,remaining_weight - item_weight)
		matrix[item_idx][remaining_weight] = max(with_item,without_item)

	return matrix[item_idx][remaining_weight]

def kickoff(item_array,remaining_weight):
	arrlen = len(item_array)
	matrix = [[-1](remaining_weight+1) for i in range(arrlen)]
	return knapsack(matrix,item_array,arrlen-1,remaining_weight)
