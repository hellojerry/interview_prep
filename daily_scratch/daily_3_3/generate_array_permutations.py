


def generate_permutations(arr):
	"""
	How did this go again?
	We want to g
	"""
	arrlen = len(arr)

	if arrlen == 0:
		return [[]]
	if arrlen == 1:
		return [arr]
	if arrlen == 2:
		return [arr[::-1], arr]
	out = []
	for ind in range(arrlen):
		el = arr[ind]
		left,right = arr[:ind], arr[ind+1:]
		for item in generate_permutations(left+right):
			out.append([el] + item)
	return out
if __name__ == "__main__":
	res = generate_permutations([1,2,3])
	print(res)
