
"""
Given a binary tree, such as 
	 10
	/ \
       2   3
      /   / \
    -5   4  -7
              \
               28
Find the maximum sum of a path on the tree. 
Path can connect between any two nodes; not just from root to leaves. 
For example, instead of just going 1 → 2 or 1 → 3, you can also go 2 → 1 → 3 ⇒ maxSum = 2 + 1 + 3 = 6 instead of 4. 
So the maxSum of the above tree will be: 4 → 2 → 1 → 3 ⇒ 4 + 2 + 1 + 3 = 10

second scenario: 28 on the furthest lowest leaf, 10 at root 

proper solution should be 2+10+3-7+28 = 36
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

class Solver(object):

	def __init__(self):
		self.current_max = None

	def get_max_path(self,root):
		"""
		We want the maximum of the root by itself,
		root + left + right,
		left hand side, and right hand side
		"""
		if root is None:
			return 0
		lhs = self.get_max_path(root.left)
		rhs = self.get_max_path(root.right)
		max_single = max(max(lhs,rhs)+root.val, root.val)
		max_top = max(max_single, root.val + lhs+rhs)
		if self.current_max is None:
			self.current_max = max_top
		else:
			self.current_max = max(max_top,self.current_max)

		return max_single




if __name__ == "__main__":
	bot_left = TreeNode(-5)
	mid_left = TreeNode(2, bot_left)
	bot_right_left = TreeNode(4)
	furthest_bottom_right_left = TreeNode(28)
	bot_right_right = TreeNode(-7, furthest_bottom_right_left)
	mid_right = TreeNode(3, bot_right_left, bot_right_right)
	root = TreeNode(10, mid_left, mid_right)
	solver = Solver()
	solver.get_max_path(root)
	assert(solver.current_max == 36)
	bot_left = TreeNode(1)
	root = TreeNode(-2, bot_left)
	solver = Solver()
	solver.get_max_path(root)
	assert(solver.current_max == 1)
