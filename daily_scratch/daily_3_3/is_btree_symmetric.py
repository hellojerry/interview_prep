"""

symmetric
	A

   B       B
     C    C


not:
	A

    B     B

C       C


not:
		A

           B        B

      	     D     C

not:
		A
           B         B
             C    C
           D

is:
		A
	B 		B
     C    D          D     C

"""

class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == str and len(val) >= 1)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%s)" % self.val
	def __repr__(self):
		return "TreeNode(%s)" % self.val

IS_ROOT = 0
IS_LEFT = 1
IS_RIGHT = 2
def is_symmetric(root):
	"""
	My first thought here - we can store the levels in an array. 
	after the first element (the root),
	we test the levels, outer to inner, wherein left.left must be == right.right.
	I think we can get close on this, if we store another value
	in the outbound tuple with is_left and is_right
	"""
	levels = []
	current_level = 0
	queue = [(root,current_level,IS_ROOT)]
	while queue:
		node,current_level,position = queue.pop(0)
		new_level = current_level + 1
		if len(levels) < new_level:
			levels.append([])
		levels[-1].append((node,current_level,position))
		current_level = new_level
		if node.left:
			queue.append((node.left,
				current_level,IS_LEFT))
		if node.right:
			queue.append((node.right,
				current_level,IS_RIGHT))
	## 1. skip the root.
	## 2. the far left must be opposite far right.
	## 3. work inward.
	for level in levels[1:]:
		## 
		level_len = len(level)
		if level_len % 2 != 0:
			return False
		left_ptr,right_ptr = 0,level_len-1
		while left_ptr < right_ptr:
			left = level[left_ptr]
			right = level[right_ptr]
			if left[0].val != right[0].val:
				return False
			if left[2] == right[2]:
				return False
			left_ptr += 1
			right_ptr -= 1

	return True

if __name__ == "__main__":
	ncl = TreeNode("C")
	ncr = TreeNode("C")
	nbl = TreeNode("B",None,ncl)
	nbr = TreeNode("B",ncr)
	root = TreeNode("A", nbl,nbr)
	assert(is_symmetric(root) == True)
	ncl = TreeNode("C")
	ncr = TreeNode("C")
	nbl = TreeNode("B",ncl)
	nbr = TreeNode("B",ncr)
	root = TreeNode("A",nbl,nbr)
	assert(is_symmetric(root) == False)

	ndl = TreeNode("D")
	ncl = TreeNode("C")
	nbl = TreeNode("B",None,ndl)
	nbr = TreeNode("B",ncl)
	root = TreeNode("A",nbl,nbr)
	assert(is_symmetric(root) == False)

	ndl = TreeNode("D")
	ncl = TreeNode("C",ndl)
	ncr = TreeNode("C")
	nbl = TreeNode("B",None,ncl)
	nbr = TreeNode("B",ncr)
	root = TreeNode("A",nbl,nbr)
	assert(is_symmetric(root) == False)


	ncl = TreeNode("C")
	ndl = TreeNode("D")
	ncr = TreeNode("C")
	ndr = TreeNode("D")
	nbl = TreeNode("B",ncl,ndl)
	nbr = TreeNode("B",ndr,ncl)
	root = TreeNode("A",nbl,nbr)
	assert(is_symmetric(root) == True)
