
def count_all_1s(num):

	ct = 0
	while num:
		if num & 1:
			ct += 1
		num >>= 1
	return ct

def easy_ct(num):
	return bin(num)[2:].count("1")

if __name__ == "__main__":
	assert(easy_ct(5) == count_all_1s(5))
	assert(easy_ct(402) == count_all_1s(402))
