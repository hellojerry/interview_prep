"""

arr: [1,2,3]
all permutations:

	[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]
"""


def get_permutations(arr):
	"""
	This is something like iterating over the array, taking 
	the target element out, doing [target] + left + right,
	[target] + right + left, [left] + [target] + [right].
	Can't remember if it's recursive. Let's see what happens
	when we don't recurse.
	"""
	arrlen = len(arr)
	if arrlen == 0:
		return [[]]
	if arrlen == 1:
		return [arr]
	out = []
	for i in range(arrlen):
		target_element = arr[i]
		left = arr[:i]
		right = arr[i+1:]
		for j in get_permutations(left+right):
			out.append([target_element] + j)
	return out


if __name__ == "__main__":
	initial = [1,2,3]
	res = get_permutations(initial)
	print(res)
