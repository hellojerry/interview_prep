"""
arr = [
	[ 1, 2, 3, 4, 5],
	[ 6, 7, 8, 9,10],
	[11,12,13,14,15],
	[16,17,18,19,20],
	[21,22,23,24,25],
]

expected = [
	[21,16,11, 6,1],
	[22,17,12, 7,2],
	[23,18,13, 8,3]
	[24,19,14, 9,4],
	[25,20,15,10,5]
]

"""
def rotate_matrix_clockwise(matrix):
	"""
	0,0 => 0,4
	0,4 => 4,4
	4,4 => 4,0
	4,0 => 0,0

	0,1 => 1,4
	1,4 => 4,3
	4,3 => 0,3
	3,0 => 0,1
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	for row in range(int(rows/2)):
		for col in range(row,rows - row - 1):
			right_edge = rows - row-1
			col_edge = cols - col - 1
			print("col_edge:%d" % (cols-col-1))
			print("[%d][%d] -> [%d][%d]" % (row,col,col,right_edge))
			print("[%d][%d] -> [%d][%d]" % (col,right_edge,right_edge,col_edge))
			print("[%d][%d] -> [%d][%d]" % (right_edge,col_edge,row,col_edge))
			print("[%d][%d] -> [%d][%d]" % (col_edge,row,row,col))
			(matrix[col][right_edge],
			matrix[right_edge][col_edge],
			matrix[col_edge][row],
			matrix[row][col]) = (

			matrix[row][col],
			matrix[col][right_edge],
			matrix[right_edge][col_edge],
			matrix[col_edge][row])




if __name__ == "__main__":

	arr = [
		[ 1, 2, 3, 4, 5],
		[ 6, 7, 8, 9,10],
		[11,12,13,14,15],
		[16,17,18,19,20],
		[21,22,23,24,25],
	]
	rotate_matrix_clockwise(arr)
	print(arr)
