

def reverse_digits(num):

	reverse, remaining = 0,abs(num)
	while remaining:
		remaining,remainder = divmod(remaining,10)
		reverse *= 10
		reverse += remainder

	if num < 0:
		return -reverse
	return reverse


if __name__ == "__main__":
	assert(reverse_digits(42) == 24)
	assert(reverse_digits(9) == 9)
	assert(reverse_digits(-413) == -314)
