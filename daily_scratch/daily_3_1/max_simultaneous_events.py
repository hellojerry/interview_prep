"""
Write a program that takes a set of events, and determines a maximum
number of events that take place concurrently.

ex:

	[(E1,1,5),(E5,2,7),(E8,4,5),(E2,6,10),(E6,8,9),(E9,9,17),(E11,11,13)(E7,12,15),(E4,14,15)]
result: 3 (E1,E5,E8)
(because they have the most overlapping results).
Don't worry about a class here, just use tuples.
"""


def max_concurrent(events):
	"""
	Sort by first element.
	Iterate from 1 to end. If prior. end > current.start,
	
	"""
	arr_len = len(events)
	if arr_len == 0:
		return 0
	if arr_len == 1:
		return 1	
	as_sorted = sorted(events, key=lambda x: x[0])
	previous = as_sorted[0]
	current_max = 1
	candidate_max = 1
	for event in as_sorted[1:]:
		if event[0] <  previous[1]:
			candidate_max += 1
			current_max = max(candidate_max,current_max)
		else:
			candidate_max = 0
		previous = event
	return current_max

if __name__ == "__main__":

	events_arr = [
		(2,7),
		(8,9),
		(14,15),
		(9,17),
		(1,5),
		(11,13),
		(12,15),
		(4,5),
		(6,10),
	]
	res = max_concurrent(events_arr)
	assert(res == 3)
