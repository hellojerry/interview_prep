

def bsearch(arr,target_val):
	arrlen = len(arr)
	left_ptr,right_ptr = 0, arrlen - 1

	while left_ptr <= right_ptr:
		midpoint = int((left_ptr+right_ptr)/2)
		if arr[midpoint] == target_val:	
			return midpoint
		elif arr[midpoint] < target_val:
			left_ptr = midpoint+1
		else:
			right_ptr = midpoint - 1

	return -1

if __name__ == "__main__":
	arr = [1,3,5,7,9,12,13,14,15,16,19]
	assert(bsearch(arr,14) == arr.index(14))
	assert(bsearch(arr,22) == -1)
