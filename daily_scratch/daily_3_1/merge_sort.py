

def merge_sort(arr):
	arrlen = len(arr)
	if arrlen <= 1:
		return
	midpoint = int(arrlen/2)
	left,right = arr[:midpoint], arr[midpoint:]
	merge_sort(left)
	merge_sort(right)

	left_ptr,right_ptr,arr_ptr = 0,0,0
	left_len = len(left)
	right_len = len(right)
	
	while left_ptr < left_len and right_ptr < right_len:
		left_val,right_val = left[left_ptr],right[right_ptr]
		if left_val < right_val:
			arr[arr_ptr] = left_val
			left_ptr += 1
		else:
			arr[arr_ptr] = right_val
			right_ptr += 1
		arr_ptr += 1

	while left_ptr < left_len:
		arr[arr_ptr] = left[left_ptr]
		left_ptr += 1
		arr_ptr += 1

	while right_ptr < right_len:
		arr[arr_ptr] = right[right_ptr]
		right_ptr += 1
		arr_ptr += 1

if __name__ == "__main__":
	arr = [0,-6,2,1,4,6,4,1,4,7,33,2,1,0,0,0,55,3,1,3]
	merge_sort(arr)
	assert(arr == sorted(arr))
