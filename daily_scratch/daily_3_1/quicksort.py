
def get_partition_idx(arr,low,high):
	pivot = arr[high]
	left_ptr = low - 1
	for i in range(low,high):
		if arr[i] <= pivot:
			left_ptr += 1
			arr[i],arr[left_ptr] = arr[left_ptr],arr[i]
	arr[high],arr[left_ptr+1] = arr[left_ptr+1],arr[high]
	return left_ptr+1

def qsort(arr,low,high):
	if low < high:
		partition_idx = get_partition_idx(arr,low,high)
		qsort(arr,partition_idx+1,high)
		qsort(arr,low,partition_idx-1)

if __name__ == "__main__":
	arr = [1,3,6,-4,2,1,3,5,32,0,-1,333,2,2,1,4,2]
	arrlen = len(arr)
	qsort(arr,0,arrlen-1)
	assert(arr == sorted(arr))
