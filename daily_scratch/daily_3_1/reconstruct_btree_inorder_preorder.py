"""
		H
   	   I        J
      K      L     O   Q
  M        N        P    R
"""

class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == str and len(val) > 0)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%s)" % self.val
	def __repr__(self):
		return "TreeNode(%s)" % self.val
def preorder(root,acc):
	if not root:
		return None
	acc.append(root.val)
	preorder(root.left,acc)
	preorder(root.right,acc)

def inorder(root,acc):
	if not root:
		return None
	inorder(root.left,acc)
	acc.append(root.val)
	inorder(root.right,acc)


def reconstruct(pre_arr,in_arr):
	"""
	we know that the root is the leftmost in 
	the preorder arr
	"""
	if len(pre_arr) == 0:
		return None
	root_val = pre_arr[0]
	root = TreeNode(root_val)
	in_root_idx = in_arr.index(root_val)
	in_left_tree = in_arr[:in_root_idx]
	in_right_tree = in_arr[in_root_idx+1:]
	left_in_len = len(in_left_tree)
	pre_left_tree = pre_arr[1:left_in_len+1]
	pre_right_tree = pre_arr[left_in_len+1:]
	root.left = reconstruct(pre_left_tree, in_left_tree)
	root.right = reconstruct(pre_right_tree,in_right_tree)
	return root


if __name__ == "__main__":
	node_M = TreeNode("M")
	node_N = TreeNode("N")
	node_P = TreeNode("P")
	node_R = TreeNode("R")

	node_K = TreeNode("K",node_M)
	node_L = TreeNode("L",node_N)
	node_O = TreeNode("O",None,node_P)
	node_Q = TreeNode("Q",None,node_R)
	node_I = TreeNode("I",node_K,node_L)
	node_J = TreeNode("J",node_O,node_Q)
	root = node_H = TreeNode("H",node_I,node_J)
	

	pre_arr = ["H","I","K","M","L","N","J","O","P","Q","R"]
	inorder_arr = ["M", "K", "I", "N", "L", "H", "O", "P", "J", "Q", "R"]
	res = reconstruct(pre_arr,inorder_arr)
	acc_a,acc_b = [],[]
	preorder(res, acc_a)
	inorder(res,acc_b)
	assert(acc_a == pre_arr)
	assert(acc_b == inorder_arr)
