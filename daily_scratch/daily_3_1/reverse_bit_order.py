

def reverse_bit_order(num):
	"""
	This involves a shift left and
	a shift right iirc.
	"""	
	reverse = 0
	while num:
		reverse <<= 1
		if num & 1:
			reverse += 1
		num >>= 1
	return reverse
		



def easy_check(num):
	as_bits = bin(num)[2:][::-1]
	return int(as_bits,2)

if __name__ == "__main__":
	expected_5 = easy_check(5)
	expected_10 = easy_check(10)
	expected_2001 = easy_check(2001)
	assert(reverse_bit_order(5) == expected_5)
	assert(reverse_bit_order(10) == expected_10)
	assert(reverse_bit_order(2001) == expected_2001)
