"""
			11
                 7                 15
              5    9          13      17
            3  6  8  10     12  14  16  
"""

class TreeNode(object):

	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.val

	def __repr__(self):
		return "TreeNode(%d)" % self.val

def find_lca(root,node_1,node_2):
	"""
	I think this is just accomplished with an inorder traversal
	"""
	if not root:
		return None
	if root is node_1 or root is node_2:
		return root
	left = find_lca(root.left,node_1,node_2)
	right = find_lca(root.right,node_1,node_2)
	if left and right:
		return root
	if left:
		return left

	return right
	


if __name__ == "__main__":
	node_3 = TreeNode(3)
	node_6 = TreeNode(6)
	node_8 = TreeNode(8)
	node_10 = TreeNode(10)
	node_12 = TreeNode(12)
	node_14 = TreeNode(14)
	node_16 = TreeNode(16)

	node_5 = TreeNode(5,node_3,node_6)
	node_9 = TreeNode(9,node_8,node_10)
	node_13 = TreeNode(13,node_12,node_14)
	node_17 = TreeNode(17,node_16)

	node_7 = TreeNode(7,node_5,node_9)
	node_15 = TreeNode(15,node_13,node_17)
	node_11 = TreeNode(11,node_7,node_15)
	root = node_11
	res = find_lca(node_11,node_5,node_8)
	
	expected = node_7
	assert(res == expected)

	res = find_lca(root,node_8,node_10)
	expected = node_9
	assert(res == expected)
	res = find_lca(root,node_12,node_17)
	expected = node_15
	assert(res == expected)
	res = find_lca(root,node_13,node_15)
	expected = node_15
	assert(res == expected)
