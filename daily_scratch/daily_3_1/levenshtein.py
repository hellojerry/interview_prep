

def levenshtein(left,right):
	"""
	1. make sure left is larger
	2. make 2d array of left*rows+1 right*cols+1 of -1
	3. fill left col and top row with range values
	4. for row in rows, for col in cols:
		if matrix[row][col] == -1:
			if left[x-1] == right[y-1]:
				matrix[x][y] = min(
					matrix[x-1][y]+1
					matrix[x-1][y-1],
					matrix[x][y-1]+1
				)
			else:
				matrix[x][y] = min(
					matrix[x-1][y]+1
					matrix[x-1][y-1]+1,
					matrix[x][y-1]+1
	5. return matrix[-1][-1]
	"""

	left_size = len(left)+1
	right_size = len(right)+1
	if left_size < right_size:
		left,right = right,left
		left_size,right_size = right_size,left_size
	matrix = [[-1]*right_size for i in range(left_size)]
	print(matrix)
	for i in range(right_size):
		matrix[0][i] = i
	for i in range(left_size):
		matrix[i][0] = i
	for x in range(left_size):
		for y in range(right_size):
			if matrix[x][y] == -1:
				if left[x-1] == right[y-1]:
					matrix[x][y] = min(
						matrix[x-1][y]+1,
						matrix[x-1][y-1],
						matrix[x][y-1]+1
					)
				else:
					matrix[x][y] = min(
						matrix[x-1][y]+1,
						matrix[x-1][y-1]+1,
						matrix[x][y-1]+1
					)
	return matrix[-1][-1]




if __name__ == "__main__":
	a = "Saturday"
	b = "Sundays"
	expected = 4
	res = levenshtein(a,b)
	assert(res == 4)
