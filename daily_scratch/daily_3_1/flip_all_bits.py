"""
So i think with flipping all the bits, you 
"""


def get_min_bits(num):
	ct = 0
	while num:
		ct += 1
		num >>= 1
	return ct

def flip_all_bits(num,num_bits=8):
	return (1<<num_bits) - 1 - num
	

def easy_flip(num):
	out = ""
	for el in bin(num)[2:]:
		if el == "1":
			out += "0"
		else:
			out += "1"
	return int(out,2)

if __name__ == "__main__":
	assert(flip_all_bits(4,get_min_bits(4)) == easy_flip(4))
	assert(flip_all_bits(10,get_min_bits(10)) == easy_flip(10))
	assert(flip_all_bits(900,get_min_bits(900)) == easy_flip(900))
	assert(flip_all_bits(1221,get_min_bits(1221)) == easy_flip(1221))
