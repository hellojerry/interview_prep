"""

The power set of S is the set of all subsets of S, including
the empty subset and S itself.

ex:
	{0,1,2}

res:
	{}
	{0}
	{1}
	{2},
	{0,1},
	{1,2}
	{0,2}
	{0,1,2}
"""

def generate_power_set(S, out):
	"""
	Can't remember. Do we start from the bottom or the top?
	I think it's the bottom.
	"""
	out_len = len(out)
	if out_len == 0:
		out.append(set())
		out.append({value})
		return out
	else:
		copied = out.copy()
		for minilist in copied:
			mini_acc = minilist.copy()
			mini_acc.add(value)
			acc.append(mini_acc)
		return acc
def find_subsets(my_set):
	acc = []
	for item in my_set:
		acc = generate_subsets(item,acc)
	return acc
