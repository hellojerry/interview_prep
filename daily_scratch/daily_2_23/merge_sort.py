

def merge_sort(arr):

	arrlen = len(arr)
	if arrlen > 1:
		mid = int(arrlen/2)
		left = arr[:mid]
		right = arr[mid:]
		merge_sort(left)
		merge_sort(right)
		left_ptr,right_ptr,arr_ptr = 0,0,0
		while left_ptr < len(left) and right_ptr < len(right):
			left_val,right_val = left[left_ptr],right[right_ptr]
			if left_val < right_val:
				arr[arr_ptr] = left_val
				left_ptr += 1
			else:
				arr[arr_ptr] = right_val
				right_ptr += 1
			arr_ptr += 1

		## stragglers
		while left_ptr < len(left):
			arr[arr_ptr] = left[left_ptr]
			left_ptr += 1
			arr_ptr += 1

		while right_ptr < len(right):
			arr[arr_ptr] = right[right_ptr]
			right_ptr += 1
			arr_ptr += 1

if __name__ == "__main__":
	arr = [994,-2,1,50,1,-803,2,4,1,5,3,1]
	merge_sort(arr)
	assert(arr == sorted(arr))
