


def reverse_bit_order(num):

	reverse = 0
	while num:
		reverse <<= 1
		reverse += num & 1
		num >>= 1
	return reverse



if __name__ == "__main__":
	in1 = int("1110000000000001",2)
	expected = int("1000000000000111",2)
	assert(reverse_bit_order(in1) == expected)
	
