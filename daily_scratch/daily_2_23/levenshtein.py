

def levenshtein_distance(lstring,rstring):

	lmax = len(lstring) + 1
	rmax = len(rstring) + 1
	matrix = [[0]*(rmax) for i in range(lmax)]

	for i in range(lmax):
		matrix[i][0] = i

	for i in range(rmax):
		matrix[0][i] = i

	for x in range(lmax):
		for y in range(rmax):
			if lstring[x-1] == rstring[y-1]:

				matrix[x][y] = min(
					matrix[x][y-1]+1,
					matrix[x-1][y-1],
					matrix[x-1][y] + 1
				)

			else:
				matrix[x][y] = min(
					matrix[x][y-1]+1,
					matrix[x-1][y-1] + 1,
					matrix[x-1][y] + 1
				)

	return matrix[-1][-1]

if __name__ == "__main__":
	s1, s2 = "Saturday","Sundays"
	res = levenshtein_distance(s1,s2)
	assert(res == 4)
