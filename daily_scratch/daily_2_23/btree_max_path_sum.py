
"""
Given a binary tree, such as 
	 10
	/ \
       2   3
      /   / \
    -5   4  -7
              \
               28
Find the maximum sum of a path on the tree. 
Path can connect between any two nodes; not just from root to leaves. 
For example, instead of just going 1 → 2 or 1 → 3, you can also go 2 → 1 → 3 ⇒ maxSum = 2 + 1 + 3 = 6 instead of 4. 
So the maxSum of the above tree will be: 4 → 2 → 1 → 3 ⇒ 4 + 2 + 1 + 3 = 10

second scenario: 28 on the furthest lowest leaf, 10 at root 

proper solution should be 2+10+3-7+28 = 36
"""
class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		self.value = value
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value





class Solver(object):

	def __init__(self):
		self.max_current = None

	def find_max_path(self,root):
	
		if not root:
			return 0
	
		lhs = self.find_max_path(root.left)
		rhs = self.find_max_path(root.right)
		max_single = max(max(lhs,rhs) + root.value, root.value)
	
		max_top = max(max_single, lhs+rhs+root.value)
		if self.max_current is None:
			self.max_current = max_top
		else:
			self.max_current = max(max_top,self.max_current)
		return max_single


if __name__ == "__main__":
	bot_left = TreeNode(-5)
	mid_left = TreeNode(2, bot_left)
	bot_right_left = TreeNode(4)
	furthest_bottom_right_left = TreeNode(28)
	bot_right_right = TreeNode(-7, furthest_bottom_right_left)
	mid_right = TreeNode(3, bot_right_left, bot_right_right)
	root = TreeNode(10, mid_left, mid_right)
	solver = Solver()
	solver.find_max_path(root)
	print(solver.max_current)

	bot_left = TreeNode(1)
	root = TreeNode(-2, bot_left)
	solver = Solver()
	solver.find_max_path(root)
	print(solver.max_current)
