

def reverse_digits(num):

	reverse,remaining = 0, abs(num)
	while remaining:
		remaining,remainder = divmod(remaining,10)
		reverse *= 10
		reverse += remainder
	if num < 0:
		return -reverse
	return reverse

if __name__ == "__main__":
	assert(reverse_digits(24) == 42)
	assert(reverse_digits(997) == 799)
	assert(reverse_digits(-401) == -104)
