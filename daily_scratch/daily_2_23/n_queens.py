
def is_legal_placement(occupied_row,occupied_col,col_array):
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if target_col == occupied_col:
			return False
		if abs(target_col - occupied_col) == abs(target_row - occupied_row):
			return False

	return True


def eight_queens(row,col_array,limit,acc):
	if row == limit:
		copied = list(zip(range(limit),col_array.copy()))
		acc.append(copied)
	else:
		for col in range(limit):
			if is_legal_placement(row,col,col_array):
				col_array[row] = col
				eight_queens(row+1,col_array,limit,acc)

if __name__ == "__main__":
	row = 0
	limit = 8
	acc = []
	col_array = list(range(limit))
	eight_queens(row,col_array,limit,acc)
	assert(len(acc) == 92)
