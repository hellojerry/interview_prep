

def generate_array_permutations(arr):
	"""
	With this one we want to remove an element in
	the range and call recursively.
	"""

	arrlen = len(arr)
	if arrlen == 0:
		return []
	if arrlen == 1:
		return [arr]
	out = []
	for i in range(arrlen):
		el = arr[i]
		left, right = arr[:i], arr[(i+1):]
		for res in generate_array_permutations(left+right):
			out.append([el] + res)

	return out

if __name__ == "__main__":
	expected = sorted([[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]])
	res = sorted(generate_array_permutations([1,2,3]))
	assert(res == expected)
	print(res)
