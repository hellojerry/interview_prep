
"""
Solve the knapsack problem.

ex:
knapsack size: 5oz
available items:
	geode1: $60, weight: 5oz
	geode2: $50, weight: 3oz
	geode3: $70, weight: 4oz
	geode4: $30, weight: 2oz

solution:
	$80, (geode2 & geode4)
"""
class Geode(object):
	def __init__(self,name,weight,value):
		assert(type(name) == str)
		for i in [weight,value]:
			assert(type(i) == int and i > 0)
		self.name = name
		self.weight = weight
		self.value = value

	def __str__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)
	def __repr__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)


def knapsack_problem(matrix,item_idx,item_array,remaining_weight):

	## if we've exhausted the item array, we're done
	if item_idx < 0:
		return 0
	## if we haven't checked this index/remaining weight combo,
	## mark the current item/weight combo with the maximum
	## of the remaining combos with and without the item
	if matrix[item_idx][remaining_weight] == -1:
		without_item = knapsack_problem(matrix,item_idx-1,item_array,remaining_weight)
		item = item_array[item_idx]
		with_item = 0
		if item.weight <= remaining_weight:
			with_item = item.value + knapsack_problem(matrix,item_idx,item_array,remaining_weight-item.weight)

		matrix[item_idx][remaining_weight] = max(with_item,without_item)
	return matrix[item_idx][remaining_weight]





if __name__ == "__main__":
	## we need a matrix of range inclusive of remaining weight
	## and all negative ones

	items = [
		Geode("G1",5,60),
		Geode("G2",3,50),
		Geode("G3",4,70),
		Geode("G4",2,30)
	]
	weight = 5
	matrix = [[-1]*(weight+1) for i in items]
	res = knapsack_problem(matrix,len(items)-1,items,weight)
	assert(res == 80)
