


def reverse_digits(num):
	reverse, remaining = 0, abs(num)
	while remaining:
		## we need
		remaining, remainder = divmod(remaining,10)
		reverse *= 10
		reverse += remainder	
	if num < 0:
		return -reverse
	return reverse

if __name__ == "__main__":
	assert(reverse_digits(42) == 24)
	assert(reverse_digits(314) == 413)
	assert(reverse_digits(-314) == -413)
