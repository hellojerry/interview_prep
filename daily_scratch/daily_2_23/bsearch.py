

def bsearch(arr,target_value):
	arrlen = len(arr)
	left_ptr,right_ptr = 0,arrlen-1

	while left_ptr <= right_ptr:
		midpoint = int((left_ptr+right_ptr)/2)
		if arr[midpoint] == target_value:
			return midpoint
		elif arr[midpoint] < target_value:
			left_ptr = midpoint+1
		else:
			right_ptr = midpoint - 1


	return -1

if __name__ == "__main__":
	arr = [1,2,9,15,25,30,50,55,56,57]
	res = bsearch(arr,9)
	assert(res == 2)
