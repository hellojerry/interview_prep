

def get_partition_idx(arr,low,high):
	pivot = arr[high]
	left_ptr = low - 1
	for i in range(low,high):
		if arr[i] <= pivot:
			left_ptr += 1
			arr[left_ptr],arr[i] = arr[i],arr[left_ptr]
	arr[high],arr[left_ptr+1] = arr[left_ptr+1],arr[high]
	return left_ptr + 1

def qsort(arr,low,high):

	if low < high:
		partition_idx = get_partition_idx(arr,low,high)
		qsort(arr,low,partition_idx-1)
		qsort(arr,partition_idx+1,high)


if __name__ == "__main__":
	arr = [1,-4,3,10,5,92,-8,1002]
	low,high = 0, len(arr)-1
	qsort(arr,low,high)
	assert(arr == sorted(arr))
