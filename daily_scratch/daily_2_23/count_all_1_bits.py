


def all_1_bits(num):

	ct_1s = 0
	while num:
		ct_1s += num & 1
		num >>= 1
	return ct_1s

def check(num):
	return bin(num)[2:].count("1")

if __name__ == "__main__":
	assert(all_1_bits(9) == check(9))
	assert(all_1_bits(55) == check(55))
	assert(all_1_bits(109) == check(109))
