"""
Write a program that takes a set of events, and determines a maximum
number of events that take place concurrently.

ex:

	[(E1,1,5),(E5,2,7),(E8,4,5),(E2,6,10),(E6,8,9),(E9,9,17),(E11,11,13)(E7,12,15),(E4,14,15)]
result: 3 (E1,E5,E8)
(because they have the most overlapping results).

"""




def max_simultaneous_events(arr):

	events = sorted(arr, key=lambda x: x[0])
	event_len = len(arr)
	if event_len == 0:
		return 0
	if event_len == 1:
		return 1
	max_simultaneous, num_simultaneous = 1,1
	prior = events[0]
	## compare current event to prior.
	## if current start < prior end, 
	## else: reset num simultaneous
	for event in events[1:]:
		if event[0] < prior[1]:
			num_simultaneous += 1
			max_simultaneous = max(num_simultaneous,max_simultaneous)
		else:
			num_simultaneous = 0
		prior = event

	return max_simultaneous
if __name__ == "__main__":
	events = [
		(2,7),
		(4,5),
		(9,17),
		(1,5),
		(8,9),
		(14,15),
		(11,13),
		(12,15),
		(6,10),
	]
	res = max_simultaneous_events(events)
	assert(res == 3)
