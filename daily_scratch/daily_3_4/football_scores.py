"""
Determine the number of combinations 
of scores for a football game of 12 points,
assuming we only have field goals, safeties, and touchdowns
with 1 point conversions
"""

def num_combinations(remaining_score,plays):
	"""
	1. base case, remaining is zero: we're good
	2. remaining score is less than zero, or no plays left: no bueno
	3. test with the current score subtracted, repeating the play, combine
		without current score subtracted, removing current play as candidate
	"""
	if remaining_score == 0:
		return 1
	remaining_plays = len(plays)
	if remaining_plays == 0 or remaining_score < 0:
		return 0
	play_val = plays[0]
	return num_combinations(
		remaining_score - play_val, 
		plays) + num_combinations(remaining_score, plays[1:])

if __name__ == "__main__":
	print(num_combinations(12,[3,7,2]))
