# Daily Scratch Directory

This directory is specifically for handling things that should be memorized.

We should do 5 a day.

## Current Daily List

1. quicksort
2. merge sort
3. n queens
4. binary search
5. levenshtein distance
6. reversing digits without using a string
7. find LCA in BST
8. generate permutations of an array.
9. reconstruct Btree from preorder and inorder arrays.
10. max path in btree
11. max simultaneous events (array of tuplies with start and end times)
12. reversing bit order
13. flipping all bits
14. count all 1 bits in integer.
15. rotate matrix clockwise 90 degrees
16. search in 2d sorted array (11.6)
17. knapsack problem
18. check if a btree is symmetric (try it with recursion)
19. morris tree traversal (inorder traversal without stack and without recursion)
20. get running median of values (heap question)
21. get square root of floating point
22. LCA of btree using a set()
23. reconstruct BST from postorder and preorder traversals
24. generate power set. (dynamic programming)
25. sudoku solver (dynamic programming)
26. bedbathandbeyond problem (dynamic programming) (no matrix on this, just use a while loop!)
27. water trapped by lines(17.7, greedy algorithm)
28. gasup problem (greedy algorithm)  (find the location where the amount of inbound gas is the minimum - that's the spot where we know we can't get any worse, and that's where the surplus is)
29. check if linked list is palindrome
30. shorten directory path (stack problem)
31. largest subset length in set
32. max nondecreasing subsequence in array (16.12)/max increasing subsequence (just use a single array, NO recursion, nested loop!)
33. gray code computation (memorize this one - it's bizarre)
34. max area of skyline (17.8)
35. faster bit parity check
36. tower of hanoi
37. football scores/count change (try the recursive solution! it's actually not too hard to understand)
38. find closest integer weight
39. divide, multiply, add and subtract with only bitwise stuff 
40. number of subsets of size K of range N
41. two-sum and three-sum problems (greedy... remember to solve two-sum first)
42. palindromic decompositions of a string
43. maximum money in coin game (16.9)
44. rectangle overlap (4.11)
45. test if two values differ by a single bit
46. pretty printing (16.11)
47. array product except self (go forwards and backwards to make two arrays) (leetcode 4)
48. max contiguous subarray (kahane's algorithm - don't overthink it)
49. max contiguous subproduct
50. coin change (fewest combinations - do the nested loop)
51. jump game (this is NOT dynamic programming - DONT GET TRICKED! it's a greedy algorithm)
52. interweave linked list (leetcode 43 - remember the slow-and-fast pointer trick)
53. implement a prefix tree (easier than it sounds)
54. merge K sorted linked lists (try an approach with a heap)
55. longest common subsequence (leetcode 75 - this is a levenshtein distance problem)
56. combination sum (leetcode 20 - it's DP, don't bother with a complex matrix setup, this can be done by tinkering with a basic recursive solution)
57. word break problem (use a queue and do a BFS!)
58. Decode ways (use a memo array and backtrack two spaces. use the prior memo value to inform current memo value. MEMORIZE THIS)
59. minimum window substring (just revisit this with a clean head. i was really close. remember to make the outer loop increment right ptr forward, and inner loop increment left)
60. pacific-atlantic water flow (use two caches!! don't get tricked)

### Thursday 2/20

1. quicksort - DONE
2. merge sort - DONE
3. n queens - DONE
4. binary search - DONE


### Friday 2/21

1. quicksort - DONE
2. merge sort - DONE
3. n queens - DONE
4. binary search - DONE
5. levenshtein distance - DONE

### Saturday 2/22

1. quicksort - DONE
2. merge sort - DONE
3. n queens - DONE
4. bsearch - DONE
5. levenshtein distance - DONE
6. reversing digits without using a string- DONE
7. find LCA in BST - DONE
8. generate permutations of an array - DONE
9. reconstruct btree from preorder and inorder arrays.

### Sunday 2/23

1. quicksort - DONE
2. merge sort - DONE
3. n queens - DONE
4. bsearch - DONE
5. levenshtein distance - DONE
6. reverse digits without using a string - DONE
7. find LCA in BST - DONE
8. generate permutations of an array - DONE
9. reconstruct btree from preorder and inorder arrays. - DONE
10. max path sum in btree - DONE
11. max simultaneous events (array of tuples with start and end times) - DONE
12. count all 1 bits in an integer - DONE
13. reverse bit order in an integer - DONE
14. flip all bits - needs more research
15. knapsack problem - DONE

### Wednesday 2/26

1. rotate matrix 90 degrees clockwise - DONE
2. quicksort - DONE
3. merge sort - DONE
4. count all 1 bits in integer - DONE
5. reverse bit order in an integer - DONE

### Thursday 2/27

1. knapsack problem
2. find LCA in BST
3. max path sum in btree
4. generate permutations of an array
5. levenshtein distance
6. reconstruct bst from preorder and postorder
7. generate power set

### Sunday 3/1


1. quicksort - DONE
2. merge sort - DONE
3. n queens - DONE
4. binary search - DONE
5. levenshtein distance - DONE
6. reversing digits without using a string - DONE
7. find LCA in BST - DONE
8. generate permutations of an array. - DONE (do again tomorrow)
9. reconstruct Btree from preorder and inorder arrays. - DONE
10. max path in btree - DONE (do again tomorrow)
11. max simultaneous events (array of tuplies with start and end times) - DONE
12. reversing bit order - DONE
13. flipping all bits - DONE (strategy here is counting the number of bits, then shifting the number left by the bit count, subtracting 1, and subtracting the number itself)
14. count all 1 bits - DONE
15. rotate matrix clockwise - DONE (do again tomorrow)

### Tuesday 3/3

1. generate permutations of an array - DONE
2. max path in btree - DONE
3. search in 2d sorted array
4. knapsack problem - DONE
5. check if btree is symmetric - DONE
6. generate power set - DONE
7. sudoku solver - DONE
8. linked list is palindrome - DONE

### Wednesday 3/25
work through all of the known servicenow questions from leetcode.
at 7pm, work through the spring tutorial.
Thursday morning, get a decent amount of exercise, even go on a run.
Have all of the questions available on a terminal. make sure the room is clean.

1. levenshtein distance - DONE
2. merge sort - DONE
3. quicksort - DONE
4. minimum substring window - DONE
5. Add Two numbers(Leetcode, leetcode problem #2)

