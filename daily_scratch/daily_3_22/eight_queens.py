

def is_legal_placement(occupied_row,occupied_col,col_array):
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if occupied_col == target_col:
			return False
		if abs(target_col-occupied_col) == abs(target_row-occupied_row):
			return False



	return True


def n_queens(row,col_array,N,acc):
	if row == N:
		copied = col_array.copy()
		acc.append(list(zip(range(N),copied)))
	else:
		for col in range(N):
			if is_legal_placement(row,col,col_array):
				col_array[row] = col
				n_queens(row+1,col_array,N,acc)

if __name__ == "__main__":
	acc = []
	col_array = list(range(8))
	N = 8
	n_queens(0,col_array,N,acc)
	print(len(acc))
