"""
count all 1 bits in an integer
"""


def count_1s(num):

	ct = 0
	while num:
		if num & 1:
			ct += 1
		num >>= 1


	return ct

def check(num):
	return bin(num)[2:].count("1")

if __name__ == "__main__":
	print(bin(5))
	print(count_1s(5) == check(5))
	print(count_1s(12132132) == check(12132132))
