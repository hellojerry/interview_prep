



def differs_by_one(a,b):
	diff = a ^ b
	return bit_diff and not (bit_diff & (bit_diff -1))

def directed_gray_code(num_bits,result,history):
	if len(result) == num_bits**2:
		return differs_by_one(result[0],result[-1])
	for i in range(num_bits):
		previous = result[-1]
		candidate = previous ^ (1 << i)
		if candidate not in history:
			history.add(candidate)
			result.append(candidate)
			if directed_gray_code(num_bits,result,history):
				return True
	return False

def gray_code(N):
	res = [0]
	directed_gray_code(N,res,set(res))
	return res


