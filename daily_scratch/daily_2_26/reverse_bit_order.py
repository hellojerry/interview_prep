

def reverse_bit_order(num):

	out = 0
	while num:
		out <<= 1
		if num & 1:
			out += 1
		num >>= 1

	return out
def check(num):
	return "0b"+ bin(num)[2:][::-1]


if __name__ == "__main__":
	assert(reverse_bit_order(5) == 5)
	assert(check(1711) == bin(reverse_bit_order(1711)))
