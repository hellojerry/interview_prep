

def get_partition_idx(arr,low,high):
	pivot = arr[high]
	left_ptr = low - 1
	for i in range(low,high):
		if arr[i] <= pivot:
			left_ptr += 1
			arr[left_ptr],arr[i] = arr[i],arr[left_ptr]
	arr[left_ptr+1],arr[high] = arr[high],arr[left_ptr+1]
	return left_ptr+1	

def qsort(arr,low,high):

	if low < high:
		partition_idx = get_partition_idx(arr,low,high)
		qsort(arr,low,partition_idx-1)
		qsort(arr,partition_idx+1,high)

if __name__ == "__main__":
	arr = [-34,1,4,1,2,4,-433,10,13,14,2,2,4,4,2,3,2,1,0]
	qsort(arr,0,len(arr)-1)
	assert(arr == sorted(arr))
