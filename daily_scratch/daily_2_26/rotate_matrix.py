"""
Rotate a matrix 90 degrees clockwise.

ex:
arr = [
	[ 1,2,3,4],
	[5,6,7,8],
	[9,10,11,12],
	[13,14,15,16]
]

res = [
	[13,9,5,1],
	[14,10,6,2],
	[15,11,7,3],
	[16,12,8,4]
]


arr2 = [
	[ 1, 2, 3, 4, 5],
	[ 6, 7, 8, 9,10],
	[11,12,13,14,15],
	[16,17,18,19,20],
	[21,22,23,24,25]
]

res2 = [

	[21,16,11,6,1],
	[22,17,12,7,2],
	[23,18,13,8,3],
	[24,19,14,9,4],
	[25,20,15,10,5]


]



"""
from pprint import pprint

def rotate_clockwise(matrix):
	rows = len(matrix)
	cols = len(matrix[0])

	"""
	First round:
	[0][0] -> [0][3]
	[0][3] -> [3][3]
	[3][3] -> [3][0]
	[3][0] -> [0][0]
	second round:
	[0][1] -> [1][3]
	[1][3] -> [3][2]
	[3][2] -> [2][0]
	[2][0] -> [0][1]
	third round:
	[0][2] -> [2][3]
	[2][3] -> [3][1]
	[3][1] -> [1][0]
	[1][0] -> [0][2]
	Next loop:
	[1][1] -> [1][2]
	[1][2] -> [2][2]
	[2][2] -> [2][1]
	[2][1] -> [1][1]
	"""
	for row in range(int(rows/2)):
		"""
		We only need to loop the outer row twice.
		So row = 0, row = 1
		"""
		for col in range(row, rows - row - 1):
			"""
			For the inner iteration, we need it three times once,
			then one time for the inner.
			First iteration:
				4 - 0 - 1 (range 3) 0->3
			Second iteration:
				4 - 1 - 1 (range (2) 1->1
			"""
			print("row: %d, col:%d" % (row,col))
			right_edge = rows - 1 - row
			print("right_edge: %d" % right_edge)
			col_edge = cols - col - 1
			print("[%d][%d] -> [%d][%d]" % (row,col,col,right_edge))
			print("[%d][%d] -> [%d][%d]" % (col,right_edge,right_edge,col_edge))
			print("[%d][%d] -> [%d][%d]" % (right_edge,col_edge,col_edge,row))
			print("[%d][%d] -> [%d][%d]" % (col_edge,row,row,col))

			(
			## 0,3 first
			matrix[col][right_edge],
			## 3,3 second
			matrix[right_edge][col_edge],
			matrix[col_edge][row],
			matrix[row][col]) = (
				matrix[row][col],
				matrix[col][right_edge],
				matrix[right_edge][col_edge],
				matrix[col_edge][row]
			)
			pprint(matrix)

if __name__ == "__main__":
	arr = [
		[ 1,2,3,4],
		[5,6,7,8],
		[9,10,11,12],
		[13,14,15,16]
	]
	rotate_clockwise(arr)
	res = [
		[13,9,5,1],
		[14,10,6,2],
		[15,11,7,3],
		[16,12,8,4]
	]
	assert(arr == res)
	arr2 = [
		[ 1, 2, 3, 4, 5],
		[ 6, 7, 8, 9,10],
		[11,12,13,14,15],
		[16,17,18,19,20],
		[21,22,23,24,25]
	]
	rotate_clockwise(arr2)
	res2 = [
		[21,16,11,6,1],
		[22,17,12,7,2],
		[23,18,13,8,3],
		[24,19,14,9,4],
		[25,20,15,10,5]
	]
	assert(arr2 == res2)
