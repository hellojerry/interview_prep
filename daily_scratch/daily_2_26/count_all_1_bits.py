

def count_all_1_bits(num):

	num_1s = 0
	while num:
		if num & 1:
			num_1s += 1
		num >>= 1
	return num_1s

def check(num):
	return bin(num)[2:].count("1")

if __name__ == "__main__":
	r = count_all_1_bits(5)
	assert(r == check(5))

	assert(count_all_1_bits(425) == check(425))
