"""
validate a BST
"""

def is_bst(root,minimum,maximum):
	if not root:
		return True
	if root.val <= minimum or root.val >= maximum:
		return False
	return is_bst(root.left,minimum,
		root.val-1) and is_bst(root.right,root.val+1,
				maximum)


def get_height(root):
	if not root:
		return 0
	return max(get_height(root.left),get_height(root.right)) + 1	

def is_height_balanced(root):
	if not root:
		return True
	lhs = get_height(root.left)
	rhs = get_height(root.right)
	if abs(lhs-rhs) <= 1 and is_height_balanced(
		root.left) and is_height_balanced(root.right):
		return True
	return False
