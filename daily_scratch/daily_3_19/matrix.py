"""

Rotate a matrix clockwise


arr = [
	[1,2,3,4,5],
	[6,7,8,9,10],
	[11,12,13,14,15],
	[16,17,18,19,20],
	[21,22,23,24,25],
]

out = [

	[21,16,11,6,1],
	[22,17,12,7,2],
	[23,18,13,8,3],
	[24,19,14,9,4],
	[25,20,15,10,5],
]
"""
def rotate(matrix):
	"""
	second rotation should be

	[0,1] = [3,0],
	[1,4] = [0,1],
	[4,3] = [1,4],
	[3,0] = [4,3]

	"""


	m = matrix
	rows = len(m)
	cols = len(m[0])
	for r in range(int(rows/2)):
		for c in range(r,rows-r-1):
			d1 = rows - r - 1
			d2 = cols - c - 1
			print("r:%d,c:%d,d1:%d,d2:%d" % (
				r,c,d1,d2))
			#m[r][c] = m[d2][r]
			#m[c][d1] = m[r][c]
			#m[d1][d2] = m[c][d1]
			#m[d2][r] = m[d1][d2]
			(m[r][c],m[c][d1],
			m[d1][d2],m[d2][r]) = (
				m[d2][r], m[r][c],
				m[c][d1], m[d1][d2])
			#print(m)
			



if __name__ == "__main__":
	
	arr = [
		[1,2,3,4,5],
		[6,7,8,9,10],
		[11,12,13,14,15],
		[16,17,18,19,20],
		[21,22,23,24,25],
	]
	rotate(arr)	
	out = [
	
		[21,16,11,6,1],
		[22,17,12,7,2],
		[23,18,13,8,3],
		[24,19,14,9,4],
		[25,20,15,10,5],
	]
	print(arr)
	assert(arr == out)
