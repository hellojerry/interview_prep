
"""
Given a btree, find it's maximum depth.

ex:
	3
     9     20
         15   7

res: 3
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def max_depth_bfs(root):
	"""
	Do a BFS, push to an array, edit the array 
	before cleanup.
	We can make this easier by just doing a max()
	instead of pushing to a new array.
	"""
	if not root:
		return []
	queue = [[root,0]]
	out = []
	while queue:
		node,current_level = queue.pop(0)
		new_level = current_level + 1
		if node.left:
			queue.append([node.left,new_level])
		if node.right:
			queue.append([node.right,new_level])
		if len(out) == 0:
			out.append([node.val,current_level])
		else:
			if out[-1][-1] != current_level:
				out[-1].pop()
				out.append([node.val,current_level])
			else:
				out[-1].insert(-1,node.val)
	out[-1].pop()
	return len(out)

def max_depth_dfs(root,current_level):
	if not root:
		return current_level
	current_level += 1
	return max(max_depth_dfs(root.left,current_level),
		max_depth_dfs(root.right,current_level))

if __name__ == "__main__":
	n9 = TreeNode(9)
	n15 = TreeNode(15)
	n7 = TreeNode(7)
	n20 = TreeNode(20,n15,n7)
	root = TreeNode(3,n9,n20)
	out = max_depth_bfs(root)
	print(out == 3)
	out = max_depth_dfs(root,0)
	print(out == 3)
