"""
remember to use a BFS on this
"""


def wb(S,words):

	slen = len(S)
	if slen == 0:
		return False
	if slen == 1:
		if S[0] in words:
			return True
		return False
	visited = [0]*slen
	queue = [0]
	while queue:
		start = queue.pop(0)
		for end in range(start+1,slen+1):
			if visited[start] == 0:
				candidate = S[start:end]
				if candidate in words:
					queue.append(end)

					if end == slen:
						return True

		visited[start] = 1
	return False
