


def word_break(S,words):
	words = set(words)
	S_len = len(S)
	visited = [0]*S_len
	queue = [0]
	while queue:
		start_idx = queue.pop(0)
		if visited[start_idx] == 0:
			for end in range(start_idx+1,S_len+1):
				candidate = S[start_idx:end]
				if candidate in words:
					queue.append(end)
					if end == S_len:
						return True
			visited[start_idx] = 1
	return False

if __name__ == "__main__":
	S = "leetcode"
	words = {"leet","code"}
	assert(word_break(S,words) == True)
