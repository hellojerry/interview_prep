

def decode(S):

	slen = len(S)
	memo = [0]*(slen+1)
	memo[0] = 1
	memo[1] = 0 if S[0] == "0" else 1
	for i in range(2,slen+1):
		el = S[i]
		if el != "0":
			memo[i] += memo[i-1]
		together = int(S[i-2:i])
		if 10 <= together <= 26:
			memo[i] += memo[i-2]
	return memo[-1]
