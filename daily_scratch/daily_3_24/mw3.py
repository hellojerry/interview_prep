
from collections import Counter, defaultdict


def minWindow(self, s, t):
    """
    :type s: str
    :type t: str
    :rtype: str
    """

    if not t or not s:
        return ""

    # Dictionary which keeps a count of all the unique characters in t.
    dict_t = Counter(t)

    # Number of unique characters in t, which need to be present in the desired window.
    required = len(dict_t)

    # left and right pointer
    l, r = 0, 0

    # formed is used to keep track of how many unique characters in t are present in the current window in its desired frequency.
    # e.g. if t is "AABC" then the window must have two A's, one B and one C. Thus formed would be = 3 when all these conditions are met.
    formed = 0

    # Dictionary which keeps a count of all the unique characters in the current window.
    window_counts = {}

    # ans tuple of the form (window length, left, right)
    ans = float("inf"), None, None

    while r < len(s):

        # Add one character from the right to the window
        character = s[r]
        window_counts[character] = window_counts.get(character, 0) + 1

        # If the frequency of the current character added equals to the desired count in t then increment the formed count by 1.
        if character in dict_t and window_counts[character] == dict_t[character]:
            formed += 1

        # Try and contract the window till the point where it ceases to be 'desirable'.
        while l <= r and formed == required:
            character = s[l]

            # Save the smallest window until now.
            if r - l + 1 < ans[0]:
                ans = (r - l + 1, l, r)

            # The character at the position pointed by the `left` pointer is no longer a part of the window.
            window_counts[character] -= 1
            if character in dict_t and window_counts[character] < dict_t[character]:
                formed -= 1

            # Move the left pointer ahead, this would help to look for a new window.
            l += 1    

        # Keep expanding the window once we are done contracting.
        r += 1    
    return "" if ans[0] == float("inf") else s[ans[1] : ans[2] + 1]



def minimum_window(S,T):
	## OK
	if not T or not S:
		return ""
	## OK
	T_freq = dict(Counter(T))
	## OK
	required = len(T_freq)
	## OK
	l,r,found = 0,0,0
	## OK
	min_window,min_left,min_right = float("inf"),-1,-1
	## OK
	subset_freq = defaultdict(int)
	while r < len(S):
		## OK
		rval = S[r]
		subset_freq[rval] += 1
		## OK
		if rval in T_freq and subset_freq[rval] == T_freq[rval]:
			found += 1
		while l <= r and found == required:
			lval = S[l]
			## compute minimum window, we've
			## got all we need
			if (r-l+1) < min_window:
				min_window = r-l+1
				min_left,min_right = l,r
			## remove left ptr
			subset_freq[lval] -= 1
			## decrement found if relevant to T
			if lval in T_freq and subset_freq[lval] < T_freq[lval]:
				found -= 1
			l += 1
		r += 1
	if min_window == float("inf"):
		return ""
	return S[min_left:min_right+1]

if __name__ == "__main__":
	S1 = "ADOBECODEBANC"
	T1 = "ABC"
	e1 = "BANC"
	r1 =minimum_window(S1,T1)
	print(r1, r1 == e1) 
	S2 = "ADOBECODEBANC"
	T2 = "ZOBE"
	e2 = ""
	r2 = minimum_window(S2,T2)
	print(r2, r2 == e2)
	S3 = "ADOBECODEBANC"
	T3 = "NAC"
	e3 = "ANC"
	r3 = minimum_window(S3,T3)
	print(r3,r3 == e3)
	S4 = "ADOBECODEBANC"
	T4 = "ONAC"
	e4 = "CODEBAN"
	r4 = minimum_window(S4,T4)
	print(r4, r4 == e4)
	S5 = "A"
	T5 = "A"
	e5 = "A"
	r5 = minimum_window(S5,T5)
	print(r5, r5 == e5)
	S6 = "AA"
	T6 = "AA"
	e6 = "AA"
	r6 = minimum_window(S6,T6)
	print(r6, r6 == e6)
	S7 = "A"
	T7 = "B"
	e7 = ""
	r7 = minimum_window(S7,T7)
	print(r7)
	S8 = "AB"
	T8 = "A"
	r8 = minimum_window(S8,T8)
	print(r8 == "A")
	S9 = "ODEBANC"
	T9 = "ON"
	R9 = minimum_window(S9,T9)
	print(R9 == "ODEBAN")
	S10 = "BBA"
	T10 = "BA"
	R10 = minimum_window(S10,T10)
	print(R10,R10 == "BA")
	S11 = "CABWEFGEWCWAEFGCF"
	T11 = "CAE"
	R11 = minimum_window(S11,T11)
	print(R11,R11 == "CWAE")
