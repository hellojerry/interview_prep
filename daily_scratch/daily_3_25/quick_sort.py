

def get_partition_idx(arr,low,high):
	pivot = arr[high]
	l = low - 1
	for i in range(low,high):
		if arr[i] <= pivot:
			l += 1
			arr[i],arr[l] = arr[l],arr[i]
	arr[l+1],arr[high] = arr[high],arr[l+1]
	return l+1

def qsort(arr,low,high):
	if low < high:
		partition_idx = get_partition_idx(arr,low,high)
		qsort(arr,low,partition_idx-1)
		qsort(arr,partition_idx+1,high)

if __name__ == "__main__":
	a = [10,5,1,4,1,3,1,3,2,1]
	qsort(a,0,len(a)-1)
	assert(a == sorted(a))
