

def levenshtein(A,B):
	lmax = len(A)+1
	rmax = len(B) + 1
	matrix = [[-1]*rmax for i in range(lmax)]
	## populate the top with values for A (left)

	for i in range(rmax):
		matrix[0][i] = i
	for i in range(lmax):
		matrix[i][0] = i
	for r in range(lmax):
		for c in range(rmax):
			if matrix[r][c] == -1:
				if A[r-1] == B[c-1]:
					matrix[r][c] = min(
						matrix[r-1][c] + 1,
						matrix[r-1][c-1],
						matrix[r][c-1] + 1
					)
	
	
				else:
					matrix[r][c] = min(
						matrix[r-1][c] + 1,
						matrix[r-1][c-1]+ 1,
						matrix[r][c-1] + 1
					)

	## populate the left with values for B(right)



	return matrix[-1][-1]

if __name__ == "__main__":
	a1 = "Saturday"
	b1 = "Sundays"
	e1 = 4
	r1 = levenshtein(a1,b1)
	print(r1)
