

def get_partition_idx(arr,low,high):
	pivot = arr[high]
	l = low - 1
	for i in range(low,high):
		if arr[i] <= pivot:
			l += 1
			arr[l],arr[i] = arr[i],arr[l]
	arr[high],arr[l+1] = arr[l+1],arr[high]
	return l+1

def qsort(arr,low,high):
	if low < high:
		partition_idx = get_partition_idx(arr,low,high)
		qsort(arr,low,partition_idx-1)
		qsort(arr,partition_idx+1,high)

if __name__ == "__main__":
	a = [-1,5,6,1,2,3,1,2]
	qsort(a,0,len(a)-1)
	assert(a == sorted(a))
