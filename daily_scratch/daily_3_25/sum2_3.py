

def add(a,b):
	carry = 0
	mask = 0xFFFFFFFF
	while b & mask != 0:
		carry = (a&b) << 1
		a = a^b
		b = carry
	if b > mask:
		return a & mask
	return a

print(add(-2,-2))
