
def merge_sort(arr):
	arrlen = len(arr)
	if arrlen > 1:
		midpoint = int(arrlen/2)
		left,right = arr[:midpoint], arr[midpoint:]	
		merge_sort(left)
		merge_sort(right)
		left_ptr,right_ptr,arr_ptr = 0,0,0
		while left_ptr < len(left) and right_ptr < len(right):
			if left[left_ptr] > right[right_ptr]:
				arr[arr_ptr] = right[right_ptr]
				right_ptr += 1
			else:
				arr[arr_ptr] = left[left_ptr]
				left_ptr += 1
			arr_ptr += 1

		while left_ptr < len(left):
			arr[arr_ptr] = left[left_ptr]
			left_ptr += 1
			arr_ptr += 1

		while right_ptr < len(right):
			arr[arr_ptr] = right[right_ptr]
			right_ptr += 1
			arr_ptr += 1
		print(arr)
	return arr

if __name__ == "__main__":
	a = [102,5,1,3,-4,2,1,5,3,2,1,3,5,7]
	a = merge_sort(a)
	print(a)
	assert(a == sorted(a))		
