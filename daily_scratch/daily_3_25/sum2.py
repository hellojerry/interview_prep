

def add(a,b):

	mask = 0xFFFFFFFF
	carry = 0
	while b != 0:
		carry = (a&b) << 1
		a = a ^ b
		b = carry
	if b > mask:
		return a & mask
	return a

print(add(1,2))
