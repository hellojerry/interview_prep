

def merge_sort(arr):
	arrlen = len(arr)
	if arrlen > 1:
		midpoint = int(arrlen/2)
		left,right = arr[:midpoint],arr[midpoint:]
		merge_sort(left)
		merge_sort(right)
		l,r,a = 0,0,0
		while l < len(left) and r < len(right):
			if left[l] > right[r]:
				arr[a] = right[r]
				r += 1
			else:
				arr[a] = left[l]
				l += 1
			a += 1
		while l < len(left):
			arr[a] = left[l]
			l += 1
			a += 1
		while r < len(left):
			arr[a] = left[r]
			r += 1
			a += 1
if __name__ == "__main__":
	a = [1,50,3,1,4,5,3,1,-3,-3,-3,22]
	merge_sort(a)
	assert(a == sorted(a))
