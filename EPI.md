# Elements of Programming Interviews

From now on, we're going to do a minimum of six hours of practicing a day.
With any luck, these questions are better written and explained than CTCI.

First, work through the "Scenario 1" problems. Do NOT stop until the problems for the day are done.

## Notes

Getting stronger on binary, but need to get a stronger grasp of bitmasks.

Remember that an AMPERSAND check returns the binary values where they match, at the most significant digit.

I.e.

15 (1111) & 1 (1) ==> 1
15 (1111) & 2 (10) ==> 2
14 (1110) & 1 (1) ==> 0 (no match)
14 (1110) & 2 (10) ==> 2
14 (1110) & 3 (11) ==> 2 (match at first digit, not at second)
14 (1110) & 5 (101) ==> 4 (100, match at first digit only)
14 (1110) & 7 (111) ==> 6 (110, match at first two digits)

To get the number of ones in a binary blob, do ampersand checks
while shifting right until zero.
```python
def get_ones(N):

	ct_1s = 0
	while N:
		ct_1s += N & 1
		N >>= 1
	return ct
```

- memorize ord() and chr() operations. Specifically:

```python
a = "1"
ord(a) - 48 == 1

chr(ord('0') + 3) == 3

```

- review min-heaps and max-heaps. Specifically,write flashcards on how to recognize the problems. Also, learn the heapq library. These questions are gimmes if you're aware of how they work. Additionally, learn how to implement a min-heap and max-heap.
- put binary search on flashcards and memorize (specifically, it's (left+right)//2, NOT right-left)
- memorize quicksort on flashcards. Useful for kth largest in array.
- review the problems for 16 and 17, specifically the two-dimensional array appproach.
- review the ord() and chr() stuff, specifically chapter 6.
- make flashcard for question 4.7. No idea how that works.
- make flashcard for question 5.12. I don't quite understand the question.
- do a onceover on ordereddict methods, it's useful for LRU cache.
- flashcard N-Queens


## Daily

1. write out merge sort
2. write out quicksort
3. write out eight queens
4. write out binary search.

Anything marked with "system design" or "multithreading" can be ignored.
Anything else marked "deferred" needs to be revisited.

## Friday 2/14

1. 4.1 - DONE
2. 5.1 - DONE
3. 5.6 - DONE
4. 6.1 - DONE
5. 7.1 - DONE
6. 8.1 - DONE
7. 9.1 - DONE
8. 10.1 - DONE
9. 11.1 - DONE
10. 12.2 - DONE
11. 13.1 - DONE
12. 14.1 - DONE
13. 15.1 - DONE
14. 16.1 - DONE
15. 17.4 - DONE
16. 18.1 - DONE
17. 19.3 (deferred - multithreading)
18. 20.13 (deferred - system design)

## Monday 2/17

1. 18.1 - DONE
2. review 17.4 and 16.1 - Deferred
3. 4.7 - DONE, review this again as i dont understand the question.
4. 5.12 - DONE
5. 5.18 - DONE
6. 6.2 - DONE
7. 6.4 - DONE
8. 7.2 - DONE, review again.
9. 7.3 - DONE
10. 8.6 - DONE

## Tuesday 2/18

11. 9.4 - DONE
12. 10.4 - DONE
13. 11.4 - DONE
14. 11.8 - DONE
15. 12.3 - DONE
16. 12.5 - DONE
17. 13.2 - DONE
18. 14.2 - DONE (review this)
19. 14.3
20. 15.2 - DONE
21. 16.2
22. 17.6
23. 18.7
24. 19.6 (deferred - multithreading)
25. 20.15 (deferred - system design)

## Wednesday 2/19

1. mock interview - DONE


## Thursday 2/20

1. daily memorization - DONE
2. 15.2 - DONE
3. 14.3 - DONE
4. 16.2 - DONE
5. 17.6 - DONE (review this, i dont see how this is a greedy algorithm)
6. 18.7 - DONE (go back to this at some point. I feel like i misread part of this)

## Friday 2/21

1. daily memorization - DONE
2. 4.8 - DONE
3. 5.2
4. 5.17 - DONE
5. 6.5 - DONE
6. 6.6
7. 7.4 - DONE
9. 8.2 - DONE
10. 8.7
11. 9.2
12. 9.12 - DONE
13. 10.3 - DONE
14. 11.3 - DONE
15. 11.9
16. 12.1 - DONE
17. 12.5
18. 13.6
19. 14.4 - DONE
20. 15.3 - DONE
21. 16.3 - DONE
22. 16.6
23. 17.5 - DONE
24. 18.2
25. 19.8 (deferred - multithreading)
26. 20.16 (deferred - system design)

## Saturday 2/22

1. daily memorization - DONE
2. 7.7 - DONE
3. 5.2 - DONE
4. 6.6 - DONE
5. 8.7 - DONE
6. 9.2 - DONE
7. 11.9 (find missing IP). skipped.
8. 12.5 - DONE
9. 13.6 - DONE (calendar)
10. 16.6 - DONE (knapsack - hard)
11. 18.2 - DONE
12. 4.3 - DONE
13. 8.3 - DONE
14. 6.11 - DONE

## Sunday

1. daily memorization - DONE. need to get an entire day of bit manipulation practice in. This might be where i'm weakest.
2. 4.11 - DONE. might need to review. had a lot of trouble.
3. 5.5 - DONE
9. 9.11 - DONE (Morris Traversal - add this to daily practice)
12. 11.10 - DONE. Review the advanced solution to this. I don't quite understand it.

## Wednesday 2/26

Revision: only do 5 review questions every day. They're eating up too much time.

1. daily memorization - DONE
2. 5.9 - DONE
3. 6.7 - DONE
4. 7.10 - DONE
5. 8.8 - DONE
6. 10.5 - DONE
7. 11.5 - DONE (review this.. very strange)
8. 12.4 - DONE
9. 13.8 - DONE
10. 14.5 - DONE
11. 15.4 - DONE
20. 19.9 - deferred (multithreading)
21. 20.1 - deferred (system design)

## Thursday - 2/27

NOTE: skip all linked list problems. We're good to go on those.
NOTE: Primary stuff is dynamic programming, recursion, greedy algorithms,
bit manipulation, matrix problems. Everything else we're fairly strong on.
Continue doing the tree problems as well. Can skip the string fuckery stuff too.
Permutation questions are good practice for recursion and DP. Can skip basic array
questions and most sorts and searches.

1. 6.8 - DONE
2. 12.6 - DONE
3. 13.11 - DONE
4. 14.8 - DONE
5. 15.9 - DONE
6. 16.5 - DONE
7. 16.7 - DONE (do this again).
8. 17.7 - DONE
9. 18.3 - DONE
10. 4.9 - DONE

## Friday - 2/28

Cancel the practice interview. Reschedule it to friday of next week.
We might as well knock out the rest of the practice materials
before going further.

1. 5.3 - DONE
2. 5.15 - DONE (This is dumb)
3. 6.9 - DONE
4. 7.11 - DONE
5. 8.4 - DONE
6. 9.13 - DONE
7. 10.6 - DONE
8. 12.9 - DONE
9. 13.9 - DONE
10. 11.7 - DONE
21. 20.9 - deferred (system design)
22. 20.2 - deferred (system design)

## Saturday -  2/29

1. 5.10 - DONE
2. 9.16 - DONE
3. 15.10 - DONE (review this)
4. 16.12 - DONE (review this)
5. 17.8 - DONE
6. 18.5 - DONE
7. 11.6 - DONE
8. 15.6 - DONE
9. 14.7 (no idea what the fuck this is actually asking) skipped
10. work through 10 review problems

## Secondary EPI


### ch4
1. 4.1
2. 4.2
3. 4.3
4. 4.4
5. 4.5
6. 4.6
7. 4.7
8. 4.8
9. 4.9
10. 4.11
11. 4.11

### ch15
1. 15.1
2. 15.2
3. 15.3
4. 15.4
5. 15.5
6. 15.6
7. 15.7
8. 15.8
9. 15.9
10. 15.10

### ch16

1. 16.1
2. 16.2
3. 16.3
4. 16.4
5. 16.5
6. 16.6
7. 16.7
8. 16.8
9. 16.9
10. 16.10
11. 16.11
12. 16.12

### ch17

1. 17.1
2. 17.2
3. 17.3
4. 17.4
5. 17.5
6. 17.6
7. 17.7
8. 17.8

## Sunday 3/1

1. review questions 1-15
2. 4.1 - DONE
3. 4.2 - DONE
4. 4.3 - DONE

## Tuesday 3/3

1. eight review questions 
2. 15.1 - DONE
3. 15.2 - DONE
4. 15.3 - DONE

## Wednesday 3/4
1. 16.1 - DONE (add this to review bucket - it's a good question
2. 16.2 - DONE
3. 16.3 - DONE
4. 17.1 - DONE
5. 17.2 - DONE
6. 17.3 - DONE
7. 4.4 - DONE
8. 4.5 - DONE
9. 4.6 - DONE (add all of these to review section)
10. 15.4 - DONE
11. 15.6 - DONE

## Thursday 3/5

It is very clear that the weak point is dynamic programming and bit twiddling.
Recursion and greedy algorithms need more practice, but I should be fine
by the time I finish the chapters up. Bit twiddling will need flashcards.
Dynamic programming is going to be a bit trickier.

1. 15.5 - DONE
2. 16.4 - DONE
3. 16.5 - DONE
4. 16.6 - DONE
5. 17.4 - DONE
6. 17.5 - DONE
7. 17.6 - DONE (going to need to memorize this)
8. 4.7 - DONE
9. 4.8 - DONE
10. 4.9 - DONE
11. 15.9 - DONE

## Friday 3.6

1. 15.7 - DONE
2. 15.8 - DONE (this question is dumb)
3. 16.8 - DONE
4. 17.7 - DONE
5. 17.8 - DONE
6. 16.9 - DONE
7. 16.10 - DONE
8. 4.10 - skipped. fuck random number questions
9. 4.11 - DONE (add this to review)

## Sunday 3/8

1. 15.10 - DONE (i really need to cram bit twiddling with flashcards)
2. 16.7 - DONE
3. 16.11 - DONE (this needs to me memorized)


## Monday 3/9

1. 16.12 - DONE
2. bit twiddling flashcards:
	- update bit
	- set bit
	- (other operations from CTCI)
	- possibly best to make an object of some sort that implements binary operations
		and allows for inspection
3. See LC.md. Knock out 14 leetcode problems.


## Tuesday 3/10

1. leetcode 75, 26-50

## Thursday 3/12

1. leetcode 75, 51-75

## Monday 3/15

1. final round of review questions

## Tuesday 3/16

1. practice interview


## After book completion

Identify unsolved dynamic programming, recursion, and greedy algorithm problems.
Finish those. Once those are done, work through the leetcode 75.
Once the leetcode 75 are complete, start working on the "honors" questions.
If we can solve the "honors" questions, we're more than ready for anything.

1. work through 10 review problems to warm up.
2. 10 of the leetcode 75 (one from each category)
3. all 30 recursive, dynamic, and greedy problems in EPI.
4. flashcards for binaries. 
5. All questions from ch4 (11 in total)

Remaining questions before ready for practice interview:

116




## Procedure

work through 10 problems a day. We should be done with the study plan by wednesday of next week.
From there, work through 10 leetcode mediums a day, focusing on the following:

	- dynamic programming
	- recursion
	- trees
	- bit manipulation
	- matrix problems.

strong areas:

	- linked lists
	- strings
	- stacks
	- heaps
	- searches
	- sorts
	- hashmaps

middling:
	- trees
	- bsts
	- graphs
	- dynamic programming (having brain farts on some specific ones involving strings)

weak:
	- bit manipulation
	- matrix problems

Bit manipulation core operations:
	- check bit
	- set bit
	- toggle bit
	- clear bit
	- clear bit range
	- toggle bit range

Specific tree stuff:
	- least common ancestor in btree
	- is_bst
	- is_symmetric
	- is_height_balanced

Specific math stuff:
	- relationship between square roots and bsearch
	- reversing digits
	- getting list of prime numbers up to number

Once finished with the study guide, finish all greedy, dynamic, bit manipulation, and matrix problems
in the book. Then, work on leetcode versions of them. leetcode will have more matrix problems as well.

System design will be studied separately, after getting strong on algorithms.
Be sure to memorize the max heap methods too.

Before being ready for interviews, go through the leetcode 75 as well.

We can skip all linked list problems. Be on the lookout for searching problems hidden as math problems.

Getting stronger on DP, recursion, and graph stuff.
Going to need to flashcard the bit manipulation shit.
Once done with book, work through all greedy, dynamic programming, bit bullshit, and recursion based problems.
Then, do the hard ones.

leetcode 75:
https://www.teamblind.com/post/New-Year-Gift---Curated-List-of-Top-75-LeetCode-Questions-to-Save-Your-Time-OaM1orEU

Before next mock interview:
	1) all dynamic programming questions - DONE
	2) all bit bullshit
	3) all of the firecode or leetcode matrix questions
	4) all of the leetcode 75
	5) all of the greedy algorithm questions - DONE
	6) all of the recursion questions - DONE
	7) do the review questions enumerated in the daily practice directory.
	8) work on the stupid randomization questions. These don't seem to show up in leetcode so it seems pointless.
	9) check leetcode and see how often they have heap questions. These seem like oddballs. memorize the heapq methods
		and flashcard what heaps do.


1. MEMORIZE the coin change problem using the dynamic programming solution:
	- matrix of [1] + [0]X amount for i in coins
1. MEMORIZE the bit twiddling operations from CTCI. 
2. add, subtract, multiply, divide, exponent in bitwise operators.
