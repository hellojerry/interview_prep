


def directed_combinations(offset, partial_combination,N,K,result):
	if len(partial_combination) == K:
		result.append(list(partial_combination))
		return
	num_remaining = K - len(partial_combination)
	i = offset
	while i <= N and num_remaining <= N - i + 1:
		directed_combinations(i+1, partial_combination + [i],N,K,result)
		i += 1

def combinations(N,K):
	result = []
	directed_combinations(1,[],N,K,result)
	return result

if __name__ == "__main__":
	print(combinations(5,2))
