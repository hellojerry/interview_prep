"""
You are reading a sequence of strings. A priori you know
that more than half the strings are repetitions of a single 
string, but the positions where the majority element occurs
are unknown. Write a program that identifies the majority
element in a single pass.

ex: [b,a,c,a,a,b,a,a,c,a] => a

ex [b,a,b,b,a,b,a,a,a,a] => a

"""


def find_majority_element(arr):
	"""
	So I think what we want to do here
	is retain two counters -> current max, and next highest.
	we start at index 1 and max things greedily
	"""
	arrlen = len(arr)
	if arrlen == 0:
		return None
	if arrlen <= 2:
		return arr[0]
	current_max,current_second = arr[0],None
	current_max_ct,current_second_ct = 1,0
	for el in arr[1:]:
		if el == current_max:
			current_max_ct += 1
			continue
		elif current_second is None:
			current_second_ct = 1
			current_second = el
		elif el == current_second:
			current_second_ct += 1
		if current_second_ct > current_max_ct:
			current_max,current_second = current_second,current_max
			current_max_ct,current_second_ct = current_second_ct,current_max_ct
	return current_max
if __name__ == "__main__":
	
	assert(find_majority_element(["b","a","c","a","a","b","a","a","c","a"]) == "a")
	assert(find_majority_element(["b","a","b","b","a","b","a","a","a","a"]) == "a")
	assert(find_majority_element(["a","a","a","b","b","b","b","c","b","b"]) == "b")
