"""

ex:

grid = [
	[1,2,3],
	[3,4,5],
	[5,6,7]
]

pattern: [1,3,4,6] occurs
pattern: [1,2,3,4] does not occur.

Write a program that takes a 2d array and a 1d array,
and checks whether the 1d array occurs in the 2d array.
legal moves are up, down, left, right.
It is acceptable to visit an entry in the grid more than once.
"""


def recursive_check(matrix,pattern,row,col):
	"""
	One way I can do this is by checking the row,col
	pair to see if legal (if not, return false)
	then recursively calling the function with the first element eliminated
	until it's empty
	"""
	pattern_len = len(pattern)
	if pattern_len == 0:
		return True
	rows = len(matrix)
	cols = len(matrix[0])
	row_max,col_max = rows - 1, cols - 1
	if matrix[row][col] != pattern[0]:
		return False
	up_eligible = row > 0
	down_eligible = row < row_max
	left_eligible = col > 0
	right_eligible = col < col_max
	if up_eligible:
		if recursive_check(matrix,pattern[1:],row-1,col):
			return True
	if down_eligible:
		if recursive_check(matrix,pattern[1:],row+1,col):
			return True
	if left_eligible:
		if recursive_check(matrix,pattern[1:],row,col-1):
			return True
	if right_eligible:
		if recursive_check(matrix,pattern[1:],row,col+1):
			return True

	return False

def exists_in_2d_array(matrix,pattern):
	rows = len(matrix)
	cols = len(matrix[0])
	for row in range(rows):
		for col in range(cols):
			if recursive_check(matrix,pattern,row,col):
				return True
	return False
if __name__ == "__main__":

	grid = [
		[1,2,3],
		[3,4,5],
		[5,6,7]
	]
	assert(exists_in_2d_array(grid,[1,3,4,6]) == True)
	assert(exists_in_2d_array(grid, [1,2,3,4]) == False)
