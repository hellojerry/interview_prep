"""
Solve the knapsack problem.

"""


def knapsack(matrix, item_idx,item_arr,remaining_weight):
	"""
	We want a matrix of unchecked values.
	From there, we do a check with and without the values.
	The final result is returning the matrix at arr_idx[remaining_weight]
	"""
	if item_idx < 0:
		return 0

	if matrix[item_idx][remaining_weight] == -1:
		without_item = knapsack(matrix,item_idx-1,item_arr,remaining_weight)
		with_item = 0
		item_value = item_arr[item_idx][0]
		item_weight = item_arr[item_idx][1]
		if item_weight <= remaining_weight:
			with_item = item_value + knapsack(matrix,item_idx,item_arr,remaining_weight-item_weight)


		matrix[item_idx][remaining_weight] = max(with_item,without_item)
	return matrix[item_idx][remaining_weight]

def kickoff(item_arr,remaining_weight):
	matrix = [[-1]*(remaining_weight+1) for item in item_arr]
	return knapsack(matrix,len(item_arr)-1,item_arr,remaining_weight)

if __name__ == "__main__":
	item_arr = [(60,5),(50,3),(70,4),(30,2)]
	remaining_weight = 5
	res = kickoff(item_arr,remaining_weight)
	print(res)
	res = kickoff(item_arr,10)
	print(res)
