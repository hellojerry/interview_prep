"""
reverse the digits of an integer.

42 -> 24
-314 -> -413
201 -> 102
"""

def reverse_digits(num):
	"""
	I think we can accomplish this with a divmod.
	if the least significant digit is a 2, we want to lead
	with that, so... every time we loop, we want to multiply the
	reverse by 10.
	"""
	if abs(num) <= 9:
		return num
	is_negative = num < 0
	reverse, remaining = 0, abs(num)
	while remaining:
		reverse *= 10
		remaining,remainder = divmod(remaining,10)
		reverse += remainder
	if is_negative:
		return -reverse
	return reverse
if __name__ == "__main__":
	assert(reverse_digits(42) == 24)
	assert(reverse_digits(-314) == -413)
	assert(reverse_digits(9) == 9)
	assert(reverse_digits(101) == 101)
