"""

gasup problem

A number of cities are arranged on a circular road.
You need to visit all of the cities and come back 
to the starting city. A certain amount of gas is available
at each city. The amount of gas summed up over all
cities is equal to the amount of gas required to go around
the road one. The gas tank has unlimited capacity.
a city is "ample" if you can begin at the city with an empty tank,
refill it, and then travel through all the cities, refilling
at each city, and return to the city without running out of gas.


				C(5)
                          200M
                       D(30)           600M
                    400M
                   E(25)                 B(20)
                    600M                900M
                      F(10)           A(50)
                         200M      100M
                             G(10)
D in this example is an ample city
"""

class City(object):
	def __init__(self,name,gallons,dist_next,next=None):
		assert(type(name) == str and len(name) > 0)
		for i in [gallons,dist_next]:
			assert(type(i) == int and i > 0)
		assert(next is None or type(next) == City)
		self.name = name
		self.gallons = gallons
		self.dist_next = dist_next
		self.next = next
	def __str__(self):
		return "City(%s, gallons:%d, dist_next:%d)" % (
			self.name,self.gallons,self.dist_next)
	def __repr__(self):
		return "City(%s, gallons:%d, dist_next:%d)" % (
			self.name,self.gallons,self.dist_next)

def find_ample_city(start):
	"""
	We get 20MPG
	"""

	gallons_remaining = start.gallons
	cur = start
	kickoff = True
	while gallons_remaining >= 0:
		if not kickoff and cur is start:
			return True
		elif kickoff and cur is start:
			gallons_remaining -= int(cur.dist_next/20)
			cur = cur.next
			kickoff = False
		else:
			gallons_remaining += cur.gallons
			gallons_remaining -= int(cur.dist_next/20)
			cur = cur.next
	return False
from collections import namedtuple
def ample_city(gallons,distances):
	assert(len(gallons) == len(distances))
	remaining_gallons = 0
	CityAndRemainingGas = namedtuple("CityAndRemainingGas",
				("city","remaining_gallons"))
	city_remaining_gallons_pair = CityAndRemainingGas(0,0)
	num_cities = len(gallons)
	for i in range(1,num_cities):
		remaining_gallons += gallons[i-1] - distances[i-1]//20
		if remaining_gallons < city_remaining_gallons_pair.remaining_gallons:
			city_remaining_gallons_pair = CityAndRemainingGas(i,remaining_gallons)

	return city_remaining_gallons_pair.city

def ample_city(gallons,distances):
	"""
	Here's how this works:
		we add the gallons of the prior city minus
		the gallons consumed getting to the current city.
		if the remaining gallons is less than
		the currently chosen city/remaining pair,
		we set the new city/remaining pair to the
		current city and remaining.

	The general idea with this is that whatever city has the very least
	amount of gas inbound is going to be the one that is the ample one.
	I.e. if we find the location with the least amount of gas, that one is our
	best one.
			
	"""
	num_cities = len(gallons)
	temp = [0,0]
	remaining_gallons = 0
	for city in range(1,num_cities):
		print(locals())
		remaining_gallons += gallons[city-1] - (
			int(distances[city-1]/20))
		if remaining_gallons < temp[1]:
			temp = [city,remaining_gallons]
	return temp[0]

if __name__ == "__main__":
	G = City("G",10,100)
	F = City("F",10,200,G)
	E = City("E",25,600,F)
	D = City("D",30,400,E)
	C = City("C",5,200,D)
	B = City("B",20,600,C)
	A = City("A",50,900,B)
	G.next = A
	city_arr = [A,B,C,D,E,F,G]
	for c in city_arr:
		print(find_ample_city(c), c)
	
	a = [50,20,5,30,25,10,10]
	b = [900,600,200,400,600,200,100]
	print(ample_city(a,b) == 3)
