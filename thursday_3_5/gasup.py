


def gasup(gallons,distances,MPG):

	temp_min_pair = [0,0]
	remaining_gallons = 0
	for i in range(1, len(gallons)):
		"""
		We want to see where the minimum is and return it
		"""
		remaining_gallons += (gallons[i-1] - int(distances[i-1]/MPG))
		if remaining_gallons < temp_min_pair[1]:
			temp_min_pair = [i,remaining_gallons]

	return temp_min_pair[0]

if __name__ == "__main__":
	a = [50,20,5,30,25,10,10]
	b = [900,600,200,400,600,200,100]
	print(gasup(a,b,20) == 3)
