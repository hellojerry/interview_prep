"""
Check if an integer is a palindrome

414 -> true
505 -> true
413 -> false
-313 -> true
"""


def is_palindrome_dumb(num):
	if num < 0:
		return int("-" + str(num)[::-1])
	return int(str(num)[::-1])

def is_palindrome(num):
	"""
	We could just reverse the digits and see
	if it matches the original
	"""
	num_as_abs = abs(num)
	if num_as_abs <= 9:
		return True
	is_neg = num < 0
	reverse, remaining = 0, num_as_abs
	while remaining:
		reverse *= 10
		remaining,remainder = divmod(remaining, 10)
		reverse += remainder
	if is_neg:
		return -reverse == num
	return reverse == num

if __name__ == "__main__":
	assert(is_palindrome(-313) == True)
	assert(is_palindrome(2000002) == True)
	assert(is_palindrome(200001) == False)
