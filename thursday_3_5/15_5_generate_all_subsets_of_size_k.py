"""
There are a number of testing applications in which it is required to
compute all subsets of a given size for a specified set.
Write a program which computes all K sized subsets
of 1,2..N where K and N are inputs.

Ex:

N=5,K=2: 

res = {1,2},{1,3},{1,4},{1,5},{2,3},{2,4},{2,5},{3,4},{3,5}{4,5}

N=5,K=3

res = {1,2,3},{1,2,4},{1,2,5}
      {1,3,4},{1,3,5}
      {1,4,5}
      {2,3,4},{2,3,5},{2,4,5}
      {3,4,5}

Hint: Think of the right function signature.
"""


def recurse(value, acc):
	if len(acc) == 0:
		acc.append(set())
		acc.append({value})
		return acc
	else:
		copied = acc.copy()
		for sub in copied:
			el = sub.copy()
			el.add(value)
			acc.append(el)
		return acc

def gen_subsets(N,K):
	"""
	So if this were a power set, I'd
	do a recursive function. I think
	we can do a recursive function here as well.
	What if we did something like:
	value :tail. 
	Let's get the range first and start with that.
	We can worry about K after getting that handled.
	That won't work.
	let's generate the powersets, and then just return
	the powersets that are of length K
	"""
	acc = []
	for el in range(1,N+1):
		acc = recurse(el,acc)
	return sorted((tuple(k) for k in acc if len(k) == K),
			key=lambda x: x[0])

if __name__ == "__main__":
	expected = [(1,2),(1,3),(1,4),(1,5),(2,3),(2,4),(2,5),(3,4),(3,5),(4,5)]
	assert(gen_subsets(5,2) == expected)
	print(gen_subsets(5,3))
