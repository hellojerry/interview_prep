"""
Write a program that takes a double X and an integer Y and 
returns x**y

Hint: exploit mathematical properties of exponentiation

Note that you can't use the exponent operator.
"""


def pow1(x,y):
	"""
	So the first thing I'd try is just using a counter
	"""
	ct = 1
	out = x
	while ct < y:
		out *= x
		ct += 1
	return out

def pow2(x,y):
	"""
	if the least significant bit of y is 1, that means
	the power is odd, and we need to multiply x by
	the power before squaring x and shifting power
	rightwards by one
	if it's even, we just square x and shift power rightwards
	"""
	result, power = 1.0, y
	if y < 0:
		power, x = -power, 1.0/x
	while power:
		if power & 1:
			result *= x
		x, power = x*x, power >> 1
	return result
	

if __name__ == "__main__":
	print(pow1(1.21,4))
