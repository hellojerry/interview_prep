"""
Design an algorithm that takes as input an array
and a number and determines if there are three entries
which add up to the specified number.

ex: [11,2,5,7,3], 21 -> True ([3,7,11],[5,5,11])
(note that each value can be used more than once)

The trick with this is solving the smaller problem first,
which is the two-sum problem.
"""

def has_two_sum(arr,target):
	arrlen = len(arr)
	left_ptr,right_ptr = 0, arrlen - 1
	while left_ptr <= right_ptr:
		left_val,right_val = arr[left_ptr],arr[right_ptr]
		if left_val + right_val == target:
			return True
		elif left_val +right_val < target:
			left_ptr += 1
		else:
			right_ptr -= 1
	return False


def three_sums(arr,target):
	"""
	So this is a greedy algorithm...
	I could stuff the numbers into a set,
	then for each value, i... nah
	Let's make a set,
	"""
	as_set = set(arr)
	arr.sort()
	for ind, val in enumerate(arr):
		test = target - val
		print(test)
		if has_two_sum(arr,test):
			return True
	return False

if __name__ == "__main__":
	assert(three_sums([11,2,5,7,3],21) == True)
	assert(three_sums([11,2,5,7,3],22) == False)
