"""
The symbol (N over K) is the short form for

n(n-1)...(n-k+1)
----------------
k(k-1)...(3)(2)(1)

defining n over K always yields an integer.

Another way to write this is


n factorial until N-k+1 / k!

so if i did (N over K) as (10 over 3),
that would be

	10*9*8*7*6
	--------------
	3*2*1


if (15 over 8), then 15 factorial until 6 / 8 factorial

	15*14*13*12*11*10*9*8*7*6
	---------------
	8*7*6*5*4*3*2*1

so... ok. This is quite odd lol. 


if (9 over 8) 
	9*8*7*6*5*4*3*2*1*0
	----------------
	8*7*6*5*4*3*2*1

if (15 over 2)

	15*14*13*12
	--------
	2*1
"""

def binomial_coefficient(N,K):
	"""
	What we need to do here is get the range from 2 to K inclusive,
	and then from K+1 to N inclusive. We then want to divide N
	by K (if possible) and do out *= N/K
	This is in the dynamic programming chapter, so I imagine the proper
	solution involves a matrix of some sort. 
	if I went with two pointers, I take the lowest N factor, 
	try to divide it against highest K factor, if that doesm't work,
	multiply by next lowest N, rinse and repeat until I can divide the accumulated
	outbound by the highest K factor, do that, then move to next lowest K factor,
	rinse and repeat
	"""
	if N -1 == K:
		return 0
	## so something like
	K_values = range(1,K+1)
	N_min_factor = N - (K+1)
	N_values = range(N_min_factor,N+1)
	left_N_ptr = 0
	right_K_ptr = len(K_values) - 1
	K_values_len,N_values_len = len(K_values), len(N_values)
	out = 1
	while left_N_ptr < N_values_len and right_K_ptr >= 0:
		N_value = N_values[left_N_ptr]
		K_value = K_values[right_K_ptr]
		if out % K_value == 0:
			out = int(out/K_value)
			right_K_ptr -= 1
		elif N_value % K_value == 0:
			out *= int(N_value / K_value)
			left_N_ptr += 1
			right_K_ptr -= 1
		else:
			out *= N_value
			left_N_ptr += 1
		print(out)
	while left_N_ptr < N_values_len:
		N_value = N_values[left_N_ptr]
		out *= N_value
		left_N_ptr += 1
	return out
		
if __name__ == "__main__":
	res = binomial_coefficient(15,8)
	print(res == 270270)
	r2 = binomial_coefficient(10,3)
	print(r2 == 5040)
	r3 = binomial_coefficient(15,2)
	print(r3 == 16380)
	r4 = binomial_coefficient(1000000000,5)
	print(r4)
