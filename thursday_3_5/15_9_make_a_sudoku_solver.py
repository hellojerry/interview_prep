"""

two parts with this:
	1. validity test
	2. iterator
"""


def is_valid(matrix,row,col,target_value):
	rows = len(matrix)
	cols = len(matrix[0])
	available_values = set(range(1,10))
	## first step: remove cols in current row for value
	for candidate_col in range(cols):
		val = matrix[row][candidate_col]
		available_values -= {val}
	if target_value not in available_values:
		return False
	## second step: remove rows in current col for value
	for candidate_row in range(rows):
		val = matrix[candidate_row][col]
		available_values -= {val}
	if target_value not in available_values:
		return False
	row_bounds,col_bounds = [0,0],[0,0]
	if 0 <= row < 3:
		row_bounds = (0,3)
	elif 3 <= row < 6:
		row_bounds = (3,6)
	else:
		row_bounds = (6,9)
	if 0 <= col < 3:
		col_bounds = (0,3)
	elif 3 <= col < 6:
		col_bounds = (3,6)
	else:
		col_bounds = (6,9)
	for candidate_row in range(row_bounds[0],row_bounds[1]):
		for candidate_col in range(col_bounds[0],col_bounds[1]):
			val = matrix[candidate_row][candidate_col]
			available_values -= {val}
	if target_value not in available_values:
		return False
	return True

def sudoku_solver(matrix):
	rows = len(matrix)
	cols = len(matrix)
	nonzero_ct = 0
	for row in range(rows):
		for col in range(cols):
			if matrix[row][col] == 0:
				for i in range(1,10):
					if is_valid(matrix,row,col,i):
						matrix[row][col] = i
						if sudoku_solver(matrix):
							return True
						matrix[row][col] = 0
				return False
			else:

				nonzero_ct += 1
	return nonzero_ct == 81



	
if __name__ == "__main__":
	arr_legal = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	res = sudoku_solver(arr_legal)
	assert(res == True)
	for row in arr_legal:
		print(row)
	expected = [ 
		[5, 3, 4, 6, 7, 8, 9, 1, 2],
		[6, 7, 2, 1, 9, 5, 3, 4, 8],
		[1, 9, 8, 3, 4, 2, 5, 6, 7],
		[8, 5, 9, 7, 6, 1, 4, 2, 3],
		[4, 2, 6, 8, 5, 3, 7, 9, 1],
		[7, 1, 3, 9, 2, 4, 8, 5, 6],
		[9, 6, 1, 5, 3, 7, 2, 8, 4],
		[2, 8, 7, 4, 1, 9, 6, 3, 5],
		[3, 4, 5, 2, 8, 6, 1, 7, 9]
	]
	assert(arr_legal == expected)
