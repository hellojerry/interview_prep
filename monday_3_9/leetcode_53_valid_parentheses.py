"""
Check if the parentheses are valid.
"""

def is_valid(my_str):
	mappings = {
		")":"(",
		"]":"[",
		"}":"{",
	}
	stack = []
	for item in my_str:
		if item not in mappings:
			stack.append(item)
		else:
			slen = len(stack)
			if slen == 0:
				return False
			mapping = mappings[item]
			if stack[-1] == mapping:
				stack.pop()
			else:
				return False
	return len(stack) == 0

if __name__ == "__main__":
	a1 = "()"
	a2 = "()[]{}"
	a3 = "(]"
	a4 = "([)]"
	a5 = "{[]}"
	assert(is_valid(a1) == True)
	assert(is_valid(a2) == True)
	assert(is_valid(a3) == False)
	assert(is_valid(a4) == False)
	assert(is_valid(a5) == True)
