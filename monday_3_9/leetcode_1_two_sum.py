"""
Given an array of integers,
return indices of two numbers
such that they add up to a specific target.
You may assume that each input would have
exactly one solution, and you may not use the
same element twice.

ex:
	[2,7,11,15], target=9

res = [0,1]
"""

def two_sum(arr,target):
	"""
	Can't sort it without too much complexity.
	If I stuff things into a dictionary and iterate,
	I think I can get a result
	"""
	mappings = {}
	for ind,val in enumerate(arr):
		required = target - val
		exists = mappings.get(required,None)
		if exists is not None:
			return [ind,exists]
		mappings[val] = ind

if __name__ == "__main__":
	arr = [2,7,11,15]
	target = 9
	print(two_sum(arr,target))
	assert(two_sum(arr,target) == [1,0])
