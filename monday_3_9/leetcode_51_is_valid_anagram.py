"""
Test if two strings are anagrams of one another
"""

def is_anagram(s,t):
	return sorted(s) == sorted(t)
