"""
given an integer array nums,
find the maximum contiguous subarray
and return its sum.

ex: [-2,1,-3,4,-1,2,1,-5,4]
res: 6

(maximum subarray is [4,-1,2,1]
"""

def get_max_subarray(arr):
	"""
	No idea why I'm not seeing
	an immediate way of doing this.

	

	"""
	max_so_far = float("-inf")
	max_ending_here = 0
	for item in arr:
		max_ending_here = max_ending_here + item
		if max_so_far < max_ending_here:
			max_so_far = max_ending_here
		if max_ending_here < 0:
			max_ending_here = 0
	return max_so_far
		

if __name__ == "__main__":
	arr = [-2,1,-3,4,-1,2,1,-5,4]
	res = get_max_subarray(arr)
	print(res)
	arr = [-7,-6,-5,-4,-3]
	print(get_max_subarray(arr))
