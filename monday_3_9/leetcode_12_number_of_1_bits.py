"""
count the number of 1 bits in a number
"""


def count_1_bits(num):

	num_1s = 0
	while num:
		if num & 1:
			num_1s += 1
		num >>= 1
	return num_1s

if __name__ == "__main__":
	print(bin(3))
	print(count_1_bits(3) == 2)

