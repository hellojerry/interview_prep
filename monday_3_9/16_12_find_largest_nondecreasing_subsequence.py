"""
The problem of finding the longest nondecreasing subsequence in
a sequence of integers has implications to all sorts of stuff,
like string matching. As a concrete instance, the
length of

a = [0,8,4,12,2,10,6,14,1,9]

is 4 ([0,4,10,14], and also [0,2,6,9])

note that elements of the nondecreasing subsequence are
not required to immediately follow one another.

Write a program that takes as input an array of numbers
and returns the length of the longest nondecreasing subsequence
in an array.

Hint: express the longest nondecreasing subsequence ending at an entry
in terms of the longest nondecreasing subsequence appearing in the subarray
consisting of preceding elements.
"""

def longest_subsequence(arr,acc):
	"""
	This is a dynamic programming problem,
	so we either need to go the matrix route,
	or have a continuously decreasing array,
	with possibly some sort of backtracking.
	My thought here is to iterate through the array,
	and append things to a slice recursively, returning
	when we've hit the tail.
	"""
	arrlen = len(arr)
	acc_len = len(acc)
	if arrlen == 0:
		return acc_len
	head = arr[0]
	if acc_len > 0:
		if head >= acc[-1]:
			acc.append(head)
		return longest_subsequence(arr[1:],acc)
	else:
		max_len = 1
		for i in range(arrlen):
			el = arr[i]
			new_acc = [el]
			max_len = max(max_len, longest_subsequence(arr[i+1:],new_acc))
		return max_len

def longest_subsequence_arr(arr,acc):	
	arrlen = len(arr)
	acc_len = len(acc)
	if arrlen == 0:
		return acc
	head = arr[0]
	if acc_len > 0:
		if head >= acc[-1]:
			acc.append(head)
		return longest_subsequence_arr(arr[1:],acc)
	else:
		max_len = 1
		res = [head]
		for i in range(arrlen):
			el = arr[i]
			new_acc = [el]
			temp_res = longest_subsequence_arr(
					arr[i+1:],new_acc)
			if len(temp_res) >= max_len:
				max_len = len(temp_res)
				res = temp_res 
		return res

if __name__ == "__main__":
	arr = [0,8,4,12,2,10,6,14,1,9]
	acc = []
	res = longest_subsequence(arr,acc)
	assert(res == 4)
	
	arr = [0,8,4,12,2,10,6,14,1,9]
	acc = []
	res = longest_subsequence_arr(arr,acc)
	assert(res == [0,8,12,14])

	arr = [10,9,8,7,6,5,4,3,2,1]
	acc = []
	res = longest_subsequence(arr,acc)
	assert(res == 1)
