"""
Check if a string is a valid palindrome,
only including alphanumeric characters.
Treat all letters as lowercase.
"""

def is_palindrome(my_str):
	sanitized = ""
	for i in my_str:
		if i.isalpha() or i.isdigit():
			sanitized += i
	return sanitized == sanitized[::-1]
