

def is_bst(root,minimum,maximum):
	if not root:
		return True
	if root.val <= minimum or root.val >= maximum:
		return False
	return is_bst(root.left,
		minimum,root.val) and is_bst(root.right,
				root.val,maximum)

if __name__ == "__main__":
	minimum,maximum = float("-inf"),float("inf")
	return is_bst(root,minimum,maximum)
