"""
Suppose you have an array for which 
the ith element is the price of a given stock
on day i.
If you can only buy and sell once,
make an algorithm to find the maximumm profit.

ex:
arr = [7,1,5,3,6,4]
res = 5
(buy at one, sell at six)
"""

def max_profit(arr):
	"""
	this is a greedy algorithm of sorts
	we want to find the minimum preceding the maximum.

	if val > current_top:
		current_top = val
		current_max = current_top - current_bottom
	if val < current_min:
		current_top = val
		current_min = val
	return current_max
	"""
	current_max,current_top,current_min = 0,None,None
	for val in arr:
		if current_min is None:
			current_top = val 
			current_min = val 
			continue
		if val > current_top:
			current_top = val
			current_max = max(current_max,current_top-current_min)
		elif val < current_min:
			current_min = val
			current_top = val
	return current_max
if __name__ == "__main__":
	arr = [3,2,6,5,0,3]
	assert(max_profit(arr) == 4)
