
class Solution(object):

	def __init__(self):
		self.current_max = None

	def get_max_path(self,root):
	
	
		if not root:
			return 0
		lhs = self.get_max_path(root.left)
		rhs = self.get_max_path(root.right)
		max_single = max(max(lhs,rhs)+root.val, root.val)
		max_top = max(max_single,lhs+rhs+root.val)
		if self.current_max is None:
			self.current_max = max_top
		else:
			self.current_max = max(max_top,self.current_max)
		return max_single
	def max_path_sum(self,root):
		self.get_max_path(root)
		return self.current_max

