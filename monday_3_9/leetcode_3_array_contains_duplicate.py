"""
Return true if an array contains a duplicate
"""

def contains_dupe(arr):
	return len(arr) > len(set(arr))

if __name__ == "__main__":
	arr = [1,2,3,1]
	assert(contains_dupe(arr) == True)
	arr = [1,2,3]
	assert(contains_dupe(arr) == False)
