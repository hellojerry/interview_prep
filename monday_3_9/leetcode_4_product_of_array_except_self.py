"""
Given an array num of N integers where
n > 1, return an array such that
output[i] equals the product of
all other elements in the array.

ex: [1,2,3,4] -> [24,12,8,6]

Can't use division, needs to be o(1)
"""

def array_product(arr):
	"""
	So my thought here is we take the tail,
	make the product, memoize current,
	then divide next by i and multiply by memo.
	Somehow I can't use division though.

	Other thing to do here would be array slicing,
	I think that's greater than o(n) however.
	Only other thing I can think of is some sort
	of thing where we have a sliding window of extra space
	"""
	window = []
	res = []
	prior = None
	for ind,val in enumerate(arr):
		if ind == 0:
			window = arr[ind+1:]
			out = val
			for item in window:
				out *= item
			res.append(out)
			prior = val
			continue
		window.pop(0)
		window.append(prior)
		out = 1
		for item in window:
			out *= item
		res.append(out)
		prior = val
	return res

def array_product(arr):
	"""
	Another way to do this is to scan from
	left to right, then right to left, missing the head
	and tail respectively, then multiplying the two together.
	"""
	left, right = [0]*len(arr),[0]*len(arr)
	left[0] = 1
	right[-1] = 1
	for i in range(len(arr)-1):
		left[i+1] = arr[i]*left[i]
	for i in range(len(arr)-1,0,-1):
		right[i-1] = arr[i]*right[i]
	
	for i in range(len(left)):
		left[i] = left[i] * right[i]
	return left

if __name__ == "__main__":
	arr = [1,2,3,4]
	expected = [24,12,8,6]
	print(array_product(arr))
	assert(array_product(arr) == expected)
