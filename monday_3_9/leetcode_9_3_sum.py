"""
given array of N integers,
are there elements a,b,c such
that a+b+c = 0?
generate all unique triplets in the array
which give a sum of zero.
"""
from collections import defaultdict

def two_sum(arr,target):
	mappings = {}
	seen = set()
	out = []
	for ind, val in enumerate(arr):
		candidate = target - val
		exists = mappings.get(candidate)
		if exists is not None:
			as_sorted = sorted([candidate,val])
			paired = tuple(as_sorted)
			if paired not in seen:
				out.append(as_sorted)
				seen.add(paired)
		mappings[val] = ind
	return out

def three_sum(arr,target):
	arrlen = len(arr)
	out = []
	arr.sort()
	seen = set()
	seen_single = set()
	for i in range(arrlen):
		candidate= arr[i]
		if candidate in seen_single:
			continue
		sub_arr = arr[:i] + arr[i+1:]
		with_two = two_sum(sub_arr,-candidate)
		if with_two:
			for item in with_two:
				item.append(candidate)
				item.sort()
				as_tup = (item[0],item[1],
					item[2])
				if as_tup not in seen:
					out.append(item)
					seen.add(as_tup)
		seen_single.add(candidate)
	return out		

if __name__ == "__main__":
	arr = [-1,0,1,2,-1,4]
#	print(two_sum(arr,-2))
#	print(three_sum(arr,0))
#	arr = [-2,0,1,1,2]
#	print(three_sum(arr,0))
	print(three_sum([-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6],0))
