"""
find the missing number
"""


def find_missing(arr):
	return list(set(range(len(arr)+1)) - set(arr))[0]
