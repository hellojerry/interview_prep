"""
Reverse a singly linked list


1->2->3->4->5 ==> 5->4->3->2->1
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val

def reverse_ll(head):
	cur = head
	if not head:
		return None
	prev = None
	while cur:
		next_ = cur.next_
		cur.next_ = prev
		prev = cur
		cur = next_
	return prev

if __name__ == "__main__":
	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	res = reverse_ll(n1)
	cur = res
	out = []
	while cur:
		out.append(cur)
		cur = cur.next_
	print(out)
