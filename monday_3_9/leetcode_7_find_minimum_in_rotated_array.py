"""
Find the minimum value in a rotated array.

ex:
	[3,4,5,1,2]
res: 1
"""

def get_minimum(arr):
	prior = None
	arrlen = len(arr)
	for item in arr:
		if prior is None:
			prior = item
			continue
		if item < prior:
			return item

	## return the very first element if no circuit-break
	return arr[0]

if __name__ == "__main__":
	
	arr = [3,4,5,1,2]
	print(get_minimum(arr))
