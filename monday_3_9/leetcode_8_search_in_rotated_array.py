"""
Find a value in a rotated array.


ex: [4,5,6,7,0,1,2], target=0
res: 4

return -1 if not found.
"""


def bsearch(arr,target):
	"""
	We do a bsearch here, 
	but we test based on the pivot.
	let's do a bsearch here to review
	"""
	arrlen = len(arr)
	left,right = 0, arrlen - 1
	while left <= right:
		midpoint = int((left+right)/2)
		if arr[midpoint] == target:
			return midpoint
		elif arr[midpoint] < target:
			left = midpoint+1
		else:
			right = midpoint-1
	return -1

def search_in_rotated(arr,target):
	"""
	Why is my brain drawing a complete blank on this?
	this is easy!
	if the midpoint is less than the target, and if the target is 
	greater than the right, that means the pivot is at the midpoint,
	so we need to make right = midpoint - 1
	"""
	arrlen = len(arr)
	left,right = 0, arrlen-1
	while left <= right:
		midpoint = int((left+right)/2)
		if arr[midpoint] == target:
			return midpoint
		print("midpoint:%d,val:%d" % (midpoint,arr[midpoint]))
		if arr[midpoint] > target:
			if arr[midpoint] > arr[right] >= target:
				## this means value is between
				## midpoint and right
				left = midpoint + 1
			else:
				right = midpoint - 1

		else:
			if arr[midpoint] < arr[left] <= target:
				## this means value is between
				## left and midpoint
				right = midpoint - 1
			else:
				## otherwise between midpoint and right
				left = midpoint + 1
	return -1

if __name__ == "__main__":
#	arr = [1,2,3,4,5]
#	print(bsearch(arr,4))

	arr = [4,5,6,7,0,1,2]
	print(search_in_rotated(arr,5) == 1)
	arr = [5,6,7,0,1,2,4]
	print(search_in_rotated(arr,7) == 2)
	print(search_in_rotated(arr,4) == 6)
	print(search_in_rotated(arr,5) == 0)

	arr = [5,1,3]
	print(search_in_rotated(arr,3))
