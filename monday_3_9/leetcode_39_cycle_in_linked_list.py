
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val

def cycle(head):
	if not head:
		return False
	slow = head
	if not slow.next_:
		return False
	if not slow.next_.next_:
		return False
	fast = slow.next_.next_
	while slow and fast:
		if slow is fast:
			return True
		if not fast.next_:
			return False
		if not fast.next_.next_:
			return False
		slow = slow.next_
		fast = fast.next_.next_
	return False

if __name__ == "__main__":
	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	n5.next_ = n1
	print(cycle(n1))
