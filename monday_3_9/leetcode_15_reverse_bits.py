"""
reverse the bits of an unsigned integer

4 -> 1  (100 -> 001)

3 -> 3 (11 -> 11)

"""

def reverse_bits(num):

	reverse = 0
	leading_zeroes = 0
	used_bits = 0
	while num:
		reverse <<= 1
		if num & 1:
			reverse += 1
		num >>= 1
		used_bits += 1
	remaining = 32 - used_bits
	while remaining > 0:
		reverse <<= 1
		remaining -= 1
	return reverse


if __name__ == "__main__":
	initial = 43261596
	res = reverse_bits(initial)
	expected = int("00111001011110000010100101000000",2)
	print(expected)
	assert(res == expected)
