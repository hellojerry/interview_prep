"""
You are a professional house robber
planning to rob houses along a street.
Each house has a certain amount of money stashed.
All of the houses are arranged in a circle.

This means the first house is the neighbor
of the last one. Meanwhile, adjacent houses
have security systems connected and will automatically
contact the police if two adjacent houses were broken into
in the same night. 

Given a list of non-negative integers representing
the amount of money of each house, determine 
the maximum amount of money you can rob without 
alerting the police.

a1 = [2,3,2]
expected1 = 3

a2 = [1,2,3,1]
expected2 = 4

a3 = [9,4,8,14,2]
expected3 = 23

a4 = [5,3,4,1,4]
expected4 = 8
"""
"""
This is similar to the other house robber problem.
We have a choice between choosing the current value
precluding the following index, or skipping the current
value and using the following index. 
The twist here is that the first and final indices preclude one 
another. I think the remedy for this is passing in an additional
boolean on the recursive call if the current idx is 0,
and returning a 0 if the boolean is flipped and we're at the final index.
"""

def house_robber_2(current_idx,arr,memo,contains_leading):
	arrlen = len(arr)
	if current_idx > arrlen - 1:
		return 0
	if contains_leading and current_idx == arrlen - 1:
		return 0
	if memo[current_idx] == -1:
		element = arr[current_idx]
		if current_idx == 0:
			with_current = element + house_robber_2(
					current_idx+2,arr,memo,True)
			without_current = house_robber_2(
					current_idx+1,arr,memo,False)
		else:
			with_current = element + house_robber_2(
					current_idx+2,arr,memo,
						contains_leading)
			without_current = house_robber_2(
					current_idx+1,
					arr,memo,contains_leading)
		memo[current_idx] = max(with_current,without_current)
	print(memo)
	return memo[current_idx]

					

def kickoff_2(arr):
	if len(arr) == 1:
		return arr[0]
	
	memo_arr = [-1 for i in range(len(arr))]
	res1 = house_robber(0,arr,memo_arr,True)
	memo_arr = [-1 for i in range(len(arr))]
	res2 = house_robber(1,arr,memo_arr,False)
	 
	return max(res1,res2)



def house_robber(current_idx,arr,memo):
	"""
	My solution worked but i dont think it was in the spirit 
	of the problem. Let's try something where we shorten the array on kickoff
	"""
	arrlen = len(arr)
	if current_idx > arrlen - 1:
		return 0
	if memo[current_idx] == -1:
		element = arr[current_idx]
		if current_idx == 0:
			## if we're at the very begining, we take out
			## the tail from consideration
			with_current = element + house_robber(
					current_idx+2,arr[:-1],memo[:-1])
			without_current = house_robber(
					current_idx+1,arr,memo)
		else:
			with_current = element + house_robber(
					current_idx+2,arr,memo
						)
			without_current = house_robber(
					current_idx+1,
					arr,memo)
		memo[current_idx] = max(with_current,without_current)
	return memo[current_idx]

def kickoff(arr):
	memo = [-1 for i in range(len(arr))]
	return house_robber(0,arr,memo)

if __name__ == "__main__":
	a1 = [2,3,2]
	expected1 = 3
	r1 = kickoff(a1)
	print(r1 == expected1)	
	a2 = [1,2,3,1]
	expected2 = 4
	r2 = kickoff(a2)
	print(r2 == expected2)	
	
	a3 = [9,4,8,14,2]
	expected3 = 23
	r3 = kickoff(a3)
	print(r3 == expected3)	
	
	a4 = [5,3,4,1,4]
	expected4 = 9
	r4 = kickoff(a4)
	print(r4 == expected4)	
	a5 = [1,3,1,3,100]
	expected5 = 103
	r5 = kickoff(a5)
	print(r5 == expected5)
	a6 = [1]
	expected6 = 1
	r6 = kickoff(a6)
	print(r6 == expected6)
