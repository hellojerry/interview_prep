"""
Given an unsorted array of integers,
find the length of the longest increasing
subsequence.

a1 = [10,9,2,5,3,7,101,18]
expected = 4 (2,5,7,101) or (2,5,7,18)
"""

def longest_increasing(arr,current):
	"""
	So a brute force thing would be to 
	scan forward in a nested loop, checking
	the length and memoizing it, returning the maximum
	length until we've hit a root point of the end.
	Let's try that, with a recursive call, and see if 
	we can't find some more insight. 

	OK, now to see if this will solve the problem from
	a logic perspective
	"""
	arrlen = len(arr)
	if arrlen == 0:
		return current
	if arrlen == 1:
		return current+1
	el = arr[0]
	longest = current+1
	for i in range(1,arrlen):
		if arr[i] > el:
			longest = max(longest,longest_increasing(arr[i:],current+1))
	return max(longest,longest_increasing(arr[1:],0))

def lic(arr, arr_idx,memo):
	if arr_idx < 0:
		return 0
	if memo[arr_idx] != -1:
		return memo[arr_idx]
	el = arr[arr_idx]
	ct = 1
	for i in range(arr_idx-1,-1,-1):
		target = arr[i]
		if target < el:
			if memo[i] == -1:
				memo[i] = lic(arr,i, memo)
			ct += memo[i]
			break
	memo[arr_idx] = ct
	print(memo)
	return max(memo[arr_idx],lic(arr,arr_idx-1,memo))

def kickoff(arr):
	"""
	I'm starting to have trouble focusing. It's almost 6pm, we've got this
	and two more to do and we can call it a night. There are a couple of things
	that come to mind when I think of optimizing for speed. 
	1. can we go backwards instead of forwards? Is there a benefit to doing so?
	2. is there some sort of caching strategy we can use? 
	If I go with a one-dimensional array and work backwards, I can try and do
	a computation that stores the number of elements less than the current one. 
	If I found the leftmost current element before an increase going backward,
	how does that help? 
	Let's see how far we can get with a one-dimensional cache. If I populate the memo
	array by number of values beneath it preceding it, i think i can at least sort
	the array and grab the max from there.
	"""
	cache = [-1 for i in range(len(arr))]
	cache[0] = 0
	res = lic(arr,len(arr)-1,cache)
	return res	


def longest_increasing(arr):
	"""
	So going backwards isn't that necessary.
	The idea with this is that the default value is 1,
	and for every value, increasing through the array,
	we see if prospective values are higher, storing the maximum value
	in the cache.
	"""

	arrlen = len(arr)
	cache = [1]*arrlen
	for i in range(arrlen):
		prior_max = 0
		for j in range(i):
			if arr[j] < arr[i]:
				prior_max = max(
					cache[j],prior_max)
		cache[i] = max(cache[i], 1+prior_max)
	return max(cache)

if __name__ == "__main__":
	a1 = [10,9,2,5,3,7,101,18]
	expected1 = 4
	r1 = longest_increasing(a1)
	print(r1, r1 == expected1)
	a2 = [2,4,5,6,7,8,4,5,10]
	expected2 = 7
	r2 = longest_increasing(a2)
	print(r2, r2 == expected2)
	a3 = [10,9,8,7,5,4]
	expected3 = 1
	r3 = longest_increasing(a3)
	print(r3, r3 == expected3)
	a4 = [1,2]
	expected4 = 2
	r4 = longest_increasing(a4)
	print(r4, r4 == expected4)
