"""
A robot is located
at the top-left corner
of an MxN grid.
The robot can only move either
down or right at any point
in time. The robot is trying
to reach the bottom-right
corner of the grid.
How many unique paths are there?

arr1 = [
	[0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0],
]
m1 = 7, n1 = 3
expected = 28

arr2 = [
	[0,0,0],
	[0,0,0],
]
m2 = 3, n2 = 2
expected = 3
"""

def recursive_basic(M,N,current_r,current_c):
	"""
	The first way i'd implement this is with a recursive
	solution. We want a test to see if we've hit the exit,
	and then we want to test if we're left eligible and down
	eligible. My guess is that the problem with this is that it has 
	exponential complexity.
	"""
	if current_r == M - 1 and current_c == N - 1:
		return 1
	out = 0
	if current_r < M - 1:
		out += recursive_basic(M,N,current_r+1,current_c)
	if current_c < N - 1:
		out += recursive_basic(M,N,current_r,current_c+1)
	return out


def recurse(M,N,r,c,matrix):
	if r == M - 1 and c == N - 1:
		return 1
	if matrix[r][c] == -1:
		out = 0
		if r < M - 1:
			out += recurse(M,N,r+1,c,matrix)
		if c < N - 1:
			out += recurse(M,N,r,c+1,matrix)
		matrix[r][c] = out

	return matrix[r][c]

def kickoff(M,N):
	"""
	Now the trick with this is to use a cache of some sort.
	I could keep a bunch of tuples with addresses in a set and invalidate
	paths that way, but I feel like that would cause some serious issues 
	insofar as memory consumption goes. I am kinda hungry.. rgh.
	The solution for this thing is some sort of matrix. as we proceed rightward
	we can sum up solutions
	"""
	matrix = [[-1]*N for i in range(M)]
	return recurse(M,N,0,0,matrix)

if __name__ == "__main__":
	arr1 = [
		[0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0],
	]
	m1,n1 = 7,3
	expected1 = 28
	r1 = kickoff(m1,n1)
	print(r1 == expected1)	
	arr2 = [
		[0,0,0],
		[0,0,0],
	]
	m2,n2 = 3,2
	expected2 = 3
	r2 = kickoff(m2,n2)
	print(r2 == expected2)
