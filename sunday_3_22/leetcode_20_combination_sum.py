"""
Given an integer array with all positive
numbers and no duplicates, find the number
of possible combinations that add up to
a positive target.

a1 = [1,2,3]
target = 4
out = 7
(possible combinations:

[1,1,1,1]
[1,1,2],
[1,2,1],
[1,3],
[2,1,1],
[2,2],
[3,1]
)
Note that elements may be repeated, and different
sequence orderings are counted as different combinations
"""



def combination_sum_naive(arr,remaining):
	"""
	I don't think permuting the array
	recursively is going to be cost-effective.
	Similarly, although this reminds me of a two-sum,
	hmm. This is a dynamic programming problem, so
	we need to find the choice that is made.
	Let's try solving it recursively first, and see if we
	can't find a choice to make inside of there.
	A recursive solution would be iterating over each element,
	subtracting it from the total, then calling it recursively again
	with the new total remaining. we never reduce the array size with these
	recursive calls. So far so good, need some more test cases to sniff
	out if this is logically correct.
	OK, so we're past 8 test cases, and we're hitting a complexity limit.
	"""
	if remaining < 0:
		return 0
	if remaining == 0:
		return 1
	out = 0
	arrlen = len(arr)
	for i in range(arrlen):
		candidate = arr[i]
		if remaining - candidate >= 0:
			out += combination_sum_naive(arr,remaining-candidate)
	return out

def combination_sum(arr,remaining,memo):
	"""
	This is a unique thing - we'll want to do this again tomorrow.
	Sometimes DP can be solved with very simple caching behavior.
	"""
	if remaining < 0:
		return 0
	if remaining == 0:
		return 1
	if memo[remaining] != -1:
		return memo[remaining]
	out = 0
	arrlen = len(arr)
	for i in range(arrlen):
		candidate = arr[i]
		if candidate <= remaining:
			out += combination_sum(arr,remaining-candidate,memo)
	memo[remaining] = out
	return out

def kickoff(arr,remaining):
	memo = [-1 for i in range(remaining+1)]
	res = combination_sum(arr,remaining,memo)
	return res

if __name__ == "__main__":
	a1 = [1,2,3]
	t1 = 4
	expected1 = 7
	r1 = kickoff(a1,t1)
	print(r1)
	print(r1 == expected1)
	arr = [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
	target = 10
	expected = 9
	r2 = kickoff(arr,target)
	print(r2)	
