"""
Given a nonempty string S and
a dictionary word_dict
containing a list of non-empty words
determine if S can be segmented into a space-separated
sequence of one or more dictionary words.

words can be reused

s1 = "leetcode"
d1 = {"leet","code"}
e1 = True

s2 = "applepenapple"
d2 = {"apple","pen"}
e2 = True

s3 = "catsandog"
d3 = {"cats","dog","and","sand","cat"}
e3 = False
"""

def w_break(word,words):
	"""
	I am having a hell of a time focusing at this point.
	The general idea with this one is to scan a section
	of characters until we have a match, mark the index
	of the end match, and recurse down until the word is empty.
	I think. Ok, so we have the recursive solution, let's 
	see how far we get complexity-wise. This solution is confirmed for
	correctness.
	"""
	word_len = len(word)
	if word_len == 0:
		return True
	if word_len == 1:
		if word in words:
			return True
		return False
	start_char = word[0]
	right_ptr = 1
	while right_ptr <= word_len:
		sl_ = word[0:right_ptr]
		print(sl_)
		if sl_ in words:
			if w_break(word[right_ptr:], words):
				return True
		right_ptr += 1
	return False

def wb(word,words):
	"""
	Our recursive solution worked. The next thing to figure out
	is how to cache things. What could we cache here?! could we use words
	as an array ? Let's step back a bit. Where is the complexity explosion
	in the recursive function? it is the recursive call to the while loop.
	every time we call w_break recursively, it iterates over potentially
	the entire remainder. Could we just try stuffing the largest words
	into the array and seeing what fits?
	"""
	word_len = len(word)
	if word_len == 0:
		return True
	if word_len == 1:
		if word in set(words):
			return True
		return False
	print(word)
	for word_ in words:
		wlen = len(word_)
		if word[:wlen] == word_:
			if wb(word[wlen:],words):
				return True
	return False
	

def kickoff(word, words):
	words.sort(key=lambda x: -len(x))
	return wb(word,words)

def wb_bfs(word,words):
	"""
	So a BFS might work, right? 
	"""

if __name__ == "__main__":

	s1 = "leetcode"
	d1 = ["leet","code"]
	e1 = True
	r1 = kickoff(s1,d1)
	print(r1)	
	s2 = "applepenapple"
	d2 = ["apple","pen"]
	e2 = True
	r2 = kickoff(s2,d2)
	print(r2)	
	s3 = "catsandog"
	d3 = ["cats","dog","and","sand","cat"]
	e3 = False
	r3 = kickoff(s3,d3)
	print(r3)
	s4 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
	d4 = ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
	r4 = kickoff(s4,d4)
	print(r4)

