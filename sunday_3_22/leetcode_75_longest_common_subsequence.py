"""
Given two strings, text1 and text2,
return the length of their longest common
subsequence.

A subsequence of a string is a new string
generated from the original string with
some characters (can be none)
deleted without changing the relative
order of the remaining characters.
eg. "ace" is a subsequence of "abcde"
while "aec" is not. a common subsequence
of two strings is a subsequence that is common
to both strings.
If there is no common subsequence, return 0.


a1,b1 = "abcde", "ace"
expected1 = 3

a2,b2 = "abc","abc"
expected2 = 3

a3,b3 = "abc","def"
expected3 = 0
"""

def longest_common_subsequence_2(left,right,current_amount):
	"""
	Ok, what is the problem we're trying to solve here?
	If we do the visual test on this, i look for common letters
	between abcde and ace, and then drop the irrelevant ones.
	So how do we model this from a data structures perspective?
	How would we even handle this recursively?
	It appears to be easier to start with the smaller string
	and look for commonalities there. 
	Now, the next part, since this is dynamic programming,
	there is some sort of choice to make.
	our basic comparison is of a single element of the left string and the right string.
	Let's think about a recursive solution. We gradually take away elements of the left
	string until none are left. For a given element location, we scan forward until
	reaching the element or exhausing the right string. 

	I dont think this solution is quite right from a correctness perspective.
	"""
	if len(left) == 0 or len(right) == 0:
		return current_amount
	target_char = left[0]
	candidate_amounts = [current_amount+1]
	found = False
	for i in range(len(right)):
		candidate = right[i]
		if candidate == target_char:
			candidate_amounts.append(longest_common_subsequence_2(left[1:], right[i+1:],current_amount+1))
			found = True
	if found:
		return max(candidate_amounts)
	print("got here ever")
	return longest_common_subsequence_2(left[1:],right,current_amount)


def longest(left,right,current_amount):
	"""
	I still don't believe this is the right way to do it.
	As best as i can tell, this will solve the problem given 
	enough time. 
	Narratively speaking, this is a brute force. We compute
	all prospective subset sizes and pick the best one
	"""
	if len(left) == 0 or len(right) == 0:
		return current_amount
	target_char = left[0]
	candidate_amounts = [current_amount]
	for i in range(len(right)):
		candidate = right[i]
		if candidate == target_char:
			candidate_amounts.append(longest(left[1:],right[i+1:],current_amount+1))
	return max(max(candidate_amounts),longest(left[1:],right,current_amount))

def kickoff_2(left,right,throwaway):
	## what if we took sets of each word and deleted the disjoints?
	## does this actually solve anything?
	left_as_set = set(left)
	right_as_set = set(left)
	to_remove_left = left_as_set - right_as_set
	to_remove_right = right_as_set - left_as_set
	left_reformed = "".join(i for i in left if i not in to_remove_left)
	right_reformed = "".join(i for i in right if i not in to_remove_right)
	res = longest(left_reformed,right_reformed,0)
	return res


def longest(left,right,left_idx,right_idx, matrix):
	"""
	I am still struggling with the "choice"
	that the algorithm needs to make, along with the caching behavior.
	Let's try this with a memoization array, and we track the index along
	the left string. 
	This is a levenshtein distance problem. Fuck. Need to practice more
	of these string things.
	"""
	if left_idx < 0 or right_idx < 0:
		return 0
	if matrix[left_idx][right_idx] == -1:
		if left[left_idx] == right[right_idx]:
			matrix[left_idx][right_idx] = 1 + longest(left,right,left_idx-1,right_idx-1,
								matrix)
		else:
			matrix[left_idx][right_idx] = max(
					longest(left,right,left_idx-1,right_idx,matrix),
					longest(left,right,left_idx,right_idx-1,matrix)
					)
	return matrix[left_idx][right_idx]

def kickoff(left,right,throwaway):
	memo = [[-1]*(len(right)) for i in range(len(left))]
	return longest(left,right,len(left)-1,len(right)-1,memo)

if __name__ == "__main__":
	a1,b1 = "abcde", "ace"
	expected1 = 3
	r1 = kickoff(a1,b1,0)
	print(r1 == expected1)	
	a2,b2 = "abc","abc"
	expected2 = 3
	r2 = kickoff(a2,b2,0)
	print(r2 == expected2)	
	a3,b3 = "abc","def"
	expected3 = 0
	r3 = kickoff(a3,b3,0)
	print(r3 == expected3)	

	a4, b4 = "raildray", "bagnum"
	expected4 = 1
	r4 = kickoff(a4,b4,0)
	print(r4 == expected4)	

	a5, b5 = "abcdef", "abedq"
	expected5 = 3
	## either abe or abd
	r5 = kickoff(a5,b5,0)
	print(r5 == expected5)
	a6,b6 = "abacab", "aqcac"
	expected6 = 3
	r6 = kickoff(a6,b6,0)
	print(r6 == expected6)

	a7,b7 = "oxcpqrsvwf","shmtulqrypy"
	expected7 = 2
	## at "qr"
	r7 = kickoff(a7,b7,0)
	print(r7 == expected7)
