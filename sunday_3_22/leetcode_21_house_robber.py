"""
You are a professional robber planning
to rob houses along a street. Each house
has a certain amount of money stashed,
the only constraint stopping you from robbing
each of them is that adjacent houses have security
systems and will call the cops if two adjacent houses
are broken into on the same night.

Given a list of non-negative integers representing
the amount of money of each house, determine
the maximum amount of money you can rob tonight
without alerting the poloce.

a1 = [1,2,3,1]
expected = 4
explanation: rob house at index 0 and at index 2

a2 = [2,7,9,3,1]
expected = 12
"""
"""
hurr.... having trouble with brain this morning.
So my first thought here is that we could do a comparison
of evens vs odds and just take one of the two.
This falls apart though in the following scenario:
[5,1,1,9] where the head and the tail are the optimal choices.
The underlying thing in this is that we want to grab the maximum
of any given three adjacent values, and then the maximum
of adjacent triplets. In a given triplet, we have three possible choices.
i.e. [1,2,3]
In a pair of adjacent triplets, we have five possible sets of choices
ex: [1,2,3][4,5,6] ->
[1,4,6],[1,3,6],[1,3,5], [2,4,6],[2,5]
So.. uhh... hm. The thing I'm struggling with on here is I am having trouble
modeling the "choice" that the program needs to make. Typically in dynamic programming
you have a choice, either "use this item" or dont use this item, and then, for example,
in the knapsack problem, the usage of the item precludes usage of some other combination
of items by virtue of units of capacity being consumed.
If we think of this in terms of choices, each given array index being consumed precludes the
usage of the following and preceding array indices. 
So if we're doing a generic choice setup here, i suppose we have something like
	with_current_idx = do_something()
	without_current_idx = do_something()
	and we memoize the total of the better of the two. 
	How, mechanically, do we preclude the following index? and what's the proper data structure
	for memoizing the results here? typically there's an array or a matrix.
	If we use a simple array mirroring the input array itself, we could do something like
		arr[current_idx] = cur_val + recurse(current_idx+2)
		arr[next_idx] = next_val
	hmm. that's not quite right. What about passing in the parent array sans the "next" idx
	and recursing in that manner??
	What is the narrative step for this? If I scan this visually, I look at each of the numbers,
	find the biggest one, and then find the other eligible values and add them together.
In the knapsack problem, we check the matrix, and then check the result with the current item, vs the result
without the current item, and take the better of the two, returning the memoized result at the end.

In this... fuck. If I take a value, preclude the following, then recursively call on the remainining
to get a result, we should be able to get this via iteration and recursion.
"""

def house_robber_recurse(current_idx, arr):
	"""
	So I believe this recursive solution leads to the proper
	answer, but it's got nasty complexity.
	"""
	arrlen = len(arr)
	if current_idx > arrlen - 1:
		return 0
	element = arr[current_idx]
	with_current = element + house_robber_recurse(current_idx+2,arr)
	without_current = house_robber_recurse(current_idx+1,arr)
	return max(with_current,without_current)

def house_robber(current_idx,arr,memo):
	arrlen = len(arr)
	if current_idx > arrlen - 1:
		return 0
	if memo[current_idx] == -1:
		element = arr[current_idx]
		with_current = element + house_robber(
				current_idx+2,arr,memo)
		without_current = house_robber(current_idx+1,arr,memo)
		memo[current_idx] = max(with_current,without_current)


	return memo[current_idx]

def kickoff(arr):
	"""
	So the problem with the basic recursive solution
	is that it has nasty complexity. We need a caching strategy.
	I'm going to try a simple array for the time being and see what that does.
	"""
	memo_arr = [-1 for i in range(len(arr))]
	return house_robber(0,arr,memo_arr)

if __name__ == "__main__":
	#a1 = [1,2,3,1]
	#expected1 = 4
	#r1 = house_robber_recurse(0,a1)
	#print(r1 == expected1)
	#a2 = [2,7,9,3,1]
	#expected2 = 12
	#r2 = house_robber_recurse(0,a2)
	#print(r2 == expected2)
	#a3 = [5,1,1,9]
	#expected3 = 14
	#r3 = house_robber_recurse(0,a3)
	#print(r3 == expected3)
	a1 = [1,2,3,1]
	expected1 = 4
	r1 = kickoff(a1)
	print(r1 == expected1)
	a2 = [2,7,9,3,1]
	expected2 = 12
	r2 = kickoff(a2)
	print(r2 == expected2)
	a3 = [5,1,1,9]
	expected3 = 14
	r3 = kickoff(a3)
	print(r3 == expected3)
