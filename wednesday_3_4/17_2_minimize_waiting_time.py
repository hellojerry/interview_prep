"""
A database has to respond to a set of client SQL queries.
The service time required for each query is known in advance.
For this application, the queries must
be processed one at a time, but can be done in any order.
The time a query waits before its turn comes is called its
waiting time.
Given service times for a set of queries, compute a schedule for processing
the queries that minimized the total waiting time. Return the minimum
total waiting time. For example, if the service times
are 

[2,5,1,3], if we schedule in the given order, the total waiting time is
0+2+(2+5)+(2+5+1) = 17
If we schedule queries in order of decreasing service times [5,3,2,1], the 
total waiting time is  0+(5)+(5+3)+(5+3+2) = 23
if we go in increasing order [1,2,3,5]
the waiting time is 0 + (1) + (1+2) + (1+2+3) = 10
The minimum time for this set of tasks is ten.
"""

def compute_waiting_time(arr):
	"""
	Maybe I'm being dumb here, but I think 
	we can just sort the array, then iterate and accumulate.
	"""
	arr.sort()
	prior = 0
	out = 0
	for i in arr[:-1]:
		print("out:%d,i:%d,prior:%d" % (out,i,prior))
		out += i + prior
		prior += i


	return out
if __name__ == "__main__":
	print(compute_waiting_time([1,5,3,2]))

