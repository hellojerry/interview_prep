"""
The weight of a nonnegative integer x is the number of bits set to 1
in the binary representation.

92 in binary is 10111000, so the weight is 4.

Write a program which takes as input a nonnegative integer x and returns a number
Y which is not equal to x but has the same weight and their difference, Y-x
is as small as possible. x is not going to be all zeros or all 1s.
The integer is 64 bits.
Hint: start with the least significant bit
"""

def find_closest(x):
	"""
	So the very brute force thing is to make a bit-counting function
	and just go up and down until we get a match.
	The proper thing to do is 	
	
	"""
	for i in range(64):
		if (x >> i) & 1 != (x >> (i+1)) & 1:
			x ^= (1 << i) | (1 << (i+1))
			return x
	raise Exception("All bits are zero or 1")
