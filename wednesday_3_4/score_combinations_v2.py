


def num_combinations(remaining_score,plays):
	"""
	Our two terminating cases:
		1. no points left, we've got success
		2. no plays left, or over the limit, failure.
	For each iteration, we test the current score - the current play
	and combine it with the current score and remaining plays.
	"""

	if remaining_score == 0:
		return 1
	play_len = len(plays)
	if remaining_score < 0 or play_len == 0:
		return 0
	play_val = plays[0]
	return num_combinations(remaining_score - play_val,
			plays) + num_combinations(remaining_score,plays[1:])

if __name__ == "__main__":
	final_score = 12
	plays = [2,3,7]
	print(num_combinations(final_score,plays))
