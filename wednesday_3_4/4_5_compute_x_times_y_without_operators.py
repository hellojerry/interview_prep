"""
Write a progra that multiplies two integers.
The only operators allowed are:

- assignment
- >>,<<,|,&,~,^
- equality checks

Hint: add using bitwise operations, multiply using shift-and-add
"""

def add_bitwise(a,b):
	while b != 0:
		carry = a & b
		a = a ^ b
		b = carry << 1
	return a


def multiply(x,y):

	out = 0
	z = 0
	while z < y:
		out = add_bitwise(out,x)
		z = add_bitwise(z,1)
	print(out)


if __name__ == "__main__":
	print(add_bitwise(4,5))
	multiply(3,4)
	multiply(10,10)
