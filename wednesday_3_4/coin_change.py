

"""
dimes, nickels, quarters, pennies
"""


def coin_combos(remaining_amount, coins):
	if remaining_amount == 0:
		return 1
	coin_len = len(coins)
	if remaining_amount < 0 or coin_len == 0:
		return 0
	current_coin = coins[0]
	return coin_combos(remaining_amount - current_coin,
		coins) + coin_combos(remaining_amount, coins[1:])


if __name__ == "__main__":
	coins = [3,5,7,8,9,10,11]
	print(coin_combos(500,coins))
	
