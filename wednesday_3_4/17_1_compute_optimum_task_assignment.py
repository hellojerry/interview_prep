"""
Each worker must be assigned exactly two tasks.
Each task takes a fixed amount of time.
Each task is independent, i.e. no constraints of
"Task 3 must be completed before Task 4 starts".
Any task can be assigned to any worker.

We want to assign tasks to workers so as to minimize 
how long it takes before all tasks are completed.

For example, if there are six tasks whose durations are 5,2,1,6,4,4 hours,
then an optimum assignment is to give the first two tasks to one worker, 
the next two to another, and the last two to another. As a result,
The total assignment would finish in eight hours.
Design an algorithm that taskes as input a set of tasks and N workers returns
an optimum assignment.
"""

def assign_tasks(tasks):
	"""
	My gut with this is to sort this and consecutively
	assign the head and tail. This works fine with
	even task amounts, but if it's odd, the final task
	should be assigned to one worker
	This should return an array of tuples
	"""
	task_len = len(tasks)
	tasks.sort()
	even_tasks = task_len % 2 == 0
	left_ptr,right_ptr = 0, task_len - 1
	out = []
	if not even_tasks:
		out.append((tasks[-1],))
		right_ptr -= 1
	while left_ptr < right_ptr:
		out.append((tasks[left_ptr],tasks[right_ptr]))
		left_ptr += 1
		right_ptr -= 1
	return out

if __name__ == "__main__": 

	tasks = [5,2,1,6,4,4,7]
	print(assign_tasks(tasks))




