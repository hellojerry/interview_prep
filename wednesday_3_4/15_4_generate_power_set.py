"""
Generate the power set of a set.

power set of {1,2,3} is {},{1},{2},{2},{1,2},{2,3},{1,2,3}

"""

def recurse(value, acc):
	acc_len = len(acc)
	if acc_len == 0:
		acc.append(set())
		acc.append({value})
		return acc
	else:
		copied = acc.copy()
		for item in copied:
			as_c = item.copy()
			as_c.add(value)
			acc.append(as_c)
		return acc

def generate_power_set(my_set):

	out = []
	for value in my_set:
		out = recurse(value,out)
	return out

if __name__ == "__main__":
	res = generate_power_set({1,2,3})
	print(res)
