"""
In football, a play can lead to 2 points, 3 points, or 7 points.

Write a program that takes a final score and scores for individual plays,
and returns the number of combinations of plays that result in a final score.
"""
from pprint import pprint

def number_combinations(matrix,final_score,plays):
	"""
	Let's walk through this.
	1. matrix of #rows = number of plays in array, # cols = range from 0 to remaining_score.
	The first column should be a 1, to indicate that we've got a hit at that location.
	If we don't have a remaining score, that means we've
	got a solution, and can return 1.
	if we're out of plays, return 0
	Each intersection of matrix[play_idx][remaining_score]
	represents a tested score combination.
	"""
	for play_idx in range(len(plays)):
		for candidate_score in range(1,final_score+1):
			without_play = 0
			if play_idx >= 1:
				without_play = matrix[play_idx-1][candidate_score]
			with_play = 0
			play_val = plays[play_idx]
			if play_val <= candidate_score:
				with_play = matrix[play_idx][
					candidate_score - play_val]
			matrix[play_idx][candidate_score] = (
				with_play + without_play)
			pprint(matrix)


	return matrix[-1][-1]


def setup(final_score,plays):
	matrix = [[1] + [0]*final_score for i in plays]
	pprint(matrix)
	res = number_combinations(matrix,final_score,plays)
	print(res)
#	print(matrix)

if __name__ == "__main__":
	setup(12,[2,3,7])
	
