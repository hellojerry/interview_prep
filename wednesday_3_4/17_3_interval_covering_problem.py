"""
Consider a foreman responsible for a number of tasks
on the factory floor. Each task starts at a fixed time
and ends at a fixed time. The foreman wants to visit
the floor to check on the tasks.
Your job is to help him minimize the number of visites he makes.
In each visit, he can check on all the tasks taking place at the time of the visit.
A visit takes place at a fixed time, and he can only check tasks
taking place at exactly that time. For example,

tasks: [0,3],[2,6],[3,4],[6,9]
Then visit times 0,2,3,6 cover all tasks. A smallet set of visit times
that covers all tasks is [3,6]

you are given a set of closed intervals. Design an efficient algorithm for finding
a minimum sized set of numbers that covers all the intervals.
"""

def find_shortest_interval(intervals):
	"""
	This is one of those interval closing things.
	1. sort it
	2. memoize end of first event. Once we hit something outside
	the end, "tie it off" and put the memoized value to an output array,
	then memoize the next one.
	"""
	intervals = sorted(intervals, key=lambda x: x[0])
	if len(intervals) == 1:
		return [intervals[0][0]]
	print(intervals)
	memo_event_ending = intervals[0][1]
	out = []
	for event in intervals[1:]:
		if event[0] > memo_event_ending:
			out.append(memo_event_ending)
			memo_event_ending = event[1]
			print("swap")
		print(event)
	if intervals[-1][0] > out[-1]:
		out.append(intervals[-1][0])
	return out


if __name__ == "__main__":

	arr = [[0,3],[2,6],[3,4],[6,9]]
	assert(find_shortest_interval(arr) == [3,6])
