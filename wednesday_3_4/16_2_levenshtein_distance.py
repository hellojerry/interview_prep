


def levenshtein(left,right):
	"""
	We need a matrix of len(left)+1 rows, len(right)+1 cols
	we want the left col and top row to be a range from 0->final_val
	we iterate over left and right (as x and y), with
		if left[x-1] == right[y-1]:
		matrix[x][y] = min(
			matrix[x-1][y] + 1
			matrix[
	"""

	left_len,right_len = len(left), len(right)
	if right_len > left_len:
		left,right = right,left
		left_len,right_len = right_len,left_len
	lmax,rmax = left_len+1,right_len+1
	matrix = [[0]*rmax for i in range(lmax)]
	for i in range(lmax):
		matrix[i][0] = i
	for i in range(rmax):
		matrix[0][i] = i
	for x in range(lmax):
		for y in range(rmax):
			if left[x-1] == right[y-1]:
				matrix[x][y] = min(
					matrix[x-1][y] + 1,
					matrix[x-1][y-1],
					matrix[x][y-1] +1
				)
			else:
				matrix[x][y] = min(
					matrix[x-1][y] + 1,
					matrix[x-1][y-1] + 1,
					matrix[x][y-1] +1
				)
	return matrix[-1][-1]

if __name__ == "__main__":
	s1,s2 = "Saturday", "Sundays"
	print(levenshtein(s1,s2))
				
