"""
Divide x by y, using only the addition, subtraction, and shifting operators.

hint: relate x/y to (x-y)/y

x/y = ((x-y)/y) + 1

"""



def divide(x,y):
	"""
	Could I just count up until y = x? This solution is faster
	"""
	result, power = 0,32
	y_power = y << power
	while x >= y:
		while y_power > x:
			y_power >>= 1
			power -= 1
		result += 1 << power
		x -= y_power
	return result

if __name__ == "__main__":
	print(divide(25,5))
