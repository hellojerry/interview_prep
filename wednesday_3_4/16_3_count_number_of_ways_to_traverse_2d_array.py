"""
Count the number of ways starting
from the top left corner of a 2d array
and getting to the bottom right corner.
All moves must go either right or down

arr = [
	[1,2,3],
	[4,5,6],
	[7,8,9]
]


"""


def count_number_of_ways(matrix,row,col):
	"""
	First step, lets just figure out eligibility
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	min_row,min_col = 0,0
	max_row,max_col = rows-1,cols-1
	down_eligible = row < max_row
	right_eligible = col < max_col
	paths = 0
	if row == max_row and col == max_col:
		return 1
	if right_eligible:
		paths += count_number_of_ways(matrix,row,col+1)
	if down_eligible:
		paths += count_number_of_ways(matrix,row+1,col)

	return paths

if __name__ == "__main__":
	arr = [
		[1,2,3],
		[4,5,6],
		[7,8,9]
	]
	print(count_number_of_ways(arr,0,0))
