"""
Write a program that takes an input number 
and returns all strings with that number of pairs
of matched parentheses.

1 -> ()
2 -> ()(), (())
3 -> ()()() ((())), ()(()), (())(), (()())
"""

def generate_pairs(N,acc):
	"""
	For this, we start with an empty set,
	add enclosing parens, then a pair on the left,
	and a pair on the right (make a set).
	Then iterate, 
	"""
	acc_len = len(acc)
	if N == 0:
		return acc
	out = []
	for ind,item in enumerate(acc):
		candidates = set()
		candidates.add("(" + item + ")")
		candidates.add("()" + item)
		candidates.add(item + "()")
		for candidate in candidates:
			out.append(candidate)
	return generate_pairs(N-1,out)

if __name__ == "__main__":
	acc = [""]
	res = generate_pairs(3,acc)
	print(res)
	print(generate_pairs(2,[""]))
