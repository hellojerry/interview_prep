"""
Given a string S and a string T, find
the minimum window in S which will contain
all the characters in T in complexity O(n)

Ex:
S1 = "ADOBECODEBANC"
T1 = "ABC"
e1 = "BANC"

if there is no such window in S that covers
all characters in T, return ""
if there is a window, there is only one window
"""
from collections import Counter,defaultdict
#def minimum_window(S,T):
#	"""
#	Getting distracted. We want the frequencies
#	of letters in S and T. On S, we can grab two 
#	pointers, representing the far left and the far right.
#	if neither far left nor far right are in the set of T,
#	we move inward.
#	if both are in the set of T, we check the frequencies of S.
#		if both values are 1, we're done - can't go any further.
#		if both are above 1, we can move both inward, reducing
#			frequencies of both in S by 1
#		if one is 1 and the other isn't, we reduce the higher one
#			by one, moving its pointer inward.
#	if neither are in the set of T, we move both inward.
#	if one is in the set of T, we move the other one inward.
#	"""
#	S_len = len(S)
#	S_freq = dict(Counter(S))
#	T_set = set(T)
#	smallest = ""
#	left_ptr,right_ptr = 0, S_len - 1
#	while left_ptr <= right_ptr:
#		lval,rval = S[left_ptr],S[right_ptr]
#		lval_ct,rval_ct = S_freq[lval],S_freq[rval]
#		print(S_freq)
#		if lval_ct <= 0 or rval_ct <= 0:
#			print("here1")
#			return smallest
#			
#		if lval not in T_set and rval not in T_set:
#			left_ptr += 1
#			right_ptr -= 1
#			print("here2")
#		elif lval in T_set and rval in T_set:
#			if lval_ct == 1 == rval_ct:
#				return smallest
#			elif lval_ct > 1 and rval_ct > 1:
#				## decrement freqs
#				left_ptr += 1
#				right_ptr -= 1
#				S_freq[lval] -= 1
#				S_freq[rval] -= 1
#			elif lval_ct > 1 and rval_ct == 1:
#				## decrement lval only
#				left_ptr += 1
#				S_freq[lval] -= 1
#			elif lval_ct == 1 and rval_ct > 1:
#				right_ptr -= 1
#				S_freq[rval] -= 1
#			else:
#				print("???")
#		elif lval not in T_set and rval in T_set:
#			if rval_ct > 1:
#				## decrement
#				right_ptr -= 1	
#				S_freq[rval] -= 1
#			S_freq[lval] -= 1
#			left_ptr += 1
#		elif lval in T_set and rval not in T_set:
#			if lval_ct > 1:
#				S_freq[lval] -= 1
#				left_ptr += 1
#			right_ptr -= 1
#			S_freq[rval] -= 1
#		else:
#			print("??")
#		smallest = S[left_ptr:right_ptr+1]
#	return smallest
#
#def minimum_window(S,T):
#	"""
#	First approach was mostly there.
#	It's too aggressive on deletions however.
#	How about this:
#	start with left pointer. scan until first value hit.
#	take right pointer, at left ptr + 1, and scan
#	until all values in a subset T frequency counter
#	are >= 1. then
#		if far left char in S freq > 1,
#			move right ptr forward again
#			until subset frequency 
#			counters are all >= 1.
#			Rinse and repeat.
#		otherwise, return smallest
#	OK, so the question wasn't clear on repeated letters.
#	This is going to be slightly tricker.
#	What we're going to want to do here is get the smallest substring
#	that contains a frequency count EQUAL to T_frequencies.
#	This might actually be simpler to write up now that I think about it.
#	""" 
#	S_freq = dict(Counter(S))
#	T_set = set(T)
#	S_len = len(S)
#	left_ptr = 0
#	right_ptr = 0
#	subset_freqs = defaultdict(int)
#	smallest = ""
#	while (right_ptr < S_len):
#		lval = S[left_ptr]
#		while lval not in T_set:
#			left_ptr += 1
#			lval = S[left_ptr]
#		if right_ptr <= left_ptr:
#			right_ptr = left_ptr + 1
#		subset_freqs[lval] += 1
#		#print(subset_freqs,T_set)
#		while set(subset_freqs) != T_set:
#			if right_ptr >= S_len:
#				return smallest
#			rval = S[right_ptr]
#			if rval in T_set:
#				subset_freqs[rval] += 1
#			right_ptr += 1
#		smallest = S[left_ptr:right_ptr+1]
#		if S_freq[lval] > 1:
#			S_freq[lval] -= 1
#			subset_freqs[lval] -= 1
#			if subset_freqs[lval] <= 0 :
#				subset_freqs.pop(lval)
#			left_ptr += 1
#		else:
#			right_ptr += 1
#			#print(smallest,right_ptr)
#	return smallest
#
#def minimum_window(S,T):
#	"""
#	We have the general right idea on this.
#	What we need to do is get a match of subset_freqs
#	to T_freqs.
#	"""
#	S_freqs = dict(Counter(S))
#	T_freqs = dict(Counter(T))
#	S_len = len(S)
#	if len(T) > S_len:
#		return ""
#	for k,v in T_freqs.items():
#		cand = S_freqs.get(k)
#		if cand is None:
#			return ""
#		if v > cand:
#			return ""
#	left_ptr,right_ptr = 0,0
#	subset_freqs = defaultdict(int)
#	smallest = ""
#	while right_ptr < S_len:
#		print("smallest",smallest)
#		lval = S[left_ptr]
#		while lval not in T_freqs:
#			smallest = smallest[1:]
#			left_ptr += 1
#			lval = S[left_ptr]
#		if right_ptr <= left_ptr:
#			right_ptr = left_ptr + 1
#		subset_freqs[lval] += 1
#		while set(subset_freqs) != set(T_freqs):
#			if right_ptr >= S_len:
#				print("here",T_freqs)
#				print(subset_freqs,lval,rval)
#				return smallest
#			rval = S[right_ptr]
#			if rval in T_freqs:
#				subset_freqs[rval] += 1
#				
#			right_ptr += 1
#		smallest = S[left_ptr:right_ptr+1]
#		print("after check",smallest)
#		while smallest[-1] not in T_freqs:
#			smallest = smallest[:-1]
#		if S_freqs[lval] > T_freqs[lval]:
#			smallest = smallest[1:]
#			S_freqs[lval] -= 1
#			subset_freqs[lval] -= 1
#			if subset_freqs[lval] <= 0:
#				subset_freqs.pop(lval)
#			left_ptr += 1
#			#lval = S[left_ptr]
#		else:
#			right_ptr += 1
#	print("returning")
#	return smallest

def gte(left,right):
	"""
	We want frequencies in left to be gte right frequencies
	"""
	for k,v in right.items():
		exists = left.get(k)
		if exists is None or exists < v:
			return False
#	print(left,right)
	return True

def minimum_window(S,T):
	"""
	1. move right pointer forward until
		we have a frequency match with T.
		Memoize minimum
	2. move left forward ahead by one until we dont
		have a frequency match.
	3. move right forward until we have another frequency match.
		compare against initial match.
	"""
	T_freqs = dict(Counter(T))
	S_len = len(S)
	left_ptr,right_ptr = 0,0
	subset_freqs = defaultdict(int)
	smallest = None
	while right_ptr < S_len:
		lval = S[left_ptr]
		if lval in T_freqs:
			subset_freqs[lval] += 1
		if right_ptr <= left_ptr:
			right_ptr = left_ptr + 1
#		right_ptr = left_ptr + 1
		print("before gte",subset_freqs)
		print(S[left_ptr:right_ptr+1])
		while not gte(subset_freqs,T_freqs):
			if right_ptr >= S_len:
				return smallest
			rval = S[right_ptr]
			if rval in T_freqs:
				subset_freqs[rval] += 1
			right_ptr += 1
		cand = S[left_ptr:right_ptr+1]
		print(cand,left_ptr,right_ptr)
		if smallest is None:
			smallest = cand
		else:
			if len(cand) < len(smallest):
				smallest = cand
		if lval in T_freqs:
			print("????",lval,S[left_ptr])
			subset_freqs = defaultdict(int)
#			subset_freqs[lval] -= 1
#			if subset_freqs[lval] == 0:
#				subset_freqs.pop(lval)
		else:
			print("LVAL",left_ptr,lval)	
		left_ptr += 1
	return smallest

def minimum_window(S,T):
	if not T or not S:
		return ""
	dict_t = Counter(T)
	required = len(dict_t)
	left,right,formed = 0,0,0
	window_counts = {}
	ans = float("inf"),None,None
	while right < len(S):
		char = S[right]
		window_counts[char] = window_counts.get(
					char,0) + 1
		if char in dict_t and (
			window_counts[char] == dict_t[
				char]):
			formed += 1
		while left <= right and formed == required:
			char = S[left]
			if right - left + 1 < ans[0]:
				ans = (right - left + 1,
					left,right)
			window_counts[char] -= 1
			if char in dict_t and (
				window_counts[
				char] < dict_t[char]):
				formed -= 1
			left += 1
		right += 1
	return "" if ans[0] == float("inf") else S[ans[1]:ans[2]+1]

if __name__ == "__main__":
	S1 = "ADOBECODEBANC"
	T1 = "ABC"
	e1 = "BANC"
	r1 =minimum_window(S1,T1)
	print(r1, r1 == e1) 
	S2 = "ADOBECODEBANC"
	T2 = "ZOBE"
	e2 = ""
	r2 = minimum_window(S2,T2)
	print(r2, r2 == e2)
	S3 = "ADOBECODEBANC"
	T3 = "NAC"
	e3 = "ANC"
	r3 = minimum_window(S3,T3)
	print(r3,r3 == e3)
	S4 = "ADOBECODEBANC"
	T4 = "ONAC"
	e4 = "ODEBANC"
	r4 = minimum_window(S4,T4)
	print(r4, r4 == e4)
	S5 = "A"
	T5 = "A"
	e5 = "A"
	r5 = minimum_window(S5,T5)
	print(r5, r5 == e5)
	S6 = "AA"
	T6 = "AA"
	e6 = "AA"
	r6 = minimum_window(S6,T6)
	print(r6, r6 == e6)
	S7 = "A"
	T7 = "B"
	e7 = ""
	r7 = minimum_window(S7,T7)
	print(r7)
	S8 = "AB"
	T8 = "A"
	r8 = minimum_window(S8,T8)
	print(r8 == "A")
	S9 = "ODEBANC"
	T9 = "ON"
	R9 = minimum_window(S9,T9)
	print(R9 == "ODEBAN")
	S10 = "BBA"
	T10 = "BA"
	R10 = minimum_window(S10,T10)
	print(R10,R10 == "BA")
	S11 = "CABWEFGEWCWAEFGCF"
	T11 = "CAE"
	R11 = minimum_window(S11,T11)
	print(R11,R11 == "CWAE")

