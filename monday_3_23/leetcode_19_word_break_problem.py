"""
Given a non-empty string S
and a dictionary WORDS
containing a list of non-empty words,
determine if S can be segmented into a space-separated
sequence of one or more dictionary words.
The same word may be reused. The dictionary
does not contain duplicate words.
"""


def kickoff(S,words):
	"""
	Yesterday's work towards a recursive solution
	solved the problem from a correctness basis,
	but had exponential complexity. Let's try a BFS instead.
	Let's discuss a BFS for this. How would we implement a BFS here?
	We need a queue - do we put single characters in the queue?
	Do we put matching words? hmm. Let's go with indices,
	taking a similar approach to what we did in the initial recursive problem.
	"""

	S_len = len(S)
	visited = [0]*S_len
	## representing the first index
	queue = [0]
	while queue:
		start_idx = queue.pop(0)
		if visited[start_idx] == 0:
			for i in range(start_idx+1, S_len+1):
				candidate = S[start_idx:i]
				if candidate in words:
					queue.append(i)
					if i == S_len:
						return True

			visited[start_idx] = 1
	return False


if __name__ == "__main__":
	s1 = "leetcode"
	d1 = ["leet","code"]
	e1 = True
	r1 = kickoff(s1,d1)
	print(r1, r1 == e1)	
	s2 = "applepenapple"
	d2 = ["apple","pen"]
	e2 = True
	r2 = kickoff(s2,d2)
	print(r2, r2 == e2)	
	s3 = "catsandog"
	d3 = ["cats","dog","and","sand","cat"]
	e3 = False
	r3 = kickoff(s3,d3)
	print(r3, r3 == e3)
	s4 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
	d4 = ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
	e4 = False
	r4 = kickoff(s4,d4)
	print(r4, r4 == e4)

