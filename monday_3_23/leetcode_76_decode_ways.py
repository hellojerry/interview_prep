"""
A message containing the letters A-Z
is being encoded to numbers using
the following mapping:

"A" ->1
"B"->2
...
"Z"->26

Given a non-empty string containing only digits,
determine the total number of ways to decode it.

a1 = "12"
e1 = 2
(could be decoded as "AB" (1 2) or "L" (12))

a2 = "226"
e2 = 3
(could be decoded as "BZ"(2 26), or "VF" (22 6)
or "BBF" (2 2 6))
"""
import string

def decode(num_str):
	"""
	So first, let's see if we can identify a choice via
	a recursive solution. Additionally, problem analysis:
	1. any number over 26 is going to have at least two
	combinations
	Logically, does 2 -> AA? from the examples provided in
	the question, "12" could be C..something, and that isn't
	the case. So the "number" component of this is a red herring.
	It is one set of symbols to another. Before anything else 
	we need a mapping of those letters.

	OK, back to analysis. If I look at a basic example
	with an eye scan, my thinking is scanning each digit
	and getting a mapping that way for one combination,
	and then each pair of digits in a window gets a mapping.
	pairs and singles aren't mutually exclusive, 
	so "226" could be "2,2,6", "22,6","2,26".
	So our scan only has to check the current value (for a mapping), then prospective forward 1. How do we account for "305"? Can
	does "05" = 5? I'm going to work under the assumption
	that it doesn't because having "05" map to "5" leads
	to data loss. Does a simple nested loop work for this?
	I feel like I'm missing something critical in here.
	In terms of a case analysis:
		first iteration: if a digit > 0, add 1
		second iteration: if prior:digit in 1-26, add 1
		third iteration 
	This straight up isn't working
	"""
	mappings = {}
	for ind, val in enumerate(string.ascii_uppercase):
		mappings[val] = str(ind+1)
	reversed_mappings = {v:k for k,v in mappings.items()}
	strlen = len(num_str)
	ct = 0
	prior = ""
	for primary in range(strlen):
		el = num_str[primary]
		if el == "0" and ct == 0:
			return 0
		if el == "0" and ct > 0:
			if prior+el in reversed_mappings:
				continue
		ct += 1
		if prior and prior+el in reversed_mappings:
			ct += 1
		else:
			ct -= 1
		prior = el
	return ct	

def decode(num_str):
	"""
	Let's try this again. We need some form of memoization
	here in an array. Where I'm confused is how to model
	this problem, at all. This might be some sort
	of levenshtein thing but I don't see much
	of a resemblance except for string fuckery.
	It is kinda tricky to tease out what the basic logic 
	is here. Let's write down all the rules.
	1. if it starts with a zero, return 0 immediately.
	How do we achieve this state for short-circuiting?
	2. if this value and the next value combined
	are in the dictionary,
		if val == 2 and 0 < next < 7:
			we have two paths to add
			(this,next) and this+next,skip ahead
		elif val == 2 and (next == 0 or next > 6):
			one path to add (this,next),skip ahead
		if val == 1 and next > 0:
			two paths to add 
			(this,next) and this+next, skip ahead
		if val == 1 and next == 0:
			one path to add (this+next), skip ahead
	else:
		one path to add, iterate forward one
	else:
		return 0 (something's broken) 
	This isn't working either. fuck
	"""
	reversed_mappings = {}
	for ind, val in enumerate(string.ascii_uppercase):
		reversed_mappings[str(ind+1)] = val
	strlen = len(num_str)
	ct = 0
	i = 0
	while i < strlen:
		el = num_str[i]
		if i == 0:
			if el == "0":
				return 0
		if el == "0" and num_str[i-1] not in ["1","2"]:
			return 0
		candidate = num_str[i:i+2]
		if candidate in reversed_mappings and len(candidate) > 1:
			print(candidate)
			if el == "2" and (candidate[1] 
				in ["1","2","3","4","5","6"]):
				ct += 2
				i += 2
			elif el == "2" and (candidate[1]
				 == "0"):
				ct += 1
				i += 2
			elif el == "1" and (candidate[1]
				in ["1","2","3","4","5",
				"6","7","8","9"]):
				ct += 2
				i += 2
			elif el == "1" and (candidate[1]
				== "0"):
				ct += 1
				i += 2
			else:
				ct += 1
				i += 1
		else:
			if len(candidate) > 1:
				if el == "2" and (candidate[1]
					in ["7","8","9"]):
					ct += 1
					i += 2
				elif el in reversed_mappings and (
					candidate[1] in reversed_mappings):
					ct += 1
					i += 2
				else:
					ct += 1
					i += 1
			else:
				if num_str[i-1] == "0":
					i += 1
				else:
					ct += 1
					i += 1
			
	return ct


def decode(num_str):
	"""
	So a case analysis thing got us close, but 
	we still aren't quite there. If we use a cache here,
	like at prior array indices, would that work? Like,
	Let's say for "110".
	If we have a count for used up on the cache,
	so initially [0,0,0]
	let's modify this a bit. we do this +prior to get an invalidation check as well
	So hmm. I think we're onto something here.
	We're a bit disorganized though and need to collect thoughts.
	proposed logic:
	run 1: won't work
	if current = valid:
		cache[current] = 1
		if prior+current not valid:
			cache[current] -= 1
	else:
		cache[current] -= 1
		if prior+current == valid:
			cache[current] += 1
	run 2:
	if cache[ind-1] < -1:
		return 0
	if current not valid:
		cache[ind] -= 1
	else:
		cache[ind] += 1
	if prior + current not valid:
		cache[ind] -= 1

	"""
	reversed_mappings = {}
	for ind, val in enumerate(string.ascii_uppercase):
		reversed_mappings[str(ind+1)] = val
	strlen = len(num_str)
	cache = [0]*strlen
	for ind,val in enumerate(num_str):
		cur_legal = val in reversed_mappings
		
		if ind == 0:
			if cur_legal:
				cache[ind] = 1
			else:
				return 0

		else:
			if cache[ind-1] < -1:
				return 0
			prior = num_str[ind-1]
			prior_legal = prior+val in reversed_mappings
			if prior_legal:
				cache[ind] += 1
			if cur_legal:
				cache[ind] += 1
			if prior_legal and cur_legal and cache[
					ind-1] > 0:
				cache[ind] -= 1
			if not cur_legal:
				print(cache,num_str,val)
				cache[ind] -= 1
				if prior_legal and sum(cache) > 1:
					cache[ind] -= 1
				
			if not prior_legal:
				cache[ind] -= 1
		#	if cache[ind] > 1:
		#		cache[ind] = 1
	print(cache)
	if sum(cache) >= 0:
		return sum(cache)
	return 0
## so those approaches didnt work either.
## the memoization helps, but fuck this is stupidly difficult.
## HOW IS THIS A MEDIUM?!

def decode(S):
	"""
	Let's try a further backtracking thing
	"""
	strlen = len(S)
	if strlen == 0:
		return 0
	memo = [0]*(strlen+1)
	memo[0] = 1
	memo[1] = 0 if S[0] == "0" else 1
	for i in range(2, len(memo)):
		if S[i-1] != "0":
			memo[i] += memo[i-1]
		two = int(S[i-2:i])
		if two >= 10 and two <= 26:
			memo[i] += memo[i-2]
	return memo[-1]


if __name__ == "__main__":
	a1 = "12"
	e1 = 2
	r1 = decode(a1)	
	print(a1,r1,r1 == e1)
	a2 = "226"
	e2 = 3
	r2 = decode(a2)
	print(a2,r2,r2 == e2)
	a3 = "0"
	e3 = 0
	r3 = decode(a3)
	print(a3,r3, r3 == e3)
	a4 = "305"
	e4 = 0
	r4 = decode(a4)
	print(a4,r4, r4==e4)
	a5 = "20"
	e5 = 1
	r5 = decode(a5)
	print(a5,r5, r5==e5)
	a6 = "27"
	e6 = 1
	r6 = decode(a6)
	print(r6,r6==e6)
	a7 = "1"
	e7 = 1
	r7 = decode(a7)
	print(r7, r7 == e7)
	a8 = "2"
	e8 = 1
	r8 = decode(a8)
	print(r8, r8 == e8)
	a9 = "99"
	e9 = 1
	r9 = decode(a9)
	print(a9,r9, r9 == e9)
	a10 = "101"
	e10 = 1
	r10 = decode(a10)
	print(a10,r10, r10 == e10)
	a11 = "110"
	e11 = 1
	r11 = decode(a11)
	print(a11,r11, r11 == e11)
	a12 = "1101"
	e12 = 1
	r12 = decode(a12)	
	print(a12,r12, r12 == e12)
	a13 = "1101101"
	r13 = decode(a13)
	print(r13)
