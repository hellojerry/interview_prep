"""
There is a new alien dictionary with uses
the latin alphabet. The order among letters
is unknown. You receive a list of non-empty
words from the dictionary, where words
are sorted lexicographically by the rules of
the new language. Derive the order
of letters in the language.

ex = [
	"wrt",
	"wrf",
	"er",
	"ett",
	"rftt"
]
e1 = "werf"

a2 = ["z","x"]
e2 = "zx"

a3 = ["z","x","z"]
e3 = "" (invalid)
if a is a prefix of B, A must appear
before b in a dictionary.
if order is invalid, return an empty string
"""
from collections import defaultdict

VISITED = 0
UNVISITED = 1
VISITING = 2


def build_adjacency_list(arr):
	"""
	shortest string needs to come before the longer one.
	"applecart" is AFTER "apple". therefore, "abc" before
	"ab" indicates no valid lexicographical order,
	where "wrt" before "wrtkj" indicates that there is a valid
	lexicographical order.

	"""
	adj = defaultdict(set)
	ptr = len(arr) - 2
	seen = set()
	all_invalid = True
	while ptr >= 0:
		## if prior is longer than cur, valid lexicographical
		## order, assuming matching letters.
		## if cur is longer than prior, INVALID lexicographical order.
		prior = arr[ptr+1]
		cur = arr[ptr]
		sub_idx = 0
		invalid_ordering = len(cur) > len(prior)
		broken_for_equality = False
		while (sub_idx < len(prior) and sub_idx < len(
				cur)) and (prior[
				sub_idx] == cur[sub_idx]): 
			seen.add(prior[sub_idx])
			sub_idx += 1
		if sub_idx < len(prior) and sub_idx < len(cur
			) and prior[sub_idx] != cur[sub_idx]:
			broken_for_equality = True
		## we can determine a lexicographical order if broken for equality.
		if broken_for_equality:
			all_invalid = False
			adj[prior[sub_idx]].add(cur[sub_idx])
			seen.add(cur[sub_idx])
			seen.add(prior[sub_idx])
		else:
			if not invalid_ordering:
				all_invalid = False
		cur_idx = sub_idx
		prior_idx = sub_idx
		while cur_idx < len(cur):
			seen.add(cur[cur_idx])
			cur_idx += 1
		while prior_idx < len(prior):
			seen.add(prior[prior_idx])
			prior_idx += 1
		ptr -= 1
	for item in seen:
		if item not in adj:
			adj[item] = set()
	return (adj,all_invalid)	


def topological_sort(key,adj,visited,out):
	if visited[key] == VISITING:
		return False
	if visited[key] == VISITED:
		return True
	visited[key] = VISITING
	for node in adj[key]:
		if not topological_sort(node,adj,visited,out):
			return False
	out.append(key)
	visited[key] = VISITED
	return True


def kickoff(arr):
	"""
	The only challenge in this is building the adjacency list.
	my hands are cold.
	"""
	adjacency_list,all_invalid = build_adjacency_list(arr)
	if all_invalid and len(arr) > 1:
		return ""
	if len(arr) == 1:
		return arr[0]
	visited = {k:UNVISITED for k,v in adjacency_list.items()}
	out = []
	for key in adjacency_list:
		if not topological_sort(
			key,adjacency_list,visited,out):
			return ""
	return "".join(out)
		


if __name__ == "__main__":
	a1 = [
		"wrt",
		"wrf",
		"er",
		"ett",
		"ettr",
		"rftt"
	]
	from pprint import pprint
	r1 = kickoff(a1)
	e1 = "wertf"
	print(r1, e1 == r1)
	a2 = ["z","x"]
	r2 = kickoff(a2)
	e2 = "zx"
	print(r2, r2 == e2)
	a3 = ["z","x","z"]
	e3 = ""
	r3 = kickoff(a3)
	print(r3,r3 == e3)
	a4 = ["z","z"]
	r4 = kickoff(a4)
	e4 = "z"
	print(r4, r4 == e4)
	a5 = ["ab","adc"]
	r5 = kickoff(a5)
	print(r5)
	a6 = ["abc","ab"]
	r6 = kickoff(a6)
	print(r6)
	a7 = ["wrt","wrtkj"]
	r7 = kickoff(a7)
	print(r7)
	a8 = ["wlnb"]
	r8 = kickoff(a8)
	print(r8)	
