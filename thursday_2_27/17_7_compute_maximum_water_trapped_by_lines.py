"""
An array of integers naturally defines a set of lines
parallel to the Y-axis, starting from x=0:


               |
             | |
         | | | |                 |
       | | | | |     |   |       |
   |   | | | | | |   |   | |   | |
 | | | | | | | | | | | | | | | | | |

which can be represented as:

[1,2,1,3,4,4,5,6,2,1,3,1,3,2,1,2,4,1]




|           |
|     | |   | |
|   | | | | | | | | |
| | | | | | | | | | | |

[4,1,2,3,3,2,4,3,2,2,2,1]
12
22
24
expected = 24

The maximum area that can be trapped (think of water) by two lines
is between 4 (height 4) and 16(height 4), because 12*4 = 48.
If we picked, for 7 and 16, we could only trap 4*9 = 36.

Write a program that takes an integer array and returns the pair
of entries that trap the maximum amount of water.
"""

def get_maximum_amount(arr):
	"""
	This vaguely seems like a bsearch.
	We take the left pointer, the right pointer,
	multiply distance by the lower height to get area.
	1. start at far right and far left. get area.
	2. move left forward and get area against initial left. if no change, move right forward.
	3. move right forward and get area against initial left. if no change, move left forward
	"""
	arrlen = len(arr)
	max_right_idx = arrlen-1
	max_left_idx = 0
	max_area = 0
	left_ptr,right_ptr = 0, arrlen - 1
	while left_ptr < right_ptr:
		left_val,right_val = arr[left_ptr],arr[right_ptr]
		distance = right_ptr - left_ptr
		area = distance*min(left_val,right_val)
		if area > max_area:
			max_area = area
			max_left_idx = left_ptr
			max_right_idx = right_ptr
		if left_val < right_val:
			left_ptr += 1
		else:
			right_ptr -= 1

	return (max_left_idx,max_right_idx,max_area)

if __name__ == "__main__":
	arr =  [1,2,1,3,4,4,5,6,2,1,3,1,3,2,1,2,4,1]
	res = get_maximum_amount(arr)
	assert(res == (4,16,48))	
	arr = [4,1,2,3,3,2,4,3,2,2,2,1]
	res = get_maximum_amount(arr)
	assert(res == (0,6,24))
