"""
When you type keywords in a search engine,
the search engine will return results, and each result contains
a digest of the web page, i.e. highlighting within
that page of the keywords that you searched for. For example,
a search for the keywords "Union" and "save" on a page
with the text of the emancipation proclamation should return the following

```
My paramount object in this struggle is to SAVE the UNION, and is not
either to save or to destroy slavery. If I could save the Union without
freeing any slave I would do it, and if I could save it by freeing
all the slaves I would do it, and if I could save it by freeing
some and leaving others alone I would also do that.
```

The digest for this page is the text in boldface, with the keywords underlined
for emphasis. It is the shortest substring of the page which contains all the keywords
in the search. The problem of computing the digest is abstracted as follows:

Write a program which contains an array of strings and a set of strings, and 
returns the indices of the starting and ending index of a shortest subarray of the given array
that "covers" the set, i.e. contains all strings in the set.
"""
import bisect
def find_smallest_subarray(str_arr, my_set):
	"""
	work through the array. If we hit an element in
	the set, start turn a boolean on, subtract the visited element
	from a temporary set, iterate until we find another element
	in the set, rinse and repeat until we've wiped out all elements in
	the set. preserve the start index and the end index with temp variables,
	as well as a distance. if we find a repeated element of the set,
	cut the counter short.

	if we haven't see
	What are the scenarios when we've seen one of the strings and hit a second one?
		1. second string is first string: move index to second string
		2. second string is first string, but we've grabbed other intermediaries:
			reorder results so that they start with first intermediary seen
		3. all intermediaries seen, last string closes out:
			make entry in a hashmap with distance from first index to final one.
			empty intermediaries array.
		upon completion, return the highest value in the hashmap
	I think the complexity with this is a bit worse than o(n).
	It gets a bit nastier when we've got a very large target set.
	"""
	seen_intermediaries = {}
	my_set_len = len(my_set)
	out_arr = []
	for ind,val in enumerate(str_arr):
		if val in my_set:
			if val in seen_intermediaries:
				seen_intermediaries[val] = ind
			else:
				## this means close it off
				if len(seen_intermediaries) == my_set_len - 1:
					
					seen_intermediaries[val] = ind
					min_ = None
					max_ = None
					for k,v in seen_intermediaries.items():
						if min_ is None:
							min_ = v
						if max_ is None:
							max_ = v
						min_ = min(min_,v)
						max_ = max(max_,v)
					bisect.insort(out_arr, max_ - min_)
					seen_intermediaries = {}
				else:
					seen_intermediaries[val] = ind

	if out_arr:
		return out_arr[0]
	return -1

if __name__ == "__main__":
	text = """
	My paramount object in this struggle is to save the Union, and is not
	either to save or to destroy slavery. If I could save the Union without
	freeing any slave I would do it, and if I could save it by freeing
	all the slaves I would do it, and if I could save it by freeing
	some and leaving others alone I would also do that.
	"""
	as_arr = text.replace("\n","").replace("\t","").split(" ")
	res = find_smallest_subarray(as_arr,{"save","Union"})
	assert(res == 2)
	t2 = """
	itty bitty baby itty bitty twinkie baby boat
	boat bitty baby itty boat baby
	"""
	as_arr_2 = t2.replace("\n","").replace("\t","").split(" ")
	r2 = find_smallest_subarray(as_arr_2, {"itty","baby","boat"})
	assert(r2 == 2)
	t3 = """
	boom boom boom boom I want you in my room
	lets spend the night together from now until forever
	boom boom boom boom I want to go boom boom
	lets spend the night together together in my room
	"""
	as_arr_3 = t3.replace("\n","").replace("\t","").split(" ")
	r3 = find_smallest_subarray(as_arr_3, {"room","together","boom"})
	assert(r3 == 9)
