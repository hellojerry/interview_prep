"""
A palindromic string is one which reads the same forwards
and backwards, i.e. "racecar".
In this program, you are to write a program which determines
if the decimal representation of an integer is a palindromic string.

ex:
	0,1,7,11,121,333,2147447412 are all true
	-1,12,100,213213491399 are all false
"""

def reverse_digits(num):
	reverse,remaining = 0, abs(num)
	while remaining:
		remaining,remainder = divmod(remaining, 10)
		reverse *= 10
		reverse += remainder
	return reverse


def is_palindrome(num):
	"""
	one way to do this is to reverse the digits,
	and then see if initial - reversed = 0.
	"""
	if num < 0:
		return False
	if num < 10:
		return True
	return reverse_digits(num) - num == 0
	


if __name__ == "__main__":
	trues = [0,1,7,11,121,333,2147447412]
	for i in trues:
		assert(is_palindrome(i) == True)

	falses =[-1,12,100,213213491399]
	for i in falses:
		assert(is_palindrome(i) == False)
