"""
Suppose you are designing a search engine. In additino to getting
keywords from a page's content, you would like yo get keywords from URLs.
For example bedbathandbeyond.com yields the keywords 

"bed","bath","beyond","bat","hand"

The first coming from the decomposition "bed","bath","beyond"
and the latter coming from the decomposition "bed" "bat" "hand" "beyond".

Given a set of strings and a name, design an efficient algorithm
that checks whether the name is the concatenation of a sequence of dictionary
words. If such a concatenation exists, return it. A dictionary word
may appear more than once in the sequence.

For example, "a" "man" "a" "plan" "a" "canal" is a valid sequence for 
"amanaplanacanal".

So I don't know if I quite understand the question.
let's just solve this for bedbathandbeyond.
"""

def word_break(word_list,word):
	if word == "":
		return True
	else:
		word_len = len(word)
		return any([(word[:i] in word_list) and word_break(
				word_list,word[i:]) for i in range(1, word_len+1)])

def get_list(name,dictionary,start):
	namelen = len(name)
	for i in range(start+1,namelen+1):
		prefix = name[start:i]
		if prefix in dictionary:
			if namelen == i:
				return [prefix]
			words = get_list(name,dictionary,i)
			print(words)
			if words:
				return [prefix] + words	

	return False
if __name__ == "__main__":
	dictionary = {"bed","and","bath","be","bat","hand","beyond"}
	name = "bedbathandbeyond"
	#print(word_break(dictionary,name))
	print(get_list(name,dictionary,0))
