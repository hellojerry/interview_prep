"""
Implement a sudoku solver.

First, let's get a pre-populated sudoku board.

This is a legal sudoku starting board.
arr_legal = [
	[5,3,0,0,7,0,0,0,0],
	[6,0,0,1,9,5,0,0,0],
	[0,9,8,0,0,0,0,6,0],
	[8,0,0,0,6,0,0,0,3],
	[4,0,0,8,0,3,0,0,1],
	[7,0,0,0,2,0,0,0,6],
	[0,6,0,0,0,0,2,8,0],
	[0,0,0,4,1,9,0,0,5],
	[0,0,0,0,8,0,0,7,9]
]
Note that in sudoku, the following rules must be satisfied.
1. all entries are between 1 and 9.
2. each row must have a unique value
3. each column must have a unique value.
4. each 9-element section (there are 9 in total) must have unique values.
"""



def is_legal(matrix):
	rows = len(matrix)

	for row in matrix:
		## col check
		remaining = set(range(1,10))
		for col in row:
			val = col
			if val != 0 and val not in remaining:
				return False
			remaining -= {val}
	for col in range(9):
		remaining = set(range(1,10))
		for row in range(9):
			val = matrix[row][col]
			if val != 0 and val not in remaining:
				return False
			remaining -= {val}
	for i in [(0,3),(3,6),(6,9)]:
		for j in [(0,3),(3,6),(6,9)]:
			remaining = set(range(1,10))
			for k in range(i[0],i[1]):
				for l in range(j[0],j[1]):
					val = matrix[k][l]
					if val != 0 and (val
						not in remaining):
						return False
					remaining -= {val}
	return True


	


def sudoku_solver(matrix):
	"""
	This is vaguely like the n-queens problem.
	The test is slightly different. instead of having diagonality 
	in the test, we need to have a err... quadrant? test.
	iterate over rows and cols. maintain a set for the row.
	maintain a per column set. For each element,
	do a quadrant check on the remaining values in the set.
	Eight queens just iterates through the rows, and
	keeps track of the columns in-place
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	nonzero_ct = 0
	for row in range(rows):
		for col in range(cols):
			if matrix[row][col] == 0:
				for i in range(1,10):
					matrix[row][col] = i
					if is_legal(matrix) and (
						sudoku_solver(matrix)):
						return True
					else:
						matrix[row][col] = 0
				return False
			else:
				nonzero_ct += 1
	if nonzero_ct == 81:
		return True
	return False

if __name__ == "__main__":	
	arr_legal = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	sudoku_solver(arr_legal)
	print(arr_legal)
	print(is_legal(arr_legal))
	for row in arr_legal:
		print(row)
	expected = [ 
		[5, 3, 4, 6, 7, 8, 9, 1, 2],
		[6, 7, 2, 1, 9, 5, 3, 4, 8],
		[1, 9, 8, 3, 4, 2, 5, 6, 7],
		[8, 5, 9, 7, 6, 1, 4, 2, 3],
		[4, 2, 6, 8, 5, 3, 7, 9, 1],
		[7, 1, 3, 9, 2, 4, 8, 5, 6],
		[9, 6, 1, 5, 3, 7, 2, 8, 4],
		[2, 8, 7, 4, 1, 9, 6, 3, 5],
		[3, 4, 5, 2, 8, 6, 1, 7, 9]
	]
	assert(arr_legal == expected)
