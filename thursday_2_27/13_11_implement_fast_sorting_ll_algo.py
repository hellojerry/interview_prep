"""
Implement a routing which sorts linked lists efficiently.
It should be a stable sort, i.e. the relative positions
of equal elements must remain unchanged.



i.e. 
	   a	b   c     d    a  d     b  c
	5->1->9->3->3->2->1 => 1->1->2->3->3->5->9

"""


class Node(object):
	def __init__(self,val,marker,next_=None):
		assert(type(val) == int)
		assert(type(marker) == str and len(marker) > 0)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.marker = marker
		self.next_ = next_
	def __str__(self):
		return "Node(%d,%s)" % (self.val,self.marker)
	def __repr__(self):
		return "Node(%d,%s)" % (self.val,self.marker)

def display(head):

	out = []
	cur = head
	while cur:
		out.append(cur)
		cur = cur.next_

def insert_into_sorted_ll(head,node):
	"""
	We've already accounted for the tail, so
	all we've got to do here is check for next_ being gt cur.
	"""
	cur = head
	while cur:
		next_ = cur.next_
		if next_.val > node.val:
			cur.next_ = node
			node.next_ = next_
			return
		cur = next_
	raise Exception("bad!!")

def ll_sort(head):
	"""
	hmm. So if i keep track of a min head and a min tail...
	if val < min_head:
		node.next_ = min_head
		min_head = val
	elif node > min_tail:
		node.next = none
		min_tail.next = node
		min_tail = node.next
	the final category is when we're in between those two.
	we have to iterate from min_head until we're lte next_
	"""
	min_head = head
	min_tail = head
	cur = head
	while cur:
		next_ = cur.next_
		if cur.val >= min_tail.val:
			min_tail.next_ = cur
			min_tail = min_tail.next_
			min_tail.next_ = None
		elif cur.val < min_head.val:
			cur.next_ = min_head
			min_head = cur
		else:
			insert_into_sorted_ll(min_head,cur)
		cur = next_
	return min_head


if __name__ == "__main__":
	nd1 = Node(1,"d")
	n2 = Node(2,"z",nd1)
	n3c = Node(3,"c",n2)
	n3b = Node(3,"b",n3c)
	n9 = Node(9,"z",n3b)
	na1 = Node(1,"a",n9)
	n5 = Node(5,"z",na1)
	result = ll_sort(n5)
	cur = result
	out = []
	while cur:
		out.append(cur)
		cur = cur.next_
	expected = [na1, nd1, n2,n3b,n3c,n5,n9]
	assert(out == expected)
