"""
Given a sorted array, the number of BSTs 
that can be build on the entries in the array
grows enormously with its size. Some of these trees
are skewed, and are closer to lists; others are more
balanced.

i.e. 

	[1,2,3,4,5,6,7,8,9]

		7
            3        11
          2   5     9     13
         1   4 6   8 10 12   14
"""

class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val


def build_bst_from_sorted_array(arr):
	"""
	So I think the trick here is to do a bsearch of sorts.
	root = midpoint
	root.left = arr[:midpoint]
	right.right = arr[midpoint+1:]
	"""
	arrlen = len(arr)
	if arrlen < 1:
		return None
	if arrlen == 1:
		return TreeNode(arr[0])
	midpoint = int(arrlen/2)
	root = TreeNode(arr[midpoint])
	root.left = build_bst_from_sorted_array(arr[:midpoint])
	root.right = build_bst_from_sorted_array(arr[midpoint+1:])
	return root

def preorder(root,out):
	if not root:
		return None
	out.append(root.val)
	preorder(root.left,out)
	if root.right:
		print(root.right)
	preorder(root.right,out)


def get_bst_height(root):
	if root is None:
		return 0
	lhs = get_bst_height(root.left) + 1
	rhs = get_bst_height(root.right) + 1
	return max(lhs,rhs)

if __name__ == "__main__":
	
	arr = [1,2,3,4,5,6,7,8,9]
	root = build_bst_from_sorted_array(arr)
	out = []
	preorder(root,out)
	assert(get_bst_height(root) == 4)
	arr = list(range(1,14))
	root = build_bst_from_sorted_array(arr)
	assert(get_bst_height(root) == 4)
	
