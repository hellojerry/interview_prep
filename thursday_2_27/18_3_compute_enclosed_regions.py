"""
Imagine a 2d array whose entries are either W or B.
Write a program that takes A, and replaces all W's that
cannot reach the boundary via other Ws with a B.

ex:

arr = [
	[B,B,B,B],
	[W,B,W,B],
	[B,W,W,B],
	[B,B,B,B]
]
result = [
	[B,B,B,B],
	[W,B,B,B],
	[B,B,B,B],
	[B,B,B,B]
]

arr = [
	[B,B,W,B,B],
	[B,B,B,W,B],
	[B,W,W,B,B],
	[B,W,B,B,W],
	[B,B,W,B,W]
]
res = [
	[B,B,W,B,B],
	[B,B,B,B,B],
	[B,B,B,B,B],
	[B,B,B,B,W],
	[B,B,W,B,W]
]

Hint: it's easier to compute the complement of the result.
"""

def recursive_mark(matrix,secondary_matrix,row,col):
	rows = len(matrix)
	cols = len(matrix[0])
	left_border,up_border = 0,0
	down_border, right_border = rows - 1, cols - 1
	up_eligible = row > up_border
	down_eligible = row < down_border
	left_eligible = col > left_border
	right_eligible = col < right_border
	## first, exit if already marked - avoid backtracking.
	if secondary_matrix[row][col] != -1:
		return
	if matrix[row][col] == "B":
		secondary_matrix[row][col] = 0
		return
	secondary_matrix[row][col] = 1
	if left_eligible:
		recursive_mark(matrix,secondary_matrix,row,col-1)
	if right_eligible:
		recursive_mark(matrix,secondary_matrix,row,col+1)
	if up_eligible:
		recursive_mark(matrix,secondary_matrix,row-1,col)
	if down_eligible:
		recursive_mark(matrix,secondary_matrix,row+1,col)
	return

def black_out_isolates(matrix):
	"""
	So this is a graph problem. I don't think it necessarily needs
	a graph though.
	1. make a secondary matrix.
	iterate over top row, bottom row, left col, and right col
	for row in main matrix:
		for col in main matrix:
			if main[row][col] = B:
				secondary[row][col] = 0
			else:
				recursively mark secondary matrix based on eligibility.
				any white adjacent inbound should have the secondary matrix marked
				as a 1.
	Once we're all the way done, iterate over the secondary matrix, and anything that's not
	marked as a 1 gets marked as a B.
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	secondary_matrix = [[-1]*cols for row in range(rows)]
	for row in [0,rows-1]:
		for col in range(cols):
			if matrix[row][col] == "B":
				secondary_matrix[row][col] = 0
			else:
				recursive_mark(matrix,
					secondary_matrix,row,col)
	for col in [0, cols-1]:
		for row in range(rows):
			if matrix[row][col] == "B":
				secondary_matrix[row][col] = 0
			else:
				recursive_mark(matrix,
					secondary_matrix,row,col)
	for row in range(rows):
		for col in range(cols):
			if secondary_matrix[row][col] != 1:
				matrix[row][col] = "B"
	return matrix


if __name__ == "__main__":
	arr = [
		["B","B","B","B"],
		["W","B","W","B"],
		["B","W","W","B"],
		["B","B","B","B"]
	]
	black_out_isolates(arr)
	expected_result = [
		["B","B","B","B"],
		["W","B","B","B"],
		["B","B","B","B"],
		["B","B","B","B"]
	]
	assert(arr == expected_result)
	arr = [
		["B","B","W","B","B"],
		["B","B","B","W","B"],
		["B","W","W","B","B"],
		["B","W","B","B","W"],
		["B","B","W","B","W"]
	]
	black_out_isolates(arr)
	expected_result = [
		["B","B","W","B","B"],
		["B","B","B","B","B"],
		["B","B","B","B","B"],
		["B","B","B","B","W"],
		["B","B","W","B","W"]
	]
	assert(arr == expected_result)
