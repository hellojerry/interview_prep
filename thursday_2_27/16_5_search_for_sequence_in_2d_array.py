"""
Suppose you are given a 2d array of integers (GRID)
and a 1D array of integers (PATTERN).
The pattern occurs in the grid if it is possible to 
start from some entry in the grid and traverse
adjacent entries in the order specified by the pattern
until all entries in the pattern have been visited.
The entries adjacent to the entry are the ones
directly above, below, to the left, and to the right.

For example. the entries adjacent to [3,4] are [3,3],
[3,5,[2,4], and [4,4]. It is acceptable to visit an entry
in the grid more than once.

ex:
arr = [
	[1,2,3],
	[3,4,5],
	[5,6,7]
]
pattern: [1,3,4,6]
The pattern exists ([0][0], [1][0],[1][1],[2][1])

Write a program that takes as arguments a 2d array and a 1d array,
and checks whether the 1d array occurs in the 2d array based on the rules
described above.
"""

def array_exists_recurse(matrix,pattern,row,col):
	"""
	so my first thought with this is 
	to add in edge eligibility checks.
	after that, scan in each legal direction,
	removing the first element (since it exists).
	once the pattern is empty, we should have a result.
	If we can't get the pattern to be empty, we're done.
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	if len(pattern) == 0:
		return True
	left_boundary,up_boundary = 0,0
	down_boundary, right_boundary = rows - 1, cols - 1
	left_eligible = col > left_boundary
	right_eligible = col < right_boundary
	up_eligible = row > up_boundary
	down_eligible = row < down_boundary
	if matrix[row][col] != pattern[0]:
		return False
	if left_eligible:
		if array_exists_recurse(matrix,pattern[1:],row,col-1):
			return True
	if right_eligible:
		if array_exists_recurse(matrix,pattern[1:],row,col+1):
			return True
	if up_eligible:
		if array_exists_recurse(matrix,pattern[1:], row-1,col):
			return True
	if down_eligible:
		if array_exists_recurse(matrix, pattern[1:], row+1, col):
			return True
	return False

def array_exists_in_matrix(matrix,pattern):
	rows = len(matrix)
	cols = len(matrix[0])
	for row in range(rows):
		for col in range(cols):
			if array_exists_recurse(matrix,pattern,row,col):
				return True
	return False


if __name__ == "__main__":
	arr = [
		[1,2,3],
		[3,4,5],
		[5,6,7]
	]
	pattern = [1,3,4,6]
	res = array_exists_in_matrix(arr,pattern)
	assert(res == True)
	pattern = [4,6,4,6]
	res = array_exists_in_matrix(arr,pattern)
	assert(res == True)
	pattern = [2,3,5,6]
	res = array_exists_in_matrix(arr,pattern)
	assert(res == False)
