"""
The look-and-say sequence starts with 1. 
Subsequent numbers are derived by describing the previous
number in terms of consecutive digits.

ex:

	1,11,21,1211,111221,312211,13112221,1113213211

Write a program that takes as input an integer N and 
returns the Nth integer in the look-and-say sequence.
Return the result as a string.
"""


def look_and_say(num):
	"""
	So the way this works is that for each iteration,
	you check the prior result, and do in-order counts
	of consecutive numbers. 
	"""
	if num == 0:
		return ""
	if num == 1:
		return "1"
	ct = 1
	out = "1"
	tmp_out = []
	while ct < num:
		strlen = len(out)
		prior = None
		tmp_ct = 0
		tmp_ptr = 0
		while tmp_ptr < strlen:
			el = out[tmp_ptr]
			if prior is None:
				prior = el
				tmp_ct = 1
				tmp_ptr += 1
				continue
			if el == prior:
				tmp_ct += 1
			else:
				## need to condense here
				tmp_out.append(str(tmp_ct) + prior)
				tmp_ct = 1
				prior = el
			tmp_ptr += 1
		## handle straggler
		tmp_out.append(str(tmp_ct) + prior)
		## need to join to out here.
		out = "".join(tmp_out)
		tmp_out = []
		ct += 1
	return out


if __name__ == "__main__":
	all_results = [1,11,21,1211,111221,312211,13112221,1113213211]
	r1 = look_and_say(1)
	r2 = look_and_say(2)
	r3 = look_and_say(3)
	r4 = look_and_say(4)
	r5 = look_and_say(5)
	r6 = look_and_say(6)
	r7 = look_and_say(7)
	r8 = look_and_say(8)
	assert(r1 == str(all_results[0]))
	assert(r2 == str(all_results[1]))
	assert(r3 == str(all_results[2]))
	assert(r4 == str(all_results[3]))
	assert(r5 == str(all_results[4]))
	assert(r6 == str(all_results[5]))
	assert(r7 == str(all_results[6]))
	assert(r8 == str(all_results[7]))

