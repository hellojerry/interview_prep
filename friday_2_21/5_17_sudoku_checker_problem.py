"""
Seppuku is a popular logic-based combinatorial
number placement puzzle. The objective is to fill
a 9x9 grid with digis subject to the constraint
that each column, each row, and each of the nine
3x3 subgrids that compose the grid contains
unique numbers in the range of 1->9.
The grid is shown with a partial assignment:

Note that "0" is a stand in for a blank number
arr = [
	[5,3,0,0,7,0,0,0,0],
	[6,0,0,1,9,5,0,0,0],
	[0,9,8,0,0,0,0,6,0],
	[8,0,0,0,6,0,0,0,3],
	[4,0,0,8,0,3,0,0,1],
	[7,0,0,0,2,0,0,0,6],
	[0,6,0,0,0,0,2,8,0],
	[0,0,0,4,1,9,0,0,5],
	[0,0,0,0,8,0,0,7,9]
]

write a function that checks if a partially populated
sudoku board is legal.
"""


def test_pairing(test_arr, el):
	if el > 0:
		test_arr[el-1] += 1
		if test_arr[el-1] > 1:
			return False
	return True

def is_legal(matrix):
	"""
	So the book is saying that there's no
	room for algorithm optimization on this.
	With that in mind, this is pretty simple.
	for each row:
		initialize empty arr with 9 values of zero.
		for each col in row, increment index up by one
		if any(test_arr) > 1:
			return False

	for each col:
		initialize empty arr with 9 values of zero.
		for each row in col, increment index up by one.
		if any(test_arr) > 1:
			return False
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	for row in range(rows):
		test_arr = [0]*9
		for col in range(cols):
			el = matrix[row][col]
			if not test_pairing(test_arr,el):
				print("ilegal dupe row")
				return False
	for col in range(cols):
		test_arr = [0]*9
		for row in range(rows):
			el = matrix[row][col]
			if not test_pairing(test_arr,el):
				print("illegal dupe col")
				return False
	## i am having a serious amount of trouble writing this in a concise
	## way. There is a way to do it, but my brain's just decided to give up.
		
	cases = [(0,3),(3,6),(6,9)]
	for start, end in cases:
		test_arr = [0]*9
		for i in range(start,end):
			for j in range(3):
				el = matrix[i][j]
				if not test_pairing(test_arr,el):
					print("bad block")
					return False
		test_arr = [0]*9
		for i in range(start,end):
			for j in range(3,6):
				el = matrix[i][j]
				if not test_pairing(test_arr,el):
					print("bad block")
					return False
		test_arr = [0]*9
		for i in range(start,end):
			for j in range(6,9):
				el = matrix[i][j]
				if not test_pairing(test_arr,el):
					print("bad block")
					return False
	return True
	


if __name__ == "__main__":
	arr_legal = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	res = is_legal(arr_legal)
	assert(res == True)
	arr_illegal_dupe_row = [
		[5,3,0,0,7,0,0,5,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	res = is_legal(arr_illegal_dupe_row)
	assert(res == False)
	arr_illegal_dupe_col = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,0,4,1,9,0,0,5],
		[5,0,0,0,8,0,0,7,9]
	]
	res = is_legal(arr_illegal_dupe_col)
	assert(res == False)
	arr_illegal_bad_block = [
		[5,3,0,0,7,0,0,0,0],
		[6,0,0,1,9,5,0,0,0],
		[0,9,8,0,0,0,0,6,0],
		[8,0,0,0,6,0,0,0,3],
		[4,0,0,8,0,3,0,0,1],
		[7,0,0,0,2,0,0,0,6],
		[0,6,0,0,0,0,2,8,0],
		[0,0,6,4,1,9,0,0,5],
		[0,0,0,0,8,0,0,7,9]
	]
	res = is_legal(arr_illegal_bad_block)
	assert(res == False)
