"""
Given two singly linked lists there may be list nodes
that are common to both.

Write a program that takes to cycle-free linked lists
and determines if there exists a node common to both lists.

"""

class Node(object):

	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val

	def __repr__(self):
		return "Node(%d)" % self.val

def test_for_overlap(head_left,head_right):
	"""
	All you have to do is get to the final node,
	and test if they're both the same.
	"""

	cur_left = head_left
	cur_right = head_right
	while cur_left.next_:
		cur_left = cur_left.next_
	while cur_right.next_:
		cur_right = cur_right.next_
	return cur_left is cur_right

def get_length(node):
	ct = 0
	while node:
		ct += 1
		node = node.next_
	return ct

def test_for_overlap_returning_first_node(head_left,head_right):
	left_length = get_length(head_left)
	right_length = get_length(head_right)

	## swap them so we don't need to duplicate code
	if right_length > left_length:
		head_left,head_right = head_right,head_left
		left_length,right_length = right_length,left_length
	to_advance = left_length - right_length
	cur_left = head_left
	cur_right = head_right
	while to_advance > 0:
		cur_left = cur_left.next_
		cur_left -= 1
	while cur_left and cur_right:
		if cur_left is cur_right:
			return cur_left
		cur_left = cur_left.next_
		cur_right = cur_right.next_
	return None
	

if __name__ == "__main__":
	## case 1: no intersection.
	n1 = Node(1)
	n2 = Node(2,n1)
	n3 = Node(3,n2)

	n4 = Node(4)
	n5 = Node(5,n4)
	n6 = Node(6,n5)
	res = test_for_overlap(n3,n6)
	assert(res == False)

	r2 = test_for_overlap_returning_first_node(n3,n6)
	assert(r2 is None)
	## case 2: intersection.

	n1 = Node(1)
	n2 = Node(2,n1)
	n3 = Node(3,n2)

	n4 = Node(4,n3)
	n6 = Node(6,n4)
	n5 = Node(5,n3)
	n7 = Node(7,n5)
	res = test_for_overlap(n6,n7)
	assert(res == True)	
	r2 = test_for_overlap_returning_first_node(n6,n7)
	assert(r2 is n3)


	
