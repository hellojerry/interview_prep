"""
Count the number of ways starting at the top left
corner of array and getting to the bottom right corner.
All moves must either go right or down.

matrix = [
	[1,2,3,4,5],
	[6,7,8,9,10],
	[11,12,13,14,15],
	[16,17,18,19,20],
	[21,22,23,24,25]
]

A 5x5 array like the above has 70 possible routes.
"""

def count_number_of_paths(matrix,row,col):
	"""
	This is a dynamic programming question.
	For dynamic programming, we need a "solution"
	point wherein the recursion stops. We also need
	some method of preserving prior steps.
	"""
	rows= len(matrix)
	cols = len(matrix[0])
	max_row = rows - 1
	max_col = cols - 1
	
	if row == max_row and col == max_col:
		## solution case - we've finished this one off.
		return 1
	## next step here is figuring out how we memoize things.
	## the starting point is at 0,0. We need to test for boundaries
	out = 0
	if row < max_row:
		out += count_number_of_paths(matrix,row+1,col)	
	if col < max_col:
		out += count_number_of_paths(matrix,row,col+1)
	return out


def compute_paths_to_xy(matrix,x,y):
	"""
	The other way to do this is to memoize values 
	using a matrix. If we hit the base case of 0,
	return 1. otherwise, mark the location in the matrix
	with the number of paths to get to that point. 
	"""
	if x == y == 0:
		return 1
	if matrix[x][y] == 0:
		ways_up = 0
		ways_left = 0
		if x != 0:
			ways_up = compute_paths_to_xy(matrix,x-1,y)
		if y != 0:
			ways_left = compute_paths_to_xy(matrix,x,y-1)
		matrix[x][y] = ways_up+ways_left
	return matrix[x][y]



if __name__ == "__main__":
	matrix = [
		[1,2,3,4,5],
		[6,7,8,9,10],
		[11,12,13,14,15],
		[16,17,18,19,20],
		[21,22,23,24,25]
	]
	res = count_number_of_paths(matrix,0,0)
	print(res)
	matrix = [
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,0,0,0,0]
	]
	res = compute_paths_to_xy(matrix,4,4)
	print(res)
