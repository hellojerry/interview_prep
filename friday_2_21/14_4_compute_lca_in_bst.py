"""
Since a BST is a specialized binary tree,
the notion of lowest common ancestor, as
expressed in problem 9.4 on page 127, 
holds for BSTs as well.

In general, computing the LCA of two nodes in a BST
is no easier than computing the LCA in a binary tree,
since structurally a binary tree can be viewed
as a BST where all the keys are equal. However, when
the keys are distinct, it's possible to improve
on the LCA algorithms for binary trees.

Design an algorithm that takes as input a BST and two
nodes, and returns the LCA of the two nodes.
For example, for the BST in figure 14.1 on page 211,
and nodes C(3) and G(17), your algorithm should return B(7).
Assume all keys are distinct; nodes do not have references
to their parents.

			19
	7 				43
     3      11			    23         47
   2   5       17                      37         53
              13                    29   41
                                      31
"""

class TreeNode(object):

	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.val

	def __repr__(self):
		return "TreeNode(%d)" % self.val


def get_lca(root,n1,n2):
	"""
	This is a preorder traversal version.
	
	"""
	if root is None:
		return 
	if root is n1 or root is n2:
		return root
	left = get_lca(root.left,n1,n2)
	right = get_lca(root.right,n1,n2)
	if left and right:
		return root
	if left is not None:
		return left
	return right


if __name__ == "__main__":
	n2 = TreeNode(2)
	n5 = TreeNode(5)
	n3 = TreeNode(3,n2,n5)

	n13 = TreeNode(13)
	n17 = TreeNode(17,n13)
	n11 = TreeNode(11,None,n17)
	n7 = TreeNode(7,n3,n11)

	n31 = TreeNode(31)
	n29 = TreeNode(29,None,n31)
	n41 = TreeNode(41)
	n37 = TreeNode(37,n29,n41)
	n23 = TreeNode(23,None,n37)

	n53 = TreeNode(53)
	n47 = TreeNode(47,None,n53)
	n43 = TreeNode(43,n23,n47)
	root = TreeNode(19,n7,n43)
	res = get_lca(root,n3,n17)
	assert(res is n7)
