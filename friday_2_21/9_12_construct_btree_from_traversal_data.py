"""
Many different binary trees
yield the same sequence of keys in an inorder,
preorder, or postorder traversal. However, given an inorder
traversal and one of the other two traversals, there exists
a unique btree that yields those orders,
assuming each node holds a distinct key.

For example, the btree whose inorder traversal sequence is

F,B,A,E,H,C,D,I,G
and whose preorder traversal sequence is
H,B,F,E,A,C,D,G,I
is:

			H
		B		C
	F	   E		     D
		A	                  G
					I

Given an inorder traversal sequence and a preorder traversal
sequence of a binary tree, write a program to reconstruct the tree.
Each node has a unique key.
"""
class TreeNode(object):
	def __init__(self,name,left=None,right=None):
		assert(type(name) == str)
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		self.name = name
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%s)" % self.name

	def __repr__(self):
		return "TreeNode(%s)" % self.name

	def __eq__(self,other):
		if other is None:
			return False
		assert(type(other) == TreeNode)
		return self.name == other.name and self.left == other.left and self.right == other.right

def preorder(root,acc):
	if root is None:
		return None
	acc.append(root)
	preorder(root.left,acc)
	preorder(root.right,acc)
def inorder(root,acc):
	if root is None:
		return None
	inorder(root.left,acc)
	acc.append(root)
	inorder(root.right,acc)

def reconstruct_from_inorder_and_preorder(inorder_arr,preorder_arr):
	"""
	We know that we can get the furthest left element
	from the inorder array, and the root from the inorder array.

	my solution is a bit different from the book's.
	1. since we know that the actual root node
	is at the beginning of the preorder array,
	we can set that as the root.
	2. from there, we've got to find the matching 
	value in the inorder array. Everything to 
	the left of the root in the inorder array
	is the left subtree. Everything to the right
	in the inorder is the right subtree.
	3. from there, we've got the sizes of both subtrees,
	so we grab the preorder left subtree from 
	the chunk of the preorder after the root (equal
	to the size of the inorder left subtree),
	the preorder right subtree from the chunk
	after that, and recursively set root.left
	and root.right.
	"""
	if not preorder_arr:
		return None
	root_val = preorder_arr[0]
	root = TreeNode(root_val)
	root_idx_inorder = inorder_arr.index(root_val)
	inorder_left_subtree = inorder_arr[:root_idx_inorder]
	inorder_root = inorder_arr[root_idx_inorder]
	inorder_right_subtree = inorder_arr[root_idx_inorder+1:]
	inorder_left_subtree_len = len(inorder_left_subtree)
	preorder_left_subtree = preorder_arr[1:inorder_left_subtree_len+1]
	preorder_right_subtree = preorder_arr[inorder_left_subtree_len+1:]
	root.left = reconstruct_from_inorder_and_preorder(
		inorder_left_subtree,preorder_left_subtree)
	root.right = reconstruct_from_inorder_and_preorder(
			inorder_right_subtree,preorder_right_subtree)
	return root	
	
	
	
	

if __name__ == "__main__":
	node_F = TreeNode("F")
	node_A = TreeNode("A")
	node_E = TreeNode("E",node_A)
	node_B = TreeNode("B",node_F,node_E)

	node_I = TreeNode("I")
	node_G = TreeNode("G",node_I)
	node_D = TreeNode("D",None,node_G)
	node_C = TreeNode("C",None,node_D)
	node_H = TreeNode("H",node_B,node_C)
	root = node_H
	acc_1 = []
	## first part of this is just making sure
	## i set the tree up right
	inorder_res_basic = [node_F,
		node_B,
		node_A,
		node_E,
		node_H,
		node_C,
		node_D,
		node_I,
		node_G]
	inorder(root,acc_1)
	assert(acc_1 == inorder_res_basic)
	preorder_res_basic = [
		node_H,
		node_B,
		node_F,
		node_E,
		node_A,
		node_C,
		node_D,
		node_G,
		node_I
	]
	acc_2 = []
	preorder(root,acc_2)
	assert(acc_2 == preorder_res_basic)
	inorder_base = 	["F","B","A","E","H","C","D","I","G"]
	preorder_base = ["H","B","F","E","A","C","D","G","I"]
	res = reconstruct_from_inorder_and_preorder(inorder_base,preorder_base)
	acc_3 = []
	preorder(res,acc_3)
	acc_4 = []
	inorder(res,acc_4)
	assert(acc_3 == preorder_res_basic)
