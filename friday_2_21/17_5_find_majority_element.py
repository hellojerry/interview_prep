"""
You are reading a sequence of strings. You know a priori that
more than half the strings are repetitions of a single string,
but the positions where the majority element occurs are unknown.
Write a program that makes a single pass over the sequence
and identifies the majority element.

ex:

	["b","a","c","a","a","b","a","a","c","a"]
majority element: a
"""

def find_majority_element_brute(arr):
	"""
	My first thought is to push this stuff to a hash table,
	and recording the max.
	"""
	freqs = {}
	top = None
	top_val = 0
	for item in arr:
		exists = freqs.get(item, None)
		if exists is None:
			if top is None:
				top = item
				top_val += 1
			freqs[item] = 1
		else:
			freqs[item] += 1
			if exists + 1 > top_val:
				top_val = exists + 1
				top = item
	return top

def find_majority_element(arr):
	"""
	The problem with the first answer is that 
	it takes up up to o(N) space.
	We can do better I think with a greedy algorithm.
	"""

	top = arr[0]
	top_ct = 1
	for item in arr[1:]:
		if top_ct == 0:
			top = item
			top_ct = 1
		elif item == top:
			top_ct += 1
		else:
			top_ct -= 1
	return top

if __name__ == "__main__":
	arr = ["b","a","c","a","a","b","a","a","c","a"]
	res = find_majority_element_brute(arr)
	expected = "a"
	assert(res == expected)
	res = find_majority_element(arr)
	assert(res == "a")
