"""
Write a program which takes an integer and returns
the integer corresponding to the digits of the input
in reverse order.

Ex:
	input: 42
	output: 24
	input: -314
	output: -413

Hint: how would you solve the problem if the input is a string?
"""

def reverse_digits_easy(integer):
	"""
	This is the first thing that comes to mind.
	I don't think this is in the spirit of the question.
	"""
	if integer < 0:
		return int("-" + str(integer)[1:][::-1])
	else:
		return int(str(integer)[::-1])


def reverse_digits(integer):
	"""
	So I think we can get this by doing some tricks with 
	a modulus on powers of ten. What do we do when
	it is a power of ten to begin with? i.e. 10,100...
	"""
	reverse,remaining = 0, abs(integer)
	while remaining:
		reverse = reverse*10 + (remaining % 10)
		remaining = int(remaining/10)
	if integer < 0:
		return -reverse
	return reverse
	

if __name__ == "__main__":
	in_ = 42
	assert(reverse_digits_easy(in_) == 24)
	in_ = -314
	assert(reverse_digits_easy(in_) == -413)
	assert(reverse_digits(in_) == -413)
