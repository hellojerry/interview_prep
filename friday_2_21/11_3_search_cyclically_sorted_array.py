"""
An array is said to be cyclically sorted if it is 
possible to cyclically shift its entries so
that it becomes sorted. 

EX:

arr = [378,478,550,631,103,203,220,234,279,368]

This array is cyclically sorted. Shifting all elements
left by 4 leads to a sorted array.

Design a log N algorithm for finding the position of the
smallest element in a cyclically sorted array.

For example, 

find_shift_point(arr) -> 4
"""

def find_shift_point(arr):
	"""
	So the strategy with this is doing a modified bsearch.
	instead of looking for a target value, we want to see 
	if there's an inversion.
	i.e. if left > right, median = right, rinse and repeat.
	if left < right, median = left.
	"""
	arrlen = len(arr)
	left_ptr, right_ptr = 0, arrlen - 1
	while left_ptr < right_ptr:
		midpoint = int((left_ptr +right_ptr)/2)
		left_val,right_val = arr[left_ptr],arr[right_ptr]
		if arr[midpoint] > right_val:
			left_ptr = midpoint+1
		else:
			right_ptr = midpoint
	return left_ptr


if __name__ == "__main__":
	arr = [378,478,550,631,103,203,220,234,279,368]
	res = find_shift_point(arr)
	assert(res == 4)
