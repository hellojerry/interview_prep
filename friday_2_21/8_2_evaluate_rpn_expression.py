"""
A string is said to be an arithmetical expression in
Reverse Polish Notation if:

1) it is a single digit or sequence with an option "-",
i.e. "6","123","-42"
2) It is the form of "A,B,O" where A and B are RPN expressions
and O is one of "+","-","X","/"

For example:

"1729"
"3,4,+,2,x,1,+"
"1,1,+,-2,x"
"-641",6,/,28,/"

Write a program that takes an expression in RPN
and returns the number it evaluates to.
"""

def evaluate_rpn(arg):
	"""
	this is a stack problem.
	for item in arg,
		if item is integer, push to stack.
		if item is operator, pop values
		on stack with the first value popped
		being on the right side, and second
		on left - send evaluated expression
		to accumulator.
	I think my solution's a bit better than the book's.
	I don't know the specifics of RPN too well,
	but I think this handles edge cases better.
	"""
	
	res = 0
	stack = []
	int_ct = 0
	arg = arg.split(",")
	if len(arg) == 1:
		return int(arg[0])
	if len(arg) == 0:
		return 0
	for item in arg:
		if item in ["+","-","x","/"]:
			lhs = None
			rhs = None
			if int_ct == 2:
				rhs = stack.pop()
				lhs = stack.pop()
			elif int_ct == 1:
				rhs = stack.pop()
				lhs = res
			int_ct = 0
			if item == "+":
				res = lhs + rhs
			elif item == "-":
				res = lhs - rhs
			elif item == "x":
				res = lhs * rhs
			else:
				res = lhs / rhs
		else:
			stack.append(int(item))
			int_ct += 1
	return res

if __name__ == "__main__":
	arg1 = "1729"
	arg2 = "3,4,+,2,x,1,+"
	arg3 = "1,1,+,-2,x"
	arg4 = "-641,6,/,28,/"
	res1 = evaluate_rpn(arg1)
	res2 = evaluate_rpn(arg2)
	res3 = evaluate_rpn(arg3)
	res4 = evaluate_rpn(arg4)
	expected_1 = 1729
	expected_2 = (3+4)*2 + 1
	expected_3 = (1+1)*-2
	expected_4 = (-641/6)/28
	assert(res1 == expected_1)
	assert(res2 == expected_2)
	assert(res3 == expected_3)
	assert(res4 == expected_4)
