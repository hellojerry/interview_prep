"""
Generate all permutations of an array.

For example:

input: [1,2]
result: [[1,2],[2,1]]
input: [1,2,3]
result: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
"""

def generate_permutations(arr):
	"""
	The approach with this is
	1. get base cases (empty array, single element)
	and return those.
	2. iterate over the array - remove a single element
	at a time, run the function on the array without
	the target element, and append the permutations
	generated (along with the target element)
	to the outbound array.
	"""
	arrlen = len(arr)
	if arrlen == 1:
		return [arr]
	if arrlen == 0:
		return []

	out = []
	for i in range(len(arr)):
		el = arr[i]
		remainder = arr[:i] + arr[i+1:]
		for item in generate_permutations(remainder):
			out.append([el] + item)
	return out

if __name__ == "__main__":
	s1 = [1,2]
	r1 = generate_permutations(s1)
	print(r1)
	s2 = [1,2,3]
	r2 = generate_permutations(s2)
	print(r2)
