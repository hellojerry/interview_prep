"""
Given a string, test and see if 
it's a palindrome.

"""

def is_palindrome_easy(my_str):
	"""
	I believe this is not in the spirit of the question.
	Also it is two passed over the string, not counting the 
	equality check.
	"""
	sanitized = "".join(k for k in my_str if k.isalpha())
	return sanitized == sanitized[::-1]

def is_palindrome(my_str):
	strlen = len(my_str)
	left_ptr = 0
	right_ptr = strlen - 1

	while left_ptr <= right_ptr:
		left_val, right_val = my_str[left_ptr].lower(), my_str[right_ptr].lower()
		left_is_char =left_val.isalpha()
		right_is_char = right_val.isalpha()
		if not left_is_char and not right_is_char:
			left_ptr += 1
			right_ptr -= 1
			continue
		if not left_is_char:
			left_ptr += 1
			continue
		if not right_is_char:
			right_ptr -= 1
			continue
		if left_val == right_val:
			left_ptr += 1
			right_ptr -= 1
		else:
			return False
	return True
if __name__ == "__main__":
	a1 =  "A man, a plan, a canal, Panama"
	a2 = "Ray A Ray"
	a3 = "racecar"
	a4 = "Able was I, ere I saw Elba!"
	assert(is_palindrome(a1) == True)
	assert(is_palindrome(a2) == False)
	assert(is_palindrome(a3) == True)
	assert(is_palindrome(a4) == True)
