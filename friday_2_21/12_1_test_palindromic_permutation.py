"""
Write a program to test whether the letters forming a string can be
permuted to form a palindrome.

Ex: "edified" -> deified
racerac -> racecar
dog = >false
"""
from collections import defaultdict
def can_be_palindrome(my_str):
	"""
	Get frequencies - can only have one odd value.
	"""
	freq = defaultdict(int)
	for item in my_str:
		freq[item] += 1
	odd_ct = 0
	for k,v in freq.items():
		if v % 2 != 0:
			odd_ct += 1
		if odd_ct > 1:
			return False
	return True

if __name__ == "__main__":
	assert(can_be_palindrome("edified") == True)
	assert(can_be_palindrome("dog") == False)
	assert(can_be_palindrome("racerac") == True)	
