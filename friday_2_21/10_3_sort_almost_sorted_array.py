"""
Write a program which takes a very long sequence
of numbers and prints the numbers in sorted order.
Each number is at most K away from its correctly-sorted
location (this is also referred to as being "k-sorted").

EX:

arr = [3,-1,2,6,4,5,8]
no value is more than two away from its final sorted position.
"""
import heapq

def sort_k_sorted_array(arr,K):
	"""
	Note that the array is supposed to be arbitrarily long,
	so typical sorting methods won't work. The question
	is specifically for sorting with a min-heap or max-heap.
	The strategy for this is
	1. turn the array into a heap.
	2. once K is reached, for every value after the current one,
	pop the minimum value from the heap, and insert it at the 
	array pointer's position.
	3. after the main iteration, exhaust the remaining
	values in the heap (they correspond to K) 
	"""

	arr_ptr = 0
	ct = 0
	out = []
	heapq.heapify(out)
	for item in arr:
		if ct < K:
			heapq.heappush(out,item)
			ct += 1
		else:
			res = heapq.heappushpop(out,item)
			arr[arr_ptr] = res
			arr_ptr += 1
	while ct > 0:
		res = heapq.heappop(out)
		ct -= 1
		arr[arr_ptr] = res
		arr_ptr += 1
	return arr


if __name__ == "__main__":

	arr = [3,-1,2,6,4,5,8]
	K = 2
	expected = sorted(arr)
	res = sort_k_sorted_array(arr,K)
	assert(res == expected)
