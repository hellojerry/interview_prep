

def is_valid(occupied_row,occupied_col,col_array):
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if target_col == occupied_col:
			return False
		if abs(target_col - occupied_col) == abs(target_row - occupied_row):
			return False


	return True

def eight_queens(row,col_array,limit,acc):

	if row == limit:
		copied = col_array.copy()
		acc.append(list(zip(range(limit),copied)))
	else:
		for col in range(limit):
			if is_valid(row,col,col_array):
				col_array[row] = col
				eight_queens(row+1,col_array,limit,acc)



if __name__ == "__main__":
	acc = []
	limit = 8
	col_array = list(range(limit))
	row = 0
	eight_queens(row,col_array,limit,acc)

	assert(len(acc) == 92)
