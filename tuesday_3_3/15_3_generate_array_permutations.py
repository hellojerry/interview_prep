"""

Generate the permutations of an array.

[1,2,3] -> [1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,2,1][3,1,2]

"""


def permutations(arr):

	arrlen = len(arr)
	if arrlen == 0:
		return []
	if arrlen == 1:
		return [arr]
	out = []
	for ind in range(arrlen):
		val = arr[ind]
		left,right = arr[:ind], arr[ind+1:]
		for res in permutations(left+right):
			out.append([val] + res)
		
	return out

if __name__ == "__main__":
	print(permutations([1,2,3]))
