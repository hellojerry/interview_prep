

def hanoi(source,mid,dest,N):
	"""
	move source to mid via dest
	move mid to dest via source
	"""
	if N == 0:
		return
	hanoi(source,dest,mid,N-1)
	if len(source) > 0:
		dest.append(source.pop())
	hanoi(mid,source,dest,N-1)

if __name__ == "__main__":
	s1,s2,s3 = ["A","B","C"],[],[]
	hanoi(s1,s2,s3,3)
	print(s3)
	
