"""
Queue insertion and deletion is FIFO.
Stack insertion and deletion is LIFO.
Implement a queue using stacks.
"""

class StackQueue(object):
	"""
	So how can we do this. hmm
	"""

	def __init__(self):
		self.lstack = []
		self.rstack = []
		

	def append(self,val):
		self.lstack.append(val)

	def pop(self):
		if not self.rstack:
			while self.lstack:
				self.rstack.append(self.lstack.pop())
		if not self.rstack:
			raise Exception("empty")
		return self.rstack.pop()
		
