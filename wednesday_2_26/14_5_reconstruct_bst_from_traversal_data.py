"""
Given a sequence of nodes that
an inorder traversal sequence visits and either
of the other two traversal sequences, there exists
a binary tree that yields those seuqneces.

Suppose you are given the sequence in which keys are visited
in an inorder traversal of a BST, and all keys are distinct.
Can you reconstruct the BST from the sequence? Solve the problem
for preorder and inorder as well.

ex: [1,2,3]
possible bsts:
		1          1           2          3    3
		  2           3       1  3      1     2
                    3       2                    3   1 

No, inorder is not possible.
It is possible for a preorder however.

[43,23,37,29,31,41,47,53]

			43
                    23      47
                      37          53
                     29
"""

class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val
def preorder(root,acc):
	if not root:
		return None
	acc.append(root)
	preorder(root.left,acc)
	preorder(root.right,acc)
def postorder(root,acc):
	if not root:
		return None
	postorder(root.left,acc)
	preorder(root.right,acc)
	acc.append(root)

def build_from_preorder(arr):
	if not arr:
		return None
	root_val = arr[0]
	root = TreeNode(root_val)
	arrlen = len(arr)
	transition_point = arrlen
	for idx in range(arrlen):
		if arr[idx] > root_val:
			transition_point = idx
	root.left = build_from_preorder(arr[1:transition_point])
	root.right = build_from_preorder(arr[transition_point:]) 
	return root	

def build_from_postorder(arr):
	if not arr:
		return
	## root is the final value in the sequence.
	arrlen = len(arr)
	root_val = arr[-1]
	root = TreeNode(root_val)
	if arrlen == 1:
		return root
	transition_point = arrlen-1
	for idx in range(arrlen):
		if arr[idx] < root_val:
			transition_point = idx
			break
	arr = arr[:-1]
	root.left = build_from_postorder(arr[transition_point:])
	root.right = build_from_postorder(arr[:transition_point])
	return root


if __name__ == "__main__":
	arr = [43,23,37,29,31,41,47,53]
	res = build_from_preorder(arr)
	acc = []
	preorder(res,acc)
	out = [k.val for k in acc]
	assert(arr == out)
	arr = [8,12,10,16,25,20,15]
	res = build_from_postorder(arr)
	acc = []
	postorder(res,acc)
	out = [k.val for k in acc]
	assert(arr == out)
