"""
A prime number is any number > 1 that
has no divisors other than 1 and itself.

Write a program that takes an integer argument
and returns all primes between 1 and the integer.

ex: 18
return: [2,3,5,7,11,13,17]
"""
import math

def is_prime(num):

	if num == 3:
		return True
	check = 3
	sqrt = int(math.sqrt(num))
	while check <= sqrt:
		if num % check == 0:
			return False
		check += 2		
	return True


def all_primes_until(number):
	if number <= 2:
		return []
	if number == 3:
		return [2]
	out = [2]
	## we can skip the even numbers for this.
	for val in range(3,number,2):
		if is_prime(val):
			out.append(val)
	return out

def all_primes(number):
	"""
	Another option is to use an array to memoize results
	"""
	primes = []
	## we start by initializing an array with 0 and 1 as false.
	is_prime_arr = [False,False] + [True] * (number-1)
	for item in range(2,number+1):
		if is_prime_arr[item]:
			primes.append(item)
			## we remove everything ahead of it that
			## is a a multiple of it.
			for i in range(item,number+1,item):
				is_prime_arr[i] = False
	return primes


if __name__ == "__main__":
	res = all_primes_until(18)
	print(res)
	assert(res == [2,3,5,7,11,13,17])
	res = all_primes(18)
	print(res)
	assert(res == [2,3,5,7,11,13,17])
