"""
Consider a singly linked list who nodes are starting
at zero. The even-odd merge of the list is the list
rearranged such that the even nodes are before the odd nodes.

ex:

	0->1->-2->3->4->5 =>  0->2->4->1->3->5

ex:
	0->1->2->3->4->5->6 => 0->2->4->6->1->3->5

make a program to do that.

Hint: Use temporary additional storage.
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val

def even_odd_merge(head):
	"""
	Couldn't I just make an even tail and an odd tail?
	"""
	if not head:
		return None
	if not head.next_:
		return None
	even_head = head
	even_tail = head
	odd_head = None
	odd_tail = None
	cur = head.next_
	ct = 1
	while cur:
		next_ = cur.next_
		cur.next_ = None
		if ct % 2 != 0:
			if not odd_head:
				odd_head = cur
				odd_tail = cur
			else:
				odd_tail.next_ = cur
				odd_tail = cur
		else:
			even_tail.next_ = cur
			even_tail = cur
		ct += 1
		cur = next_
	even_tail.next_ = odd_head
	return even_head


if __name__ == "__main__":
	n5 = Node(5)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	n0 = Node(0,n1)
	res = even_odd_merge(n0)
	out = []
	cur = res
	while cur:
		out.append(cur)
		cur = cur.next_
	expected = [n0,n2,n4,n1,n3,n5]
	assert(out == expected)


	n6 = Node(6)
	n5 = Node(5,n6)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	n0 = Node(0,n1)
	out = []
	res = even_odd_merge(n0)
	cur = res
	while cur:
		out.append(cur)
		cur = cur.next_
	expected = [n0,n2,n4,n6,n1,n3,n5]
	assert(out == expected)
