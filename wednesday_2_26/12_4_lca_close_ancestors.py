"""
Design an algorithm for computing the LCA
of two nodes in a binary tree.
The algorithm's time complexity 
should only depend on the distance from the
two nodes to the LCA.
"""



def lca(node0,node1):
	"""
	So the idea with this is that
	
	"""

	i0,i1 = node0, node1
	nodes_on_path = set()
	while i0 or i1:
		if i0:
			if i0 in nodes_on_path:
				return i0
			nodes_on_path.add(i0)
			i0 = i0.parent
		if i1:
			if i1 in nodes_on_path:
				return i1
			nodex_on_path.add(i1)
			i1 = i1.parent
	raise Exception("not in the same tree")
