"""

The power set of S is the set of all subsets of S, including
the empty subset and S itself.

ex:
	{0,1,2}

res:
	{}
	{0}
	{1}
	{2},
	{0,1},
	{1,2}
	{0,2}
	{0,1,2}
"""

def generate_subsets(value, acc):
	"""
	Start at the base case. We need to get an empty set.
	"""
	if len(acc) == 0:
		acc.append(set())
		acc.append({value})
		return acc
	else:
		## from here, we copy the existing outbound values,
		## and for each subset of outbound, we want to add
		## the value to it.
		copied = acc.copy()
		for minilist in copied:
			mini_acc = minilist.copy()
			mini_acc.add(value)
			acc.append(mini_acc)
		return acc

def find_subsets(my_set):
	"""
	We wa
	"""

	acc = []
	for item in my_set:
		acc = generate_subsets(item,acc)
	return acc

if __name__ == "__main__":
	res = find_subsets({1,0,2})
	print(res)
