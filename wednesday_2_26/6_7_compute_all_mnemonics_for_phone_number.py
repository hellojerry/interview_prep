"""
Each digit, apart from 0 and 1, in a phone keypad,
corresponds to one of three or four letters of the alphabet.
Since words are easier to remember than numbers,
it is natural to ask if a 7 or 10 digit phone number
can be represented by a word.

For example, "2276696" corresponds to "ACRONYM" as
well as "ABPOMZN"

1
2 "ABC"
3 "DEF"
4 "GHI"
5 "JKL"
6 "MNO"
7 "PQRS"
8 "TUV"
9 "WXYZ"
0

Write a program which takes as input a phone number,
specified as a string of digits, and returns all
possible character sequences that correspond to the phone number.

Hint: use recursion
"""


def generate_phone_numbers(number,out):
	"""
	This reminds me of one of those permutations problems.

	we reference the prior accumulated arrays.
	for each sub-array, return an array of arrays with each

	"""
	mappings = {
		"1":["1"],
		"2":["A","B","C"],
		"3":["D","E","F"],
		"4":["G","H","I"],
		"5":["J","K","L"],
		"6":["M","N","O"],
		"7":["P","Q","R","S"],
		"8":["T","U","V"],
		"9":["W","X","Y","Z"],
		"0":["0"]
	}
	acc = []
	if len(number) == 0:
		return out
	target_el = number[0]
	mapping = mappings[target_el]
	for sub_arr in out:
		for item in mapping:
			acc.append(sub_arr.copy() + [item])
	return generate_phone_numbers(number[1:],acc)

	

if __name__ == "__main__":
	basic = "123"
	possible = [
		["1","A","D"],
		["1","A","E"],
		["1","A","F"],
		["1","B","D"],
		["1","B","E"],
		["1","B","F"],
		["1","C","D"],
		["1","C","E"],
		["1","C","F"],
	]
	acc = [[]]
	res = generate_phone_numbers(basic,acc)
	assert(sorted(res) == sorted(possible))


