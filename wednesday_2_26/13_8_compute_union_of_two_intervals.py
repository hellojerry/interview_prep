"""
Make an algorithm that takes 
an input as a set of intervals,
and outputs their union as a set of disjoint
intervals.

ex:

[ [0,3],[2,4],[3,4],[1,1],
  [5,7],[7,8],[8,11],[9,11],
  [12,14],[12,16],[13,15],[16,17]
]
result:
	[[0,4],[5,11],[12,17]]
"""

def merge_intervals(intervals):
	"""
	sort intervals by their initial value.
	Then, iterate over the intervals.
	if current.start <= prior.end:
		prior.end = current.start
	else:
		out.append(interval)
	"""
	by_first = sorted(intervals, key=lambda x: x[0])
	out = []
	for interval in by_first:
		if len(out) == 0:
			out.append(interval)
			continue
		prior_interval = out[-1]
		if interval[0] <= prior_interval[1]:
			if interval[1] > prior_interval[1]:
				out[-1][1] = interval[1]
		else:
			out.append(interval)
	return out


if __name__ == "__main__":
	intervals = [ [0,3],[2,4],[3,4],[9,11],
	  [12,16],[13,15],[16,17],
	  [5,7],[7,8],[8,11],[1,1],[12,14]
	]
	res = merge_intervals(intervals)
	expected = [[0,4],[5,11],[12,17]]
	assert(res == expected)
