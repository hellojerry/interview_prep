"""
You want to compute the running median of 
a sequence of numbers. The sequence
is presented to you in a streaming fashion - you
cannot read back to an earlier value, you need to
out[ut the median after reading in each new element.

Ex:
	[1,0,3,5,2,0,1]
output:

	[1,0.5,1,2,2,1.5,1]

1 -> 1
1,0 -> 0.5
0,1,3 -> 1
0,1,3,5 -> 2
0,1,2,3,5 -> 2
0,0,1,2,3,5 -> 1.5
0,0,1,1,2,3,5 -> 1

Design an algorithm for computing the running median of a sequence
"""
import heapq
def running_median(sequence):

	result = []
	min_heap = []
	max_heap = []
	for item in sequence:
		heapq.heappush(max_heap, -heapq.heappushpop(min_heap, item))
		if len(max_heap) > len(min_heap):
			heapq.heappush(min_heap, -heapq.heappop(max_heap))
		if len(min_heap) == len(max_heap):
			result.append(0.5* (min_heap[0] + (-max_heap[0])))
		else:
			result.append(min_heap[0])
	return result

if __name__ == "__main__":
	in_seq = [1,0,3,5,2,0,1]
	res = running_median(in_seq)
	print(res)
	expected = [1,0.5,1,2,2,1.5,1]
	assert(res == expected)
