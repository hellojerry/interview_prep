"""
Implement a function which takes as input a floating point
value and returns its square root.

ex:
	1.44 -> 1.2
"""
import math
def compute_square_root_float(num):
	"""
	So... hmm. Well.. uhh...
	I could take the values right of the decimal point,
	and work towards the decimal, taking the root.
	"""
	left,right = (num,1.0) if num < 1.0 else (1.0,num)

	while not math.isclose(left,right):
		mid = 0.5 * (left+right)
		mid_squared = mid**2
		if mid_squared > num:
			right = mid
		else:
			left = mid
	return left
		



if __name__ == "__main__":
	res = compute_square_root_float(1.44)
	print(res)
