

def get_partition_idx():
	pivot = arr[high]
	i = low - 1
	for j in range(low,high):
		if arr[j] <= pivot:
			i += 1
			arr[i],arr[j] = arr[j],arr[i]
	arr[i+1],arr[high] = arr[high],arr[i+1]
	return i+1



def qsort(arr,low,high):
	if low < high:
		partition_idx = get_partition_idx(arr,low,high)
		quick_sort(arr,low,partition_idx-1)
		quick_sort(arr,partition_idx+1,high)

	
