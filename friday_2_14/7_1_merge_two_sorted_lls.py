"""
Consider two singly linked lists in which each node holds
a number. Assume the lists are sorted, i.e. the numbers
appear in ascending order within each list. The merge of the two
lists is a linked list consisting of the nodes where the numbers
appear in ascending order.

I.e.:

LL1:   1 -> 2->5->7
LL2:	1->3->11

Result: 1->1->2->3->5->7->11
"""


class Node(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		if next_ is not None:
			assert(type(next_) == Node)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "Node(%d)" % self.value

	def __repr__(self):
		return "Node(%d)" % self.value


def merge_two_linked_lists(l1,l2):
	"""
	This function should return the head of the linked list.
	I don't think this one should be too hard.

	We need a pointer for each head, and then an output LL.
	"""
	left_head,right_head = l1,l2
	left_current,right_current = l1,l2
	out_head = None
	out_tail = None

	while left_current and right_current:
		if left_current.value > right_current.value:
			if out_head is None:
				out_head = right_current
				right_current = right_current.next_
				out_head.next_ = None
				out_tail = out_head
			else:
				out_tail.next_ = right_current
				right_current = right_current.next_
				out_tail = out_tail.next_


		elif left_current.value < right_current.value:
			if out_head is None:
				out_head = left_current
				left_current = left_current.next_
				out_head.next_ = None
				out_tail = out_head
			else:
				out_tail.next_ = left_current
				left_current = left_current.next_
				out_tail = out_tail.next_
		else:
			if out_head is None:
				out_head = left_current
				out_tail = right_current
				left_current = left_current.next_
				right_current = right_current.next_
				out_head.next_ = out_tail
				out_tail.next_ = None
			else:
				out_tail.next_ = left_current
				left_current = left_current.next_
				out_tail = out_tail.next_
				out_tail.next_ = right_current
				right_current = right_current.next_
				out_tail.next_ = None

	## get the stragglers.
	if left_current:
		out_tail.next_ = left_current
	if right_current:
		out_tail.next_ = right_current


	return out_head

if __name__ == "__main__":
	ll1_4 = Node(7)
	ll1_3 = Node(5,ll1_4)
	ll1_2 = Node(2,ll1_3)
	ll1_1 = Node(1,ll1_2)

	ll2_3 = Node(11)
	ll2_2 = Node(3,ll2_3)
	ll2_1 = Node(1,ll2_2)

	head = merge_two_linked_lists(ll1_1,ll2_1)
	current = head
	out_arr = []
	while current:
		out_arr.append(current)
		current = current.next_
	assert(out_arr == [ll1_1,ll2_1,ll1_2,ll2_2,ll1_3,ll1_4,ll2_3])
