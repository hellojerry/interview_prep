"""
Design an algorithm that takes as input an array and a number,
and determines if there are three entries in the array (not necessarily
distinct) that add up to the specified number.

input: [11,2,5,7,3]
value: 21

result: True (11,7,3),(5,5,11)

"""

def has_two_sum(arr, target_val):
	left_ptr, right_ptr = 0, len(arr) -1

	while left_ptr <= right_ptr:
		combined = arr[left_ptr] + arr[right_ptr]
		if combined == target_val:
			return True
		elif combined < target_val:
			left_ptr += 1
		else:
			right_ptr -= 1
	return False

def has_three_sum(arr,target_value):
	"""	
	Review this
	"""
	arr.sort()

	return any(has_two_sum(arr, target_value - a) for a in arr)

if __name__ == "__main__":
	res = has_three_sum([11,2,5,7,3],21)
	assert(res == True)
	res = has_three_sum([11,2,5,7,3],5)
	print(res)
