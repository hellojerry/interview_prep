"""

Test if a binary tree is height balanced.
"""


def get_height(node):
	if not node:
		return 0

	return max(get_height(node.left),get_height(node.right)) + 1


def is_balanced(node):

	if not node:
		return True

	lhs = get_height(node.left)
	rhs = get_height(node.right)

	if abs(lhs - rhs) <= 1 and is_balanced(node.left) and is_balanced(node.right):
		return True

	return False
