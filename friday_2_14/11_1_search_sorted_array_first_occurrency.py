"""
Write a method that takes a sorted array and a key and returns the index of the first 
occurrence of that key in the array. Return -1 if the key does not appear in the array.

ex: 

search value: 4

[1,1,2,3,4,4,4,4,5,10,21,22]
result: 4
"""



def find_first_index(arr, value):
	"""
	This is a bsearch.
	get the midpoint, left, and right pointers.
	Once the value is found, iterate left from the point.
	"""

	arrlen = len(arr)
	if arrlen == 0:
		return -1
	left_ptr = 0
	right_ptr = arrlen - 1
	while left_ptr <= right_ptr:
		midpoint = int((right_ptr+left_ptr)/2)
		mid_val = arr[midpoint]
		if mid_val == value:
			for i in range(midpoint,-1,-1):
				if arr[i] != value:
					return i+1
			return 0
		if value > mid_val:
			left_ptr = midpoint+1
		else:
			right_ptr = midpoint - 1
	return -1


if __name__ == "__main__":
	arr = [1,1,2,3,4,4,4,4,5,10,21,22]
	res = find_first_index(arr,4)
	assert(res == 4)
	arr = [1,1,1,1,1,1,1,1,1]
	res = find_first_index(arr,1)
	assert(res==0)

	arr = [1,2,3,4,5,6,7,8,9]
	res = find_first_index(arr,3)
	assert(res==2)
	arr = [1,2,3,4,5,6,7,8,9]
	res = find_first_index(arr,8)
	assert(res == 7)
