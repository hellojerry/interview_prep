"""
Implement an integer to string conversion function, and a string to integer
conversion function. For example, if the input to the first function is 314,
it should return "314". If the input to the second function is "314", it should
return 314.

This should handle negative integers. Additionally, you can't use "int" and "str".
"""

def string_to_integer(my_str):
	end_idx, is_negative = -1, False
	if my_str[0] == "-":
		end_idx = 0
		is_negative = True
	num_tens = 0
	out = 0
	str_len = len(my_str)
	for i in range(str_len-1,end_idx,-1):
		item = my_str[i]
		as_int = ord(item) - 48
		out += (10**num_tens)*as_int
		num_tens += 1
	if is_negative:
		return -out
	return out


def integer_to_string(N):
	acc = ""
	abs_n = abs(N)
	amt, remainder = divmod(abs_n,10)
	while amt > 0:
		acc = chr(ord('0')+remainder) + acc
		amt,remainder = divmod(amt,10)
	acc = chr(ord('0')+remainder) + acc
	if N < 0:
		acc = "-" + acc
	return acc

r1 = integer_to_string(423)
r2 = integer_to_string(4230)
r3 = integer_to_string(-423299921)
r4 = integer_to_string(0)
r5 = integer_to_string(-1)

assert(r1 == "423")
assert(r2 == "4230")
assert(r3 == "-423299921")
assert(r4 == "0")
assert(r5 == "-1")

assert(string_to_integer(r1) == 423)
assert(string_to_integer(r2) == 4230)
assert(string_to_integer(r3) == -423299921)
assert(string_to_integer(r4) == 0)
assert(string_to_integer(r5) == -1)


