"""
Solve the tower of hanoi problem
"""



def tower_of_hanoi(source,mid,dest,N):
	## none left to do
	if N == 0:
		return
	## move from source to mid with dest as buffer
	tower_of_hanoi(source,dest,mid,N-1)

	## if source isn't empty, pop from source to dest
	if len(source) > 0:
		dest.append(source.pop())

	## move from mid to dest with source as buffer
	tower_of_hanoi(mid,source,dest,N-1)

if __name__ == "__main__":
	s1, s2, s3 = [1,2,3],[],[]
	N = len(s1)
	tower_of_hanoi(s1,s2,s3,N)
	assert(s3 == [1,2,3])
