"""
Write a program which takes text for an anonymous letter and text for a magazine and 
determines if it is possible to write the anonymous letter using the magazine.

The anonymous letter can be written using the magazine if for each character in
the anonymous letter, the number of times it appears in the anonymous letter
is no more than the number of times it appears in the magazine.
"""
from collections import defaultdict

def get_frequencies(text):
	frequencies = defaultdict(int)
	for letter in text:
		frequencies[letter] += 1
	return frequencies
	

def can_be_constructed(letter_text,magazine_text):
	"""
	This question is really strange. I think
	what it is asking is to get a character frequency
	count for the letter text and for the magazine text.
	"""

	letter_frequencies = get_frequencies(letter_text)
	magazine_frequencies = get_frequencies(magazine_text)

	for k,v in letter_frequencies.items():
		found_key = magazine_frequencies.get(k, None)
		if not found_key:
			return False
		if found_key < v:
			return False
	return True


def can_be_constructed_faster(letter_text,magazine_text):
	"""
	This is a bit faster - it's only two passes, one for the letter, one for the mag.
	"""

	magazine_frequencies = get_frequencies(magazine_text)
	for char in letter:
		found_key = magazine_frequencies.get(char, None)
		if not found_key:
			return False
		magazine_frequencies[char] -= 1
		if magazine_frequencies[char] <= 0:
			magazine_frequencies.pop(char)
	return True


if __name__ == "__main__":

	letter = "aaabbbcccdddeeeggghhhh"
	magazine = "aaaabbbbccccddddeeeegggghhhh"
	res = can_be_constructed_faster(letter,magazine)
	assert(res == True)

	letter = "aaabbbcccdddeeeggghhhhiiiiiii"
	res = can_be_constructed_faster(letter,magazine)
	assert(res ==False)

	letter = "aaaaabbbbcccddddeeegggghh"
	res = can_be_constructed_faster(letter,magazine)
	assert(res==False)


