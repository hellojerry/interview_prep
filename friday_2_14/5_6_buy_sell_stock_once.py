"""

Design an algorithm that determines the maximum profit that could have been made
by buying and then selling that one share of stock.

I am not clear on this question if it means only buying and selling once.
Based on the example provided, I think it means only one buy and one sell.

ex: [310,315,275,295,260,270,290,230,255,250]

maximum profit: 30 (buy at 260, sell at 290)
"""

def find_max_profit_brute(arr):
	"""
	I think i've done something like this before.
	The naive way to do this would be n log n,
	Where you start at an index, scan the tail,
	and take the max profit of arr[prospective] - arr[current]
	"""
	arrlen = len(arr)
	top_profit = 0
	for cur_idx in range(arrlen-2):
		for prospective_idx in range(cur_idx+1,arrlen):
			top_profit = max(arr[prospective_idx] - arr[cur_idx],top_profit)
	return top_profit

def find_max_profit_2(arr):
	"""
	Another way to do this is just to find a current minimum and current maximum
	and iterate through things 
	"""
	min_so_far, max_profit = float("inf"), 0
	for price in arr:
		min_so_far = min(price,min_so_far)
		today_profit = price - min_so_far
		max_profit = max(max_profit,today_profit)
	return max_profit



if __name__ == "__main__":
	arr1 = [310,315,275,295,260,270,290,230,255,250]
	res1 = find_max_profit_brute(arr1)
	print(res1)
	assert(res1 == 30)
	arr1 = [310,315,275,295,260,270,290,230,255,250]
	res1 = find_max_profit_2(arr1)
	print(res1)
	assert(res1 == 30)
