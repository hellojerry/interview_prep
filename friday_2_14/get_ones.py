def get_ones(N):
	ct_1s = 0
	while N:
		## check if final digit matches with one
		ct_1s += N & 1
		## shift right one digit
		N >>= 1
	return ct_1s


print(bin(15))
print(get_ones(15))
print(bin(7))
print(get_ones(7))
