"""
Design a stack that includes a max operation,
along with push() and pop(). The max() method should
return the maximum value in the stack.
"""


class CustomStack(object):
	"""
	Remember that stacks are LIFO.
	"""

	def __init__(self):
		self.inner_arr = []
		self.max_value = None


	def __str__(self):
		return "CustomStack(%s)" % str(self.inner_arr[::-1])

	def __repr___(self):
		return "CustomStack(%s)" % str(self.inner_arr[::-1])

	def push(self,value):
		if self.max_value is None:
			self.max_value = value
		elif value > self.max_value:
			self.max_value = value
		self.inner_arr.append(value)

	def pop(self):
		if len(self.inner_arr) == 0:
			raise Exception("EMPTY!!")
		out = self.inner_arr.pop()
		if len(self.inner_arr) == 0:
			self.max_value = None
		else:
			self.max_value = sorted(self.inner_arr)[-1]
		return out

	def max(self):
		if len(self.inner_arr) == 0:
			raise Exception("empty")
		return self.max_value
