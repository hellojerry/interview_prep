"""
find the kth largest node in a BST

		10
	     /      \
	8 	    12
     /	   \        /  \ 
    3 	   9       11  14


"""


class TreeNode(object):

	def __init__(self,value,left=None,right=None):
		assert(type(value) == int)
		for i in [left,right]:
			if i is not None:
				assert(type(i) == TreeNode)
		self.value = value
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value 

	def __repr__(self):
		return "TreeNode(%d)" % self.value 

def find_kth_largest(node, val,acc):
	"""
	We need to to a traversal that goes right, then current,
	then left. 
	"""

	if not node or acc[0] >=val:
		return None

	res = find_kth_largest(node.right, val,acc)
	if res is not None:
		return res
	acc[0] += 1
	if acc[0] == val:
		return node
	return find_kth_largest(node.left,val,acc)


	


if __name__ == "__main__":


	far_left = TreeNode(3)
	far_left_right = TreeNode(9)
	mid_left = TreeNode(8,far_left,far_left_right)
	right_left = TreeNode(11)
	far_right = TreeNode(14)
	mid_right = TreeNode(12,right_left,far_right)
	root = TreeNode(10,mid_left,mid_right)

	acc = [0]	
	val = find_kth_largest(root, 3,acc)
	print(val)
