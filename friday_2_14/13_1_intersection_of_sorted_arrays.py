"""
A natural implementation for a search engine is to retrieve
documents that match the set of words in a query by maintaining
an inverted index. Each page is assigned an integer identifier,
its "document ID". The inverted index is a mapping that takes
a word W and returns a sorted array of page-ids
which contain W. The sort order could be, for example, the page
rank in descending order. When a query contains multiple words,
the search engine finds the sorted array for each word and then
computes the intersection of these arrays (these are the pages 
containing all the words in the query). The most computationally
intensive step of doing this is finding the intersection of the sorted
arrays.

Write a program which takes as input two sorted arrays, and returns a new array 
containing elements that are present in both of the input arrays. The input arrays
may hae duplicate entries, but the returned array should be free of duplicates.

Example:

arr1 = [2,3,3,5,5,6,7,7,8,12]
arr2 = [5,5,6,8,8,9,10,10]

returns [5,6,8]
"""

def intersection_of_two_sorted_arrays(arr1,arr2):
	"""
	My first thought is to take two pointers
	and iterate up until both are complete.
	We'll want to skip the dupes.
	"""


	left_len = len(arr1)
	right_len = len(arr2)
	left_ptr, right_ptr = 0,0
	out = []

	while left_ptr < left_len and right_ptr < right_len:
		left_val = arr1[left_ptr]
		right_val = arr2[right_ptr]
		memo = None
		if len(out) > 0:
			### deduping strategy
			memo = out[-1]
			skip = False
			if left_val == memo:
				skip = True
				left_ptr += 1
			if right_val == memo:
				skip = True
				right_ptr += 1
			if skip:
				continue	
		
		## don't need to worry about memos from here on
		## if left is less than right, move left up until
		## it's GTE right. if it's 
		if left_val == right_val:
			out.append(left_val)
			left_ptr += 1
			right_ptr += 1
		elif left_val < right_val:
			left_ptr += 1
		elif left_val > right_val:
			right_ptr += 1

	return out
if __name__ == "__main__":
	arr1 = [2,3,3,5,5,6,7,7,8,12]
	arr2 = [5,5,6,8,8,9,10,10]
	res = intersection_of_two_sorted_arrays(arr1,arr2)
	assert(res == [5,6,8])

	arr1 = [-1,-1,-1,-1,-1,4,6,6,6,6,6,6,7,8,8,9,10,14]
	arr2 = [-1,2,3,6,10]
	res = intersection_of_two_sorted_arrays(arr1,arr2)
	assert(res == [-1,6,10])
