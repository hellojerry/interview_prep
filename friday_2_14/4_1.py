"""
EPI 4.1

The parity of a binary word is 1 if the number of 1s in the word is odd; otherwise it is 0.
For example, the parity of 1011 is 1, and the parity of 10001000 is 0. Parity checks are used
to detect single bit errors in data storage and comunication. It is fairly straightforward 
to write code that computes the parity of a single 64-bit word.

How would you compute the parity of a very large number of 64-bit words?

Hint: Use a lookup table, but don't use 2^64 entries!

"""

def get_parity(N):
	"""
	I may be misunderstanding this.
	Ok, I was misunderstanding this. When they use "word" in this question,
	they're not talking about text, they're just talking about arbitrary binary blobs.

	This is o(n) but I think we can make this faster.
	It counts the number of 
	"""
	ct_1s = 0
	while N:
		ct_1s += N & 1
		N >>= 1
	return int(ct_1s % 2 != 0)

def x_is_power_of_two(N):
	"""
	The only scenario where this returns zero (so true)
	is if N is 1 with nothing but zeroes trailing it.
	"""

	return N & (N-1) == 0

def parity_assert_helper(num):
	"""
	This accomplishes the count in a visual way but it doesn't
	follow the spirit of the question. 
	""" 
	return int(bin(num)[2:].count("1") % 2 != 0)

if __name__ == "__main__":
	expected_parity = parity_assert_helper(1)
	res1 = get_parity(1)
	assert(res1 == 1)
	res2 = get_parity(2)
	assert(res2 == 1)
	assert(parity_assert_helper(15) == get_parity(15))

	r1 = x_is_power_of_two(2)
	assert(r1 == True)
	r2 = x_is_power_of_two(5)
	assert(r2 == False)
	r3 = x_is_power_of_two(7)
	assert(r3 == False)
	r4 = x_is_power_of_two(8)
	assert(r4 == True)
	r5 = x_is_power_of_two(16)
	assert(r5 == True)
