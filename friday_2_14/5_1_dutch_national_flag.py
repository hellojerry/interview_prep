"""
Write a program that takes an array A and an index i into A,
and rearranges elements such that all elementsless than A[i] (the pivot)
appear first, followed by elements equal to the pivot, followed by elements
greater than the pivot.

Hint: think about the partition step in quicksort.
"""

def dutch_national_flag(arr, pivot_idx):
	"""
	Thinking about a quicksort might be a red herring.
	Couldn't I just take two pointers and swap things?
	"""

	left_ptr = 0
	pivot_val = arr[pivot_idx]
	print(pivot_val)
	## first step:
	## move stuff less than the pivot to the beginning.
	arrlen = len(arr)
	for i in range(arrlen):
		if arr[i] < pivot_val:
			arr[i], arr[left_ptr] = arr[left_ptr], arr[i]
			left_ptr += 1
	right_ptr = arrlen-1
	## second step: move stuff greater than the pivot to the end
	for i in range(arrlen-1,-1,-1):
		if arr[i] > pivot_val:
			arr[i], arr[right_ptr] = arr[right_ptr], arr[i]
			right_ptr -= 1



if __name__ == "__main__":
	my_arr = [1,2,5,3,3,4,5,6,1,1,6,3,2,-3]
	dutch_national_flag(my_arr,4)
	print(my_arr)
	
