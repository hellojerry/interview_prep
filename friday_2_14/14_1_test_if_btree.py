"""
Write a program that takes a btree and determines if it is a BST.

				      19
			          /       \
			      7            43
			    /   \         /    \
			  3     11       23     47
"""

class TreeNode(object):

	def __init__(self,value,left=None,right=None):
		assert(type(value) == int)
		for i in [left,right]:
			if i is not None:
				assert(type(i) == TreeNode)
		self.value = value
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value 

	def __repr__(self):
		return "TreeNode(%d)" % self.value 



def is_bst(node,minimum,maximum):
	"""
	So the strategy here is to recursively check if the right side
	is bigger than the minimum and the left side is smaller than the maximum.
	If node is None, we return True
	"""

	if not node:
		return True
	if node.value < minimum or node.value > maximum:
		return False

	return is_bst(node.left,minimum,node.value) and is_bst(node.right,node.value,maximum)

if __name__ == "__main__":

	bottom_left_left = TreeNode(3)
	bottom_left_right = TreeNode(11)
	mid_left = TreeNode(7,bottom_left_left,bottom_left_right)
	bottom_right_left = TreeNode(23)
	bottom_right_right = TreeNode(47)
	mid_right = TreeNode(43,bottom_right_left,bottom_right_right)
	root_node = TreeNode(19,mid_left,mid_right)

	minimum, maximum = float("-inf"), float("inf")
	(is_bst(root_node,minimum,maximum) == True)
