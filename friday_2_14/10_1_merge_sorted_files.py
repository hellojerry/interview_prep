"""
You are given 500 files, each containing stock trade information for
an s&p 500 company. Each trade is encoded by a line in the following
format:

1232111,AAPL,30,456.12

The first number is the time of the trade expressed as the number of milliseconds since
the start of the day's trading. lines within each file are sorted in increasing order of time.
The remaining values are the stock symbol, number of shares, and price.
Create a single sorted file of all of the 500 files sorted in order of increasing trade times.

In abstract, write a program that takes a set of sorted sequences and computes
the union of these sequences as a sorted sequence. For example,if the input is 

[3,5,7],[0,6],[0,6,28],

The output is

[0,0,3,5,6,6,7,28]

"""
import heapq

def merge_sorted_sequences(file_handles_arr):
	"""
	So the way I'd do this is different than what the book specifies,
	because the efficient thing to do here is get pointers to all 500
	files, and have a pointer to an outfile. grab all of the first pointers,
	get the lowest value, continue iterating the pointer at the lowest value
	until it's exceeded, rinse and repeat, flushing to the outfile
	when the value is exceeded across all pointers.
	"""
	pass




def merge_sorted_arrays(sorted_arrays):
	"""
	What is a heapq and a min-heap?

	a min-heap is a btree where the parent node is smaller than its child nodes.
	This function works by kicking off iterators on all of the arrays,
	and pushing them to the min-heap. The library will handle the sorting
	and insertion.

	The second component simply pops the root node off (which gets the smallest entry),
	then it grabs the iterator from the smallest entry popped, and inserts the next entry
	from the iterator into the min-heap.
	"""

	min_heap = []
	## build list of iterators for each array in sorted arrays.
	sorted_arrays_iters = [iter(x) for x in sorted_arrays]
	
	## push the first element from each iterator into the min heap.
	for ind, val in enumerate(sorted_arrays_iters):
		first_element = next(val, None)
		if first_element is not None:
			heapq.heappush(min_heap, (first_element,ind))
	result = []
	while min_heap:
		smallest_entry, smallest_array_i = heapq.heappop(min_heap)
		smallest_array_iter = sorted_arrays_iters[smallest_array_i]
		result.append(smallest_entry)
		next_element = next(smallest_array_iter, None)
		if next_element is not None:
			heapq.heappush(min_heap, (next_element, smallest_array_i))
			print(min_heap)
	return result


if __name__ == "__main__":
	arr1 = sorted([1,2,4,6,82,23,156])
	arr2 = sorted([5,2,51,34,312,3,1,-3,1,3])
	arr3 = sorted([1,2,14,531,642,1,4,31,6,])
	res = merge_sorted_arrays([arr1,arr2,arr3])
	print(res)




