"""
In football, one can score via:

- safety (2 points)
- field goal (3 points)
- touchdown (7 points)

Write a program that takes a final score and scores
for individual plays and returns the number of combinations 
of given plays that result in the final score
"""

def combinations(final_score, individual_scores):
	"""
	Review this.
	"""

	num_combinations_for_score = [[1] + [0]*final_score for i in individual_scores]
	for i in range(len(individual_scores)):
		for j in range(1, final_score+1):
			without_current_play = 0
			if i >= 1:
				without_current_play = num_combinations_for_score[i-1][j]
			with_current_play = 0
			if j >= individual_scores[i]:
				with_current_play = num_combinations_for_score[i][
					j-individual_scores[i]]
			num_combinations_for_score[i][j] = with_current_play + without_current_play
	return num_combinations_for_score[-1][-1]

if __name__ == "__main__":
	print(combinations(9, [3,2,7]))
