"""
Suppose you need to reorder the elements of
a very large array such that equal elements
appear together.

ex: [b,a,c,b,d,a,b,d] => [a,a,b,b,b,c,d,d] 

If entries are integers, this is trivially done via sorting.
If the number of distinct integers is very small relative to the
size of the array, an efficient approach to sorting
the array is to count the number of occurrences of each
distinct integer and write the appropriate number of each integer, 
in sorted order, to the array. When array entries are objects,
the problem is harder to solve.

You are given an array of student objects. Each student has 
an integer age field that is to be treated as a key. Rearrange
elements of the array so that students of equal age appear together.
The order in which ages appear is not important.
"""
from collections import defaultdict

class Student(object):
	def __init__(self,age,name):
		assert(type(age) == int)
		assert(type(name) == str and len(name) > 0)
		self.name = name
		self.age = age
	def __str__(self):
		return "Student(%d,%s)" % (self.age,self.name)
	def __repr__(self):
		return "Student(%d,%s)" % (self.age,self.name)

def sort_to_group(arr):
	"""
	I figured that this involved a hashmap.
	"""
	frequencies = defaultdict(list)
	for student in arr:
		frequencies[student.age].append(student)
	idx = 0
	for k,v in frequencies.items():
		sublen = len(v)
		for element in v:
			arr[idx] = element
			idx += 1 
	return arr

if __name__ == "__main__":
	arr = [
		Student(8,"A"),
		Student(9,"B"),
		Student(7,"C"),
		Student(9,"D"),
		Student(8,"E"),
		Student(5,"F"),
		Student(8,"G"),
		Student(5,"H"),
		Student(3,"I"),
		Student(8,"J"),
		Student(7,"K"),
		Student(7,"L"),
		Student(9,"M"),
		Student(8,"N"),
		Student(5,"O"),
		Student(5,"P"),
		Student(7,"Q"),
		Student(7,"R"),
		Student(9,"S"),
		Student(8,"T"),
		Student(7,"U"),
		Student(7,"V"),
	]
	print(sort_to_group(arr))
