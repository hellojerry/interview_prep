"""
A file or directory can be specified via a string
called the pathname. The string may specify an absolute
path, i.e. /usr/bin/gcc, or a relative path, i.e. ../mike/stuff,
daily_123/stuff/daily

The same directory may be specified by multiple directory paths.
For example,

	/usr/lib/../bin/gcc -> same as /usr/bin/gcc
	daily_scratch//./../daily_scratch -> same as daily_scratch

. -> current directory
.. -> back one
// -> ignore second slash (in groups of repeated slashes, ignore all but the first).
/ as prefix -> root

Write a program that takes a pathname, and returns the shortest equivalent pathname.
"""


def shortest_path(path):
	"""
	So this is from the stack and queue chapter.
	First thing to do is figure out how we segment stuff.
	a queue would be fifo on this, where a stack is lifo.
	I think we want lifo, so we can eliminate redundancies.
	iterate over the path. if prior is 
	"""

	stack = []
	items = path.split("/")
	for item in items:
		## eliminate current directory.
		if item == ".":
			continue
		if item == "":
			if len(stack) > 0 and stack[-1] == "/":
				continue
			stack.append("/")
			continue
		if item == "..":
			if len(stack) > 1:
				stack.pop()
				stack.pop()
			else:
				stack = []
			continue
		if len(stack) > 0:
			if stack[-1] != "/":
				stack.append("/")
		stack.append(item)
	return "".join(stack)

if __name__ == "__main__":
	expected = "/usr/bin/gcc"
	a1 = "/usr/lib/../././bin/./gcc"
	a2 = "/usr/lib/../bin/gcc"
	r1 = shortest_path(a1)
	r2 = shortest_path(a2)
	assert(r1 == r2 == expected)

	expected = "daily_scratch"
	a1 = "daily_scratch////./../daily_scratch"
	a2 = "./daily_scratch/.././daily_scratch"
	r1 = shortest_path(a1)
	r2 = shortest_path(a2)
	assert(r1 == r2 == expected)
