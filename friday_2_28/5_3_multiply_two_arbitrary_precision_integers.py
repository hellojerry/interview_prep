"""
Certain applications require arbitrary precision arithmetic.
One way to achieve this is to use arrays to represent integers,
i.e. with one digit per array entry, with the most significant
digit appearing first, and a negative leading digit denoting
a negative integer.

ex:
	[1,9,3,7,0,7,7,2,1] -> 193707721
	[-7,6,1,8,3,8,2,5,7,2,8,7] -> -761838257287

write a program that takes two arrays representing integers
and returns an integer representing their product.

the result of the two integers above multiplied is -147573952589676412927.

Hint: use grade-school arithmetic.

Steps for grade-school multiplication.
larger one on top. do least significant on lower
times each digit in the top. if it overflows past ten,
carry the 1, rinse and repeat. for the next significant 
digit, add a zero, then do the same.
"""
import random


def multiply_bad(left_arr,right_arr):
	"""
	This is trickier to model than I'd expected.
	I'm discarding this solution because the question
	is poorly-worded. If we're doing this to get
	around integer overflow, then we should return
	an array.
	"""
	left_len,right_len = len(left_arr),len(right_arr)
	## make sure the left arr is always longer.
	if right_len > left_len:
		left_arr,right_arr = right_arr,left_arr
		left_len,right_len = right_len,left_len
	if left_len == 0 or right_len == 0:
		return 0
	## temporarily remove negatives so we have some sanity.
	leading_left_negative = left_arr[0] < 0
	leading_right_negative = right_arr[0] < 0
	left_arr[0] = abs(left_arr[0])
	right_arr[0] = abs(right_arr[0])
	tens_ct = 0
	## this is a little ugly and can be simplified.
	result = 0
	for i in range(left_len-1,-1,-1):
		bottom_element = left_arr[i]
		carryforward = 0
		sub_res = 0
		## 
		sub_tens_ct = 0
		for j in range(right_len-1,-1,-1):
			top_element = right_arr[j]
			mul_res = (bottom_element*
				top_element) + carryforward
			carryforward = 0
			if mul_res < 10:
				carryforward, to_add = 0,mul_res 
			else:
				carryforward, to_add = divmod(
						mul_res,10)
			sub_res += to_add*(10**sub_tens_ct)
			sub_tens_ct += 1
		## if there's a carryforward, add
		## the carryforward as the most significant digit.
		if carryforward > 0:
			sub_res += (10**sub_tens_ct)*carryforward
		result += sub_res*(10**tens_ct)
		tens_ct += 1
	## determine sign of the result.
	is_negative = False
	if leading_left_negative and not leading_right_negative:
		is_negative = True
	elif not leading_left_negative and leading_right_negative:
		is_negative = True
	if is_negative:
		return -result
	return result

def add_arrays(left_arr,right_arr):
	left_len,right_len = len(left_arr),len(right_arr)
	## make sure the left arr is always longer.
	if right_len > left_len:
		left_arr,right_arr = right_arr,left_arr
		left_len,right_len = right_len,left_len
	if right_len == 0:
		## base case.
		return left_arr
	diff = left_len - right_len
	right_arr = [0]*diff + right_arr
	right_len = len(right_arr)
	ptr = right_len - 1
	carryforward = 0
	out = []
	while ptr >= 0:
		left_val,right_val = left_arr[ptr],right_arr[ptr]
		add_res = left_val + right_val + carryforward
		carryforward,to_add = divmod(add_res,10)
		out.insert(0,to_add)
		ptr -= 1
	if carryforward > 0:
		out.insert(0,carryforward)
	return out

	

def multiply(left_arr,right_arr):
	"""
	I think the question is poorly worded.
	The purpose of this question is to devise
	a way to get past integer overflows. It
	makes little sense to return an integer
	if we're worried about overflows.
	Instead, return an array.
	"""
	left_len,right_len = len(left_arr),len(right_arr)
	## make sure the left arr is always longer.
	if right_len > left_len:
		left_arr,right_arr = right_arr,left_arr
		left_len,right_len = right_len,left_len
	if left_len == 0 or right_len == 0:
		return 0
	## temporarily remove negatives so we have some sanity.
	leading_left_negative = left_arr[0] < 0
	leading_right_negative = right_arr[0] < 0
	left_arr[0] = abs(left_arr[0])
	right_arr[0] = abs(right_arr[0])
	result = []
	tens_ct = 0
	for i in range(left_len-1,-1,-1):
		bottom_element = left_arr[i]
		carryforward = 0
		sub_res = []
		for j in range(right_len-1,-1,-1):
			top_element = right_arr[j]
			mul_res = (bottom_element*
				top_element) + carryforward
			carryforward,to_add = divmod(mul_res,10)
			sub_res.insert(0,to_add)
		if carryforward > 0:
			sub_res.insert(0,carryforward)
		for t in range(tens_ct):
			sub_res.append(0)
		result = add_arrays(result,sub_res)
		tens_ct += 1
	is_negative = False
	if leading_left_negative and not leading_right_negative:
		is_negative = True
	elif not leading_left_negative and leading_right_negative:
		is_negative = True
	if is_negative:
		result[0] = -result[0]
	return result
	## now we've got a bunch of arrays of increasing size.
	## simplest thing to do here is implement an array adding
	## function instead of appending to the result.
 

def as_int(arr):
	intermediate = int("".join(str(i) for i in arr))
	
def easy_multiply(A,B):
	if A[0] < 0 and B[0] >= 0:
		return_neg = True
	elif A[0] >= 0 and B[0] < 0:
		return_neg = True
	else:
		return_neg = False
	A[0],B[0] = abs(A[0]),abs(B[0])
	intermediate_a = int("".join(str(i) for i in A))
	intermediate_b = int("".join(str(i) for i in B))
	mult = str(intermediate_a * intermediate_b)
	res = [int(i) for i in list(mult)]
	if not return_neg:
		return res
	else:
		res[0] = -res[0]
		return res
	
	

if __name__ == "__main__":
	a1 = [1,2,5]
	a2 = [1,2,7]
	r1 = add_arrays(a1,a2)
	assert(r1 == [2,5,2])
	expected = easy_multiply(a1,a2) 
	res = multiply(a1,a2)
	assert(res == expected)
	a1 = [-1]
	a2 = [2,2]
	expected = easy_multiply(a1.copy(),a2.copy())
	res = multiply(a1,a2)
	assert(res == expected)

	a = [random.randint(1,9) for j in range(
		1,random.randint(2,100))]
	b = [random.randint(1,9) for j in range(
		1,random.randint(2,100))]
	expected = easy_multiply(a,b)
	res = multiply(a,b)
	assert(res == expected)

