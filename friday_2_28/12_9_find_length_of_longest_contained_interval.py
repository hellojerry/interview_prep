"""
Write a program wihch takes a set of integers represented 
by an array, and returns the size of the largest subset of integers
in the array where if two integers are in the subset, so are all integers
in between them.

ex: [3,-2,7,9,8,1,2,0,-1,5,8]
largest subset: [-2,-1,-0,1,2,3]
res = 6
"""

def largest_subset(arr):
	"""
	I see a couple of ways to do this.
	One way is to sort the array and then do a simple
	continuity check, returning the max.
	Another way to do this is to make an array equal to the distance
	between the biggest value and the smallest value, iterate over the array,
	flagging the matched values in the sequence, and returning the max sequence of trues.
	Seems to be pretty much the same thing as the last one, except instead of log n +n,
	it's only o(n), but the memory consumption could be huge.
	This question is in the hashmap section, so there might be a way to do this with a hashmap.
	another way to do this is stuff this all into a set.
	for each element, if el+1 or el-1 in the set, increment up, and remove the
	"""
	as_set = set(arr)
	max_len = 1
	while len(as_set) > 0:
		candidate_len = 1
		current_value = as_set.pop()
		one_less = current_value - 1
		one_more = current_value + 1
		if one_less in as_set:
			while True:
				candidate_len += 1
				as_set -= {one_less}
				one_less -= 1
				if one_less not in as_set:
					break
		if one_more in as_set:
			while True:
				candidate_len += 1
				as_set -= {one_more}
				one_more += 1
				if one_more not in as_set:
					break
		max_len = max(candidate_len,max_len)
	return max_len	
if __name__ == "__main__":

	res = largest_subset([3,-2,7,9,8,1,2,0,-1,5,8])
	expected = 6
	print(res)
	assert(res == expected)
	res = largest_subset([1,3,5,7,9,11])
	assert(res == 1)
