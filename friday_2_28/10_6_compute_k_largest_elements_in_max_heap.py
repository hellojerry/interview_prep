"""
A heap contains limited information 
about the ordering of elements, so unlike a sorted 
array or a balanced BST, naive algorithms for computing the K
largest elements in a max heap have a time complexity that
depends linearly on the number of elements in the collection.

Given a max-heap, represented by an array, design an algorithm
that comutes the K largest elements in the heap. You cannot
modify the heap.

ex: [561,314,401,28,156,359,271,11,3], K=4:
res = [561,314,401,359]

note: python heaps are min-heaps
"""
import heapq
def get_k_largest(heap,K):
	"""
	So this is pretty weird, but whatever.
	The trick is just to iterate over the initial heap,
	doing heappush (negative) onto a new heap, then
	doing heappop K times on the new heap. 
	"""

	new_heap = []
	new_heap.append((-heap[0],0))
	heapq.heapify(new_heap)
	result = []
	for i in range(K):
		candidate_idx = new_heap[0][1]
		result.append(-heapq.headppop(candidate_max_heap)[0])
		left_child_idx = (2*candidate_idx)+1
		if left_child_idx < len(heap):
			heapq.heappush(new_heap,(-heap[left_child_idx],left_child_idx))
		right_child_idx = 2*candidate_idx + 2
		if right_child_idx < len(heap):
			heapq.heappush(new_heap, (-A[right_child_idx],right_child_idx))
	return result
