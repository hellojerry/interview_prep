"""
Many different binary trees have the same preorder traversal sequence.
In this problem, the preorder traversal is modified to mark where a left
or right child is empty.

ex:
			H
		B               C
	F	    E               D
                  A                     G
                                     I

sequence: [H,B,F,None,None,E,A,None,None,None,C,None,D,None,G,I,None,None,None]

Make an algorithm to reconstruct a btree based on a marked preorder traversal.

Hint: it's difficult to solve this from left-to-right
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == str and len(val) > 0)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%s)" % self.val
	def __repr__(self):
		return "TreeNode(%s)" % self.val

def preorder(root,arr):
	if root is None:
		arr.append(None)
		return
	arr.append(root.val)
	preorder(root.left,arr)
	preorder(root.right,arr)

def construct_from_preorder(arr):
	"""
	First step on this is making the preorder 
	traversal from test data, so 
	I understand the steps fully.

	So I have a couple things I know a priori.
	1. the first value is the root.
	2. the item before the first null value is
	the furthest left node.
	if I pass in a counter recursively,
	I could start root.right at arr[ct_idx]
	alternatively, I could eliminate values from
	the array 
	"""
	arrlen = len(arr)
	if arrlen == 0:
		return None
	root_val = arr.pop(0)
	if root_val is None:
		return None
	root = TreeNode(root_val)
	root.left = construct_from_preorder(arr)
	root.right = construct_from_preorder(arr)
	return root


if __name__ == "__main__":
	F = TreeNode("F")
	A = TreeNode("A")
	E = TreeNode("E",A)
	B = TreeNode("B",F,E)
	I = TreeNode("I")
	G = TreeNode("G",I)
	D = TreeNode("D",None,G)
	C = TreeNode("C",None,D)
	root = TreeNode("H",B,C)
	expected_acc = []
	preorder(root,expected_acc)
	expected_acc = ["H","B","F",None,None,"E","A",None,None,None,"C",None,"D",None,"G","I",None,None,None]
	root = construct_from_preorder(expected_acc.copy())
	acc = []
	preorder(root,acc)
	assert(acc == expected_acc)
