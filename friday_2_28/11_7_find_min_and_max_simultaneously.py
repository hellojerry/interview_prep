"""
Design an algorithm to find the min and max elements in
an array.

ex: 
	a = [3,2,5,1,2,4]
	min = 1, max = 5
Hint: use the fact that a < b and b < c implies
a < c to reduce the number of comparisons
"""

def get_min_and_max_easy(arr):
	"""
	This might not be in the spirit of the question.
	"""
	as_sorted = sorted(arr)
	return as_sorted[0], as_sorted[-1]

def get_min_and_max(arr):
	"""
	Another way to do this is just do two at a time.
	This would mean iterating over the array only half of it
	"""
	arrlen = len(arr)
	if arrlen == 0:
		return (0,0)
	even = arrlen % 2 == 0
	ptr = 0
	min_ = arr[ptr]
	max_ = arr[ptr]
	if not even:
		ptr += 1
	while ptr < arrlen:
		left = arr[ptr]
		right = arr[ptr+1]
		sub_min = left
		sub_max = right
		if left > right:
			sub_min = right
			sub_max = left
		min_ = min(min_,sub_min)
		max_ = max(max_,sub_max)
		ptr += 2
	return min_,max_

if __name__ == "__main__":
	a = [3,2,5,1,2,4]
	res = get_min_and_max(a)
	assert(res == (1,5))
	a = [3,2,0,1,2,4,5]
	res = get_min_and_max(a)
	assert(res == (0,5))
