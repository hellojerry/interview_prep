"""
Roman numbers uses I,V,X,L,C,D,M.

I -> 1
V -> 5
X -> 10
L -> 50
C -> 100
D -> 500
M -> 1000

Roman numbers are valid if symbols appear in
non-increasing order, with the following exceptions:

I can immediately precede V and X
X can immediately precede L and C
C and immediately precede D and M

back-to-back exceptions are not allowed, i.e.

illegal: IXC
illegal: CDM

A valid complex roman number string represents the integer which is the sum
of the symbols tgat do not correspond to exceptions. For the exceptions,
add the difference of the larger symbol and smaller symbol.

ex:

XXXXXIIIIIIIII
LVIIII
LIX
are all valid number strings representing 59.

Write a program which takes as input a valid roman number string S and returns
the integer value.

Hint: start by solving the problem assuming no exceptions exist.
"""


def roman_to_integer(S):
	"""
	How do we account for exceptions?
	
	"""
	mappings = {
		"I":1,
		"V":5,
		"X":10,
		"L":50,
		"C":100,
		"D":500,
		"M":1000
	}
	allowed_priors = {
		## first one listed is an exception
		"V": ["I","V","X","L","C","D","M"],
		"X": ["I","X","L","C","D","M"],
		"L": ["X","L","C","D","M"],
		"C": ["X","C","D","M"],
		"D": ["C","D","M"],
		"M": ["C","M"],
		"I":["I","V","X","L","C","D","M"],
	}
	out = 0
	prior = None
	prior_value = 0
	prior_is_exception = False
	for element in S:
		mapping = mappings[element]
		if prior is None:
			prior = element
			prior_value = mapping
			out += mapping
			prior_has_exception = False
			continue
		if prior not in allowed_priors[element]:
			raise Exception("illegal")
		if prior_is_exception and mapping > prior:
			raise Exception("illegal")
		if prior_value < mapping:
			## reverse prior computation
			## and then add in larger - smaller
			out -= prior_value
			out += (mapping - prior_value)
			prior_value = mapping
			prior_has_exception = True
		else:
			out += mapping
			prior_value = mapping
			prior_has_exception = False
	return out	




if __name__ == "__main__":
	a1 = "XXXXXIIIIIIIII"
	r1 = roman_to_integer(a1)
	a2 = "LVIIII"
	r2 = roman_to_integer(a2)
	a3 = "LIX"
	r3 = roman_to_integer(a3)
	a4 = "XIX"
	r4 = roman_to_integer(a4)
	assert(r1 == 59)
	assert(r2 == 59)
	assert(r3 == 59)
	assert(r4 == 19)
	a5 = "MMMMCM"
	r5 = roman_to_integer(a5)
	assert(r5 == 4900)
