"""
Write a proigram that takes a positive integer N
and a size K <= N and returns a size K subset
of {0,1,2..n-1}
"""
import random

def random_subset(N,K):
	changed_elements = {}
	for i in range(K):
		rand_idx = random.randrange(i,N)
		rand_idx_mapped = changed_elements.get(rand_idx,rand_idx)
		i_mapped = changed_elements.get(i,i)
		changed_elements[rand_idx] = i_mapped
		changed_elements[i] = rand_idx_mapped
	return [changed_elements[i] for i in range(K)]
