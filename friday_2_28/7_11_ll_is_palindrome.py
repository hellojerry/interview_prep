"""
Write a program that tests whether a singly linked list is palindromic.


ex:
	1->3->5->3->1 => True
	1->3->5->5->3->1 => True
	1->3->5->4->1 => False
	1->3->5->5->4->1 => False

Hint: It's easier if you traverse forwards and backwards simultaneously.
"""
class Node(object):
	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_
	def __str__(self):
		return "Node(%d)" % self.val
	def __repr__(self):
		return "Node(%d)" % self.val
def reverse_ll(head):


	prior = None
	cur = head
	while cur:
		next_ = cur.next_
		cur.next_ = prior
		prior = cur
		cur = next_
	return prior

def is_palindrome(head):
	"""
	I've done this one before.
	The easy thing is stuff the values into an array
	and test the array. Not in the spirit of the question though.
	I could count to the end, match head with end,
	then move to n2, go forward ct-1 nodes, match, 
	until ct = 0.
	Another way to do this is traverse to the end, getting the count,
	then reversing the pointers back until ct == original_ct/2.
	Then, I iterate from the head and second head, checking equality.

	even better:
	traverse to the end with slow and fast pointers. fast pointer will get the count.
	get slow head and fast head.
	reverse the slow head after traversal to get the second half in a reversed fashion.
	iterate over the original head and slow head simultaneously and compare values.
	when there's one remaining (if count is odd) or there are none remaining, we're good

	"""
	cur = head
	ct = 0
	slow, fast = cur,cur
	while fast and fast.next_:
		slow = slow.next_
		fast = fast.next_.next_
	reversed_head = reverse_ll(slow)
	reversed_cur = reversed_head
	while cur and reversed_cur:
		if cur.val != reversed_cur.val:
			return False
		cur = cur.next_
		reversed_cur = reversed_cur.next_

	return True


if __name__ == "__main__":
	n1b = Node(1)
	n3b = Node(3,n1b)
	n5 = Node(5,n3b)
	n3 = Node(3,n5)
	n1 = Node(1,n3)
	reversed_head = reverse_ll(n1)
	out = []
	cur = reversed_head
	while cur:
		out.append(cur)
		cur = cur.next_
	n1 = reverse_ll(n1)
	assert(is_palindrome(n1) == True)

	n1b = Node(1)
	n3b = Node(3,n1b)
	n5b = Node(5,n3b)
	n5 = Node(5,n5b)
	n3 = Node(3,n5)
	n1 = Node(1,n3)
	assert(is_palindrome(n1) == True)

	n1b = Node(1)
	n4 = Node(4,n1b)
	n5 = Node(5,n4)
	n3 = Node(3,n5)
	n1 = Node(1,n3)
	assert(is_palindrome(n1) == False)

	n1b = Node(1)
	n4b = Node(4,n1b)
	n5b = Node(5,n4b)
	n5 = Node(5,n5b)
	n3 = Node(3,n5)
	n1 = Node(1,n3)
	assert(is_palindrome(n1) == False)
