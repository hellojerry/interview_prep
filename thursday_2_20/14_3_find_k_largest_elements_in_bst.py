"""
Write a program that takes a BST and an integer K.
Return the K largest elements in increasing order.
			19
		7		    43
	   3	   11	      23          47
         2  5        17       	37           53
                    13        29   41
                               31

K = 3
result: [53,47,43]
"""

class TreeNode(object):

	def __init__(self,value,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(value) == int)
		self.value = value
		self.left = left
		self.right = right

	def __repr__(self):
		return "TreeNode(%d)" % self.value

	def __str__(self):
		return "TreeNode(%d)" % self.value


def find_k_largest(root,ref,acc):
	"""
	So we know that the output is supposed to be an array.
	The question is, how do we get the array?
	If it's a BST, we know that the values
	are always left < center < right.

	The book is hinting at doing an inorder traversal, so left,then center, then right.
	I think doing a reverse inorder is what we want to do.
	First step: let's see what a reverse inorder traversal returns.
	Ok, so we have a clear set of results
	from a reverse inorder traversal. 
	appending to the accumulator inorder isn't quite working.
	If this were C++ i'd use a reference, but that won't
	quite work. We can use a temp 
	"""

	
	if not root:
		return None
	if root.right:
		find_k_largest(root.right,ref,acc)
	ref[0] -= 1
	if ref[0] < 0:
		return
	acc.append(root.value)
	if root.left:
		find_k_largest(root.left,ref,acc)
	return


if __name__ == "__main__":
	n2 = TreeNode(2)
	n5 = TreeNode(5)
	n3 = TreeNode(3,n2,n5)
	n13 = TreeNode(13)
	n17 = TreeNode(17,n13)
	n11 = TreeNode(11,None,n17)
	n7 = TreeNode(7,n3,n11)

	n31 = TreeNode(31)
	n29 = TreeNode(29,None,n31)
	n41 = TreeNode(41)
	n37 = TreeNode(37,n29, n41)
	n23 = TreeNode(23,None,n37)

	n53 = TreeNode(53)
	n47 = TreeNode(47,None,n53)
	n43 = TreeNode(43,n23,n47)
	root = TreeNode(19,n7,n43)
	acc = []
	ref = [3]
	res = find_k_largest(root,ref,acc)
	assert(acc == [53,47,43])

	acc = []
	ref = [9]
	res = find_k_largest(root,ref,acc)
	assert(acc == [53,47,43,41,37,31,29,23,19])

	acc = []
	ref = [14]
	res = find_k_largest(root,ref,acc)
	assert(acc == [53,47,43,41,37,31,29,23,19,17,13,11,7,5])
