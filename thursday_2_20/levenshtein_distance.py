

def levenshtein_distance(left,right):

	lmax = len(left)+1
	rmax = len(right)+1
	## make a matrix with rmax columns and lmax rows
	matrix = [[0 for i in range(rmax)] for j in range(lmax)]
	## make the top row go from 0 to rmax
	## make the left column go from 0 to lmax
	for col in range(rmax):
		matrix[0][col] = col
	for row in range(lmax):
		matrix[row][0] = row
	for x in range(lmax):
		for y in range(rmax):
			if left[x-1] == right[y-1]:
				matrix[x][y] = min(
					matrix[x][y-1]+1,
					matrix[x-1][y-1],
					matrix[x-1][y]+1
				)
			else:
				matrix[x][y] = min(
					matrix[x][y-1]+1,
					matrix[x-1][y-1]+1,
					matrix[x-1][y]+1
				)
	return matrix[-1][-1]

if __name__ == "__main__":
	left,right = "Sundays", "Saturday"
	res = levenshtein_distance(left,right)
	assert(res == 4)
