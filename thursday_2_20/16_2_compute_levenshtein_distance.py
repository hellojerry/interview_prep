"""
Spell checkers make suggestions for misspelled
words. Given a misspelled string, a spell checker
should return words in the dictionary which are close
to the misspelled string.

In 1965, Vladimir Levenshtein defined the distance
between two words as the minimum number of "edits"
it would take to transform the misspelled word into
the correct word, where a single edit is the insertion, deletion,
or substitution of a single character. For example, the
Levenshtein distance between "Saturday" and "Sundays"
is 4:
	- delete the first "a" and "t",
	- substitute "r" with "n"
	- insert the trailing "s"

Write a program that takes two strings and computes
the minimum number of edits needed to transform the 
first string into the second string.

Hint: Consider the same problem for prefixes of two strings.
"""


def levenshtein_distance(sleft,sright):
	"""
	So this is a dynamic programming problem.

	The formula for this is:
		dist(left[0,ltail],right[0,rtail]) = 
			1 + min([
					dist(left[0,ltail-1],right[0,rtail-1]),
					dist(left[0,ltail],right[0,rtail-1]),
					dist(left[0,ltail-1],right[0,rtail])
				])	
		The three terms in the formula forrespond to:
			1. transform left(0,ltail-1) to right(rtail-1) then
				substituting left's last character with 
				right's last character
			2. transform left(0,ltail) to right(rtail-1) then
				adding right's last character at the end
			3. transform left(0,ltail-1) to right(rtail) then
				deleting left's last character.
	The strategy for this is populating a matrix
	that is len(left+1) x len(right+1), and then
	on the left column, populating downward
	with 1 increasing, and on the top row, populating
	rightward with one increasing. Then
	iterate over len(left+1) with subloop of len(right+1)
	and check the equality of left[x-1] == right[y-1].
	if equal, apply the formula:
		matrix[x][y] = min(
				matrix[x-1][y] + 1,
				matrix[x-1][y-1],
				matrix[x][y-1] + 1
	else, apply this formula:
		matrix[x][y] = min(
				matrix[x-1][y] + 1,
				matrix[x-1][y-1] + 1
				matrix[x][y-1]
				)

	when completed, return matrix[-1][-1]
	"""
	left_len = len(sleft) + 1
	right_len = len(sright)+1
	matrix = [[0 for i in range(right_len)] for j in range(left_len)]
	for row in range(1,len(matrix)):
		matrix[row][0] = row
	for col in range(len(matrix[0])):
		matrix[0][col] = col
	for x in range(1,left_len):
		for y in range(1,right_len):
			if left[x-1] == right[y-1]:
				matrix[x][y] = min(
					matrix[x-1][y] + 1,
					matrix[x-1][y-1],
					matrix[x][y-1]+1
				)
			else:
				matrix[x][y] = min(
					matrix[x-1][y] + 1,
					matrix[x-1][y-1] + 1,
					matrix[x][y-1] + 1
				)
	return matrix[-1][-1]

if __name__ == "__main__":
	left,right = "Saturday", "Sundays"
	res = levenshtein_distance(left,right)
	assert(res == 4)
	left = "bog"
	right = "bogoo"
	res = levenshtein_distance(left,right)
	assert(res == 2)

