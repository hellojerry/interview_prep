"""
A number of cities are arranged on a circular road.
You need to visit all the cities and come back to the starting city.
A certain amount of gas is available at each city. The amount of gas
summed up over all cities is equal to the amount of gas required to 
go around the road once. The gas tank has unlimited capacity.
Call a city "ample" if you can begin at that city
with an empty tank, refill it, then travel through all the remaining cities,
refilling at each, and return to the ample city, without running out
of gas at any point.

				5Gal(C)
			//200M
		30Gal(D)			\\600M
		||400M			
		25Gal(E)			20Gal (B)
		||600M				||900M
		10Gal(F)			50Gal (A)
			\\200M		//100M 	
				10Gal(G)
The truck gets 20mpg. The total miles in this road is 3000.
We can start with no gas at city D and complete a circular route.
Thus, D is an "ample" city.
"""

class Node(object):

	def __init__(self,name,gas_amt,dist_next,next_=None):
		assert(type(name) == str)
		for i in [gas_amt,dist_next]:
			assert(type(i) == int and i > 0)
		assert(next_ is None or type(next_) == Node)
		self.name = name
		self.gas_amt = gas_amt
		self.dist_next = dist_next
		self.next_ = next_

	def __str__(self):
		return "Node(%s,gas:%d,dist_next:%d)" % (self.name,self.gas_amt,self.dist_next)
	def __repr__(self):
		return "Node(%s,gas:%d,dist_next:%d)" % (self.name,self.gas_amt,self.dist_next)


def is_ample_city(node):
	"""
	First question is, how do we model this from a data structure perspective?
	I could do this with a linked list.
	This is a brute-force solution for solving
	all possible ample and non-ample cities.
	Each test takes o(n) with n being the number of nodes.
	"""
	gas_remaining = 0
	starting_point = node
	cur_node = node
	has_passed = False
	while cur_node.next_:
		if cur_node is starting_point and has_passed:
			return True
		elif cur_node is starting_point and not has_passed:
			has_passed = True
		gas_remaining += cur_node.gas_amt
		gas_to_use = cur_node.dist_next / 20
		gas_remaining -= gas_to_use
		if gas_remaining < 0:
			return False
		cur_node = cur_node.next_

if __name__ == "__main__":
	node_G = Node("G",10,100)
	node_F = Node("F",10,200,node_G)
	node_E = Node("E",25,600,node_F)
	node_D = Node("D",30,400,node_E)
	node_C = Node("C",5,200,node_D)
	node_B = Node("B",20,600,node_C)
	node_A = Node("A",50,900,node_B)
	node_G.next_ = node_A
	res = is_ample_city(node_D)
	assert(res == True)

	for i in [node_G,node_F,node_E]:
		print(is_ample_city(i))
