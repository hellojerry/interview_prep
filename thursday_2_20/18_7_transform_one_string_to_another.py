"""
Let S and T be strings and D a dictionary, i.e. a set of strings.

Define S to produce T if there exists a sequence of strings from the dictionary {
such that the first string is S, the last string is T, 
and the adjacent strings have the same length and differ in exactly one character.
The sequence P is called a production sequence. For example:

dictionary:
	[bat,cot,dog,dag,dot,cat]
production sequence:
	[cat,cot,dot,dog]

Given a dictionary D and two strings S and T, write a program to determine if S
produces T. assume all characters are lowercase and alphabetical. If S
does produce T, output the length of the shortest production sequence, otherwise output -1.

Treat strings as vertices in an undirected grapg, with an edge between u and v if and only
if the corresponding strings differe in one character.
"""

## I have no idea what this is actually asking.
import string
class DistanceString(object):
	def __init__(self, str_, distance):
		self.string = str_
		self.distance = distance
	def __str__(self):
		return "DistanceString(%s, %d)" % (self.string,self.distance)

	def __repr__(self):
		return "DistanceString(%s, %d)" % (self.string,self.distance)

def transform_string(D, S, T):
	assert(type(D) == set)
	queue = [DistanceString(S,0)]
	D.remove(S.string)
	while queue:
		dist_string = queue.pop(0)
		if dist_string.string == T:
			return dist_string.distance
		for i in range(len(dist_string.string)):
			for char in string.ascii_lowercase:
				candidate = dist_string.string[:i] + c + dist_string.string[(i+1):]
				if candidate in D:
					D.remove(candidate)
					queue.append(DistanceString(candidate,dist_string.distance+1))
	return -1

