"""
An N-bit gray code is a permutation of [0,1,2,...(2**n)-1]
such that the binary representations of successive intevers 
in the sequence only differ in one place. Additionally,
The first and final integers may only differ in one place.

Two possible gray codes for N=3:


[0,4,5,7,6,2,3,1]
000,100,101,111,110,010,011,001

[0,1,3,2,6,7,5,4]
000,001,011,010,110,111,101,100

Write a program that takes N as input
and returns an N-bit Gray Code.

Hint: Write out gray codes for N=2,n=3,n=4
"""

def differs_by_single_bit(a,b):
	bit_diff = a ^ b
	return bit_diff and not (bit_diff & (bit_diff - 1))

def directed_gray_code(num_bits,result, history):
	## first, test if first and last codes differ by one bit.
	if len(result) == 2**num_bits:
		return differs_by_single_bit(result[0], result[-1])	

	for i in range(num_bits):
		previous_code = result[-1]
		candidate_next = previous_code ^ (1 << i)
		if candidate_next not in history:
			history.add(candidate_next)
			result.append(candidate_next)
			if directed_gray_code(num_bits, result,history):
				return True
	return False


def generate_gray_code(N):
	"""
	The strategy with this is to add the value
	to the sequence only if it is distinct from
	the values in the sequence, and differs 
	in exactly one place from the previous value.
	"""
	res = [0]
	out = directed_gray_code(N,res,set(res))
	return res


if __name__ == "__main__":
	print(generate_gray_code(2))
	print(generate_gray_code(3))
