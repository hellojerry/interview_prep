"""
Given a set of string and a name, design an efficient algorithm
that checks whether the name is the concatenation of a sequence
of dictionary words. If such a concatenation exists, return it.
A dictionary word may appear more than once in the sequence.

ex:
"amanaplanacanal"

["a","man","plan","canal"]

hint: solve the generalized problem - determine for each prefix of the name
whether it is the concatenation of dictionary words.
"""


def bedbathandbeyond_old(word,dictionary,acc):
	"""
	I have no idea how i'd tackle this.
	Let's see - this is dynamic programming, but i don't see
	how a matrix would fit in. My guess is that we 
	take the first letter, see if it's a word - if not, add the next 
	letter. if those combine to make a word, add it to an accumulator
	and go again.
	This version is ugly and i think it might have some bugs.
	"""
	if len(word) == 0:
		return acc
	acc_len = len(acc)
	idx = 0
	while idx < len(word):
		section = word[0:idx+1]
		if section in dictionary:
			acc.append(section)
			return bedbathandbeyond(
				word[idx+1:],dictionary,acc)
		idx += 1
	## if we can't locate a word,
	## ...try again vs prior?
	idx = 0
	if len(acc) == 0:
		return []
	prior = acc[-1]
	while idx < len(word):
		print(word)
		section = word[0:idx+1]
		if prior+section in dictionary:
			acc[-1] = prior + section
			return bedbathandbeyond(
				word[idx+1:],dictionary,acc)
		idx += 1
	if len(acc) < 2:
		return []
	prior_2_combined = acc[-2] + acc[-1]
	idx = 0
	while idx < len(word):
		section = word[0:idx+1]
		if prior_2_combined + section in dictionary:
			acc.pop()
			acc[-1] = prior_2_combined+section
			return bedbathandbeyond(word[idx+1:],
				dictionary,acc)
		idx += 1
	return []

def bedbathandbeyond(word,dictionary,acc):
	if len(word) == 0:
		return True
	word_len = len(word)
	idx = 0
	while idx < len(word):
		section = word[0:idx+1]
		if section in dictionary:
			acc.append(section)
			if bedbathandbeyond(word[idx+1:],
				dictionary,acc):
				return True
			else:
				acc.pop()
		idx += 1
	return False

if __name__ == "__main__":
	word = "bedbathandbeyond"
	dictionary = {"bed","bath","and","be","beyond","hand","bat","than","on"}
	acc = []
	res = bedbathandbeyond(word,dictionary,acc)
	assert(res == True)
	assert(acc == ["bed","bat","hand","beyond"])
	word = "amanaplanacanalpanama"
	dictionary = {"anal","a","man","plan","canal","am","panama","pan","can"}
	acc = []
	res = bedbathandbeyond(word,dictionary,acc)
	assert(res == True)
	assert(acc == ["a","man","a","plan","a","canal","pan","am","a"])

	word = "gargamel"
	dictionary = {"a","am","mell","gorg","gorge"}
	acc = []
	res = bedbathandbeyond(word,dictionary,acc)
	assert(res == False)
	assert(acc == [])

	word = "gramophone"
	dictionary = {"am","gramophone","phone","mop","hon","gram"}
	acc = []
	res = bedbathandbeyond(word,dictionary,acc)
	assert(res == True)
	assert(acc == ["gramophone"])
