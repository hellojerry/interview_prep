"""
Given a binary tree, such as 
	 10
	/ \
       2   3
      /   / \
    -5   4  -7
              \
               28
Find the maximum sum of a path on the tree. 
Path can connect between any two nodes; not just from root to leaves. 
For example, instead of just going 1 → 2 or 1 → 3, you can also go 2 → 1 → 3 ⇒ maxSum = 2 + 1 + 3 = 6 instead of 4. 
So the maxSum of the above tree will be: 4 → 2 → 1 → 3 ⇒ 4 + 2 + 1 + 3 = 10

second scenario: 28 on the furthest lowest leaf, 10 at root 

proper solution should be 2+10+3-7+28 = 36
"""
class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		self.value = value
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

class Solver(object):

	def __init__(self):
		self.current_max = 0
		self.current_max_unset = True

	def find_max_path(self,node):
		if not node:
			return 0
		l_res = self.find_max_path(node.left)
		r_res = self.find_max_path(node.right)
		max_of_leaves = max(l_res,r_res) + node.value
		max_with_without_leaves = max(max_of_leaves, node.value)
		max_incl_base = max(max_with_without_leaves, node.value + l_res + r_res)

		if self.current_max_unset:
			self.current_max = max_with_without_leaves
			self.current_max_unset = False
		else:
			self.current_max = max(max_with_without_leaves, self.current_max)
		return max_with_without_leaves


if __name__ == "__main__":
	bot_left = TreeNode(-5)
	mid_left = TreeNode(2, bot_left)
	bot_right_left = TreeNode(4)
	furthest_bottom_right_left = TreeNode(28)
	bot_right_right = TreeNode(-7, furthest_bottom_right_left)
	mid_right = TreeNode(3, bot_right_left, bot_right_right)
	root = TreeNode(10, mid_left, mid_right)
	solver = Solver()
	solver.find_max_path(root)
	print(solver.current_max)

	bot_left = TreeNode(1)
	root = TreeNode(-2, bot_left)
	solver = Solver()
	solver.find_max_path(root)
	print(solver.current_max)
