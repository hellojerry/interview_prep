"""
Write a function to determine the number of bits you'd need
to flip to convert integer A to integer B

Ex:

input 29 (11101), 15 (01111)

output: 2
"""



def count_set_bits(number):

	ct = 0
	while number:
		ct += number & 1
		number >>= 1
	return ct

def get_number_required(left, right):

#	ct = 0
#	as_bin = bin(left ^ right)[2:]
	return count_set_bits(left ^ right)
#	for item in as_bin:
#		if item == "1":
#			ct += 1
	return ct


if __name__ == "__main__":
	r1 = get_number_required(29,15)
	r2 = get_number_required(15,29)
	assert(r1 == r2 == 2)
	r3 = get_number_required(87,15)
	r4 = get_number_required(15,87)
	assert(r3 == r4 == 3)

