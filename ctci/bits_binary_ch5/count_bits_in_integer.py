"""
Use a binary trick to count the number of 1 bits in an integer.
"""

def count_set_bits(number):
	count = 0
	while number:
		count += number & 1
		number >>= 1
	return count

def count_unset_bits(number):
	count = 0
	while number:
		count += number & 0
		number <<= 1
	return count

print(bin(5))
print(count_unset_bits(5))
print(count_set_bits(5))
