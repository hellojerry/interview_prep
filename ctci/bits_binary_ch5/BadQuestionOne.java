/*

You are given two 32-bit numbers, N and M, and two bit positions, I and J.
Write a method to insert M into N such that M starts at bit J and ends at bit I.
You can assume that the bits J through I have enough space to fit all of M.

In other words, if M = 10011, you can assume that there are at least 5 bits between J and I.
You would not, for example, have J = 3 and I = 2, because M could not fully fit between bit 3 and bit 2.

Ex:

input:
	N = 10000000000
	M = 10011
	I = 2
	J = 6

output:
	N = 10001001100

Remember that J and I are zero-indexed, and count from the RIGHT.

*/


class BadQuestionOne{

	/*
		So we have set_bit, update_bit,
		clear_bits left-to-right
		clear_bits right-to-left,
		clear bit, 
		get bit.
	
		I think we could just do

		int ct = 0
		for i in range(i,j+1):
			bit_val: = M[ct]
			new_val = update_bit(N, i, bit_val)

	*/


	static boolean getBit(int num, int i){
		int mask = (1 << i);
		return (num & mask) != 0;

	}

	static int updateBit(int num, int i,boolean isOne){
		int value = isOne ? 1 : 0;
		int leftMask = ~(1 << i);
		int rightMask = (value << i);
		return (num & leftMask) | rightMask;
	} 

	static int solve(int M, int N, int I, int J){
		int mPosition = 0;
		int newN = N;
		for (int i = I; i < J+1; i++){
			boolean bitAtVal = getBit(M, mPosition);
			newN = updateBit(newN, i, bitAtVal);
			mPosition++;
		}
		return newN;
	}

	public static void main(String[] args){
		int N = 0B10000000000;
		int M = 0B10011;
		int I = 2;
		int J = 6;
		int result = solve(M, N, I, J);
		/*
			The output in this should be 1100, which is equal
			10001001100 in binary.
			To verify, after compilation do java -ea BadQuestionOne
		*/
		System.out.println("result: " + result);
		assert result == 1100;
	}
}
