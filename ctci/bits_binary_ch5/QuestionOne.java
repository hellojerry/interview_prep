/*

You are given two 32-bit numbers, N and M, and two bit positions, I and J.
Write a method to insert M into N such that M starts at bit J and ends at bit I.
You can assume that the bits J through I have enough space to fit all of M.

In other words, if M = 10011, you can assume that there are at least 5 bits between J and I.
You would not, for example, have J = 3 and I = 2, because M could not fully fit between bit 3 and bit 2.

Ex:

input:
	N = 10000000000
	M = 10011
	I = 2
	J = 6

output:
	N = 10001001100

Remember that J and I are zero-indexed, and count from the RIGHT.

*/


class BadQuestionOne{



	/*
		OK, new attempt. We need to be able to do 
		all of the binary questions in effectively constant time.
		Let's try this:
			- clear out bits I through J in N
			- shift M so that it lines up with I through J
			- merge M into N with an | statement.
	*/

	static int shiftMLeft(int M, int I){
		return M << I;
	}

	static int clearBitsIThroughJ(int N, int I, int J){
		/*
			We need to make sure that bits outside
			of this range on N don't get affected.
			This means that we want a mask with all 1's
			outside of the range that we can & against.
		*/

		int all_ones = ~0;
		int left_mask = all_ones << (J+1);
		int right_mask = (1 << I) -1;
		int mixed = left_mask | right_mask;
		int cleared_N = N & mixed;
		return cleared_N;	
	}

	static int solve(int M, int N, int I, int J){
		int m_shifted_left = shiftMLeft(M, I);
		int cleared_n = clearBitsIThroughJ(N, I, J);
		return cleared_n | m_shifted_left;


	}

	public static void main(String[] args){
		int N = 0B10000000000;
		int M = 0B10011;
		int I = 2;
		int J = 6;
		int result = solve(M, N, I, J);
		/*
			The output in this should be 1100, which is equal
			10001001100 in binary.
			To verify, after compilation do java -ea BadQuestionOne
		*/
		System.out.println("result: " + result);
		assert result == 1100;
	}
}
