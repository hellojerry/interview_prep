"""
Given a positive integer, print the next smallest and next largest number that have
the same number of one 1 bits in their binary representation.
"""

def count_ones(string):
	ct = 0
	for el in string:
		if el == "1":
			ct += 1
	return ct

def get_next(val):
	as_bin = bin(val)[2:]
	target_ct = count_ones(as_bin)	
	smaller_not_found = True
	smaller_test = val - 1
	next_smaller = None
	while smaller_not_found:
		amt = count_ones(bin(smaller_test)[2:])
		if amt == target_ct:
			next_smaller = smaller_test
			smaller_not_found = False
		smaller_test -= 1
	bigger_not_found = True
	next_bigger = None
	bigger_test = val + 1
	while bigger_not_found:
		amt = count_ones(bin(bigger_test)[2:])
		if amt == target_ct:
			next_bigger = bigger_test
			bigger_not_found = False
		bigger_test += 1
	return (next_smaller, next_bigger)


if __name__ == "__main__":
	print(get_next(5))
	
