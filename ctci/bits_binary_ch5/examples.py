

def get_bit_short(num, i):
	assert(type(num) == int)
	assert(type(i) == int)
	return (num & ( 1 << i)) != 0

def get_bit(num, i):

	shifted = 1 << i
	print(shifted)
	print(bin(shifted))
	print(bin(num))
	print(num & shifted)
	print(bin(num & shifted))
	print("------")
	return num & shifted != 0

def set_bit(num, i):
	"""
	So if I have the number 4, 
	which is 100, and i want to set
	the final bit to be 1, we should return 5.
	which is 101
	"""
	print(bin( 1 << i))
	print(1 << i)
	return num | ( 1 << i)

def clear_bit(num, i):
	## check Scratch.java
	pass

def clear_bits_msb_to_i(num, i):
	mask = (1 << i) - 1
	return num & mask

def clear_bits_i_to_zero(num, i):
	print(bin(-1))
	print(bin(-1 << (i+1)))
	mask = (-1 << (i + 1))
	return num & mask

if __name__ == "__main__":
	print(clear_bits_msb_to_i(5,5))
	print(clear_bits_i_to_zero(5,2))
