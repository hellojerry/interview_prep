/*
Given a real number between 0 and 1 (i.e. 0.72) that is passed in as a double,
print the binary representation. If the number cannot be represented accurately 
in binary with at most 32 characters, print "ERROR"
*/


class Q2 {


	static String solve(double initial_val){
		if (initial_val <= 0 || initial_val >= 1){
			return "ERROR";
		}

		StringBuilder sb = new StringBuilder();
		sb.append(".");


		while (initial_val > 0){
			System.out.println(sb.toString());
			if (sb.length() >= 32){
				return "ERROR";
			}
			double duped = initial_val * 2;
			if (duped >= 1){
				sb.append(1);
				initial_val = duped - 1;
			} else {
				sb.append(0);
				initial_val = duped;
			}		
		}
		return sb.toString();
	}



	public static void main(String[] args){
		double value = 0.5;
		String result = solve(value);
		System.out.println("result: " + result);
		System.out.println(Long.toBinaryString(Double.doubleToRawLongBits(0.72)));
	}


}
