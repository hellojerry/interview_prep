"""
You have an integer and you can flip exactly one bit from zero to 1.
write a program to find the length of the longest sequence of ones you can create.

Come back to this. having trouble thinking.
"""


def find_longest_sequence(num):

	## this is to get rid of the 0b
	binary_prefix, as_binary_no_prefix = bin(num)[:2], bin(num)[2:]

	"""
	This is really a consecutive string thing.
	What we can do is iterate from left to right,
	"""
	streaks = []
	prior = -1
	ones_first = False
	for item in as_binary_no_prefix:	
		if prior == -1:
			if item == "0":
				prior = "0"
			else:
				prior = "1"
				ones_first = True
			streaks.append(1)
			continue
		if item == prior:
			streaks[-1] += 1
		else:
			prior = item
			streaks.append(1)

	
	
	


if __name__ == "__main__":

	res = find_longest_sequence(1775)
	
	expected = 8
