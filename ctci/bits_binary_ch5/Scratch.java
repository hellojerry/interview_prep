

class Scratch{

	static int clearBit(int num, int i){
		int mask = ~( 1 << i);
		return num & mask;
	}

	static int updateBit(int num, int i, boolean bit_is_1){
		int value = bit_is_1 ? 1 : 0;
		int mask = ~(1 << i);
		return (num & mask) | (value << i);

	}

	public static void main(String[] args){
		int res = clearBit(5,2);
		System.out.println(res);
		int r2 = updateBit(5,2,false);
		System.out.println(r2);

	}

}
