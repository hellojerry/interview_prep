"""
Implement a data structure SetOfStacks that creates a new stack once the previous
stack exceeds capacity.
SetOfStacks should behave identically to a single stack

"""


class SetOfStacks(object):


	def __init__(self):
		self.container_limit = 3
		self.containers = []

	def push(self, value):
		if len(self.containers) == 0:
			self.containers.append([value])
		else:
			target_container = self.containers[-1]
			if len(target_container) == self.container_limit:
				self.containers.append([value])
			else:
				target_container.append(value)

	def pop(self):

		if len(self.containers) == 0:
			raise Exception("Empty")
		target_container = self.containers[-1]
		ret_val = target_container.pop()
		if len(target_container) == 0:
			self.containers.pop()
		return ret_val

	
