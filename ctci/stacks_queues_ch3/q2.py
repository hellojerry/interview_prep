"""
Design a stack which, in addition to push and pop, has a function min()
which returns the minimum element.
"""

class MinStack(object):


	def __init__(self):
		self.arr = []
		self.min = None

	def is_empty(self):
		return len(self.arr) < 1

	def pop(self):
		return self.arr.pop()

	def push(self, value):
		if self.min is None:
			self.min = value
		else:
			if value < self.min:
				self.min = value
		self.arr.append(value)

	def min(self):
		return self.min
