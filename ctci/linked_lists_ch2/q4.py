"""
Write code to partition a linked list around a value X, such that
all nodes less than X come before all nodes greater than or equal to X.
If X is contained within the list, the values of X only need to be after
the elements less than X. the partition element X can appear anywhere
in the "right partition". It does not need to appear between the left
and right partitions.

EX:

input: 3,5,8,5,10,2,1 (partition = 5)
output: 3->1->2->10->5->5->8
"""




class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value


def partition_linked_list_bad(node, partition_key):
	"""
	This is a little confusing.

	I don't think it's in the spirit of the question,
	but i could iterate through the linked list and populate
	two dynamic arrays, one for GTE and the other one for LT,
	and then merge the two and reassign the pointers.

	I could do something slightly more memory-efficient,
	which is to iterate over the LL, and move all
	of the GTE pieces to an orphans array, and then repoint
	the orphans back in once i've scanned the whole thing.

	A third way to do this is to iterate over the LL,
	continuing normally until i run into a GTE node.
	From there, memoize prior, kick off a while loop until 
		a) an LT node is found OR
		b) next_ = None (can exit at this point, since we're partitioned - just return the head)
	upon finding an LT node, make the LT's prior.next = LT.next_,
	then make prior.next = LT, and then make LT.next = current

	I believe this third option is in the spirit of the question, since
	it's one iteration and doesn't expand memory usage.
	THIS WONT WORK!!! IGNORE MEEEEEE
	"""

	head = node
	current_node = head
	prior = None
	while current_node:
		if current_node.value < partition_key:
			prior = current_node
			current_node = current_node.next_
			continue
		lookahead = current_node.next_
		lookahead_prior = current_node
		while lookahead:
			if lookahead.next_ == None:
				## this means we've got a full partition
				return head
			if lookahead.value < partition_key:
				lt_next = lookahead.next_
				lookahead_prior.next_ = lt_next
				prior.next_ = lookahead
				lookahead.next_ = current_node
				lookahead = None
				continue
			lookahead_prior = lookahead
			lookahead = lookahead.next_
		current_node = current_node.next_
	return head

def partition_linked_list(node, partition_key):
	"""
	That didn't quite work, but it got close.
	How about this:
	First time we run into a GTE value, we scan
	to the end of the LL. every LT value gets put as a next_
	to prior (and then prior is redefined). simultaneously,
	once we've gotten the lookahead's next_ value, 

	iterate over the LL.
	make a new head for LT, and a new head for GT.
	"""
	lt_head = None
	lt_current = None
	gte_head = None
	gte_current = None

	current_node = node
	while current_node:
		if current_node.value < partition_key:
			if not lt_head:
				lt_head = current_node
				lt_current = current_node
				current_node = current_node.next_
				lt_current.next_ = None
			else:
				lt_current.next_ = current_node
				current_node = current_node.next_
				lt_current = lt_current.next_
		else:		
			if not gte_head:
				gte_head = current_node
				gte_current = current_node
				current_node = current_node.next_
				gte_current.next_ = None
			else:
				gte_current.next_ = current_node
				current_node = current_node.next_
				gte_current = gte_current.next_
	gte_current.next_ = None
	lt_current.next_ = gte_head
	return lt_head


if __name__ == "__main__":
	n7 = LLNode(1)
	n6 = LLNode(2, n7)
	n5 = LLNode(10, n6)
	n4 = LLNode(5, n5)
	n3 = LLNode(8, n4)
	n2 = LLNode(5, n3)
	n1 = LLNode(3, n2)
	partition_linked_list(n1, 5)
	current_node = n1
	values = []
	while current_node:
		print(current_node)
		values.append(current_node.value)
		current_node = current_node.next_
	assert(values == [3,2,1,5,8,5,10])

