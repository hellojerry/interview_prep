"""
Implement an algorithm to find the Nth to last element of
a singly linked list.

I.e. 

ABCDEE

K = 3
The value would be D
"""


class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value

def find_kth_to_last(node, K):
	"""
	So my first guess is to iterate over the linked list
	and reverse the pointers in-place.
	Once i've reached the end, just count up until the desired element
	index is reached and return it.
	"""

	current_node = node
	prior = None
	while current_node.next_:
		ahead = current_node.next_
		current_node.next_ = prior
		prior = current_node
		current_node = ahead
	current_node.next_ = prior
	ct = 0
	while current_node:
		ct += 1
		if ct == K:
			return current_node
		current_node = current_node.next_

if __name__ == "__main__":
	
	n7 = LLNode(1)
	n6 = LLNode(1, n7)
	n5 = LLNode(7, n6)
	n4 = LLNode(4, n5)
	n3 = LLNode(3, n4)
	n2 = LLNode(2, n3)
	n1 = LLNode(1, n2)

	third_to_last = find_kth_to_last(n1, 3)
	assert(third_to_last.value == 7)
