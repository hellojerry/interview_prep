"""
You have two numbers represented by a linked list,
where each node contains a single digit. The digits are stored
in reverse order, such that the 1's digit is at the head of the list.
Write a function that adds the two numbers and returns the sum as a linked list.

Example:

input: (7->1->6) + (5->9->2)  (aka 617 + 295)
output: (2->1->9)  (aka 912)

After solving the first part, try it in the forward order

Example:

input: (6->1->7) + (2->9->5)  
output: (9->1->2)
"""



class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value

def add_two_linked_lists(head_1, head_2):
	"""
	So i think what i can do here is just add the heads.
	Oh, ok, i get it. This is like that stupid floating point
	addition thing.

	What you do is add the two current nodes together. 
	if it's >= 10,	take the remainder and add it as a node
	to the results node, and then put the carryforward digit in
	a temporary variable.
	if it's < 10, just add the digit as a new node.
	Return the head of the result node.
	if one node gets exhausted before the other, make sure
	to exhaust the incomplete node before continuing to the solution.
	"""
	
	left_current = head_1
	right_current = head_2
	result_head = None
	result_current = None
	carryforward = 0

	while left_current and right_current:
		addition_result = left_current.value + right_current.value + carryforward
		if addition_result < 10:
			new_node = LLNode(addition_result)
			if result_current:
				result_current.next_ = new_node
				result_current = new_node
			else:
				result_head = new_node
				result_current = result_head
			carryforward = 0
		else:
			final_digit = addition_result % 10
			carryforward = int((addition_result - final_digit)/10)
			new_node = LLNode(final_digit)
			if result_current:
				result_current.next_ = new_node
				result_current = new_node
			else:
				result_head = new_node
				result_current = result_head
		left_current = left_current.next_
		right_current = right_current.next_


	straggler = None
	if left_current:
		straggler = left_current
	elif right_current:
		straggler = right_current
	if straggler:
		while straggler:
			addition_result = straggler.value + carryforward
			if addition_result < 10:
				carryforward = 0
				result_current.next_ = LLNode(addition_result)
				result_current = result_current.next_
			else:
				final_digit = addition_result % 10
				carryforward = int((addition_result - final_digit)/10)
				new_node = LLNode(final_digit)
				result_current.next_ = new_node
				result_current = result_current.next_
			straggler = straggler.next_
	if carryforward > 0:
		for item in str(carryforward)[::-1]:
			result_current.next_ = LLNode(int(item))
			result_current = result_current.next_

	return result_head


if __name__ == "__main__":
	
	n1 = LLNode(6)
	n2 = LLNode(1,n1)
	left_head = LLNode(7,n2)

	m1 = LLNode(2)
	m2 = LLNode(9,m1)
	right_head = LLNode(5,m2)

	new_head = add_two_linked_lists(left_head, right_head)
	values = []
	while new_head:
		values.append(new_head.value)
		new_head = new_head.next_
	assert(values == [2,1,9])

	"""
	Round 2: with stragglers

	1,6,7,7,7 (77761)
	5,9,2 (295)
	should return: 78056
	aka (6,5,0,8,7)
	"""
	n1 = LLNode(7)
	n2 = LLNode(7,n1)
	n3 = LLNode(7,n2)
	n4 = LLNode(6,n3)
	left_head = LLNode(1,n4)

	m1 = LLNode(2)
	m2 = LLNode(9,m1)
	right_head = LLNode(5,m2)

	new_head = add_two_linked_lists(left_head, right_head)
	values = []
	while new_head:
		values.append(new_head.value)
		new_head = new_head.next_
	assert(values == [6,5,0,8,7])

	"""
	Round 3: stragglers, plus hopping to four digits
	5,9,9 -> 995
	3,2 -> 23
	should return 1018
	aka: 8,1,0,1
	"""
	n1 = LLNode(9)
	n2 = LLNode(9,n1)
	left_head = LLNode(5, n2)

	m1 = LLNode(2)
	right_head = LLNode(3, m1)
	new_head = add_two_linked_lists(left_head, right_head)
	values = []
	while new_head:
		values.append(new_head.value)
		new_head = new_head.next_
	assert(values == [8,1,0,1])

