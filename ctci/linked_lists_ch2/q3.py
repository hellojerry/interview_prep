"""
Implement an algorithm to delete a node in the middle (any node but the first and last node)
of a singly linked list, given ONLY access to that node.


Addendum: this question is worded strangely, because strictly speaking this is impossible.
The closest one can get is deleting the value contained at the node - the linked list will
still contain the pointer address for the deleted node, it will not pass the "is" test.
"""

class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value

def delete_node_in_middle(node):
	"""
	So the closest I can get is copying the next node's 
	next_ pointer into this node's next_pointer, along with its value.
	"""

	current_node = node
	ahead = current_node.next_
	current_node.value = ahead.value
	current_node.next_ = ahead.next_

if __name__ == "__main__":
	n7 = LLNode(1)
	n6 = LLNode(1, n7)
	n5 = LLNode(7, n6)
	n4 = LLNode(4, n5)
	n3 = LLNode(3, n4)
	n2 = LLNode(2, n3)
	n1 = LLNode(1, n2)
	delete_node_in_middle(n4)
	current_node = n1
	values = []
	while current_node:
		values.append(current_node.value)
		current_node = current_node.next_
	assert(values == [1,2,3,7,1,1])

