"""
Given two singly linked lists, determine if they intersect.
Return the intersecting node. "Intersect" in this context
means that a specific node on both linked lists passes the "is"
test, i.e. has the same underlying pointer.

Diagram

1 -> 2 -> 3
	    \
	     4 -> 5->6->8
	   /
1->1->3->4


"""

class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value

def linked_lists_intersect(head_1,head_2):
	"""
	So the first thing that occurs to me
	is doing an o(n^2) thing on this,
	just doing an "is" test on every element.


	Ok, looking at the diagram, this makes more sense.

	Let's scan to the end of both LLs, tracking the lengths.
	If the two tails dont match, return None (no intersection)
	from there, advance the longer linked list by the difference
	in lengths, and then start tracking them with two pointers
	until we get a successful "is" test, and return the node.
	"""
	left_head = head_1
	left_current = left_head
	left_length = 0
	left_tail = None
	right_head = head_2
	right_current = right_head
	right_length = 0
	right_tail = None
	while left_current:
		left_length += 1
		left_tail = left_current
		left_current = left_current.next_
	while right_current:
		right_length += 1
		right_tail = right_current
		right_current = right_current.next_
	left_current = left_head
	right_current = right_head
	if not(left_tail is right_tail):
		return None
	smaller_current, larger_current = None, None
	to_skip = 0
	if left_length <= right_length:
		smaller_current = left_current
		larger_current = right_current
		to_skip = right_length - left_length
	else:
		smaller_current = right_current
		larger_current = left_current
		to_skip = left_length - right_length

	while to_skip > 0:
		larger_current = larger_current.next_
		to_skip -= 1
	while smaller_current and larger_current:
		if smaller_current is larger_current:
			return smaller_current
		smaller_current = smaller_current.next_
		larger_current = larger_current.next_



if __name__ == "__main__":

	n1 = LLNode(9)
	n2 = LLNode(8, n1)
	n3 = LLNode(7, n2)

	m1 = LLNode(6, n3)
	m2 = LLNode(5, m1)
	m3 = LLNode(5, m2)

	o1 = LLNode(2, n3)
	o2 = LLNode(1, o1)

	intersect = linked_lists_intersect(m3, o2)
	assert(n3 is intersect)

	m1.next_ = None
	intersect = linked_lists_intersect(m3, o2)
	assert(intersect is None)

