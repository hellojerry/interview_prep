"""
Given a circular linked list, implement an algorithm that returns the node at the beginning of the loop.

Example:

	A->B->C->D->E->C  ==> C

Note that this is by reference, not by value. 
"""

class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value

def find_cycle_location(node):
	"""
	This is the "find a cycle in a linked list" thing.
	We need to have two pointers for this.
	"""
	head = node
	current_node = head
	fast_node = current_node.next_
	while current_node and fast_node:
		if current_node is fast_node:
			return current_node
		fast_node = fast_node.next_.next_
		current_node = current_node.next_

if __name__ == "__main__":
	## the cycle is at n5.
	n1 = LLNode(9)
	n2 = LLNode(8,n1)
	n3 = LLNode(7, n2)
	n4 = LLNode(6, n3)
	n5 = LLNode(5, n4)
	n1.next_ = n5
	head = n5
	current_node = head
	cycle = find_cycle_location(n4)
	assert(cycle is n5)
