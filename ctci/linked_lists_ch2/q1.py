"""
Write code to remove duplicates from an unsorted linked list.

follow-up: Try doing this without a temporary array.
"""


class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value


def remove_ll_dupes(node):
	"""
	Hmm. I guess we should return the head node?

	first idea:
		- iterate through LL node.
		- check if value is in the set.
		- if it isn't, add it and continue.

		edge case is that the final ll node is a dupe.
		if that is the case, we need to null out the prior node's
		"next" pointer
	"""
	existing = set()
	head = node
	current_node = head
	prior = None
	while current_node:
		if current_node.value not in existing:
			existing.add(current_node.value)
			prior = current_node
			current_node = current_node.next_
		else:
			prior.next_ = current_node.next_
			current_node = current_node.next_

	return head


def remove_ll_dupes_v2(node):
	"""
	The way to do this without keeping a buffer
	is by using a leading faster pointer to
	prospectively dedupe things.
	This is weak from a runtime complexity perspective
	but uses up less RAM.
	"""
	head = node
	current_node = head
	while current_node:
		fast_ptr = current_node
		while fast_ptr.next_:
			if fast_ptr.next_.value == current_node.value:
				fast_ptr.next_ = fast_ptr.next_.next_
			else:
				fast_ptr = fast_ptr.next_
		current_node = current_node.next_
	return head
		




if __name__ == "__main__":

	n7 = LLNode(1)
	n6 = LLNode(1, n7)
	n5 = LLNode(2, n6)
	n4 = LLNode(4, n5)
	n3 = LLNode(3, n4)
	n2 = LLNode(2, n3)
	n1 = LLNode(1, n2)

	## i don't know of a convenient way to test this
	## except returning the head node.
	deduped_head = remove_ll_dupes(n1)
	values = []
	while deduped_head:
		values.append(deduped_head.value)
		deduped_head = deduped_head.next_
	print(values)
	assert(values == [1,2,3,4])
	n7 = LLNode(1)
	n6 = LLNode(1, n7)
	n5 = LLNode(2, n6)
	n4 = LLNode(4, n5)
	n3 = LLNode(3, n4)
	n2 = LLNode(2, n3)
	n1 = LLNode(1, n2)
	deduped_head = remove_ll_dupes_v2(n1)
	values = []
	while deduped_head:
		values.append(deduped_head.value)
		deduped_head = deduped_head.next_
	print(values)
	assert(values == [1,2,3,4])
