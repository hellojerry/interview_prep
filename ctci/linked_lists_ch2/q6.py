"""
Implement a function to check if a linked list is a palindrome.

ex:
	5->1->1->1->5 ==> true
	5->1->2->3->5 ==> false
"""

class LLNode(object):

	def __init__(self, value, next_=None):
		assert(type(value) == int)
		self.value = value
		self.next_ = next_

	def __str__(self):
		return "LLNode(%d)" % self.value

	def __repr__(self):
		return "LLNode(%d)" % self.value

def is_palindrome(node):
	"""
	Ok, so one way to do this is to keep a pointer to the head
	and go all the way to the tail. for each iteration, increment a counter.
	Then, iterate starting from the head again, and call next_ for the number
	of values in the counter until you reach zero, and test that value.
	assuming a match, decrement the counter by one, move the ll pointer forward,
	rinse and repeat. The complexity on this one would be o(n) + o(log n)

	Another way to attack this is start at head, go to end, match at end, increment count to end,
	test, move head ahead one, go up to count, test, decrement, rinse and repeat
	Gonna do the easier way first.

	Havign a bit of trouble writing the code for the second version. This should be fine.
	"""
	head = node
	current_node = head
	node_ct = 0

	while current_node.next_:
		node_ct += 1
		current_node = current_node.next_
	## check the head and the tail
	if current_node.value != head.value:
		return False
	current_node = head.next_
	node_ct -= 1
	test_node = None
	while node_ct > 0:
		for i in range(node_ct-1):
			test_node = current_node.next_
		if test_node.value == current_node.value:
			node_ct -= 1
			current_node = current_node.next_
			continue
		else:
			return False
	
	return True


if __name__ == "__main__":
	n1 = LLNode(5)
	n2 = LLNode(1,n1)
	n3 = LLNode(1,n2)
	n4 = LLNode(1,n3)
	head = LLNode(5,n4)
	assert(is_palindrome(head) == True)
	n1 = LLNode(5)
	n2 = LLNode(3,n1)
	n3 = LLNode(1,n2)
	n4 = LLNode(1,n3)
	head = LLNode(5,n4)
	assert(is_palindrome(head) == False)
	n1 = LLNode(5)
	n2 = LLNode(1,n1)
	n3 = LLNode(1,n2)
	head = LLNode(5,n3)
	assert(is_palindrome(head) == True)
