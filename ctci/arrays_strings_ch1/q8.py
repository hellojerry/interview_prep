"""
Write an algorithm such that if an element in
an MxN matrix is zero, its entire row and column
are set to zero.

ex:

[
	[ 1,2,3,0,5],
	[0,7,8,9,10],
	[12,13,0,1,9],
	[5,4,3,2,1]
]

[
	[0,0,0,0,0],
	[0,0,0,0,0],
	[0,0,0,0,0],
	[0,4,3,0,1]
]


"""


def zero_row_col(matrix):
	"""
	So my first thought here is
	to scan each row and column
	and push the row and column indices
	containing a zero to a pair of arrays,
	then looping through a second time 
	and setting to zero if there's a match on either index.
	That has horrible time complexity.

	We could immediately zero out the row once a circuit is tripped,
	and memoize the column index for future ones. Complexity is still pretty bad though

	hmm. if we scan columns first, we can zero out an entire row trivially.
	I guess the same goes in reverse... hmm.	

	ok. go over each row. if zero trips, zero out row, memoize column name.
	target_cols = []
	for each row:
		if val == 0:
			zero_out_row()
			target_cols.append(col)
			continue
		if col in target_cols:
			set_to_zero()
			continue
	we can make this more efficient by reducing the number of cols targeted per row.
	This would allow us to zero_out the columns without getting beaned by our own algorithm.
	This leaves us with the problem of having two initial zero values in a row. hm. 

	ok, i got it. two pointers. if the target element is zero, zero out the row.
	before zeroing, check if the initial value is zero. if it is, add the column
	to the target cols, zero out the column, and move to the next row.
	if it itself isn't zero, check if the column is in the target column and zero the element 
	if it is. increment the column pointer, and if we're at the end of a row, 
	reset the column pointer and increment the row pointer.

	This i think is o(n^2) worst case - that target_col lookup is constant time
	as a set() data structure is a form of hashmap, so access time is constant.

	The pathological case is when there's a zero on the bottom right.

	I think we could speed this up by testing both ends at the same time, i.e.
	testing the top left and bottom right at the same time, since we can compute the bottom 
	right's location corresponding with the top left's. this would let us zero out two rows
	and two columns simultaneously, and we'd only scan 50% of the matrix.
	"""

	rows = len(matrix)
	cols = len(matrix[0])

	target_cols = set()
	row_ptr, col_ptr = 0,0
	while row_ptr < rows and col_ptr < cols:
		el = matrix[row_ptr][col_ptr]
		if el == 0:
			for col in range(cols):
				if matrix[row_ptr][col] == 0:
					target_cols.add(col)
				matrix[row_ptr][col] = 0
			row_ptr += 1
			col_ptr = 0
			continue
		if col_ptr in target_cols:
			matrix[row_ptr][col_ptr] = 0
		col_ptr += 1
		if col_ptr == cols:
			col_ptr = 0
			row_ptr += 1

def zero_row_col_v2(matrix):
	rows = len(matrix)
	cols = len(matrix[0])

	target_cols = set()
	top_row_ptr, left_col_ptr = 0,0
	bottom_row_ptr, right_col_ptr = (rows-1), (cols-1)
	while top_row_ptr < bottom_row_ptr+1:
		top_left = matrix[top_row_ptr][left_col_ptr]
		bottom_right = matrix[bottom_row_ptr][right_col_ptr]
		top_left_breaker = False
		bottom_right_breaker = False
		if top_left == 0:
			for col in range(cols):
				if matrix[top_row_ptr][col] == 0:
					target_cols.add(col)
				matrix[top_row_ptr][col] = 0
			#top_row_ptr += 1
			#left_col_ptr = 0
			top_left_breaker = True

		if bottom_right == 0:
			for col in range(cols):
				if matrix[bottom_row_ptr][col] == 0:
					target_cols.add(col)
				matrix[bottom_row_ptr][col] = 0
			#bottom_row_ptr -= 1
			#right_col_ptr = 0
			bottom_right_breaker = True
		if left_col_ptr in target_cols:
			matrix[top_row_ptr][left_col_ptr] = 0
		if right_col_ptr in target_cols:
			matrix[bottom_row_ptr][right_col_ptr] = 0
		if top_left_breaker:
			left_col_ptr = 0
			top_row_ptr += 1
		else:
			left_col_ptr += 1
			if left_col_ptr == cols:
				left_col_ptr = 0
				top_row_ptr += 1
		if bottom_right_breaker:
			right_col_ptr = (cols-1)
			bottom_row_ptr -= 1
		else:
			right_col_ptr -= 1
			if right_col_ptr == 0:
				right_col_ptr = (cols - 1)
				bottom_row_ptr -= 1


if __name__ == "__main__":
	initial = [
		[1, 2, 3,0, 5],
		[0, 7, 8,9,10],
		[12,13,0,1, 9],
		[5, 4, 3,2, 1]
	]

	expected = [
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,4,0,0,1]
	]
	zero_row_col(initial)
	assert(initial == expected)
	initial = [
		[1, 2, 3,0, 5],
		[0, 7, 8,9,10],
		[12,13,0,1, 9],
		[5, 4, 3,2, 1]
	]

	expected = [
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,0,0,0,0],
		[0,4,0,0,1]
	]
	zero_row_col_v2(initial)
	print(initial)
	assert(initial == expected)
