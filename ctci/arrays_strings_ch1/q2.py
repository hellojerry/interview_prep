"""
Write a method to determine if one string is a permutation of the other.

permutation meaning the same characters but in different order
"""


def is_permutation_v1(str_1, str_2):
	if len(str_1) != len(str_2):
		return False
	## this is 2n + 2(log n)
	return sorted(str_1) == sorted(str_2)


def is_permutation_v2(str_1, str_2):
	"""
	Let's try a better complexity on this.
	i think i could call ord() on every character in both
	strings and add them together 
	that would get us down to 2n lol.
	"""
	ord_1 = sum(ord(k) for k in str_1)
	ord_2 = sum(ord(k) for k in str_2)
	return ord_1 == ord_2

def is_permutation_v3(str_1, str_2):
	"""
	Ok, a bit less space usage.
	"""
	ord_acc_1 = 0
	ord_acc_2 = 0
	for i in str_1:
		ord_acc_1 += ord(i)
	for i in str_2:
		ord_acc_2 += ord(i)
	return ord_acc_1 == ord_acc_2


if __name__ == "__main__":
	sample_1 = "abcdef"
	sample_2 = "".join(reversed(sample_1))
	sample_3 = "zort"
	sample_4 = "narf"
	assert(is_permutation_v1(sample_1,sample_2) == True)
	assert(is_permutation_v1(sample_3,sample_4) == False)
	print("round 1 complete")
	assert(is_permutation_v2(sample_1,sample_2) == True)
	assert(is_permutation_v2(sample_3,sample_4) == False)
	print("round 2 complete")
	assert(is_permutation_v3(sample_1,sample_2) == True)
	assert(is_permutation_v3(sample_3,sample_4) == False)
	print("round 3 complete")
