"""
Assume you have a method is_substring which checks if one word is a substring of another.
Given two strings, s1 and s2, write code to check if s2 is a rotation of s1
using only one call to is_substring.

ex: "waterbottle", "erbottlewat" -> true
ex: "crap", "fart" -> false
"""

def is_substring(a, b):
	return a in b

def check_if_rotation_v1(my_str_1, my_str_2):
	"""
	This is a tricky one. let's finish the dishes first.
	we want to see if string one is a rotation of string 2.

	i guess we start with string one and...hmm.
	the trick with this has to be finding the rotation point.
	hmm. how about this. we try and find the first matching character,
	and then do is_substring from that point until the end.
	"""
	str1_len = len(my_str_1)
	str2_len = len(my_str_2)
	if str1_len != str2_len:
		return False

	for i in range(str1_len):
		if my_str_1[i] == my_str_2[0]:
			tail_slice = my_str_1[i:]
			leading_slice = my_str_1[:i]
			return (tail_slice + leading_slice) == my_str_2
	return False

def check_if_rotation_v2(my_str_1,my_str_2):

	str1_len = len(my_str_1)
	str2_len = len(my_str_2)
	if str1_len != str2_len:
		return False

	return is_substring(my_str_2, my_str_1 + my_str_1) 


if __name__ == "__main__":
	sample_1 = "waterbottle"
	sample_2 = "erbottlewat"
	sample_3 = "crap"
	sample_4 = "fart"

	assert(check_if_rotation_v1(sample_1, sample_2) == True)
	assert(check_if_rotation_v1(sample_4, sample_3) == False)
	assert(check_if_rotation_v2(sample_1, sample_2) == True)
	assert(check_if_rotation_v2(sample_4, sample_3) == False)
