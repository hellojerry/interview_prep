"""
Given an image represented by an NxN matrix, where each element is a 4 byte pixel, 
write a method to rotate the image by 90 degrees... clockwise, in-place.

Ex:

[
	[ 1, 2, 3, 4],
	[ 5, 6, 7, 8],
	[ 9, 10, 11, 12],
	[13, 14,15,16]
]

to:

[
	[13, 9,  5, 1],
	[14, 10, 6, 2],
	[15, 11, 7, 3],
	[16, 12, 8, 4],
]

"""

def rotate_clockwise_inplace(matrix):
	"""
	0,0 -> 0, 3
	0,1 -> 1,3
	3, 0 -> 0, 0
	3, 3 -> 3, 0
	0, 3 -> 3,3

	1,1 -> 1,2
	2,1 -> 1,1
	"""
	rows = len(matrix)
	columns = len(matrix[0])
	if columns < 2:
		return
	row_mid = int(rows/2)
	for row in range(row_mid):
		for col in range(row, rows-row-1):
			new_row = rows - 1 - row
			new_col = rows - 1 - col

			(matrix[new_row][new_col], matrix[row][col],
			matrix[new_col][row], matrix[col][new_row]) = (
				matrix[col][new_row], matrix[new_col][row],
				matrix[new_row][new_col], matrix[row][col]
			)

if __name__ == "__main__":
	input_matrix = [
		[1, 2,  3, 4],
		[5, 6,  7, 8],
		[9, 10, 11,12],
		[13,14, 15,16]
	]
	expected_output = [
		[13, 9,  5, 1],
		[14, 10, 6, 2],
		[15, 11, 7, 3],
		[16, 12, 8, 4],
	]
	rotate_clockwise_inplace(input_matrix)
	assert(input_matrix == expected_output)
	input_matrix_2 = [
		[1, 2, 3, 4, 5],
		[6, 7, 8, 9, 10],
		[11,12,13,14,15],
		[16,17,18,19,20],
		[21,22,23,24,25]
	]
	expected_output_2 = [
		[21,16,11,6,1],
		[22,17,12,7,2],
		[23,18,13,8,3],
		[24,19,14,9,4],
		[25,20,15,10,5]
	]
	assert(input_matrix == expected_output)


