"""
Given a string, write a function to check if it is a permutation of a palindrome.

A palindrome is a word or phrase that is the same forwards and backwards. It's not limited to dictionary
words, it's any combination of characters.
"""

def palindrome_permutation_v1(my_str):
	"""
	This is a trickier one. I think the best bet is
	to get a count of all of the unique characters.
	They should all be even counts, except for one 
	character which can be odd.

	This one is kind of ugly.
	Two loops :(
	"""
	frequencies = {}
	for i in my_str:
		if i == " ":
			continue
		as_lower = i.lower()
		key_ref = frequencies.get(as_lower)
		if not key_ref:
			frequencies[as_lower] = 1
		else:
			frequencies[as_lower] += 1
	odd_ct = 0
	for k,v in frequencies.items():
		if v % 2 != 0:
			odd_ct += 1
	return odd_ct <= 1

def palindrome_permutation_v2(my_str):
	## 65-90: add 32
	"""
	This is also two loops. There's got to be a way to short-circuit this to one loop.
	This is slightly better in that we don't have an array resize operation.
	"""

	storage_arr = [0] * 128
	for char in my_str:
		if char == " ":
			continue
		as_ord = ord(char)
		if as_ord >= 65 and as_ord <= 90:
			as_ord += 32
		storage_arr[as_ord] += 1
	odd_ct = 0
	for i in storage_arr:
		if i % 2 != 0:
			odd_ct += 1
		if odd_ct > 1:
			return False
	return True


if __name__ == "__main__":
	sample_1 = "Tact Coa"
	sample_2 = "rejigger the fuckulator"
	assert(palindrome_permutation_v1(sample_1) == True)
	assert(palindrome_permutation_v1(sample_2) == False)
	assert(palindrome_permutation_v2(sample_1) == True)
	assert(palindrome_permutation_v2(sample_2) == False)
