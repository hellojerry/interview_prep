
/*
Write a method to replace all spaces in a string with '%20'.
Don't use a library for this, do this with a loop.
assume the string has all the capacity it needs.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int urlify(const char* input, char output_buf[18]){
	size_t len = strlen(input);
	int target_ptr = 0;
	for (int i=0; i < len; i++){
		char target = input[i];
		if (target == ' '){
			output_buf[target_ptr] = '%';
			target_ptr++;
			output_buf[target_ptr] = '2';
			target_ptr++;
			output_buf[target_ptr] = '0';
			target_ptr++;
		} else {
			output_buf[target_ptr] = target;
			target_ptr++;
		}
	}
	return 0;
}


int main(){
	const char* sample = " mike mike ";
	char output[18];
	memset(output, 0x00, 18);
	const char* expected = "%20mike%20mike%20";
	urlify(sample,output);
	int strcmp_res = strcmp(output,expected);	
	if (strcmp_res == 0){
		printf("success\n");
	} else {
		printf("failure\n");
	}
	return 0;
}
