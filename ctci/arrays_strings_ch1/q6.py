"""
Implement a method to perform basic string compression using the counts of repeated characters.
If the compressed string is not smaller than the original string, return the original string.

example:

aabcccccaaa -> a2b1c5a3
abcd -> abcd

The string only has uppercase and lowercase letters.
"""


def compress_v1(my_str):
	"""
	Iterate over the string, memoize the current value,
	increment it for each repetition, when it's a new value,
	append the memoized value to the accumulator with the current ct 
	and then reset the memoized value.

	Thisis fine from a complexity point of view - it's o(n).
	The problem with this is that it uses up too much RAM.
	I don't know of a way to get around this in python.
	If this is C, i can use a buffer for it.
	"""
	initial_len = len(my_str)
	if initial_len < 3:
		return my_str
	acc = ""
	ptr = 0
	prior = None
	current_ct = 0
	while ptr < initial_len:
		if my_str[ptr] == prior:
			current_ct += 1
			ptr += 1
			if ptr >= initial_len:
				acc += prior + str(current_ct)
				if len(acc) < initial_len:
					return acc
				else:
					return my_str
		elif prior is None:
			prior = my_str[ptr]
			current_ct = 1
			ptr += 1
		else:
			acc += prior + str(current_ct)
			current_ct = 1
			prior = my_str[ptr]
			ptr += 1
	acc += prior + str(current_ct)
	if len(acc) < initial_len:
		return acc
	return my_str 



if __name__ == "__main__":
	assert(compress_v1("aabcccccaaa") == "a2b1c5a3")
	assert(compress_v1("abcd") == "abcd")
	assert(compress_v1("aabcccccaaaf") == "a2b1c5a3f1")
