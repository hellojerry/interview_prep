"""
Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?

"""


def is_unique_v1(my_str):
	as_sorted = sorted(my_str)
	prior = None
	for i in as_sorted:
		if i == prior:
			return False
		prior = i
	return True


def is_unique_v2(my_str):
	return len(my_str) == len(set(my_str))

def is_unique_v3(my_str):
	"""
	Uglier way to do this.
	Start at the beginning, and count towards the end. 
	"""

	start_idx = 0
	strlen = len(my_str)
	end_idx = strlen - 1

	while start_idx < end_idx:
		test_char = my_str[start_idx]
		for i in range(start_idx+1,strlen):
			if test_char == my_str[i]:
				return False
		start_idx += 1

	return True

if __name__ == "__main__":
	sample_1 = "abcdeff"
	sample_2 = "abcdef"
	assert(is_unique_v1(sample_1) == False)
	assert(is_unique_v1(sample_2) == True)
	print("round 1 complete")
	assert(is_unique_v2(sample_1) == False)
	assert(is_unique_v2(sample_2) == True)
	print("round 2 complete")
	assert(is_unique_v3(sample_1) == False)
	assert(is_unique_v3(sample_2) == True)
	print("round 3 complete")
