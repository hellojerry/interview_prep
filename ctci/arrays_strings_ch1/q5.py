"""
There are three types of edits that can be performed on strings:
	- insert a character
	- remove a character
	- replace a character
Given two strings, write a function that determines if they are one or zero edits away.

example:

pale, ple -> true
pales, pale -> true
pale, bale -> true
pale, bake -> false
"""

def one_away_v1(str_1, str_2):
	"""
	Best thing we can do here is a form of short circuiting with
	two pointers. Get the string lengths. if they're two apart, reject.
	If they're one apart,  then all we can hope for is a deletion.
	if there's a size mismatch, iterate over the smaller one.
	if you run into a char mismatch, try it against the next ptr in the larger one.
	if it matches, you up the deletion count. if deletion count is above one, return false.
	if there's no size mismatch,
	"""
	str1_len = len(str_1)
	str2_len = len(str_2)
	if abs(str2_len - str1_len) > 1:
		return False
	smaller, larger = None, None
	mismatch_ct = 0
	has_size_mismatch = True
	if str1_len < str2_len:
		smaller, larger = str_1, str_2
		smaller_len, larger_len = str1_len, str2_len
		mismatch_ct = 1
	elif str1_len == str2_len:
		smaller, larger = str_1, str_2
		smaller_len, larger_len = str1_len, str2_len
		has_size_mismatch = False
	else:
		smaller, larger = str_2, str_1
		smaller_len, larger_len = str2_len, str1_len
		mismatch_ct = 1

	if not has_size_mismatch:
		edit_count = 0
		for i in range(smaller_len):
			if smaller[i] != larger[i]:
				edit_count += 1
			if edit_count > 1:
				return False
		return True
	larger_ptr = 0
	for i in range(smaller_len):
		if smaller[i] != larger[larger_ptr]:
			larger_ptr += 1
			if smaller[i] != larger[larger_ptr]:
				return False 
		larger_ptr += 1
	return True

if __name__ == "__main__":
	assert(one_away_v1("pale", "ple" ) == True)
	assert(one_away_v1("pales","pale") == True)
	assert(one_away_v1("pale", "bale") == True)
	assert(one_away_v1("pale", "bake") == False)
	assert(one_away_v1("gornt ", "gornt") == True)
