

def tower_of_hanoi(source, mid, dest, N):

	if N == 0:
		return
	## move source to mid with dest as buffer
	tower_of_hanoi(source,dest,mid,N-1)
	## append to dest
	if source:
		dest.append(source.pop())
	## move mid to dest with source as buffer
	tower_of_hanoi(mid,source,dest,N-1)


if __name__ == "__main__":
	s1,s2,s3 = [1,2,3],[],[]
	N = len(s1)
	tower_of_hanoi(s1,s2,s3,N)
	print(s3)
