"""
Implement the "paint fill" function that one might see on many image editing programs.
That is, given a screen (represented by a two-dimensional array of colors), a point,
and a new color, fill in the surrounding area until the color changes from the original color.


This does NOT mean extend past a barrier consisting of the intended color.

in other words, green to blue:

	green,green,blue,green

becomes:
	blue,blue,blue,green

I believe this means something like:

input:
matrix = [

	[green,green,blue,red,blue],
	[blue,red,blue,green,red],
	[blue,red,blue,blue,blue],
	[green,green,red,green,blue],
	[green,blue,red,blue,blue]
]
Point at 2,2 (which is originally blue) should be changed to green.

matrix = [

	[green,green,green,red,blue],
	[blue,red,green,green,red],
	[blue,red,green,green,green],
	[green,green,red,green,green],
	[green,blue,red,green,green]
]
"""
from pprint import pprint

def paint_fill(matrix,row,col,current_color,target_color):
	"""
	We get the color at the current point. change the color to the target color.
	Next, we check to see if we're at a boundary (top,left,right,bottom)
	mark any boundaries as checks to be ignored.
	for the remaining eligible directions (top,left,right,bottom),
	check if the color at that square is the current color. If it is,
		run paint_fill on that new row/col pair. If it isn't, don't do anything.

	We can speed this up by memoizing the prior row in the function and disqualifying it from the checks.
	"""
	## base case. 
	if matrix[row][col] != current_color:
		return
	matrix[row][col] = target_color

	rows = len(matrix)
	cols = len(matrix[0])
	left_boundary,top_boundary = 0,0
	right_boundary = cols - 1
	bottom_boundary = rows - 1
	left_eligible,right_eligible,up_eligible,down_eligible = True,True,True,True
	## if we memoize the prior column/row pair, we can avoid the backtracking that can
	## happen
	
	if (col - 1) < left_boundary:
		left_eligible = False
	else:
		if matrix[row][col-1] != current_color:
			left_eligible = False
	if (col+1) > right_boundary:
		right_eligible = False
	else:
		if matrix[row][col+1] != current_color:
			right_eligible = False
	if (row - 1) < top_boundary:
		up_eligible = False
	else:
		if matrix[row-1][col] != current_color:
			up_eligible = False	
	if (row+1) > bottom_boundary:
		down_eligible = False
	else:
		if matrix[row+1][col] != current_color:
			down_eligible = False
	if left_eligible:
		paint_fill(matrix,row,col-1,current_color,target_color)
	if right_eligible:
		paint_fill(matrix,row,col+1,current_color,target_color)
	if up_eligible:
		paint_fill(matrix,row-1,col,current_color,target_color)
	if down_eligible:
		paint_fill(matrix,row+1,col,current_color,target_color)
	return

if __name__ == "__main__":
	matrix = [
		["G","G","B","R","B"],
		["B","R","B","G","R"],
		["B","R","B","B","B"],
		["G","G","R","G","B"],
		["G","B","R","B","B"]
	]

	paint_fill(matrix,2,2,"B","G")
	expected_matrix = [
		["G","G","G","R","B"],
		["B","R","G","G","R"],
		["B","R","G","G","G"],
		["G","G","R","G","G"],
		["G","B","R","G","G"]
	]
	assert(matrix == expected_matrix)
