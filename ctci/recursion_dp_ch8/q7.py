"""
Write a method to compute all permutations of a string of unique characters.
"""


def compute_all_permutations(char,acc,target_size, final_acc):
	"""
	Ok,so this one kinda resembles that set permutation thing.

	Our initial base case is having a single letter. We return the single letter.
	if it's 2, then we take the results of a single letter scan, and add the letter to each possible index
	if it's 3, we copy the results of 2, and then make a new permutation by adding the letter to each possible index.
	"""
	acc_len = len(acc)
	if acc_len == 0:
		acc.append(char)
		return acc
	else:
		new_acc = acc.copy()
		for element in acc:
			## this should add the new char at every element.
			for i in range(0, len(element)+1):
				new_el = list(element)
				new_el.insert(i,char)
				new_acc.append("".join(new_el))
				if len(new_el) == target_size:
					final_acc.append("".join(new_el))
		return new_acc

def get_perms(original):
	acc = []
	target_size = len(original)
	final_acc = []
	for i in original:
		acc = compute_all_permutations(i,acc,target_size,final_acc)
	return final_acc


if __name__ == "__main__":
	original = "dog"
	perms = ["dog","dgo", "odg","ogd","gdo","god"]
	res = get_perms(original)
	assert(sorted(perms) == sorted(res))

	original = "mike"
	res = get_perms(original)
	expected = [
		'eikm', 'eimk', 'ekim', 'ekmi', 'emik', 'emki', 
		'iekm', 'iemk', 'ikem', 'ikme', 'imek', 'imke', 
		'keim', 'kemi', 'kiem', 'kime', 'kmei', 'kmie', 
		'meik', 'meki', 'miek', 'mike', 'mkei', 'mkie'
	]
	assert(sorted(res) == expected)
