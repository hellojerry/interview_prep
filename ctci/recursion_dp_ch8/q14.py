"""
Given a boolean expression consisting of the symbols 0(false),
1(true), &(AND), |(OR), and ^(XOR), and a desired boolean result value
result (R), implement a function to count the number of ways of parenthesizing
the expression such that it evaluates to R. The expression should be fully
parenthesized but not extraneously.

Example:

input: "0&0&0&1^1|0", true -> 10
"""


def get_number_combos(statement,t_or_f):
	assert(type(statement) == str)
	assert(type(t_or_f) == bool)
	"""
	This question was extremely poorly written and very poorly explained.
	The strategy with this is to take the string and test the situations
	in order where you wrap the left and right sides of a given operator
	with parentheses.

	for example, in 0&0&0^1, you do tests at
	left and right of char 1: (0)&(0&0^1)
	left and right of char 3: (0&0)&(0^1)
	left and right of char 5: (0&0&0)^(1)

	Then, for each statement, determine if the pairing satisfies the RESULT
	requirement, i.e. true or false, and return that amount.

	We multiply 

	"""


	stmt_len = len(statement)
	if stmt_len == 0:
		return 0
	if stmt_len == 1:
		## if it's a single value, we've hit an end point.
		as_bool = bool(int(statement))
		if as_bool == t_or_f:
			return 1
		return 0

	amt = 0
	for i in range(1,stmt_len, 2):
		target_operator = statement[i]
		left_block, right_block = statement[:i], statement[(1+i):]

		left_true = get_number_combos(left_block, True)
		right_true = get_number_combos(right_block, True)
		left_false = get_number_combos(left_block, False)
		right_false = get_number_combos(right_block, False)
		total = (left_true+left_false) * (right_true+right_false)
		total_true = 0
		if target_operator == "&":
			## for AND, we need 
			total_true = left_true * right_true
		elif target_operator == "|":
			total_true = (left_true*right_true) + (left_false*right_true
					) + (left_true*right_false)
		elif target_operator == "^":
			total_true = (left_true*right_false) + (left_false*right_true)
		else:
			raise Exception("there's a bug if you've gotten here.")
		if t_or_f == True:
			amt += total_true
		else:
			amt += (total - total_true)
	return amt



if __name__ == "__main__":
	statement = "0&0&0&1^1|0"
	res = get_number_combos(statement,True)
	assert(res == 10)
