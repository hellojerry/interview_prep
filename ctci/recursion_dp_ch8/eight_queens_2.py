"""
Write an algorithm to print all ways of arranging eight queens on an 8x8 chessboard
so that none of them share the same row, column, or diagonal. In this case, "diagonal"
means all diagonals on the board, not just the two that go all the way to the corners.
"""


def is_valid(col_array,occupied_row,occupied_col):
	"""
	This is how we accomplish our backtracking on older row/column pairs.
	"""
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if target_col == occupied_col or target_row == occupied_row:
			return False
		if abs(target_col - occupied_col) == abs(target_row - occupied_row):
			return False
	return True


def eight_queens(row,col_array,N,acc):
	"""
	The way this works is that we take a single row, starting with 0,
	then work through the range of columns that could be paired with the row.
	If we have a legal placement at a given row/col pair, we edit the column
	array to include that column for that specific row, then recursively
	call this again with the row pointer moved forward until it hits an end position.
	"""

	print(col_array)
	if row == N:
		## end condition: we've gotten through all the columns recursively
		## without a False call.
		copied = list(zip(range(N),col_array.copy()))
		acc.append(copied)
	else:
		for col in range(N):
			if is_valid(col_array,row,col):
				col_array[row] = col
				eight_queens(row+1,col_array,N,acc)
if __name__ == "__main__":
	N = 8
	acc = []
	col_array = list(range(N))
	eight_queens(0,col_array,N,acc)
	assert(len(acc) == 92)
