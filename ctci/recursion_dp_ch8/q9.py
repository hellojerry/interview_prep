"""
Implement an algorithm to print all valid combinations of N pairs
of parentheses.

Example:

input: 3

result: ((())), (()()), (())(), ()(()), ()()()

"""

def generate_balanced_parentheses(N):
	"""
	What we can do here is build the current set based on the smaller set.
	For each pair in the smaller set, we insert enclosing parentheses to generate a value,
	and insert a pair on the right and left.
	"""

	if N == 1:
		return {"()"}
	else:
		smaller_set = generate_balanced_parentheses(N-1)
		copied = smaller_set.copy()
		for item in copied:
			with_right = item + "()"
			with_enclosing = "(" + item + ")"
			with_left = "()" + item
			smaller_set -= {item}
			smaller_set.add(with_right)
			smaller_set.add(with_enclosing)
			smaller_set.add(with_left)
		return smaller_set



if __name__ == "__main__":
	N = 3
	expected = ["((()))", "(()())", "(())()", "()(())", "()()()"]
	results = generate_balanced_parentheses(N)
	assert(set(expected) == set(results))
	N = 1
	expected = {"()"}
	results = generate_balanced_parentheses(N)
	assert(expected == results)
	N = 2
	expected = {"()()","(())"}
	results = generate_balanced_parentheses(N)
	assert(expected == results)
	N = 4
	expected = {
		"()(())()", "(()())()", "((())())", 
		"()()()()", "()(()())", "()()(())", 
		"(()(()))", "((()))()", "()((()))", 
		"((()()))", "(()()())", "(())()()", 
		"(((())))"
	}
	results = generate_balanced_parentheses(N)
	assert(expected == results)
