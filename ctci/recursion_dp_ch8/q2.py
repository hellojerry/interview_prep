"""
Imagine a robot sitting on the upper left corner
of a grid with R rows and C columns. The robot can only
move in two directions, right and down, but certain cells
are "off limits" such that the robot cannot step on them,
Design an algorithm to find a path for the robot from the top left
to the bottom right.
"""


def find_path(maze,row,col, output_arr):
	"""
	First, we'll try and solve for when we know
	a solution actually exists.

	The function should return an output array.

	What's the recursive condition in this? I guess we want to continue to check until 
	both sides are illegal to go to, or we've hit the bottom right corner.
	"""
	rows = len(maze)
	cols = len(maze[0])
	final_row = rows - 1
	final_col = cols - 1
	print(row,col)

	if row == final_row and col == final_col:
		# condition when we've hit the end.
		output_arr.append((row,col))
		return True
	if row == final_row:
		## in this scenario only go right. if no right, return false
		if maze[row][col+1] == 0:
			return False
		else:
			down_arr = []
			down_res = find_path(maze,row,col+1,down_arr)
			if down_res:
				output_arr.append((row,col))
				for item in down_arr:
					output_arr.append(item)
				return True
			return False
		
	elif col == final_col:
		## in this scenario we can only go downward. 
		## if downward doesn't work, return False
		if maze[row+1][col] == 0:
			return False
		else:
			right_arr = []
			right_res = find_path(maze,row+1,col,right_arr)
			if right_res:
				output_arr.append((row,col))
				for item in right_arr:
					output_arr.append(item)
				return True
			return False

	else:
		down_arr, right_arr = [],[]
		if maze[row+1][col] == 1:
			down_res = find_path(maze,row+1,col,down_arr)
			if down_res:
				output_arr.append((row,col))
				for item in down_arr:
					output_arr.append(item)
				return True
		if maze[row][col+1] == 1:
			right_res = find_path(maze,row,col+1,right_arr)
			if right_res:
				output_arr.append((row,col))
				for item in right_arr:
					output_arr.append(item)
				return True
		return False

if __name__ == "__main__":
	maze_1 = [
		[1,0,1],
		[1,1,0],
		[0,1,1]
	]

	output_arr_1 = []
	res_1 = find_path(maze_1,0,0,output_arr_1)
	expected_2 = [(0,0),(1,0),(1,1),(2,1),(2,2)]
	assert(output_arr_1 == expected_2)
	assert(res_1 == True)

	maze_2 = [
		[1,1,1],
		[0,1,1],
		[0,1,1]
	]
	output_arr_2 = []
	res_2 = find_path(maze_2,0,0,output_arr_2)
	expected_2 = [(0,0),(0,1),(1,1),(2,1),(2,2)]
	assert(res_2 == True)
	assert(expected_2 == output_arr_2)

	maze_3 = [
		[1,1,0,0],
		[1,0,0,0],
		[1,1,1,1]
	]
	output_arr_3 = []
	res_3 = find_path(maze_3, 0,0,output_arr_3)
	expected_3 = [(0,0),(1,0),(2,0),(2,1),(2,2),(2,3)]
	assert(res_3 == True)
	assert(output_arr_3 == expected_3)

	maze_4 = [
		[1,1,0,0],
		[1,0,0,0],
		[0,1,1,1]
	]
	output_arr_4 = []
	res_4 = find_path(maze_4,0,0,output_arr_4)
	assert(res_4 == False)

	maze_5 = [
		[1,1,1,0,0],
		[1,0,1,1,1],
		[1,0,1,0,0],
		[1,1,1,1,1]
	]
	output_arr_5 = []
	res_5 = find_path(maze_5, 0,0,output_arr_5)
	expected_5 = [(0,0),(1,0),(2,0),(3,0),(3,1),(3,2),(3,3),(3,4)]
	assert(output_arr_5 == expected_5)


	maze_6 = [
		[1,0,0,0,0],
		[1,1,1,0,1],
		[1,0,1,1,1],
		[1,0,1,0,1],
		[0,1,0,1,1]
	]
	output_arr_6 = []
	res_6 = find_path(maze_6,0,0,output_arr_6)
	assert(res_6 == True)
	expected_6 = [(0, 0), (1, 0), (1, 1), (1, 2), (2, 2), (2, 3), (2, 4), (3, 4), (4, 4)]
	assert(output_arr_6 == expected_6)

