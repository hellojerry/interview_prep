"""
Towers of Hanoi. You have 3 towers(A,B,C) and N disks of different sizes
which can slide onto any tower. The puzzle starts with disks 
sorted in ascending order from bottom to top (so the smallest is on top).

Write a program to move the disks from the left tower to the right tower
using stacks.

constraints:
	1) only one disk can be moved at a time.
	2) a disk must be slid off the top tower to go onto another tower.
	3) a disk cannot be placed on top of a smaller disk.
"""

class StackMetaphor(object):

	def __init__(self, name, values):
		assert(type(name) == str)
		assert(type(values) == list)
		self.name = name
		self.values = values

	def __str__(self):
		return "Stack %s %s" % (self.name, str(self.values))

	def __repr__(self):
		return "Stack %s %s" % (self.name, str(self.values))

	def pop(self):
		return self.values.pop(0)
	def append(self,value):
		return self.values.insert(0,value)

def towers_of_hanoi(arr_1, arr_2, arr_3, N):
	"""
	Ok, so... what should the output be?
	For that matter, what is the input?
	I guess the input is stack<int>,stack<int>,stack<int>,
	with the caveat that the stacks are sorted lowest (in width)
	to highest)

	So the initial input is:
		Stack A [1,2,3]
		Stack B []
		Stack C

	and the output should be
		Stack A []
		Stack B []
		Stack C [1,2,3]

	"""

	"""
	So. first. What is our end condition? If we have 0 left for N,
	That means we've moved everything.
	"""
	if N < 1:
		return
	towers_of_hanoi(arr_1, arr_3, arr_2, N-1)
	if arr_1:
		arr_3.append(arr_1.pop())
	towers_of_hanoi(arr_2,arr_1,arr_3, N-1)
		

	




if __name__ == "__main__":
	N = 3
	arr_1 = [1,2,3]
	arr_2 = []
	arr_3 = []

	towers_of_hanoi(arr_1,arr_2,arr_3,N)
	print(arr_3)


	arr_1 = list(range(1,10))
	N = len(arr_1)
	arr_2, arr_3 = [],[]
	towers_of_hanoi(arr_1,arr_2,arr_3,N)
	print(arr_3)

