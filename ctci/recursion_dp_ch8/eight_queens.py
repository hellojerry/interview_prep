


def is_valid(col_array,occupied_row,occupied_col):
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if target_col == occupied_col or target_row == occupied_row:
			return False

		if abs(target_col - occupied_col) == abs(target_row - occupied_row):
			return False
	return True


def eight_queens(row,col_array,N,acc):
	if row == N:
		copied = list(zip(range(N),col_array.copy()))
		acc.append(copied)
	else:
		for col in range(N):
			if is_valid(col_array,row,col):
				col_array[row] = col
				eight_queens(row+1,col_array,N,acc)

if __name__ == "__main__":
	N = 8
	acc = []
	col_array = list(range(N))
	eight_queens(0,col_array,N,acc)
	assert(len(acc) == 92)
