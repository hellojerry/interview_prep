"""
A child is running up a staircase with N steps and can hop either 1,2, or 3 steps
at a time. implement a method to count how many possible ways the child can
run up the steps.
"""


def count_steps(N):
	"""
	Let's start at a final case and work backward.
	if N = 1 remaining, then we have one possible method (1 step)
	if N = 2 remaining, then we have two possible methods
		[(1,1),(2)]
	if N = 3 remaining, then we have four possible methods
		(1,1,1),(2,1),(1,2),(3)
	if N = 4 remaining, we have
		(1,1,1,1),(2,1,1),(1,1,2)(1,2,1)
		(1,3)(3,1),(2,2)
	if N = 5 remaining, we have thirteen possible methods

	"""
	if N < 0:
		return 0
	elif N == 0:
		return 1
	else:
		return  count_steps(N-1) + count_steps(N-2)+count_steps(N-3)



if __name__ == "__main__":
	r1 = count_steps(1)
	print("arg_1: 1, r1: %d" % r1)
	r2 = count_steps(2)
	print("arg_2: 2, r2: %d" % r2)
	r3 = count_steps(3)
	print("arg_3: 3, r3: %d" % r3)
	r4 = count_steps(4)
	print("arg_4: 4, r4: %d" % r4)
	r5 = count_steps(5)
	print("arg_5: 5, r5: %d" % r5)
