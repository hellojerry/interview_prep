"""
A magic index in an array A [1...n-1] is defined to be an index such that
A[i] = i. Given a sorted array of distinct integers, write a method to find
a magic index, if one exists, in array A.

Follow-up:  what if the values are not distinct?

"""


def find_magic_index_slow(arr):
	"""
	So... What's the problem with iterating over the array?
	"""
	arrlen = len(arr)
	idx = 0
	while idx < arrlen:
		if arr[idx] == idx:
			return idx
		idx += 1
	return -1


def find_magic_index(arr):
	"""
	This solution needs to be faster than o(n).
	I imagine we can do a binary search on this.
	"""
	pass




if __name__ == "__main__":
	arr_1 = [1,2,3,4,5,5]
	print(find_magic_index(arr_1))
	assert(find_magic_index(arr_1) == 5)

