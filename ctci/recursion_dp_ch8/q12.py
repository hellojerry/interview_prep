"""
Write an algorithm to print all ways of arranging eight queens on an 8x8 chessboard
so that none of them share the same row, column, or diagonal. In this case, "diagonal"
means all diagonals on the board, not just the two that go all the way to the corners.


"""


def cols_are_valid(col_array,occupied_row,occupied_col):
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if target_col == occupied_col:	
			return False
		## this won't happen
		if target_row == occupied_row:
			return False

		if abs(target_row-occupied_row) == abs(target_col - occupied_col):
			return False
	return True


def eight_queens(row,col_array,N,acc):
	"""
	Ok, so the first thing - how do we determine what is a legal placement?
	A legal placement in relation to the placed piece means that
		A) the row is not shared
		B) the column is not shared.
		C) abs(rowA-rowB) != abs(colA-colB)
	let's be sure of this.

	0,0 and 7,7 are diagonal to one another. abs(rowA-rowB) == abs(colA-colB)
	0,0 and 6,7 are not.  abs(rowA-rowB) != abs(colA-colB)
	0,7 and 7,0 are diagonal. abs(rowA-rowB) == abs(colA-colB)

	The next question is, how do we solve for one possible combination?

	If we iterate over every row, then do all columns for each row, we can
	add a given combination to the accumulator once we reach the limit.
	"""
	if row == N:
		copied = list(zip(range(N),col_array.copy()))
		acc.append(copied)
	else:
		for col in range(N):
			if cols_are_valid(col_array,row,col):
				col_array[row] = col
				eight_queens(row+1,col_array,N,acc)

if __name__ == "__main__":
	N = 8
	cols = [0,1,2,3,4,5,6,7]
	acc = []
	eight_queens(0,cols,8,acc)
	print(acc)
	print(len(acc))

