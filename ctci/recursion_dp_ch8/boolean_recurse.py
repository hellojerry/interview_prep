


def count_eval(statement,t_or_f):
	assert(type(statement) == str)
	assert(type(t_or_f) == bool)

	stmt_len = len(statement)
	if stmt_len == 0:
		return 0
	if stmt_len == 1:
		as_bool = bool(int(statement))
		if as_bool == t_or_f:
			return 1
		return 0

	amt = 0
	## iterate by steps of two.
	for i in range(1,stmt_len,2):
		target_operator = statement[i]
		left,right = statement[:i], statement[(1+i):]

		left_true = count_eval(left,True)
		left_false = count_eval(left,False)
		right_true = count_eval(right,True)
		right_false = count_eval(right,False)
		total = (left_true + left_false) * (right_true + right_false)
		total_true = 0
		if target_operator == "&":
			total_true = (left_true*right_true)
		elif target_operator == "|":
			total_true = (left_true*right_true) + (left_true*right_false
					) + (left_false*right_true)
		elif target_operator == "^":
			total_true = (left_true * right_false) + (left_false * right_true)

		if t_or_f == True:
			amt += total_true
		else:
			amt += total - total_true
	return amt

if __name__ == "__main__":

	statement = "0&0&0&1^1|0"
	res = count_eval(statement,True)
	assert(res == 10)
