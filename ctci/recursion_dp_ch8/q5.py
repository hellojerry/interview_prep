"""
Write a recursive function to multiple two positive integers without using the * operator
or the / operator. You can use addition, subtraction, and bit shifting, but you should minimize
the number of these operations.
"""


def multiply_brute(left,right):
	"""
	So my first thought is
	to take the initial value
	and accumulate it with a while loop
	equal to the number of times 
	that left needs to get added to itself.
	"""
	iter_ct = 0
	res = 0
	while iter_ct < right:
		res += left
		iter_ct += 1
	return res





if __name__ == "__main__":
	res_1 = multiply_brute(3,5)
	print(res_1)
