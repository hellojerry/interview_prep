"""
You have a stack of N boxes, with widths Wi, Heights Hi, and depths Di.
The boxes cannot be rotated and can only be stacked on top of one another if each box in the stack is strictly
larger than the box above it in width, height, and depth. Implement
a method to compute the height of the tallest possible stack. The height of a stack is the sum of the heights
of each box.
"""


class Box(object):

	def __init__(self, width=1,height=1,depth=1):
		for i in [width,height,depth]:
			assert(type(i) == int)
			assert(i >= 1)
		self.width = width
		self.height = height
		self.depth = depth

	def can_be_above(self,box):
		if self.width > box.width and self.height > box.height and self.depth > box.depth:
			return True
		return False

	def __str__(self):
		return "Box(w(%d),h(%d),d(%d))" % (self.width,self.height,self.depth)

	def __repr__(self):
		return "Box(w(%d),h(%d),d(%d))" % (self.width,self.height,self.depth)

def create_stack(boxes, bottom_idx):
	bottom = boxes[bottom_idx]
	max_height = 0
	for i in range(bottom_idx+1, len(boxes)):
		if boxes[i].can_be_above(bottom):
			print(boxes[i])
			height = create_stack(boxes, i)
			max_height = max(height,max_height)
	max_height += bottom.height
	return max_height


def stack_boxes(box_arr):
	"""
	First part: how to represent a box. Easiest thing would be to do a tuple of (W,H,D).
	How do we deal with two boxes where theyre the same width and different heights and depths?

	First step is sorting the boxes by any old attribute. This means we don't need to do 
	a backtrack.

	Second step is doing successively smaller comparisons on the max height, similar
	to finding a max path in a btree.

	"""
	max_height = 0
	sorted_by_height = sorted(box_arr, key=lambda x:x.height)
	for i in range(len(sorted_by_height)):
		h = create_stack(box_arr, i)
		max_height = max(max_height, h)
	return max_height



if __name__ == "__main__":
	"""
	Review this again.
	"""

	b1 = Box(1,1,1)
	b2 = Box(1,2,1)
	b3 = Box(2,2,2)
	b4 = Box(4,1,1)
	b5 = Box(3,3,2)
	b6 = Box(3,3,4)
	box_arr = [b1,b2,b3,b4,b5,b6]
	res = stack_boxes(box_arr)
	print(res)




