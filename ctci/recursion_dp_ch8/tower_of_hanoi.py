"""
"""


def tower_of_hanoi(source,mid,dest,N):
	if N == 0:
		return
	tower_of_hanoi(source,dest,mid,N-1)
	if source:
		dest.append(source.pop())
	## then move from buffer to dest with source as buffer
	tower_of_hanoi(mid,source,dest,N-1)


if __name__ == "__main__":
	N = 3
	source = list(range(1,4))
	mid, dest = [],[]
	tower_of_hanoi(source,mid,dest,N)
	print(dest)
