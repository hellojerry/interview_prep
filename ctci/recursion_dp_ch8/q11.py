"""
Given an infinite number of quarters (25 cents), dimes (10 cents), nickels(5 cents) and
pennies (1 cent), write code to calculate the number of ways representing N cents.

Ok, this one is a bit more confusing. Let's try an example.

37 cents.

quarter, dime, two pennies
quarter, two nickels, two pennies
three dimes, a nickel, two pennies
seven nickels, two pennies
37 pennies
five nickels, a dime, two pennies
four nickels, a dime, seven pennies

ten cents:

	- ten pennies
	- one dime
	- one nickel, five pennies
	- two nickels
15 cents:
	- 15 pennies
	- one dime, one nickel
	- one dime, five pennies
	- three nickels
	- five pennies, two nickels
	- ten pennies, one nickel


25 cents:
	- 25 pennies
	- five nickels
	- one quarter
	- four nickels, five pennies
	- three nickels, ten pennies
	- two nickels, fifteen pennies
	- one nickel, 20 pennies
	- one dime, three nickels
	- one dime, fifteen pennies
	- one dime, two nickels, five pennies
	- one dime, one nickel, ten pennies
	- two dimes, one nickel
	- two dimes, five pennies

"""

def make_change_ct(target_cents,coins):
	"""
	Ok, so, for every unit of five cents, we have two combinations:
		(5 pennies) (1 nickel)
	for every unit of ten cents, we have 
		(10 pennies) (two nickels) (one dime) (five pennies, one nickel)
	for every unit of 25 cents, we have the two representations of five cents X 5
		(the four representations of ten cents x 2) X (2 representatoins of 5 cents) X 2 +  a quarter,

	So... is this top-down or bottom-up? We can call make_change_ct on recursively smaller numbers
	by doing a modulus and a flat division for each set of units and adding the number to that.
	divmod returns # of times, remainder
	"""

	if target_cents == 0:
		## subtracting a penny made this zero, making it a combo.
		return 1
	elif len(coins) == 0 or target_cents < 0:
		## nothing left - coins array exhausted or below zero due to too large of a subtraction.
		return 0
	else:
		## left call tracks 
		return make_change_ct(target_cents - coins[0], 
			coins) + make_change_ct(target_cents, coins[1:])

def make_change(total):
	count = 0
	quarters, dimes, nickels = 0,0,0
	while quarters <= (total//25):
		total_less_than_q = total - (quarters*25)
		while dimes <= (total_less_than_q // 10):
			total_less_than_q_d = total_less_than_q - (dimes*10)
			while nickels <= (total_less_than_q_d // 5):
				count+= 1
				nickels += 1
			nickels = 0
			dimes += 1
		dimes = 0
		quarters += 1
	return count

if __name__ == "__main__":
	res1 = make_change(7)
	print(res1)
	res2 = make_change(10)
	print(res2)
	res3 = make_change(15)
	print(res3)
	res4 = make_change(25)
	print(res4)

