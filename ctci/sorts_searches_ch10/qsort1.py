"""

Implement quick sort.
"""


def partition(arr, low, high):

	pivot = arr[high]
	i = low - 1
	for j in range(low,high):
		if arr[j] <= pivot:
			i+= 1
			arr[i],arr[j] = arr[j],arr[i]
	arr[i+1],arr[high] = arr[high], arr[i+1]
	return i+1

def quick_sort(arr, low, high):
	if low < high:
		partition_idx = partition(arr,low,high)
		quick_sort(arr,low, partition_idx-1)
		quick_sort(arr,partition_idx+1, high)

if __name__ == "__main__":
	arr1 = [9,142,43,2,1,5,7,3,1,4,6,-2]
	quick_sort(arr1,0,len(arr1)-1)
	print(arr1)

	arr2 = []
	quick_sort(arr2,0,len(arr2)-1)
	print(arr2)
	arr3 = [-3,-21421,-32,0.4,-3,1.2222,5]
	quick_sort(arr3,0,len(arr3)-1)
	print(arr3)

