"""
You are given two sorted arrays, A and B,
where A has a large enough buffer at the end to hold B.
Write a method to merge B into A in sorted order.
"""


def merge_arrays(arr_A, arr_B):
	"""
	This is a weird variation on a merge sort.
	Set the two pointers up as the tail idx,
	then iterate over them, incrementing only one pointer
	at a time.
	"""

	len_left = len(arr_A)
	len_right = len(arr_B)
	right_ptr = len_right - 1
	left_ptr = len_left - 1
	out_ptr = len_left - 1

	while arr_A[left_ptr] == None:
		## move the left ptr out of the null region.
		left_ptr -= 1

	while left_ptr >= 0 and right_ptr >= 0:
		left = arr_A[left_ptr]
		right = arr_B[right_ptr]
		if left > right:
			arr_A[out_ptr] = left
			left_ptr -= 1
			out_ptr -= 1
		elif left < right:
			arr_A[out_ptr] = right
			right_ptr -= 1
			out_ptr -= 1
		else:
			arr_A[out_ptr] = left
			out_ptr -= 1
			arr_A[out_ptr] = right
			out_ptr -= 1
			left_ptr -= 1
			right_ptr -= 1
	## now for the stragglers
	while left_ptr >= 0:
		left = arr_A[left_ptr]
		arr_A[out_ptr] = left
		left_ptr -= 1
		out_ptr -= 1

	while right_ptr >= 0:
		right = arr_B[right_ptr]
		arr_A[out_ptr] = right
		right_ptr -= 1
		out_ptr -= 1



if __name__ == "__main__":

	arr_a = [1,2,3,4,5,None,None,None,None,None]
	arr_b = [0,2,4,5,6]
	expected = [0,1,2,2,3,4,4,5,5,6]
	merge_arrays(arr_a,arr_b)
	assert(arr_a ==expected)

	arr_a = [1,2,2,None,None,None,None,None,None,None]
	arr_b = [-7,0,0,1,1,2,9]
	expected = [-7,0,0,1,1,1,2,2,2,9]
	merge_arrays(arr_a, arr_b)
	assert(arr_a == expected)

	arr_a = [-12,-4,0,0,0,1,2,None,None,None]
	arr_b = [-13,-7,4]
	expected = [-13,-12,-7,-4,0,0,0,1,2,4]
	merge_arrays(arr_a,arr_b)
	assert(arr_a == expected)


