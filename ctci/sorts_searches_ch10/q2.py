"""
Write a method to sort an array of strings so that all the anagrams 
are next to each other.

I don't really understand this one honestly. It would be nice to get sample inputs and outputs.


I believe it would be something like this:

bog
gib
zort
flot

input:  ["bog","gib","zort","flot","gob","big","troz","tolf","folt","obg","otrz","ibg"]
output: [ "big","gib","ibg","bog","gob","obg",  "otrz","troz","zort", "flot","folt","tolf"]

"""


from collections import defaultdict

def sort_into_anagrams(arr):
	"""
	So we have two separate issues to solve here.

	1) how do we determine an anagram?
	2) how do we get out a consistent sort order after grouping these things without using python's "sorted"?

	The answer to one can be done in two ways:
		A) sorting the string and checking the sorted string against a hashtable, and then appending the unsorted string into a list in the hashtable value
		B) doing a letter frequency count with a dictionary in a separate method, and transforming the hashmap into a loosely-compressed string to use as a hashtable key, then appending the original value in, and re-sorting the array

	Now that i think of it, option A is better. This lets us solve the second half as well, which is getting a consistent grouping in the output.
	We iterate over the hashmap to get a result. This particular question is better handled with java, which has a TreeMap which is a sorted hashmap by key.
	"""

	anagrams = defaultdict(list)
	for item in to_sort:
		key = "".join(sorted(item))
		anagrams[key].append(item)
	ct = 0
	for key, sub_arr in sorted(anagrams.items()):
		for value in sub_arr:
			arr[ct] = value
			ct += 1
				

if __name__ == "__main__":
	to_sort = ["bog","gib","zort","flot","gob","big","troz","tolf","folt","obg","otrz","ibg"]
	expected_out =["big","gib","ibg","bog","gob","obg","otrz","troz","zort", "flot","folt","tolf"]
	sort_into_anagrams(to_sort)
	print(to_sort)
