

def bsearch(arr,val):

	arrlen = len(arr)
	mid = int(arrlen/2)

	left_ptr = 0
	right_ptr = arrlen - 1
	while left_ptr <= right_ptr:
		mid = int((right_ptr-left_ptr)/2)
		if arr[mid] < val:
			left_ptr = mid+1
		elif arr[mid] > val:
			right_ptr = mid - 1
		else:
			return mid
	return -1


if __name__ == "__main__":

	arr = [1,2,3,3,3,3,3,3,4,4,4,5,5,6,7,8,9,10]
	print(bsearch(arr,2))
