

def bsearch(arr,val):
	arrlen = len(arr)
	left = 0
	right = arrlen - 1
	while left <= right:
		mid = int((left+right)/2)
		if arr[mid] < val:
			left = mid+1
		elif arr[mid] > val:
			right = mid -1
		else:
			return mid
	return -1

if __name__ == "__main__":

	arr = [1,2,3,3,3,3,3,3,4,4,4,5,5,6,7,8,9,10]
	print(bsearch(arr,2))
