"""
Given a sorted array of N integers that has been rotated an unknown number of times,
write code to find an element in the array. You may assume that the array was originally in
increasing order.


input = [15,16,19,20,25,1,3,4,5,7,10,14], value: 5
output = 8 (the index where 5 is) 

I can't fucking think. wtf.

"""



def find_element_in_rotated_array(input_arr, value):
	"""
	1. take midpoint, compare to head and tail.
	2a. if head > midpoint, bsearch left side until rotation point found. memoize rotation point
	2b. if tail < midpoint, bsearch right side until rotation point found. memoize rotation point.
	3. compare head, rotation point value, and tail to value.
		if rotation_point < value < tail:
			bsearch right side from rotation point to tail
		elif rotation_point > value > head:
			bsearch left side from head to rotation point.
	"""
