

def merge_sort(arr):

	arrlen = len(arr)
	if arrlen > 1:
		midpoint = int(arrlen/2)
		left = arr[:midpoint]
		right = arr[midpoint:]
		merge_sort(left)
		merge_sort(right)

		left_ptr,right_ptr,arr_ptr = 0,0,0
		while left_ptr < len(left) and right_ptr < len(right):
			if left[left_ptr] < right[right_ptr]:
				arr[arr_ptr] = left[left_ptr]
				left_ptr += 1
			else:
				arr[arr_ptr] = right[right_ptr]
				right_ptr += 1
			arr_ptr += 1

		## now for stragglers
		while left_ptr < len(left):
			arr[arr_ptr] = left[left_ptr]
			left_ptr += 1
			arr_ptr += 1

		while right_ptr < len(right):
			arr[arr_ptr] = right[right_ptr]
			right_ptr += 1
			arr_ptr += 1

if __name__ == "__main__":
	arr1 = [9,142,43,2,1,5,7,3,1,4,6,-2]
	merge_sort(arr1)
	print(arr1)

	arr2 = []
	merge_sort(arr2)
	print(arr2)
	arr3 = [-3,-21421,-32,0.4,-3,1.2222,5]
	merge_sort(arr3)
	print(arr3)
