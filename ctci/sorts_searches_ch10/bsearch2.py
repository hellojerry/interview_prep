

def bsearch(arr,val):

	arrlen = len(arr)
	left_ptr = 0
	midpoint = None
	right_ptr = arrlen - 1

	while left_ptr <= right_ptr:
		midpoint = int((right_ptr+left_ptr)/2)
		if arr[midpoint] < val:
			left_ptr = midpoint + 1
		elif arr[midpoint] > val:
			right_ptr = midpoint - 1
		else:
			return midpoint
	return -1

if __name__ == "__main__":

	arr = [1,2,3,3,3,3,3,3,4,4,4,5,5,6,7,8,9,10]
	print(bsearch(arr,2))
