


class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value


def contains(root, node):
	if root is None:
		return False
	if root is node:
		return True
	return contains(root.left, node) or contains(root.right, node)

def find_common_ancestor(root, node_1, node_2):
	if root is None or root is node_1 or root is node_2:
		return root

	n1_is_left = contains(root.left, node_1)
	n2_is_left = contains(root.left, node_2)
	if n1_is_left != n2_is_left:
		return root
	if n1_is_left:
		return find_common_ancestor(root.left, node_1, node_2)
	return find_common_ancestor(root.right, node_1, node_2)

if __name__ == "__main__":
	left_left_bottom = TreeNode(4)
	left_right_bottom = TreeNode(5)
	left_mid = TreeNode(2,left_left_bottom,left_right_bottom)
	right_left_bottom = TreeNode(7)
	right_right_bottom = TreeNode(6)
	right_mid = TreeNode(3, right_left_bottom, right_right_bottom)
	root = TreeNode(1, left_mid, right_mid)

	out_1 = find_common_ancestor(root, left_mid, right_left_bottom)
	assert(out_1 is root)
	out_2 = find_common_ancestor(root, left_left_bottom, left_right_bottom)
	assert(out_2 is left_mid)
	out_3 = find_common_ancestor(root, left_mid, left_right_bottom)
	assert(out_3 is left_mid)
