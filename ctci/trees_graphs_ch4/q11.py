"""
You are implementing a binary search tree class from scratch,
which, in addition to insert, find, and delete, has a method called
get_random_node() which returns a random node from the tree.
All nodes should be equally likely to be chosen.

Design and implement and algorithm for get_random_node(),
and explain how you would implement the rest of the methods.
"""
import random

class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value


class BST(object):


	def __init__(self, root):
		assert(type(root) == TreeNode)
		self.root = root
		self.number_of_nodes = 1
		self.max_number = 0

	def find(self, value):
		pass

	def insert(self, value):
		"""
		This would need to increment the number of nodes
		assuming the target value is deleted.
		"""
		pass

	def delete(self, value):
		"""
		This method would need to decrement the number of nodes
		assuming the target value is deleted.
		"""
		pass


	def dfs_preorder_with_acc(self, node):
		self.max_number -= 1
		if self.max_number == 0:
			return node
		if node.left:
			left_res = self.dfs_preorder_with_acc(node.left)
			if left_res:
				return left_res
		if node.right:
			right_res = self.dfs_preorder_with_acc(node.right)
			if right_res:
				return right_res
		return None
	
			
		

	def get_random_node(self):
		"""
		So what I would do here is get a random number
		from between 1 and the number of nodes.
		From there, i'd probably do a DFS with an accumulator value
		(i'd just to a preorder search), and return the value
		once the limit is reached. insert and delete would
		reduce number_of_nodes or increase them depending on
		whether the insert or delete actually was successful.

		We could speed this up by checking the node value against
		the root and doing postorder vs preorder - we'd need
		the container class to keep track of both the left and right
		side of the trees though.
		"""
		random_val = random.randint(1,self.number_of_nodes)
		self.max_number = random_val
		node = self.dfs_preorder_with_acc(self.root)
		return node


if __name__ == "__main__":

	bot_left_left = TreeNode(19)
	bot_left_right = TreeNode(20)
	mid_left = TreeNode(13, bot_left_left, bot_left_right)
	bot_right_left = TreeNode(21)
	bot_right_right = TreeNode(22)
	mid_right = TreeNode(14, bot_right_left, bot_right_right)
	t2_root = TreeNode(9, mid_left, mid_right) 
	bst = BST(t2_root)
	bst.number_of_nodes = 7
	node = bst.get_random_node()

	results_set = set()
	for i in range(100):
		node = bst.get_random_node()
		results_set.add(node.value)
	## theoretically we could get less than 7 with the RNG
	## but this is fine.
	assert(sorted(results_set) == [9,13,14,19,20,21,22])

