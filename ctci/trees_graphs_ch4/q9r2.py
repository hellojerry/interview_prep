
"""
A binary search tree was created by traversing through an array from
left to right and inserting each element. Given a BST with distinct
elements, print all possible arrays that could have led to this tree.


Ex:
		2
	      /   \
             1     3

output: [2,1,3], [2,3,1]


ex:
		   5
                /     \
               3       7 
             /   \    / \
            2     4  6   8

output: 
	[5,3,7,2,4,6,8], [5,3,7,4,2,6,8],
	[5,3,7,2,4,8,6], [5,3,7,4,2,8,6]

	[5,7,3,6,8,2,4],[5,7,3,6,8,4,2],
	[5,7,3,8,6,2,4],[5,7,3,8,6,4,2]

"""


class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

def weave_lists(first, second, results, prefix):
	if not first or not second:
		results.append(prefix+first+second)
		return
	first_head, first_tail = first[0], first[1:]
	weave_lists(first_tail, second, results, prefix + [first_head])
	second_head, second_tail = second[0], second[1:]
	weave_lists(first, second_tail, results, prefix+[second_head])


def all_sequences(root):
	if root is None:
		return []
	answer = []
	prefix = [root.value]
	left = all_sequences(root.left) or [[]]
	right = all_sequences(root.right) or [[]]
	for i in range(len(left)):
		weaved = []
		weave_lists(left[i], right[len(right)-1], weaved, prefix)
		answer.extend(weaved)
	return answer






if __name__ == "__main__":
	bottom_left = TreeNode(1)
	bottom_right = TreeNode(3)
	root = TreeNode(2, bottom_left, bottom_right)
	res = all_sequences(root)
	print(res)
	assert(res == [[2,1,3],[2,3,1]])


	bottom_left_left = TreeNode(2)
	bottom_left_right = TreeNode(4)
	mid_left = TreeNode(3, bottom_left_left, bottom_left_right)

	bottom_right_left = TreeNode(6)
	bottom_right_right = TreeNode(8)
	mid_right = TreeNode(7, bottom_right_left, bottom_right_right)
	root = TreeNode(5, mid_left, mid_right)
	print(all_sequences(root))
