"""

An adjacency matrix is a bit weirder conceptually.

It's a matrix of NxN booleans where N is the number of nodes
being represented. a "true" or "1" value at matrix[i][j] means
that there is a connection from i to j.

In an undirected graph, the matrix will be symmetric.
In a directed graph, the matrix might or might not be symmetric.

The two examples in this will be a directed graph of 1->2->3 (then back to 1)
and an undirected graph of 1<->2<->3 (and back to 1)

"""
from pprint import pprint

if __name__ == "__main__":
	directed_adjacency_matrix = [
		[0,1,0]
		[0,0,1]
		[1,0,0]
	]

	pprint(directed_adjacency_matrix)

	undirected_adjacency_matrix = [
		[0,1,1],
		[1,0,1],
		[1,1,0]
	]
