"""
You are given a binary tree in which each node contains an integer value
(which might be positive or negative). Design an algorithm to count
the number of paths that sum to a given value.

The path does not need to start or end at the root or a leaf, but it must 
go downwards (traveling only from parent nodes to child nodes)


			    10 
			/        \
		      5           -3
                    /  \            \
                   3    2            11
                  / \     \
                 3  -2     1

With an input of 8,

the result is: 3 (5->2->1, 5->3, -3->11)


"""

class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

class BTree(object):

	def __init__(self, root):
		assert(type(root) == TreeNode)
		self.root = root


	def count_paths_containing_sum(self,root, target_val):
		if not root:
			return 0
		paths_with_sum_root = self.count_paths_with_sum_from_node(root, 
				target_val, 0)
	
		left_paths = self.count_paths_containing_sum(root.left, target_val)
		right_paths = self.count_paths_containing_sum(root.right, target_val)
	
		return paths_with_sum_root + left_paths + right_paths

	def count_paths_with_sum_from_node(self, node, target_sum, current_sum):
		if not node:
			return 0
		current_sum += node.value
		total_paths = 0
		if current_sum == target_sum:
			total_paths += 1
		total_paths += self.count_paths_with_sum_from_node(node.left, target_sum, 
			current_sum)
		total_paths += self.count_paths_with_sum_from_node(node.right, target_sum,
			current_sum)
		return total_paths 



if __name__ == "__main__":
	l4_1 = TreeNode(3)
	l4_2 = TreeNode(-2)
	l4_4 = TreeNode(1)
	l3_1 = TreeNode(3, l4_1, l4_2)
	l3_2 = TreeNode(2, None, l4_4)
	l3_4 = TreeNode(11)
	l2_1 = TreeNode(5, l3_1, l3_2)
	l2_2 = TreeNode(-3, None, l3_4)
	root = TreeNode(10, l2_1, l2_2)
	btree = BTree(root)
	res = btree.count_paths_containing_sum(root, 8)
	print(res)
