"""
Given a binary tree, design an algorithm which
creates a linked list of all the nodes at each depth.
In order words, if you have a tree with depth of 3,
there'll be 3 linked lists.
"""

class LLNode(object):

	def __init__(self, value, level, next_=None):
		if next_ is not None:
			assert(type(next_) == LLNode)
		assert(type(value) == int)
		assert(type(level) == int)
		self.value = value
		self.level = level
		self.next_ = next_

	def __str__(self):
		return "LLNode(value=%d,level=%d)" % (self.value, self.level)

	def __repr__(self):
		return "LLNode(value=%dmlevel=%d)" % (self.value, self.level)

	def __eq__(self, other):
		"""
		Need to override this for the purposes of the script
		to verify equality without a lot of trouble.
		We don't need pointer equality for this assertion
		"""

		if type(other) != LLNode:
			return False
		next_same = False
		if self.next_ is None and other.next_ is None:
			next_same = True
		if self.next_ is None and other.next_ is not None:
			return False
		if self.next_ is not None and other.next_ is None:
			return False
		if not next_same:
			if self.next_.value == other.next_.value:
				next_same = True
			else:
				return False
		return self.value == other.value


class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		self.value = value
		if left is not None:
			assert(type(left) == TreeNode)
		self.left = left
		if right is not None:
			assert(type(right) == TreeNode)
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

def generate_linked_lists_with_levels(root_tree_node):
	"""
	This is uglier than it should be, only because
	it's a bit tricky to construct the result.

	In character this is a BFS. 

	As we go through the BFS, we increment the current level
	value up based on what gets popped out.

	We don't technically need the sub-arrays in the output,
	but it's a bit easier to read the result in the terminal this way.

	"""

	if not root_tree_node:
		return None
	assert(type(root_tree_node) == TreeNode)
	queue = []
	level = 0
	root_ll_node = LLNode(root_tree_node.value, level)
	out_pile = [[root_ll_node]]
	queue.append([root_tree_node,level])
	while len(queue) > 0:
		current_node, current_level = queue.pop(0)
		current_level += 1
		if current_node.left or current_node.right:
			if not len(out_pile) == current_level+1:
				out_pile.append([])
		left_ll_node, right_ll_node = None, None
		if current_node.left:
			left_ll_node = LLNode(current_node.left.value, current_level)
			queue.append([current_node.left, current_level])
		if current_node.right:
			right_ll_node = LLNode(current_node.right.value, current_level)
			queue.append([current_node.right, current_level])
		target_far_right = out_pile[-1]
		if len(target_far_right) == 0:
			if left_ll_node and right_ll_node:
				target_far_right.append(left_ll_node)
				target_far_right.append(right_ll_node)
				left_ll_node.next_ = right_ll_node

			elif left_ll_node and not right_ll_node:
				target_far_right.append(left_ll_node)

			elif not left_ll_node and right_ll_node:
				target_far_right.append(right_ll_node)

		else:
			final_el = target_far_right[-1]
			if left_ll_node and right_ll_node:
				final_el.next_ = left_ll_node
				left_ll_node.next_ = right_ll_node
				target_far_right.append(left_ll_node)
				target_far_right.append(right_ll_node)
			elif left_ll_node and not right_ll_node:
				final_el.next_ = left_ll_node
				target_far_right.append(left_ll_node)

			elif not left_ll_node and right_ll_node:
				final_el.next_ = right_ll_node
				target_far_right.append(right_ll_node)
	return out_pile


if __name__ == "__main__":
	"""
	Let's make a tree with 3 levels.
		   0
		 /   \
		1     2
	       / \   / \
              3   4 5   6

	this should return the following:

	[LLNode(0)]
	[LLNode(1)->LLNode(2)]
	[LLNode(3)->LLNode(4)->LLNode(5)->LLNode(6)]
	"""
	bottom_left_left = TreeNode(3)
	bottom_left_right = TreeNode(4)
	bottom_right_left = TreeNode(5)
	bottom_right_right = TreeNode(6)
	middle_left = TreeNode(1, bottom_left_left, bottom_left_right)
	middle_right = TreeNode(2, bottom_right_left, bottom_right_right)
	root = TreeNode(0, middle_left, middle_right)
	res = generate_linked_lists_with_levels(root)
	print(res)

	l2n4 = LLNode(6,2)
	l2n3 = LLNode(5,2,l2n4)
	l2n2 = LLNode(4,2,l2n3)
	l2n1 = LLNode(3,2,l2n2)
	l1n2 = LLNode(2,1)
	l1n1 = LLNode(1,1,l1n2)
	l0n1 = LLNode(0,0)
	expected = []
	expected.append([l0n1])
	expected.append([l1n1,l1n2])
	expected.append([l2n1,l2n2,l2n3,l2n4])

	print(expected)
	assert(expected == res)



