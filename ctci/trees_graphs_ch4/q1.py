"""
Given a directed graph, design an algorithm
to find out whether there is a route between two nodes.
"""


class GraphNode(object):

	def __init__(self, val, adjacent):
		assert(type(val) == int)
		assert(type(adjacent) == list)
		self.val = val
		self.visited = False
		for item in adjacent:
			assert(type(item) == GraphNode)
		self.adjacent = adjacent

	def __str__(self):
		return "GraphNode(%d)" % self.val

	def __repr__(self):
		return "GraphNode(%d)" % self.val


def find_path_dfs(start_node, current_node):

	## edge case
	if current_node is start_node:
		return True
	current_node.visited = True
	for node in current_node.adjacent:
		if node is start_node:
			return True
		if node.visited == False:
			node.visited = True
			find_path_dfs(start_node, node)
	return False

def find_path_bfs(start_node, target_node):

	current_node = start_node
	if current_node is target_node:
		return True
	current_node.visited = True
	queue = []
	queue.append(current_node)
	while len(queue) > 0:
		node = queue.pop(0)
		for n in node.adjacent:
			if n is target_node:
				return True
			if n.visited == False:
				n.visited = True
				queue.append(n)
	return False


if __name__ == "__main__":
	n3 = GraphNode(3,[])
	n2 = GraphNode(2,[n3])
	n1 = GraphNode(1,[n2])
	n3.adjacent = [n1]
	res = find_path_dfs(n2, n1)
	assert(res == True)


	n3 = GraphNode(3,[])
	n2 = GraphNode(2,[])
	n1 = GraphNode(1,[n2])
	res = find_path_dfs(n1, n3)
	assert(res == False)

	n3 = GraphNode(3,[])
	n2 = GraphNode(2,[n3])
	n1 = GraphNode(1,[n2])
	n3.adjacent = [n1]
	res = find_path_bfs(n2, n1)
	assert(res == True)

	n3 = GraphNode(3,[])
	n2 = GraphNode(2,[])
	n1 = GraphNode(1,[n2])
	res = find_path_dfs(n1, n3)
	assert(res == False)
