"""
You are given a list of projects and a list of dependencies (which is
a list of pairs of projects, where the second project is dependent on the first project).
All of a project's dependencies must be built before the project is. Find a build that will
allow the projects to be built. If there is no valid build order, return an error.

Projects: a,b,c,d,e,f
Dependendies: (a,d),(f,b),(b,d),(f,a),(d,c)



a->d
f->b
b->d
f->a
d->c
e (no dependencies)

dependencies go left to right:

        e

	f--b---d-- c 
	  \   /
	    a
	 
"""


UNVISITED = 0
VISITING = 1
VISITED = 2


def topological_sort(key, visited_list, adjacency_list, output_arr):
	"""
	The problem with this implementation is that it doesn't detect 
	circular dependencies.
	"""
	if not visited_list[key]:
		visited_list[key] = True
		for neighbor in adjacency_list[key]:
			topological_sort(neighbor, visited_list, adjacency_list, output_arr)
		output_arr.insert(0, key)


def cycle_proof_topological_sort(key, visited_list, adjacency_list, output_arr):
	"""
	This is a DFS of an adjacency list graph.

	DFS is better when you want to visit all nodes.
	BFS is better for finding specific paths.

	The idea with this is that if we've already "visited" a node,
	that means its already had its dependencies resolved.
	If it's VISITING, that means that we're trying to resolve
	a dependency of node A that simultaneously depends on node A,
	a.k.a. we have a cycle.

	The other way to do this is to throw an exception in the event
	That the visited_list[key] == VISITING, thereby short-circuiting the system.

	This particular function should be called with:
		results = []
		for key in visited_list:
			out = cycle_proof_topological_sort(...)
			results.append(out)
		if not all(results):
			return False
		return True

	"""
	if visited_list[key] == UNVISITED:
		visited_list[key] = VISITING
		for neighbor in adjacency_list[key]:
			out = cycle_proof_topological_sort(neighbor, 
				visited_list, adjacency_list, output_arr)
			if not out:
				return False
		visited_list[key] = VISITED
		output_arr.insert(0,key)
		return True
	elif visited_list[key] == VISITING:
		return False
	return True

if __name__ == "__main__":
	adjacency_list = {
		"f": ["b","a"],
		"a": ["d"],
		"b": ["d"],
		"d": ["c"],
		"c": [],
		"e": [],
		"j": ["b"],
	}

	visited_list = {
		"b": False,
		"f": False,
		"a": False,
		"c": False,
		"d": False,
		"e": False,
		"j": False,
	}
	out_arr = []

	for key in visited_list:
		topological_sort(key, visited_list, adjacency_list, out_arr)
	"""
	second version: return false if there's a circular dependency.
	In the second run, the circular dependency is between "g" and "b" - 
	both depend upon one another to build.
	"""
	adjacency_list = {
		"f": ["b","a"],
		"a": ["d"],
		"b": ["d"],
		"d": ["c"],
		"c": [],
		"e": [],
		"j": ["b"],
	}

	visited_list = {
		"b": UNVISITED,
		"f": UNVISITED,
		"a": UNVISITED,
		"c": UNVISITED,
		"d": UNVISITED,
		"e": UNVISITED,
		"j": UNVISITED,
	}
	out_arr = []
	results = []
	for key in visited_list:
		out = cycle_proof_topological_sort(key, visited_list, adjacency_list, out_arr)
		results.append(out)
	## no cycle found.
	assert(out_arr == ["j","e","f","a","b","d","c"])
	assert(all(results) == True)
	adjacency_list = {
		"f": ["b","a"],
		"a": ["d"],
		"b": ["d","g"],
		"d": ["c"],
		"c": [],
		"e": [],
		"g": ["b"],
	}
	visited_list = {
		"a": UNVISITED,
		"b": UNVISITED,
		"c": UNVISITED,
		"d": UNVISITED,
		"e": UNVISITED,
		"f": UNVISITED,
		"g": UNVISITED,
	}
	out_arr = []
	results = []
	for key in visited_list:
		out = cycle_proof_topological_sort(key, visited_list, adjacency_list, out_arr)
		results.append(out)
	## This means that we've got a cycle.
	assert(all(results) == False)
