"""
A binary search tree was created by traversing through an array from
left to right and inserting each element. Given a BST with distinct
elements, print all possible arrays that could have led to this tree.


Ex:
		2
	      /   \
             1     3

output: [2,1,3], [2,3,1]


ex:
		   5
                /     \
               3       7 
             /   \    / \
            2     4  6   8

output: 
	[5,3,7,2,4,6,8], [5,3,7,4,2,6,8],
	[5,3,7,2,4,8,6], [5,3,7,4,2,8,6]

	[5,7,3,6,8,2,4],[5,7,3,6,8,4,2],
	[5,7,3,8,6,2,4],[5,7,3,8,6,4,2]

"""

def find_all_possible_arrays_old(root):
	"""
	Ok, so we know that we need to do a preorder and a broken preorder
	for each node that has children. 
	"""
	queue = []
	queue.append(root)
	output_arrays = []
	level = 0
	while len(queue) > 0:
		current_node = queue.pop(0)

		left_out, right_out = [], []
		preorder(current_node, left_out)
		if len(left_out) > 0 and current_node.left:
			output_arrays.append(left_out)
			current_node.left
		reverse_preorder(current_node, right_out)
		if len(right_out) > 0 and current_node.right:
			output_arrays.append(right_out)
			queue.append(current_node.right)
		print(output_arrays)
		print("----")
#		if current_node.left:
#			queue.append(current_node.left)
#		if current_node.right:
#			queue.append(current_node.right)
		level += 1
	return output_arrays


class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

def preorder(root, acc):
#	new_acc = []
	if root is not None:
		acc.append(root.value)
	if root.left:
		preorder(root.left, acc)
	if root.right:
		preorder(root.right, acc)

def reverse_preorder(root, acc):
	if root is not None:
		acc.append(root.value)
	if root.right:
		reverse_preorder(root.right, acc)
	if root.left:
		reverse_preorder(root.left, acc)


def preorder_switched(root, acc, left_first=True):

	if root is not None:
		acc.append(root.value)
	if left_first:
		if root.left:
			preorder_switched(root.left,acc, left_first)
		if root.right:
			preorder_switched(root.right, acc,left_first)
	else:
		if root.right:
			preorder_switched(root.right, acc,left_first)
		if root.left:
			preorder_switched(root.left, acc,left_first)




def find_all_possible_arrays(root):
	"""
	OK, I was pretty close.
	What we need to do here is pass in a prefix
	"""



if __name__ == "__main__":
	bottom_left = TreeNode(1)
	bottom_right = TreeNode(3)
	root = TreeNode(2, bottom_left, bottom_right)
	res = find_all_possible_arrays(root)
	print(res)
#	assert(res == [[2,1,3],[2,3,1]])
#
#
	bottom_left_left = TreeNode(2)
	bottom_left_right = TreeNode(4)
	mid_left = TreeNode(3, bottom_left_left, bottom_left_right)

	bottom_right_left = TreeNode(6)
	bottom_right_right = TreeNode(8)
	mid_right = TreeNode(7, bottom_right_left, bottom_right_right)
	root = TreeNode(5, mid_left, mid_right)
	print(find_all_possible_arrays(root))
