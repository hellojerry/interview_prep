"""
Given a sorted (increasing order) array with unique integer elements,
write an algorithm to create a binary search tree with minimal height.
"""


class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		self.value = value
		if left is not None:
			assert(type(left) == TreeNode)
		self.left = left
		if right is not None:
			assert(type(right) == TreeNode)
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

class BinarySearchTree(object):


	
	def insert_value(self, arr):
		"""
		The trick with this is grabbing the midpoint,
		generating a node for it, and then
		doing a preorder insertion.
		"""
		if not arr:
			return None
		arrlen = len(arr)
		midpoint = int(arrlen / 2)
		root = TreeNode(arr[midpoint])
		root.left = self.insert_value(arr[:midpoint])
		root.right = self.insert_value(arr[(midpoint+1):])
		return root


	def inorder(self, root, acc):
		if not root:
			return None
		self.inorder(root.left,acc)
		acc.append(root)
		self.inorder(root.right,acc)

	def preorder(self, root,acc):

		if not root:
			return None
		print(root)
		acc.append(root)
		self.preorder(root.left,acc)
		self.preorder(root.right,acc)
		return


if __name__ == "__main__":
	sorted_integers = list(range(1,8))
	root_value = BinarySearchTree().insert_value(sorted_integers)
	acc = []
	BinarySearchTree().preorder(root_value,acc)
	print(acc)
	acc_as_answer = [item.value for item in acc]
	assert(acc_as_answer == [4,2,1,3,6,5,7])
