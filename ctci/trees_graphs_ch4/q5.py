"""
Implement a function to determine if a binary tree is a binary search tree.

The definition of binary search tree is that all values left of the parent
must be less than or equal to the parent, and all values right of the parent
must be greater than the parent.

BST

		 5
	       /    \
	      3	     7
	     / \    /  \
            2   4  6    8

Non-BST

		5
	      /    \
	     3	     7
	    / \    /  \
	   2   6  4    8
"""
import sys

class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		self.value = value
		if left is not None:
			assert(type(left) == TreeNode)
		self.left = left
		if right is not None:
			assert(type(right) == TreeNode)
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

def is_bst(root,minimum, maximum):
	"""
	Ok, so for a tree to be a BST, all lefthand values
	must be LTE the parent, all righthand must be GT parent.

	I think our easiest route with this is a DFS, with the caveat
	that we need to keep the grandparent factored in.
	"""

	if not root:
		return True

	if root.value < minimum or root.value > maximum:
		return False

	return is_bst(root.left,minimum, root.value) and is_bst(root.right, root.value, maximum)

if __name__ == "__main__":

	bstl1 = TreeNode(2)
	bstl2 = TreeNode(4)
	bstml = TreeNode(3, bstl1,bstl2)

	bstr1 = TreeNode(6)
	bstr2 = TreeNode(8)
	bstmr = TreeNode(7,bstr1, bstr2)
	bst_root = TreeNode(5, bstml, bstmr)
	## float("-inf") and float("inf") 
	## are minimum and maximum numbers for the interpreter.
	assert(is_bst(bst_root, float("-inf"), float("inf")) == True)

	non_bstl1 = TreeNode(2)
	non_bstl2 = TreeNode(6)
	non_bstml = TreeNode(3, non_bstl1,non_bstl2)

	non_bstr1 = TreeNode(4)
	non_bstr2 = TreeNode(8)
	non_bstmr = TreeNode(7,non_bstr1, non_bstr2)

	non_bst_root = TreeNode(5, non_bstml, non_bstmr)

	assert(is_bst(non_bst_root, float("-inf"), float("inf")) == False)
