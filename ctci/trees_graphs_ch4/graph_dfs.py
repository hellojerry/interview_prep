"""

The first example is for a DFS
of a graph. DFS uses recursion.

You can either keep track of the "visited"
attribute on the node object itself,
implemented via the "search_1" method,

or you can keep a prepopulated array and pass it recursively
until all values are reached
"""

class GraphNode(object):


	def __init__(self, name, children):
		assert(type(name) == str)
		assert(type(children) == list)
		if len(children) > 0:
			for child in children:
				assert(type(child) == GraphNode)
		self.children = children
		self.name = name
		self.visited = False


	def __str__(self):
		return "GraphNode(%s)" % self.name

	def __repr__(self):
		return "GraphNode(%s)" % self.name



class Graph(object):


	def __init__(self, nodes):
		for node in nodes:
			assert(type(node) == GraphNode)
		self.nodes = nodes


	def search_1(self, root):
		print(root)
		if not root:
			return
		root.visited = True
		for node in root.children:
			if node.visited == False:
				self.search_1(node)



	def call_dfs(self, v):

		visited = [False] * len(self.graph)
		self.search(v, visited)

if __name__ == "__main__":
	n3 = GraphNode("3",[])
	n2 = GraphNode("2",[n3])
	n1 = GraphNode("1",[n2])
	n3.children = [n1]

	my_graph = Graph([n1,n2,n3])
	my_graph.search_1(n1)
