"""
Design an algorithm and write code to find the first common ancestor of two nodes
in a binary tree. Avoid storing additional nodes in a data structure. 
Note that this isn't necessarily a BST.

Example:

		   1
		/     \
	       2       3
              /  \    / \
             4    5  7   6

input: [Node(2) Node(7)]
output: Node(1) 

input: Node(4), Node(5)
output: Node(2)

input: Node(2), Node(5)
output: Node(2)
"""



class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

def contains(root, test_node):
	if root is None:
		return False
	if root is test_node:
		return True
	return contains(root.left, test_node) or contains(root.right, test_node)

def find_common_ancestor(root, node_1, node_2):
	"""
	So this one is actually a DFS of sorts.

	You basically want to check if the nodes are both
	on the left, both on the right, or one for each.
	If it's one for each, the root value is the common ancestor.
	"""
	if root is None or root is node_1 or root is node_2:
		## This means that we've got a match.
		return root

	first_is_left = contains(root.left, node_1)
	second_is_left = contains(root.left, node_2)

	if first_is_left != second_is_left:
		## this neans we've found the ancestor.
		return root
	if first_is_left:
		return find_common_ancestor(root.left, node_1, node_2)
	return find_common_ancestor(root.right, node_1, node_2)



if __name__ == "__main__":
	left_left_bottom = TreeNode(4)
	left_right_bottom = TreeNode(5)
	left_mid = TreeNode(2,left_left_bottom,left_right_bottom)
	right_left_bottom = TreeNode(7)
	right_right_bottom = TreeNode(6)
	right_mid = TreeNode(3, right_left_bottom, right_right_bottom)
	root = TreeNode(1, left_mid, right_mid)

	out_1 = find_common_ancestor(root, left_mid, right_left_bottom)
	assert(out_1 is root)
	out_2 = find_common_ancestor(root, left_left_bottom, left_right_bottom)
	assert(out_2 is left_mid)
	out_3 = find_common_ancestor(root, left_mid, left_right_bottom)
	assert(out_3 is left_mid)
