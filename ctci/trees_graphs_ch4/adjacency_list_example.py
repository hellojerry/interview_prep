
"""
Adjacency List can be represented as the Graph and GraphNode
pairings below. It can also be represented as a hashmap.

"""

import time

class GraphNode(object):


	def __init__(self, name, children):
		assert(type(name) == str)
		assert(type(children) == list)
		if len(children) > 0:
			for child in children:
				assert(type(child) == GraphNode)
		self.children = children
		self.name = name


	def __str__(self):
		return "GraphNode(%s)" % self.name

	def __repr__(self):
		return "GraphNode(%s)" % self.name

class Graph(object):


	def __init__(self, nodes):
		for node in nodes:
			assert(type(node) == GraphNode)
		self.nodes = nodes


if __name__ == "__main__":

	n3 = GraphNode("3",[])
	n2 = GraphNode("2",[n3])
	n1 = GraphNode("1",[n2])
	n3.children = [n1]

	my_graph = Graph([n1,n2,n3])
	next_ = my_graph.nodes[0]
	"""
	This is an example of a cyclic graph.
	""" 
	while next_:
		print(next_)
		next_ = next_.children[0]
		time.sleep(1)
	## an adjacency list can also be represented with a dictionary.

	my_adjacency_hashmap = {
		"1": ["2"],
		"2": ["3"],
		"3": ["1"]
	}
