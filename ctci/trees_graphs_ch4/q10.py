"""
T1 and T2 are two very large binary trees, with T1 much bigger than T2.
Create an algorithm to determine if T2 is a subtree of T2.

A tree T2 is a subtree of T1 if there exists a node N in T1 such that the subtree of N is 
identical to T2. That is, if you cut off the tree at node N, the two trees are identical.


Tree 1:

			     5
			/         \
		      7            9
                    /   \         /  \
                  10    12       13   14
                 / \    / \     / \   /  \
               15   16 17  18  19 20 21  22


Tree 2:


                  9
                /    \
               13      14
              /  \     / \
             19   20   21  22

(should return true)

Tree 3:


                   9
                 /   \
               13     14
              /       / \
            19       21  22
                 
              
Should return False
"""


class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		assert(type(value) == int)
		for item in [left, right]:
			if item is not None:
				assert(type(item) == TreeNode)
		self.value = value
		self.left = left
		self.right = right


	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value

def two_trees_match(t1, t2):
	t1_queue, t2_queue = [],[]
#	if t1 is None and t2 is None:
#		return True
#	if t1 is not None and t2 is None:
#		return False
#	if t1 is None and t2 is not None:
#		return False
	t1_is_none = t1 is None
	t2_is_none = t2 is None
	if t1_is_none != t2_is_none:
		return False

	t1_queue.append(t1)
	t2_queue.append(t2)
	while len(t1_queue) > 0 and len(t2_queue) > 0:
		t1_current = t1_queue.pop(0)
		t2_current = t2_queue.pop(0)
		if t1_current.value != t2_current.value:
			return False
		## we don't need all of the left and right checks.
		## if the two queues don't match at any point in time,
		## they're not the same tree.
		if len(t1_queue) != len(t2_queue):
			return False
#		if t1_current.left and not t2_current.left:
#			return False
#		if not t1_current.left and t2_current.left:
#			return False
#		if t1_current.right and not t2_current.right:
#			return False
#		if not t1_current.right and t2_current.right:
#			return False
		if t1_current.left:
			t1_queue.append(t1_current.left)
		if t2_current.left:
			t2_queue.append(t2_current.left)
		if t1_current.right:
			t1_queue.append(t1_current.right)
		if t2_current.right:
			t2_queue.append(t2_current.right)
	if len(t1_queue) != len(t2_queue):
		return False
	return True


def is_subtree(t1, t2):
	"""
	OK, the basic strategy of this is to 
	do a BFS or a DFS of the left tree. The first time
	we get a matching node of t1 with the root of t2,
	we call a separate function that matches the two trees 
	together. 
	"""
	t1_queue = []
	if t1 is None and t2 is None:
		return True
	if (t1 is None and t2 is not None) or (t1 is not None and t2 is None):
		return False
	t1_queue.append(t1)
	while len(t1_queue) > 0:
		t1_current = t1_queue.pop(0)
		if t1_current.value == t2.value:
			if two_trees_match(t1_current, t2):
				return True
		if t1_current.left:
			t1_queue.append(t1_current.left)
		if t1_current.right:
			t1_queue.append(t1_current.right)

	return False

if __name__ == "__main__":
	row_4_1 = TreeNode(15)
	row_4_2 = TreeNode(16)
	row_3_1 = TreeNode(10,row_4_1,row_4_2)
	row_4_3 = TreeNode(17)
	row_4_4 = TreeNode(18)
	row_3_2 = TreeNode(12, row_4_3,row_4_4)
	row_4_5 = TreeNode(19)
	row_4_6 = TreeNode(20)
	row_3_3 = TreeNode(13, row_4_5, row_4_6)
	row_4_7 = TreeNode(21)
	row_4_8 = TreeNode(22)
	row_3_4 = TreeNode(14, row_4_7, row_4_8)
	row_2_1 = TreeNode(7, row_3_1, row_3_2)
	row_2_2 = TreeNode(9, row_3_3, row_3_4)
	t1_root = TreeNode(5, row_2_1, row_2_2)


	bot_left_left = TreeNode(19)
	bot_left_right = TreeNode(20)
	mid_left = TreeNode(13, bot_left_left, bot_left_right)
	bot_right_left = TreeNode(21)
	bot_right_right = TreeNode(22)
	mid_right = TreeNode(14, bot_right_left, bot_right_right)
	t2_root = TreeNode(9, mid_left, mid_right) 

	res = is_subtree(t1_root, t2_root)
	assert(res == True)


	bad_left_left = TreeNode(19)
	bad_mid_left = TreeNode(13, bad_left_left)
	bad_right_left = TreeNode(21)
	bad_right_right = TreeNode(22)
	bad_mid_right = TreeNode(14, bad_right_left, bad_right_right)
	t3_root = TreeNode(9, bad_mid_left, bad_mid_right)

	res2 = is_subtree(t1_root, t3_root)
	assert(res2 == False)
