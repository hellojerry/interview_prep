"""
Implement a function to check if a binary tree is balanced.
For the purposes of this question, a balanced binary tree is defined
as a tree where the heights of the two subtrees of any node
never differ by more than one.
"""



class TreeNode(object):

	def __init__(self, value, left=None, right=None):
		self.value = value
		if left is not None:
			assert(type(left) == TreeNode)
		self.left = left
		if right is not None:
			assert(type(right) == TreeNode)
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.value

	def __repr__(self):
		return "TreeNode(%d)" % self.value


def get_height(root):

	if not root:
		return 0

	return max(get_height(root.left), get_height(root.right)) + 1
	


def is_balanced(root):
	if not root:
		return True


	left_hand_height = get_height(root.left)
	right_hand_height = get_height(root.right)

	if abs(left_hand_height - right_hand_height) <= 1 and \
		is_balanced(root.left) and is_balanced(root.right):
		return True

	return False
	


if __name__ == "__main__":
	"""
	Height Balanced Binary Tree:
		   0
		 /   \
		1     2
	       / 
              3   

	Height Unbalanced Binary Tree:
		      0
		     /  \
		    1    2
		   /
		  3
		 /
		4
	"""

	balanced_left_bottom = TreeNode(3)
	balanced_left_mid = TreeNode(1, balanced_left_bottom)
	balanced_right_mid = TreeNode(2)
	balanced_root = TreeNode(balanced_left_mid,balanced_right_mid)
	is_balanced(balanced_root)
