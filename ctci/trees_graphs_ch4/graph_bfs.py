"""
Graph BFS uses an iterative approach with a queue.

Remember that in a BFS, each neighboar is visited before visiting the neighbor's
neighbors.

"""


class GraphNode(object):


	def __init__(self, name, children):
		assert(type(name) == str)
		assert(type(children) == list)
		if len(children) > 0:
			for child in children:
				assert(type(child) == GraphNode)
		self.children = children
		self.name = name
		self.visited = False


	def __str__(self):
		return "GraphNode(%s)" % self.name

	def __repr__(self):
		return "GraphNode(%s)" % self.name


def breadth_first_search(root):
	queue = []
	root.visited = True
	queue.append(root)
	while len(queue) > 0:
		node = queue.pop(0)
		print(node)
		for n in node.children:
			if n.visited == False:
				n.visited = True
				queue.append(n)
				

if __name__ == "__main__":
	n3 = GraphNode("3",[])
	n2 = GraphNode("2",[n3])
	n1 = GraphNode("1",[n2])
	n3.children = [n1]
	breadth_first_search(n1)
