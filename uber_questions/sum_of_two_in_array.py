"""
Complete the number of pairs function. it has two parameters:

array of integers (A)
a long integer K

it must return the count of the total number of DISTINCT pairs.

For the purposes of this question,

(1,2) and (2,1) do not count as distinct.
Additionally, two occurrences of (2,1) do not count as distinct.
"""



def two_numbers_in_array(arr, long_integer):
	"""
	I see two ways of doing this.
	First way:
	1. sort the array.
	compare first value to end value.
	beg + end must equal the long.
	if they do, memoize them both,
	increment by one, and pop from the beginning and end.
	for second one, check if left is memoized, if true,
	continue left forward, and check if right is memoized.
	continue until array exhausted, incrementing up when new pairs are found.

	This would automatically dedupe things.

	The other thing to do would be to take the first number, subtract it from the
	desired result, and scan the array for the desired result along with the original
	number. if the desired result is found, incrememnt distinct by one, and then
	remove all instances of the desired and initial values from the array.
	this would be tricky to find the complexity for. My guess is that having lots of pairs
	would allow this to be faster than the first one, but the scenario where there 
	are lots of duds might outweigh it.

	"""

	## nlogn or o(n) - python uses a timsort
	sorted_arr = sorted(arr)
	arrlen = len(arr)

	memoized_left, memoized_right = None, None
	left_ptr, right_ptr = 0, arrlen - 1
	distinct_total_pairs = 0

	while left_ptr < right_ptr:
		left_val, right_val = sorted_arr[left_ptr], sorted_arr[right_ptr]
		if left_val == memoized_left:
			while left_val == memoized_left:
				left_ptr += 1
				left_val = sorted_arr[left_ptr]
				if left_ptr == right_ptr:
					return distinct_total_pairs
		if right_val == memoized_right:
			while right_val == memoized_right:
				right_ptr -= 1
				right_val = sorted_arr[right_ptr]
				if right_ptr == left_ptr:
					return distinct_total_pairs
		if left_val + right_val == long_integer:
			distinct_total_pairs += 1
			memoized_left, memoized_right = left_val, right_val
			left_ptr += 1
			right_ptr -= 1
		else:
			left_ptr += 1

	return distinct_total_pairs


if __name__ == "__main__":
			
	res1 = two_numbers_in_array([1,3,46,1,3,9],47)
	print(res1)
	res2 = two_numbers_in_array([6,6,3,9,3,5,1],12)
	print(res2)	
