
"""

Get the bitwise complement of a number.

5 -> 101

bitwise complement of 5 is 010, aka 2
"""


def get_bitwise_complement(num):
	print("initial value: %d, %s" % (num, bin(num)))
	c = 1
	while num*2 > c:
		num ^= c
		print(num, bin(num))
		c <<= 1
		print(c, bin(c))
	print("result: %d, %s" % (num, bin(num)))
	print("-----------")
	return num


if __name__ == "__main__":
	get_bitwise_complement(5)
	get_bitwise_complement(10)
