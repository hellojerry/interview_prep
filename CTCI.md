# Plan of Attack - Coding Interview

Overall, we need to figure out what we'll actually get asked.

I imagine most of this shit will come out of cracking the coding interview.

Looking through this book, there are a few chapters that aren't necessary to study.

Let's try and get four hours of studying in per day, with another two hours of flashcards where applicable.

After finishing the mediums, grind 50-60 leetcode problems. Grind specifically the weakest areas.

Might be best to go through firecode again.

## concepts to understand

### data structures
linked lists
trees
tries
graphs
stacks/queues
heaps
dynamic arrays
hashtables

### algorithms

BFS
DFS
binary search
merge sort
quick sort

### other shit

bit manipulation
recursion
dynamic programming



## arrays and strings

9 questions

### Weaknesses

- matrix rotations. Make flashcards for the different types of rotations. There's 45 degrees clockwise, 90 degrees clockwise, counterclockwise, and then flipping them.

## linked lists

8 questions

less than a day

### Weaknesses

none, just need to put some tricks into flashcards.


## stacks and queues

6 questions

less than a day

Skipped q1. Nonsensical question.

## trees and graphs
12 questions

1 day


1. be able to get max path with no help
2. do BFS with no help (btree)
3. do BFS with no help (graph)
4. do DFS with no help (btree)
5. do DFS with no help (graph)
5. determine if btree is height-balanced:

```python3

def get_height(root):
	if not root:
		return 0

	return max(get_height(root.left), get_height(root.right)) + 1

def is_balanced(root):
	if not root:
		return True

	lhs = get_height(root.left)
	rhs = get_height(root.right)

	if abs(lhs-rhs) <= 1 and is_balanced(root.left) and is_balanced(root.right):
		return True

```

- get bigger flashcards
- memorize the "generate arrays from btree" question (q9r2.py)

- best bet here is to cut printer paper into quarters and do flashcards that way.

### stuff to review

- memorize flashcards
- extracting minimum element from min-heap
- inserting value into min-heap
- put all questions into large flashcards

## bit manipulation

8 questions

less than a day

## recursion and dynamic programming

14 questions

2 days

## sorting and searching

11 questions

1 day

## math puzzles

10 questions

### mediums

26

### hards

26


# Weaknesses After CTCI


1. recursion and dynamic programming (need a lot of practice for this)
3. bit manipulation
4. matrix manipulation

# Middling

1. trees (don't need as much practice, they're all recursion-dp problems)
2. sorting and searching

# Strengths

1. linked lists
2. string and array manipulation

