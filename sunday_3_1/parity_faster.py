

def parity_faster(num):
	num ^= num >> 32
	num ^= num >> 16
	num ^= num >> 8
	num ^= num >> 4
	num ^= num >> 2
	num ^= num >> 1
	return num & 0x1

print(parity_faster(1))
print(parity_faster(4))
print(parity_faster(3))
print(bin(3))
