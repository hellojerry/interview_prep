"""
reverse bits of an integer.
"""

def reverse_bits(num):
	reverse = 0
	while num:
		reverse <<= 1
		if num & 1:
			reverse += 1

		num >>= 1
	return reverse
