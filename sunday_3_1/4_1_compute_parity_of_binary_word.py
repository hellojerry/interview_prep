"""
The parity of a binary word is 1 if the number of 1s in the word is odd.
"""

def count_1_bits(num):
	ct = 0
	while num:
		if num & 1:
			ct += 1
		num >>= 1
	if ct % 2 == 0:
		return 0
	return 1

if __name__ == "__main__":
	print(bin(4))
	print(count_1_bits(4))
	print(count_1_bits(0))
	print(bin(7))
	print(count_1_bits(7))
