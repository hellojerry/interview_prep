"""
implement a program that takes a 64 bit input
and swaps the bits at indices I and J

initial: 

	01001001 I = 1, J = 6

	00001011 => res

binary has most significant bit on the left!
"""

def swap_bits(num,I,J):
	"""
	1. extract the Ith and Jth bits, and see if they differ.
	if they don't, return.
	2. if they do differ, make a bitmask of 1 shifted left I
	and 1 shifted left J, and XOR the number against the bitmask 
	"""
	Ith = num >> I
	Jth = num >> J
	if Ith == Jth:
		return num
	mask = (1 << I) | (1 << J)
	return num ^ mask

	


if __name__ == "__main__":
	assert(swap_bits(25,0,2) == 28)
