"""
Given an MxN matrix of non-negative
integers representing the height
of each cell in a continent,
the pacific ocean touches the left
and top edges of the matrix
and the atlantic touches the bottom
and right edges.
Water can only flow in four directions (up,
down, left, and right) and from a cell
to another with equal height or lower.
find a list of coordinates on the grid
where one could reach the pacific (top or left)
AND the atlantic (bottom and/or right)

ex:

arr = [
	[  1,  2,  2,  3,(5)],
	[  3,  2,  3,(4),(4)],
	[  2,  4,(5),  3,  1],
	[(6),(7),  1,  4,  5],
	[(5),  1,  1,  2,  4]
]

res = [
	[0,4],[1,3],[1,4],[2,2],[3,0],
	[3,1],[4,0]

"""
UNVISITED = 0
VISITED = 1
UNTOUCHED = -1
NEITHER = 0
PACIFIC = 1
ATLANTIC = 2
BOTH = 3
import copy
def recurse(matrix,row,col,visited):
	## I'm wondering if i need the adjacenc list at all
	max_row,max_col = len(matrix)-1,len(matrix[0])-1
	if visited[row][col] == VISITED:
		## this may be incorrect.
		return (False,False)
	pacific = row == 0 or col == 0
	atlantic = row == max_row or col == max_col 
	if pacific == True and atlantic == True:
		return (pacific,atlantic)
	cur = matrix[row][col]
	visited[row][col] = VISITED
	if pacific and not atlantic:
		## can only go right or down
		right_eligible = matrix[row][col+1] <= cur
		down_eligible = matrix[row+1][col] <= cur
		if right_eligible:
			v_copy = copy.deepcopy(visited)
			throw,atlantic = recurse(
					matrix,row,col+1,
					v_copy)
			if atlantic:
				return (pacific,atlantic)
		if down_eligible:
			v_copy = copy.deepcopy(visited)
			throw,atlantic = recurse(matrix,
					row+1,col,v_copy)
			if atlantic:
				return (pacific,atlantic)
		return (pacific,atlantic)
	if not pacific and atlantic:
		## can only go left or up
		left_eligible = matrix[row][col-1] <= cur
		up_eligible = matrix[row-1][col] <= cur
		if left_eligible:
			v_copy = copy.deepcopy(visited)
			pacific,throw = recurse(
				matrix,row,col-1,v_copy)
			if pacific:
				return (pacific,atlantic)
		if up_eligible:
			v_copy = copy.deepcopy(visited)
			pacific,throw = recurse(
				matrix,row-1,col,v_copy)
			if pacific:
				return (pacific,atlantic)
		return (pacific,atlantic)
	left_eligible = matrix[row][col-1] <= cur
	up_eligible = matrix[row-1][col] <= cur
	right_eligible = matrix[row][col+1] <= cur
	down_eligible = matrix[row+1][col] <= cur
	a_found,p_found = False,False
	for grp in [(left_eligible,row,col-1),
		(up_eligible,row-1,col),
		(right_eligible,row,col+1),
		(down_eligible,row+1,col)]:
		if grp[0]:
			v_copy = copy.deepcopy(visited)
			p,a = recurse(matrix,grp[1],grp[2],v_copy)
			if a:
				a_found = True
			if p:
				p_found = True
		if a_found and p_found:
			return (True,True)
	return (p_found,a_found)

def recurse(matrix,row,col,visited,cache):
	"""
	The cache works as follows:
	-1: untouched
	0: no connection, pacific or atlantic
	1: pacific connection
	2: atlantic connection
	3: pacific and atlantic connection
	naive loop: 44 cache, 106 no cache.
	"""
	## first, short-circuit
	f = open("tail.txt","a")
	if visited[row][col] == VISITED:
		f.write("cache\n")
		if cache[row][col] == BOTH:
			return (True,True)
		if cache[row][col] == NEITHER:
			return (False,False)
		if cache[row][col] == PACIFIC:
			return (True,False)
		if cache[row][col] == ATLANTIC:
			return (False,True)
		return (False,False)
	else:
		f.write("no_cache\n")
#		print("no cache")
	f.close()
	max_row,max_col = len(matrix)-1,len(matrix[0])-1
	pacific = row == 0 or col == 0
	atlantic = row == max_row or col == max_col
	visited[row][col] = VISITED
	if pacific == True and atlantic == True:
		cache[row][col] = BOTH
		return (True,True)
	cur = matrix[row][col]
	left_eligible = 0 < col and matrix[row][col-1] <= cur
	right_eligible = col < max_col and matrix[row][col+1] <= cur
	up_eligible = 0 < row and matrix[row-1][col] <= cur
	down_eligible = row < max_row and matrix[row+1][col] <= cur
	for grp in [(left_eligible,row,col-1),
			(right_eligible,row,col+1),
			(up_eligible,row-1,col),
			(down_eligible,row+1,col)]:
		if grp[0]:
			v_copy = visited
			p,a = recurse(matrix,grp[1],grp[2],
				v_copy,cache)
			if p:
				pacific = True
			if a:
				atlantic = True
		if pacific and atlantic:
			cache[row][col] = BOTH
			return (True,True)
	if pacific and not atlantic:
		cache[row][col] = PACIFIC
	elif not pacific and atlantic:
		cache[row][col] = ATLANTIC
	elif not pacific and not atlantic:
		cache[row][col] = NEITHER
	return (pacific,atlantic)

#def recurse(matrix,row,col,cache):
#	"""
#	The cache works as follows:
#	-1: untouched
#	0: no connection, pacific or atlantic
#	1: pacific connection
#	2: atlantic connection
#	3: pacific and atlantic connection
#	"""
#	## first, short-circuit
#	if cache[row][col] != UNTOUCHED:
#		if cache[row][col] == BOTH:
#			return (True,True)
#		if cache[row][col] == NEITHER:
#			return (False,False)
#		if cache[row][col] == PACIFIC:
#			return (True,False)
#		if cache[row][col] == ATLANTIC:
#			return (False,True)
#		return (False,False)
#	max_row,max_col = len(matrix)-1,len(matrix[0])-1
#	pacific = row == 0 or col == 0
#	atlantic = row == max_row or col == max_col
#	cache[row][col] = NEITHER
#	if pacific == True and atlantic == True:
#		cache[row][col] = BOTH
#		return (True,True)
#	cur = matrix[row][col]
#	left_eligible = 0 < col and matrix[row][col-1] <= cur
#	right_eligible = col < max_col and matrix[row][col+1] <= cur
#	up_eligible = 0 < row and matrix[row-1][col] <= cur
#	down_eligible = row < max_row and matrix[row+1][col] <= cur
#	for grp in [(left_eligible,row,col-1),
#			(right_eligible,row,col+1),
#			(up_eligible,row-1,col),
#			(down_eligible,row+1,col)]:
#		if grp[0]:
#			#v_copy = copy.deepcopy(visited)
#			p,a = recurse(matrix,grp[1],grp[2],
#				cache)
#			if p:
#				pacific = True
#			if a:
#				atlantic = True
#		if pacific and atlantic:
#			cache[row][col] = BOTH
#			return (True,True)
#	if pacific and not atlantic:
#		cache[row][col] = PACIFIC
#	elif not pacific and atlantic:
#		cache[row][col] = ATLANTIC
##	elif not pacific and not atlantic:
##		cache[row][col] = NEITHER
#	return (pacific,atlantic)


	

def waterflow(matrix):
	"""
	Ok, so some analysis:
	A successful coordinate pair can reach both
	(left and/or top) AND (right and/or bottom).
	"Can reach" means that there is a path 
	from the coordinate pair to the side 
	where all successive coordinate values
	are <= prior.
	If we do a DFS, we would scan each adjacent
	value, seeing if we've hit an edge (good),
	if an adjacent value is legal (good) or illegal
	(bad), and we need to ensure that we don't visit
	the same location more than once in a given path lookup.
	we'd need an adjacency list, and then a visited list.
	we'd need to copy the visited list for each recursive call.

	OK, so this solution works, but it's really sloppy.
	First step is refactoring the recurse function so it's more readable.
	Ok, so we've got the caching in place, but the time limit
	is still being hit. I think we've got to modify the cache
	to take the place of visited.
	"""
	if not matrix:
		return []
	rows = len(matrix)
	cols = len(matrix[0])
	max_row,max_col = rows-1,cols-1
	visited_canonical = [[UNVISITED]*(cols) for i in range(rows)]
	cache = [[-1]*(cols) for i in range(rows)]
	pairings = []
	## i could be a bit smarter with the cache.
	for row in range(rows):
		for col in range(cols):
			if col == max_col and row == 0:
				cache[row][col] = BOTH
			elif col == 0 and row == max_row:
				cache[row][col] = BOTH
			elif col == max_col or row == max_row:
				cache[row][col] = ATLANTIC
			elif col == 0 or row == 0:
				cache[row][col] = PACIFIC
	for row in range(rows):
		for col in range(cols):
			visited = copy.deepcopy(visited_canonical)
			a,p = recurse(matrix,row,col,visited,cache)
			visited_canonical[row][col] = VISITED
			if a == True and p == True:
				pairings.append((row,col))
	return pairings

def recurse(matrix,cache,row,col,prior):
	max_row,max_col = len(matrix)-1,len(matrix[0])-1
	if not (0 <= row <= max_row):
		return
	if not (0 <= col <= max_col):
		return
	if cache[row][col]:
		return
	if matrix[row][col] < prior:
		return
	cache[row][col] = True
	cur = matrix[row][col]
	recurse(matrix,cache,row+1,col,cur)
	recurse(matrix,cache,row-1,col,cur)
	recurse(matrix,cache,row,col-1,cur)
	recurse(matrix,cache,row,col+1,cur)

def waterflow(matrix):
	"""
	prior solution worked, but dumb complexity stuff.
	get a 2d array for atlantic and pacific. For each of them,
	do a DFS, iterating over the four directions, marking visited
	locations. Then, after all that, iterate over both matrixes
	and find where both are visited. 
	"""
	if not matrix: return []
	rows,cols = len(matrix),len(matrix[0])
	pacific = [[False]*cols for i in range(rows)]
	atlantic = [[False]*cols for i in range(rows)]
	for row in range(rows):
		recurse(matrix,pacific,row,0,float("-inf"))
		recurse(matrix,atlantic,row,cols-1,float("-inf"))
	for col in range(cols):
		recurse(matrix,pacific,0,col,float("-inf"))
		recurse(matrix,atlantic,rows-1,col,float("-inf"))
	out = []
	for row in range(rows):
		for col in range(cols):
			if pacific[row][col] and atlantic[row][col]:
				out.append((row,col))
	return out
if __name__ == "__main__":
	arr1 = [
		[  1,  2,  2,  3,5],
		[  3,  2,  3,4,4],
		[  2,  4,5,  3,  1],
		[6,7,  1,  4,  5],
		[5,  1,  1,  2,  4]
	]
	
	e1 = [
		(0,4),(1,3),(1,4),(2,2),(3,0),
		(3,1),(4,0)
	]
	r1 = waterflow(arr1)
	print(r1,r1  == e1)
	#a2 = []
	#pri
