"""
Given a string S and a string T, find
the minimum window in S which will contain
all the characters in T in complexity O(n)

Ex:
S1 = "ADOBECODEBANC"
T1 = "ABC"
e1 = "BANC"

if there is no such window in S that covers
all characters in T, return ""
if there is a window, there is only one window
"""
from collections import Counter, defaultdict

def minimum_window(S,T):
	"""
	The strategy with this is
	to grab the left pointer, then move
	the right pointer outwards until an adequate
	subset is found. Then, move lleft pointer
	forwards, rinse and repeat
	"""
	S_len = len(S)
	dict_t = dict(Counter(T))
	l,r,ct = 0, 0,0
	required = len(dict_t)
	window_counts = defaultdict(int)
	min_window, min_left,min_right = float("inf"),-1,-1
	while r < S_len:
		rval = S[r]
		window_counts[rval] += 1
		if rval in dict_t and window_counts[
			rval] == dict_t[rval]:
			ct += 1
		while l <= r and ct == required:
			lval = S[l]
			if (r - l + 1) < min_window:
				min_window = r-l + 1
				min_left = l
				min_right = r
			window_counts[lval] -= 1
			if lval in dict_t and window_counts[
				lval] < dict_t[lval]:
				ct -= 1
			l += 1
		r += 1
	if min_window == float("inf"):
		return ""
	return S[min_left:min_right+1]

if __name__ == "__main__":
	S1 = "ADOBECODEBANC"
	T1 = "ABC"
	e1 = "BANC"
	r1 =minimum_window(S1,T1)
	print(r1, r1 == e1) 
#	S2 = "ADOBECODEBANC"
#	T2 = "ZOBE"
#	e2 = ""
#	r2 = minimum_window(S2,T2)
#	print(r2, r2 == e2)
#	S3 = "ADOBECODEBANC"
#	T3 = "NAC"
#	e3 = "ANC"
#	r3 = minimum_window(S3,T3)
#	print(r3,r3 == e3)
#	S4 = "ADOBECODEBANC"
#	T4 = "ONAC"
#	e4 = "ODEBANC"
#	r4 = minimum_window(S4,T4)
#	print(r4, r4 == e4)
#	S5 = "A"
#	T5 = "A"
#	e5 = "A"
#	r5 = minimum_window(S5,T5)
#	print(r5, r5 == e5)
#	S6 = "AA"
#	T6 = "AA"
#	e6 = "AA"
#	r6 = minimum_window(S6,T6)
#	print(r6, r6 == e6)
#	S7 = "A"
#	T7 = "B"
#	e7 = ""
#	r7 = minimum_window(S7,T7)
#	print(r7)
#	S8 = "AB"
#	T8 = "A"
#	r8 = minimum_window(S8,T8)
#	print(r8 == "A")
#	S9 = "ODEBANC"
#	T9 = "ON"
#	R9 = minimum_window(S9,T9)
#	print(R9 == "ODEBAN")
#	S10 = "BBA"
#	T10 = "BA"
#	R10 = minimum_window(S10,T10)
#	print(R10,R10 == "BA")
#	S11 = "CABWEFGEWCWAEFGCF"
#	T11 = "CAE"
#	R11 = minimum_window(S11,T11)
#	print(R11,R11 == "CWAE")
