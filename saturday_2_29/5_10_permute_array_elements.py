"""
A permutation is a rearrangement of members
of a sequence into a new sequence.
There are 24 permutations of [a,b,c,d] ->
[b,a,c,d], [b,c,d,a], etc

For this problem, we'll define a permutation directive
as an array of integers representing indices. 
For example, P of [2,0,1,3] means
map element currently at location 0 to 2,
map element currently at location 1 to 0,
map element currently at location 2 to 1,
map element currently at location 3 to 3

Given an array of N elements and a permutation P,
apply P to A.

"""

def map_permutation_double_size(arr, permutation):
	"""
	One way to do this is to create an entirely 
	new array with all 0s, Then, iterate over the permutation,
	for ind, val in enumerate(permutation):
		new_array[val] = array[ind]
	"""
	arrlen = len(arr)
	new_array = [-1]*arrlen
	for ind, val in enumerate(permutation):
		new_array[val] = arr[ind]
	return new_array

def map_permutation_inplace(arr,permutation):
	"""
	How would we go about this in-place?
	if we iterate over the permutation, we only really have one straggler at
	a time to worry about
	"""
	arrlen = len(arr)
	for i in range(arrlen):
		next_ = i
		while permutation[next_] >= 0:
			arr[i], arr[permutation[next_]] = (arr[
					permutation[next_]], arr[i])
			temp = permutation[next_]
			permutation[next_] -= len(permutation)
			next_ = temp
		
			
	return arr


if __name__ == "__main__":
	A = ["A","B","C","D"]
	permutation = [2,0,1,3]
	expected = ["B","C","A","D"]
	res = map_permutation_inplace(A,permutation)
	print(res)
