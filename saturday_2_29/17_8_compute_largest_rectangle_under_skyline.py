"""
You are given a sequence of adjacent buildings.
Each has a unit width and an integer height.
These form the skyline.
An architect wants to know the largest rectangle contained
in this skyline.
let A be an array representing the heights of adjacent buildings
of unit width.

Compute the largest area rectangle in the skyline.

arr = [ (2,10),(4,5),(9,4),(14,3),(19,4)]






"""


def find_largest_area(arr):
	"""
	So this is a greedy algorithm
	We start from the far left and the far right.
	Hmm. We could 
	"""
	pillar_indices, max_area = [],0
	for index, h in enumerate(arr + [0]):
		while pillar_indices and heights[pillar_indices[-1]] >= h:
			height = heights[pillar_indices.pop()]
			width = i if not pillar_indices else i - pillar_indices[-1] - 1
			max_area = max(max_area, height(width)
		pillar_indices.append(i)
	return max_area
