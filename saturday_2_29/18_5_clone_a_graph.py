"""
Consider a vertex type for a directed graph 
in which there are two fields:

1) an integer label 
2) a list of references to other vertices

Design an algorithm that takes a reference to a vertex
U and creates a copy of the graph on the vertices reachable
from U.
"""


def clone_graph(initial,visited,element,new):
	"""
	Every other solution did shit with 
	a queue, this wasn't too complicated.
	"""
	if visited[element] == True:
		return new
	new[element] = []
	visited[element] = True
	for conn in initial[element]:
		new[element].append(conn)
		clone_graph(initial,visited,conn,new)
	return new

if __name__ == "__main__":
	"""
	The tricky thing here is that I don't 
	know what the output is supposed to be.
	"""

	graph = {
		"A": ["D","B"],
		"B": ["E","D"],
		"C": ["A","B"],
		"D": ["C","B"],
		"E": ["D","A"],
	}
	visited = {
		"A": False,
		"B": False,
		"C": False,
		"D": False,
		"E": False
	}
	new_graph = {}
	out = clone_graph(graph,visited,"A",new_graph)
	assert(out == graph)
	graph = {
		"A": ["D","B"],
		"B": ["E","D"],
		"C": ["A","B"],
		"D": ["C","B"],
		"E": ["D"],
	}
	visited = {
		"A": False,
		"B": False,
		"C": False,
		"D": False,
		"E": False
	}
	out = clone_graph(graph,visited,"A",new_graph)
	assert(out == graph)
	graph = {
		"A": ["D","B"],
		"B": ["E","D"],
		"C": ["A","B"],
		"D": ["C","B"],
		"E": ["D","A","C"],
	}
	visited = {
		"A": False,
		"B": False,
		"C": False,
		"D": False,
		"E": False
	}
	out = clone_graph(graph,visited,"A",new_graph)
	assert(out == graph)
	graph = {
		"A": ["D","B","E"],
		"B": ["E","D"],
		"C": ["A","B"],
		"D": ["C","B"],
		"E": ["D","A","C"],
	}
	visited = {
		"A": False,
		"B": False,
		"C": False,
		"D": False,
		"E": False
	}
	out = clone_graph(graph,visited,"A",new_graph)
	assert(out == graph)
