"""
an N-bit Gray code is a permutation of [0,1,2,...,(2**N)-1] such
that the binary representations of successive integers in the sequence differ
in only one place. (This is with wraparound, i.e. the first and 
last elements must also differ in only one place).

Ex:
	binary: ( 000,100,101,111,110,010,011,001)
	decimal: 0,4,5,7,6,2,3,1

	binary: (000,001,011,010,110,111,101,100)
	decimal: 0,1,3,2,6,7,5,4 
are both gray codes for N = 3

Write out a program with takes N as input and returns
an N-bit Gray code.

Hint:
	write out gray codes for 2,3 and 4

	gray code 2:
	
"""


def gray_code(N):
	"""
	So this is in the recursive chapter.
	We need a length of 2**N, and a final index
	of 2**N-1.So the length for N=3
	would be 8,with only 3 bit integers,
	the length for N=2 would be 4,
	the length of N=4 would be 16.

	Some notes:
		- the minimum N-bit integer is always zero.
		- the maximum N-bit integer is all entirely ones,
		and it's equal to 2**N-1
	If we start by generating the gray code for 1,
	that's simply [0] or [1].
	if it's a gray code for 2,
	00 01 11 10
	so we make the tail = 1+ prior tail,
	then 

	Apparently gray codes have unique entries.
	This should make it a bit easier.
	From there, we can shift the far right element
	left by one bit to make a new element, then subtract
	one, then add one, right?

	Now how do we figure out where to start and end this?
	If we count down from N, we can take the prior generated
	gray code, double the size required, make a set
	from the range of 0,size, subtract existing elements
	from the set, then do arr[-1] shift left, + 1, and -1
	and append the first legal change to the output
	(removing from candidates) until we've exhausted the
	candidate list, then call recursively with N-1
	"""
	if N == 0:
		return [0]
	gray_code_num_bits_minus_1 = gray_code(N-1)
	leading_bit_one = 1 << (N-1)
	return gray_code_num_bits_minus_1 + [
		leading_bit_one | i for i in reversed(
		gray_code_num_bits_minus_1)]
if __name__ == "__main__":
	N = 2
	res = gray_code(N)
	print(res)
	print([bin(i) for i in res])
	N = 3
	res = gray_code(N)
	print([bin(i) for i in res])
