"""
For this problem, assume that each binary tree node has
an extra field called level_next, which holds a binary tree
node distinct from the fields for left and right children. 
The level_next field will be used to compute a map from nodes to their right
siblings. The input is a perfect binary tree.

				A
		   B                    I
               C      F              J     M
            D   E   G   H          K  L   N   O

Write a program that takes a perfect binary tree and sets each node's
level-next field to the node on its right, if the right node exists.

This means D's level_next field would be E, E's would be G, G's would be H,
C's would be F, F's is J, J's is M, O's is None, and A's is None

Hint: think of an appropriate traversal order.
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == str)
		self.val = val
		self.left = left
		self.right = right
		self.level_next = None
	def __str__(self):
		return "TreeNode(%s), level_next: %s" % (self.val, str(self.level_next))
	def __repr__(self):
		return "TreeNode(%s), level_next: %s" % (self.val, str(self.level_next))

def set_right_siblings(root):
	"""
	This appears to be a way to make a b+tree.
	if I do an inorder traversal, I set left child's level
	next = right child. This won't work once we've
	gotten to grandchildren
	If I do postorder, hmm.
	If i do a BFS to generate an array with levels,
	I can just work back through the array and set things
	based on the level.
	"""

	current_level = 0
	queue = [(root,current_level)]
	levels = []
	while len(queue) > 0:
		node, current_level = queue.pop(0)
		levels.append((node,current_level))
		current_level += 1
		if node.left:
			queue.append((node.left,current_level))
		if node.right:
			queue.append((node.right,current_level))
	remaining = len(levels) - 1
	while remaining > 0:
		current = levels[remaining]
		prior = levels[remaining-1]
		if current[1] == prior[1]:
			prior[0].level_next = current[0]
		remaining -= 1
	return root	
		
if __name__ == "__main__":
	node_D = TreeNode("D")
	node_E = TreeNode("E")
	node_C = TreeNode("C",node_D,node_E)
	node_G = TreeNode("G")
	node_H = TreeNode("H")
	node_F = TreeNode("F",node_G,node_H)
	node_B = TreeNode("B",node_C,node_F)
	node_K = TreeNode("K")
	node_L = TreeNode("L")
	node_J = TreeNode("J",node_K,node_L)
	node_N = TreeNode("N")
	node_O = TreeNode("O")
	node_M = TreeNode("M",node_N,node_O)
	node_I = TreeNode("I",node_J,node_M)
	node_A = TreeNode("A",node_B,node_I)
	set_right_siblings(node_A)	

