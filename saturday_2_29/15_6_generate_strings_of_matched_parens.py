"""
Strings in which parentheses are matched are defined by the following
rules:

1. an empty string counts as matching parentheses.
2. The addition of a leading left and a tailing right parentheses
to a string in which parentheses are matched results in a new
string with matched parentheses. For example, since (())()
is a string with matched parentheses, so is ((())()).
3. The concatenation of two strings in which parentheses
are matched is itself a string in which parentheses are matched.
For example, since (())() and () are strings with matched parentheses,
so are (())()() and ()(())().

For example, the set of strings containing two pairs of matched parentheses
is 

- (()),()()
- three matched: ((())),(()()), (())(), ()(()), ()()()

Write a program that takes as input a number and returns all the strings 
with that number of matched parentheses.
"""

def generate_matched_parentheses(num,out):
	"""
	This is a recursive thing.
	We copy out, and then add a left pair, right pair,
	enclosing pair, and then for each index inside of the initial
	out, we add another enclosed pair.
	I can't remember how we determine the end point on this. 
	Do we start with remaining, and then branch from there?
	If we start with remaining, we just do one iteration of the process,
	counting down until we have zero. Let's try that.
	"""
	if num == 0:
		return out
	copied = set()
	if len(out) == 0:
		copied.add("()")
		return generate_matched_parentheses(num-1,copied)
	for item in out:
		## add left pair
		## add right pair
		## add enclosing pair
		## not sure if we need to do central insertion.
		with_left = "()" + item
		with_right = item + "()"
		enclosed = "(" + item + ")"
		to_insert = "()"
		copied.add(with_left)
		copied.add(with_right)
		copied.add(enclosed)
	return generate_matched_parentheses(num-1, copied)

if __name__ == "__main__":
	out = set()
	n = 2
	res = generate_matched_parentheses(n,out)
	expected_2 = {"(())","()()"}
	assert(res == expected_2)
	n = 3
	out = set()
	res = generate_matched_parentheses(n,out)
	expected = {"((()))","(()())", "(())()", "()(())", "()()()"}
	assert(res == expected)
	n = 4
	res = generate_matched_parentheses(n,out)
	expected = {'(()(()))', '()(())()', '()()(())', '(()()())', '(())()()', '(((())))', '((()()))', '((()))()', '(()())()', '()((()))', '()()()()', '()(()())', '((())())'}
	assert(res == expected)
