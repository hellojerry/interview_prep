"""
Call a 2d array sorted if its rows and its columns are nondecreasing.
Ex:

arr = [
	[-1, 2, 4, 4, 6],
	[ 1, 5, 5, 9, 2],
	[ 3, 6, 6, 9,22],
	[ 3, 6, 8,10,24],
	[ 6, 8, 9,12,25],
	[ 8,10,12,13,40]
]

Design an algorithm that takes a 2d sorted array and a number
and checks whether that number appears in the array.


i.e. if the number to look for is 7, in the array above,
return false. If it's 8, return true
"""

def bsearch(arr,val):
	arrlen = len(arr)
	if arrlen < 0:
		return -1
	left_ptr,right_ptr = 0, arrlen - 1
	while left_ptr <= right_ptr:
		midpoint = int((left_ptr+right_ptr)/2)
		if arr[midpoint] < val:
			left_ptr = midpoint+1
		elif arr[midpoint] > val:
			right_ptr = midpoint - 1
		else:
			return midpoint
	return -1

def exists_in_2d_array_bad(matrix,target_value):
	"""
	So for this i think we can do a binary search 
	of sorts. First step is narrowing things
	down to a single row. We can take the top row
	and the bottom row. If val < top[right], we know it's the top row
	if val > bottom[left], we know it's the bottom, and can proceed
	to step 2 on the single found array.

	another way to do this is find the mid row. 
	If value > mid[-1], mid_row = mid_row + 1
	if value < mid[0], mid_row=  mid_row - 1
	otherwise, exit first loop and do a bsearch on a single row.

	This isn't quite right. With each row, we can test
	 
	"""
	rows = len(matrix)
	left_ptr,right_ptr = 0, rows - 1
	breakout = False
	while left_ptr <= right_ptr:
		mid_row_idx = int((left_ptr+right_ptr)/2)
		mid_row = matrix[mid_row_idx]
			
		if mid_row[0] <= target_value <= mid_row[-1]:
			if bsearch(mid_row,target_value) >= 0:
				return True
		if mid_row[0] > target_value:
			right_ptr = mid_row_idx - 1
		else:
			left_ptr = mid_row_idx + 1

	return False

def exists_in_2d_array(matrix,target_val):
	"""
	Went about this in the wrong way.
	Easiest thing to do here is just eliminate
	a row or a column at a time
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	row = 0
	col = cols - 1
	while row < rows and col >= 0:
		if matrix[row][col] == target_val:
			return True
		elif matrix[row][col] < target_val:
			row += 1
		else:
			col -= 1
	return False



if __name__ == "__main__":
	arr = [
		[-1, 2, 4, 4, 6],
		[ 1, 5, 5, 9, 2],
		[ 3, 6, 6, 9,22],
		[ 3, 6, 8,10,24],
		[ 6, 8, 9,12,25],
		[ 8,10,12,13,40]
	]
	res = exists_in_2d_array(arr,7)
	assert(res == False)
	res = exists_in_2d_array(arr,8)
	assert(res == True)
