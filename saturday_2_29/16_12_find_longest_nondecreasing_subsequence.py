"""
The problem of finding the longest nondecreasing subsequence
in a sequence of integers has implications to many disciplines,
including string matching and analyzing card games.
As a concrete instance, the length of a longest nondecreasing
subsequence in

A = [0,8,4,12,2,10,6,14,1,9]

is 4. There are multiple longest nondecreasing subsequences:

[0,4,10,14], [0,2,6,9]

Note that elements of a nondecreasing subsequence are not
required to immediately follow each other in the original
sequence.

Write a program that takes as input an array of numbers
and returns the length of the longest nondecreasing subsequence 
in the array.

Hint: express the longest nondecreasing subsequence ending
at an entry in terms of the longest nondecreasing subsequence
appearing in the subarray consisting of the preceding elements.
"""

def longest_subsequence(arr):
	"""
	This is a dynamic programming problem.
	What the hint is getting at is passing
	the current longest into the remainder of the array.
	I could iterate over prior results, checking the current
	value against the final idx of the prior. if higher,
		add cloned result to current, increase index,
		and then call longest again.
		if lower, dump that subsequence
	"""

	arrlen = len(arr)
	max_length = [1]*arrlen
	for i in range(1,arrlen):
		max_length[i] = max(1 + max(
			(max_length[j] for j in range(i) if arr[i] >= arr[j]),default=0),max_length[i])

	return max(max_length)

if __name__ == "__main__":
	A = [0,8,4,12,2,10,6,14,1,9]
	expected  = 4
	out = []
	res = longest_subsequence(A)
	print(res)
