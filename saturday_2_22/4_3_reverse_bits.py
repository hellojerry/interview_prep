"""
Write a program that takes a uin64_T and returns 
the uin64_t comsiting of the bits of input in reverse order.

ex:
input:  1110000000000001
output: 1000000000000111
"""


def reverse_bit_order(num):
	"""
	So this is going to involve some sort 
	of masking. 
	We can shift left and shift right on this 
	as well.
	"""
	reverse = 0
	while num:
		reverse = (reverse << 1) + (num &1)
		num >>= 1
	return reverse

if __name__ == "__main__":
	## 57345, 32775
	in1 = int("1110000000000001",2)
	expected = int("1000000000000111",2)
	assert(reverse_bit_order(in1) == expected)
