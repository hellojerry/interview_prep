
class Geode(object):
	def __init__(self,name,weight,value):
		assert(type(name) == str)
		for i in [weight,value]:
			assert(type(i) == int and i > 0)
		self.name = name
		self.weight = weight
		self.value = value

	def __str__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)
	def __repr__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)


def get_optimum_weight(matrix,item_array,item_idx,remaining_weight):
	## can't do anything now
	if item_idx == 0:
		return 0
	## if we havent seen this before, get the better of the matrix
	## WITH and WITHOUT the item
	if matrix[item_idx][remaining_weight] == -1:
		without_item = get_optimum_weight(matrix,item_array,item_idx-1,remaining_weight)
		with_item = 0
		item_weight = item_array[item_idx].weight
		item_value = item_array[item_idx].value
		if item_weight <= remaining_weight:
			with_item = item_value + get_optimum_weight(matrix,item_array,item_idx,remaining_weight - item_weight)
		matrix[item_idx][remaining_weight] = max(with_item,without_item)
	return matrix[item_idx][remaining_weight]


if __name__ == "__main__":

	items = [
		Geode("G1",5,60),
		Geode("G2",3,50),
		Geode("G3",4,70),
		Geode("G4",2,30)
	]
	weight = 5
	matrix = [[-1] * (weight+1) for item in items]
	res = get_optimum_weight(matrix,items,len(items)-1,weight)
	print(res)
	assert(res == 80)
