"""
Consider the problem of designing an online calendaring application.
One component of the design is to render the calendar, i.e. display
it visually.

Suppose each day consists of a number of events, where an event
is specified as a start time and a finish time. Individual events
for a day are to be rendered as nonoverlapping rectangular
regions whose sides are parallel to the X and Y axes.
Let the X-axis correspond to time.
If an event starts at time B and ends at time E, the upper and lower
sides of its corresponding rectangle must be at E and B, respectively.

I don't really know what the fuck this is even talking about.

Write a program that takes a set of events, and determines a maximum
number of events that take place concurrently.

ex:

	[(E1,1,5),(E5,2,7),(E8,4,5),(E2,6,10),(E6,8,9),(E9,9,17),(E11,11,13)(E7,12,15),(E4,14,15)]
result: 3 (E1,E5,E8)
(because they have the most overlapping results).

"""

class Event(object):

	def __init__(self,name,start,end):
		assert(type(name) == str)
		for i in [start,end]:
			assert(type(i) == int and i >= 0 and i < 24)
		self.name = name
		self.start = start
		self.end = end

	def __str__(self):
		return "Event(%s,%d,%d)" % (self.name,self.start,self.end)

	def __repr__(self):
		return "Event(%s,%d,%d)" % (self.name,self.start,self.end)

	def __lt__(self,other):
		assert(type(other) == Event)
		return self.start < other.start

def find_max_overlap_easy(events):
	"""
	So I think i've done a problem like this before.
	After sorting it, we iterate through the array starting with index one.
	You know, I could stuff the ranges of each event into an array,
	incrementing each hour index by one, and take the maximum hour.
	I don't think this is necessarily a good idea though... It seems nasty from
	a complexity basis
	"""
	hours = [0 for i in range(24)]
	for event in events:
		for hour in range(event.start,event.end):
			hours[hour] += 1
	return max(hours)

def find_max_overlap(events):
	"""
	We can also sort the events by their start time.
	Compare the start and end.
	If event.start < prior_event.end, then we can increment the
	simultaneous amount up, and compare it to the asbolute maximum.
	Otherwise, we have a disconnect and need to reset the local counter.
	"""
	sorted_events = sorted(events)
	prior_event = events[0]
	max_simultaneous, num_simultaneous = 0,0
	for item in sorted_events[1:]:
		if item.start < prior_event.end:
			num_simultaneous += 1
			max_simultaneous = max(max_simultaneous,num_simultaneous)
		else:
			num_simultaneous = 0
		prior_event = item
	return max_simultaneous

if __name__ == "__main__":

	events = [
		Event("E5",2,7),
		Event("E8",4,5),
		Event("E9",9,17),
		Event("E1",1,5),
		Event("E6",8,9),
		Event("E4",14,15),
		Event("E11",11,13),
		Event("E7",12,15),
		Event("E2",6,10),
	]
	res = find_max_overlap(events)
	assert(res == 3)
