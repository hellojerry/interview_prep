
class Geode(object):
	def __init__(self,name,weight,value):
		assert(type(name) == str)
		for i in [weight,value]:
			assert(type(i) == int and i > 0)
		self.name = name
		self.weight = weight
		self.value = value

	def __str__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)
	def __repr__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)


def get_optimum_result(matrix,item_idx,item_array,remaining_weight):
	"""
	This has a similar solution to the levenshtein distance problem.
	"""
	if item_idx == 0:
		return 0
	if matrix[item_idx][remaining_weight] == -1:
		## this means we havent seen it before
		without_item = get_optimum_result(matrix,item_idx-1,item_array,remaining_weight)
		item = item_array[item_idx]
		with_item = 0
		if item.weight <= remaining_weight:
			with_item = item.value + get_optimum_result(matrix,item_idx,
					item_array,remaining_weight - item.weight)

		matrix[item_idx][remaining_weight] = max(with_item,without_item)



	return matrix[item_idx][remaining_weight]


if __name__ == "__main__":
	weight = 5
	items = [
		Geode("G1",5,60),
		Geode("G2",3,50),
		Geode("G3",4,70),
		Geode("G4",2,30)
	]
	matrix = [[-1]*(weight+1) for item in items]
	res = get_optimum_result(matrix,len(items)-1,items,weight)
	assert(res == 80)
