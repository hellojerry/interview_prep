"""
Given a string containing a set of words
separated by whitespace, reverse the words.

I.e.

input: "Bob likes Alice"
output: "Alice likes Bob"
"""

def reverse_words_easy(sentence):
	"""
	So i don't think this is in the spirit of the question.
	"""
	return " ".join(k for k in reversed(sentence.split(" ")))

def reverse_words(sentence):
	"""
	What about this.
	1. iterate backwards over sentence.
	2. when hitting a space, append to output in reverse order
	"""
	slen = len(sentence)
	arr_ptr = slen - 1
	out = ""
	initial_idx = arr_ptr
	while arr_ptr >= 0:
		el = sentence[arr_ptr]
		if el == " ":
			for i in range(arr_ptr+1,initial_idx+1):
				out += sentence[i]
			out += " "
			initial_idx = arr_ptr - 1
		arr_ptr -= 1
	for i in range(arr_ptr+1,initial_idx+1):
		out += sentence[i]
	return out

if __name__ == "__main__":

	expected = reverse_words_easy("Bob likes Alice")
	in1 = "Bob likes Alice"
	res = reverse_words(in1)
	assert(res == expected)
