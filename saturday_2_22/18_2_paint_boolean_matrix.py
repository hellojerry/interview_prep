"""
let A be a boolean 2d array encoding a black and white image.

The entry A(a,b) can be viewed as encoding the color at entry (a,b).

this is a paint fill problem. It's written in a really stilted way.


ex:

arr = [
	[True,False,False,False,True],
	[False,True,False,False,True],
	[False,True,True,False,True],
	[False,True,True,False,True],
	[False,False,True,False,True]
]

paint_fill(arr,0,2) result:
arr = [
	[True,True,True,True,True],
	[False,True,True,True,True],
	[False,True,True,True,True],
	[False,True,True,True,True],
	[False,False,True,True,True]
]

ex2:
arr = [
	[True,False,False,False,True],
	[False,True,False,False,True],
	[False,True,True,False,True],
	[False,True,True,False,True],
	[False,False,False,False,True]
]

paint_fill(arr,0,2) result:
	arr = [
		[True,True,True,True,True],
		[True,True,True,True,True],
		[True,True,True,True,True],
		[True,True,True,True,True],
		[True,True,True,True,True]
	]
"""

def paint_fill(matrix,row,col,target_color):
	"""
	So the way I see it, we need to get test
	cases for edges. An edge would be:
		- hitting a row or column boundary
		- a given direction is already the desired color
	The way I'd solve this initially is recursively. 
	1. check if current value is the target color. If so, return.
	2. mark current location as target color.
	3. get dimensions of matrix.
	4. determine eligibility for left, right, etc, based on dimensions.
	5. call eligible locations recursively.
	"""
	current_color = matrix[row][col]
	if current_color == target_color:
		return
	matrix[row][col] = target_color
	min_row,min_col = 0,0
	max_row,max_col = len(matrix) -1,len(matrix[0]) - 1
	left_eligible = 0 < col
	right_eligible = col < max_col
	up_eligible = 0 < row
	down_eligible = row < max_row
	if left_eligible:
		if matrix[row][col-1] != target_color:
			paint_fill(matrix,row,col-1,target_color)
	if right_eligible:
		if matrix[row][col+1] != target_color:
			paint_fill(matrix,row,col+1,target_color)
	if up_eligible:
		if matrix[row-1][col] != target_color:
			paint_fill(matrix,row-1,col,target_color)
	if down_eligible:
		if matrix[row+1][col] != target_color:
			paint_fill(matrix,row+1,col,target_color)
	


if __name__ == "__main__":
	arr = [
		[True,False,False,False,True],
		[False,True,False,False,True],
		[False,True,True,False,True],
		[False,True,True,False,True],
		[False,False,True,False,True]
	]
	
	paint_fill(arr,0,2,True)
	expected_arr = [
		[True,True,True,True,True],
		[False,True,True,True,True],
		[False,True,True,True,True],
		[False,True,True,True,True],
		[False,False,True,True,True]
	]
	assert(arr == expected_arr)
	arr = [
		[True,False,False,False,True],
		[False,True,False,False,True],
		[False,True,True,False,True],
		[False,True,True,False,True],
		[False,False,False,False,True]
	]
	paint_fill(arr,0,2,True)
	expected_arr = [
		[True,True,True,True,True],
		[True,True,True,True,True],
		[True,True,True,True,True],
		[True,True,True,True,True],
		[True,True,True,True,True]
	]
	assert(arr == expected_arr)
