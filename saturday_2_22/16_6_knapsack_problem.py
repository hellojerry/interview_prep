"""
Solve the knapsack problem.

ex:
knapsack size: 5oz
available items:
	geode1: $60, weight: 5oz
	geode2: $50, weight: 3oz
	geode3: $70, weight: 4oz
	geode4: $30, weight: 2oz

solution:
	$80, (geode2 & geode4)
"""
class Geode(object):
	def __init__(self,name,weight,value):
		assert(type(name) == str)
		for i in [weight,value]:
			assert(type(i) == int and i > 0)
		self.name = name
		self.weight = weight
		self.value = value

	def __str__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)
	def __repr__(self):
		return "Geode(%s, weight:%d, value:$%d)" % (self.name,self.weight,self.value)

def optimum(matrix,items,K,remaining_weight):
	"""
	Here's how this works:
		if we're at 0 for K, we can't do anything
		if the matrix at row for K and the remaining weight is unpopulated,
			rerun the optimum function again, with AND without the item.
			mark the matrix with the better of the two results,
			and return the value at matrix[K][remaining weight]
	"""
	if K < 0:
		return 0
	if matrix[K][remaining_weight] == -1:
		without_item = optimum(matrix,items,K-1,remaining_weight)
		with_item = 0
		if remaining_weight >= items[K].weight:
			with_item = items[K].value + optimum(matrix,items,K-1,remaining_weight - items[K].weight)
		matrix[K][remaining_weight] = max(without_item,with_item)
	return matrix[K][remaining_weight]

def knapsack_problem(weight,items):
	matrix = [[-1] * (weight+1) for item in items]
	return optimum(matrix,items,len(items)-1,weight)


if __name__ == "__main__":
	obj_arr = [
		Geode("G1",5,60),
		Geode("G2",3,50),
		Geode("G3",4,70),
		Geode("G4",2,30)
	]
	print(obj_arr)

	res = knapsack_problem(5,obj_arr)
	assert(res == 80)
	
