"""
Given a singly linked list and an integer K,
write a program to remove the Kth last element
from the list.

i.e. 1->2->3->4->5, 2

res = 1->2->3->5


1->2->3->4->5->6->7->8->9, 3

res = 1->2->3->4->5->6->8->9

"""
class Node(object):


	def __init__(self,val,next_=None):
		assert(type(val) == int)
		assert(next_ is None or type(next_) == Node)
		self.val = val
		self.next_ = next_

	def __str__(self):
		return "Node(%d)" % self.val

	def __repr__(self):
		return "Node(%d)" % self.val

def remove_kth_last(head,K):
	"""
	This isn't too bad. Iterate to the end, but reverse the pointers
	as you scan through. Once next is none, then you have the tail.
	From there, iterate again, counting down, and reversing pointers again
	until the element is reached. for that specific element, set prior.next = next,
	then continue until the new tail is reached, reversing the pointers.
	"""
	cur = head
	prior = None
	while cur:
		## base case:
		## get next, move cur.next_ to prior
		next_ = cur.next_
		if next_ is None:
			## this means we've hit the end
			cur.next_ = prior
			break
		temp = cur
		cur = next_
		temp.next_ = prior
		prior = temp
	## next step:
	## 
	tail = cur
	prior = None
	iter_ct = 0
	while cur.next_:
		next_ = cur.next_
		iter_ct += 1
		## special case
		if iter_ct == K:
			## why am i having a brain freeze
			## for skipping a fucking node.
			cur = next_
		else:
			temp = cur
			cur = next_
			temp.next_ = prior
			prior = temp
	cur.next_ = prior
	return cur

if __name__ == "__main__":
	n9 = Node(9)
	n8 = Node(8,n9)
	n7 = Node(7,n8)
	n6 = Node(6,n7)
	n5 = Node(5,n6)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)

	root = remove_kth_last(n1,3)
	cur = root
	out = []
	while cur:
		out.append(cur)
		cur = cur.next_
	assert(out == [n1,n2,n3,n4,n5,n6,n8,n9])
	
	n9 = Node(9)
	n8 = Node(8,n9)
	n7 = Node(7,n8)
	n6 = Node(6,n7)
	n5 = Node(5,n6)
	n4 = Node(4,n5)
	n3 = Node(3,n4)
	n2 = Node(2,n3)
	n1 = Node(1,n2)
	root = remove_kth_last(n1,1)
	cur = root
	out = []
	while cur:
		out.append(cur)
		cur = cur.next_
	assert(out == [n1,n2,n3,n4,n5,n6,n7,n8])
