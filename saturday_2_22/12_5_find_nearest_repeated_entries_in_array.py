"""
Write a program that takes as an input
an array and finds the distance between the closest
pair of equal entries.

ex:

	["All","work","and","no","play","makes","for","no","work","no","fun","and","no","results"]

result = 2 (the distance between second and third "no" entry)

"""

def get_distance(arr):
	"""
	This is a hashmap problem.
	I think we can get this in one pass.
	If I iterate over it, 
	"""
	min_distance = float("inf")
	cur_min = None
	priors = {}
	for index,word in enumerate(arr):
		exists = priors.get(word, None)
		if exists is not None:
			dist = index - exists
			if dist < min_distance:
				min_distance = dist
				cur_min = word
			priors[word] = index
		else:
			priors[word] = index
	return min_distance


if __name__ == "__main__":
	arr = ["All","work","and","no","play","makes","for","no","work","no","fun","and","no","results"]
	res = get_distance(arr)
	assert(res == 2)
