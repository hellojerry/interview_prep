"""
A queue can be implement using an array and two
additional fields - the beginnign and end indices.

The structure is sometimes referred to as a circular queue.
Both enqueue and dequeue have O(1) time complexity.

If the array is fixed, there is a maximum number 
of entries that can be stored.

If the array is dynamically resized, the total time M
for combined enqueue and dequeue operations is o(M)

implement a queue API using an array for storing
elements. Your API should include a constructor function,
which takes as argument the initial capacity of the queue,
enqueue and dequeue functions, and a function which returns
the number of elements stored.

Implement dynamic resizing to support storing an arbitrarily
large number of elements.
"""

class CircularQueue(object):

	def __init__(self,capacity):
		assert(type(capacity) == int and capacity > 0)
		self.arr = [None] * capacity
		self.head = 0
		self.tail = 0
		self.num_used = 0

	def enqueue(self, val):
		"""
		So with this, it's supposed to be constant time,
		so it's got to be an array index.
		ok, misread the question - if we go past the capacity,
		we've got to increase it. When there's no resize,
		it's constant time. We can improve this by, say, increasing
		capacity by 255 each time.
		"""
		if len(self.arr) == self.num_used:
			self.arr = self.arr[self.head:] + self.arr[:self.head]
			self.head, self.tail = 0, self.num_used
			self.entries += [None] * (len(self.entries) * CircularQueue.SCALE_FACTOR - len(self.entries))
		self.entries[self.tail] = val
		self.tail = (self.tail + 1) % len(self.entries)
		self.num_used += 1

	def size(self):
		return self.num_used

	def dequeue(self):
		if self.num_used <= 0:
			raise Exception("Queue is empty")
		self.num_used -= 1
		result = self.entries[self.head]
		self.head = (self.head+ 1) % len(self.entries)
		return result

			
		return val

