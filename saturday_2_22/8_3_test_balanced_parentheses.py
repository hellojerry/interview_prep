"""
A string containing any grouping of the characters "{([])}"
is said to be well-formed if the different types of brackets
match in the correct order.

ex:

([]){()} -> well-formed

{), [()[]{()() -> malformed

Write a program that tests if a string containing those
characters is matched properly or not.
"""

def is_well_formed(my_string):
	"""
	This is a stack problem and we need a hashmap.
	"""
	mappings = {
		"{":"}",
		"}":"{",
		"(":")",
		")":"(",
		"[":"]",
		"]":"["
	}
	closers = [")","]","}"]
	stack = []
	for item in my_string:
		if item in closers:
			if len(stack) == 0:
				return False
			test_item = stack.pop()
			if mappings[item] != test_item:
				return False
		else:
			stack.append(item)

	return len(stack) == 0


if __name__ == "__main__":
	s1 = "()(){}[]"
	s2 = "(}{)"
	s3 = "([]){()}"
	s4 = "{)"
	s5 = "[()[]{()()"
	s1_res = is_well_formed(s1)
	s2_res = is_well_formed(s2)
	s3_res = is_well_formed(s3)
	s4_res = is_well_formed(s4)
	s5_res = is_well_formed(s5)
	assert(s1_res == True)
	assert(s2_res == False)
	assert(s3_res == True)
	assert(s4_res == False)
	assert(s5_res == False)
