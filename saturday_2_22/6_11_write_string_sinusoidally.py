"""
Write a string in a sinusoidal fashion,
returning it as a snake string.

ex:

	Hello World!

sinusoidal:
	   e       _        l
	H    l   o   W   r    d
               l       o        !

snake string:

e_lHloWrdlo!
"""

def generate_snake_string(my_str):
	"""
	top level is 1,5,9,13,
	mid level is 0, 2, 4, 6,8,10
	bottom level is 3,7,11,15,19
	"""
	strlen = len(my_str)
	mid_ct = 0
	top_ct = 1
	bottom_ct = 3
	out = ""
	for i in range(1,strlen,4):
		out += my_str[i]
	for i in range(0,strlen,2):
		out += my_str[i]
	for i in range(3,strlen,4):
		out += my_str[i]
	return out
		

if __name__ == "__main__":
	s1 = "Hello_World!"
	expected = "e_lHloWrdlo!"
	res = generate_snake_string(s1)
	assert(res == expected)
