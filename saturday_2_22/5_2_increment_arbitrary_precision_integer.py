"""
Write a program which takes as an input an array of digits
encoding a nonnegative decimal inteeer D and updates
the array to represent the interger D + 1.

For example, if the input is [1,2,9], then
you should update the array to be [1,3,0].

ex:

in = [1,2,9]
res = [1,3,0]

in = [9,9]
out = [1,0,0]
in = [0]
out = [1]
"""

def update_integer_array(arr):
	"""
	So I believe that this is one of those bookkeeping problems.
	"""
	arrlen = len(arr)
	ptr = arrlen - 1

	carryforward = 0

	while ptr >= 0:
		target = arr[ptr]
		if ptr == arrlen - 1:
			"""
			First step is kicking the loop off.
			"""
			val = target + 1
			if val < 10:
				arr[ptr] = val
				return arr
			else:
				arr[ptr] = 0
				carryforward += 1
		else:
			val = carryforward + target
			if val < 10:
				arr[ptr] = val
				return arr
			else:
				carryforward,val = divmod(val,10)
				arr[ptr] = val	
		ptr -= 1
	if carryforward > 0:
		arr.insert(0,carryforward)

	return arr


if __name__ == "__main__":
	in3 = [0]
	expected3 = [1]
	res = update_integer_array(in3)
	assert(res == expected3)
	in1 = [1,2,9]
	expected1 = [1,3,0]
	res = update_integer_array(in1)
	assert(res == expected1)
	in2 = [9,9]
	expected2 = [1,0,0]
	res = update_integer_array(in2)
	assert(res == expected2)
	in4 = [1,9,9,8,8,9,9,9]
	expected_4 = [1,9,9,8,9,0,0,0]
	res = update_integer_array(in4)
	assert(res == expected_4)
	in5 = [2,9,9,9,9,9]
	res = update_integer_array(in5)
	assert(res == [3,0,0,0,0,0])
