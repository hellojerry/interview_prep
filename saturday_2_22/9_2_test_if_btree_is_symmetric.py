"""
A btree is symmetric is you can draw
a vertical line through the root and then the left
subtree is the mirror image of the right subtree.

symmetric:
			314
		6                6
		  2            2
		    3         3

asymmetric 1:
			314
		6		6
	   	  561          2
                    3        1

asymmetric 2:
			314
		6		6
		  561	     561 
		    3

asymmetric 3:
			314
		6		6
	561		     561
	   3                3

asymmetric 4:
			314
		6		6
	561		     561
	   3                3	2
"""

class TreeNode(object):

	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.val

	def __repr__(self):
		return "TreeNode(%d)" % self.val

POSITION_ROOT = 0
POSITION_LEFT = 1
POSITION_RIGHT = 2

def is_symmetric(root):
	"""
	I believe that this is a BFS problem.
	We push root to a queue, with a level number,
	then push left and right to the queue, including
	the current level. Let's see what that gets us first.
	"""
	if not root:
		return True
	current_level = 0
	queue = [(root,current_level,POSITION_ROOT)]
	prior_level = -1
	prior_position = None
	prior_val = -1
	while queue:
		node, current_level,position = queue.pop(0)
		if prior_level == current_level:
			if prior_position == position:
				## this means that one's going left and the other is right.
				return False
			if node.val != prior_val:
				## this means that the values don't match.
				return False
		### if the prior and current level don't match, don't worry
		### about a test. Just memoize
		memoized_prior = prior_level
		prior_level = current_level
		current_level += 1
		prior_position = position
		prior_val = node.val

		if node.left:
			queue.append((node.left,current_level,POSITION_LEFT))
		if node.right:
			queue.append((node.right,current_level,POSITION_RIGHT))
		if len(queue) == 1:
			## the trick with this is 
			if memoized_prior == prior_level:
				return False
			
	return True
	


if __name__ == "__main__":
	## case 1: symmetric
	n3l = TreeNode(3)
	n2l = TreeNode(2, None, n3l)
	n6l = TreeNode(6,None, n2l)
	n3r = TreeNode(3)
	n2r = TreeNode(2,n3r)
	n6r = TreeNode(6,n2r)
	root = TreeNode(314,n6l,n6r)
	res1 = is_symmetric(root)
	assert(res1 == True)
	## case 2: asymmetric
	n3l = TreeNode(3)
	n561l = TreeNode(561,None,n3l)
	n6l = TreeNode(6,None,n561l)

	n1r = TreeNode(1)
	n2r = TreeNode(2,n1r)
	n6r = TreeNode(6,n2r)
	root = TreeNode(314,n6l,n6r)
	res2 = is_symmetric(root)
	assert(res2 == False)

	## case 4: asymmetric to to leaf position
	n3l = TreeNode(3)
	n561l = TreeNode(561,None,n3l)
	n6l = TreeNode(6,n561l)
	n3r = TreeNode(3)
	n561r = TreeNode(561,n3r)
	n6r = TreeNode(6,n561r)
	root = TreeNode(314,n6l,n6r)
	res4 = is_symmetric(root)
	assert(res4 == False)

	## case 5: additional leaf position check
	n3l = TreeNode(3)
	n561l = TreeNode(561,None,n3l)
	n6l = TreeNode(6,n561l)
	n3r = TreeNode(3)
	n2r = TreeNode(2)
	n561r = TreeNode(561,n3r,n2r)
	n6r = TreeNode(6,n561r)
	root = TreeNode(314,n6l,n6r)
	res5 = is_symmetric(root)
	assert(res5 == False)
	## case 3: asymmetric due to missing leaf
	n3l = TreeNode(3)
	n561l = TreeNode(561,None,n3l)
	n6l = TreeNode(6,None,n561l)
	n561r = TreeNode(561)
	n6r = TreeNode(6,n561r)
	root = TreeNode(314,n6l,n6r)
	res3 = is_symmetric(root)
	assert(res3 == False)
