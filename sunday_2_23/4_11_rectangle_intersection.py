"""
Write a program that tests if two rectangles have a nonempty
intersection. If the intersection is nonempty, return the rectangle
formed by their intersection.



			

	3,0, 3,2
	    A
	5,0  5,2
This is a little tricky to model in a terminal.

"""


class Rectangle(object):

	def __init__(self,name,x,y,width,height):
		assert(type(name) == str)
		assert(type(width) == int and width > 0)
		assert(type(height) == int and height > 0)
		assert(type(x) == int and x >= 0)
		assert(type(y) == int and y >= 0)
		self.x = x
		self.y = y
		self.width = width
		self.height = height
		


def is_intersect(r1,r2):
	return (r1.x <= r2.x + r2.width and r1.x + r1.width >= r2.x and (
			r1.y <= r2.y + r2.height) and (
			r1.y +r1.height >= r2.y)

def intersect_rectangles(r1,r2):
	if not is_intersect(r1,r2):
		return False, 0

	return Rectangle("RES",max(r1.x,r2.x),max(r1.y,r2.y),
		min(r1.x + r1.width,r2.x+r2.width) - max(r1.x,r2.x),
		min(r1.y + r1.height, r2.y + r2.height) - max(r1.y,r2.y))
