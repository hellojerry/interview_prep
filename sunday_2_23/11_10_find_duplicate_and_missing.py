"""
You are given an array of N integers, each between 0 and N-1 inclusive.
Exactly one element appears twice, implying that exactly one number between 0 and 
N-1 is missing from the array. How would you compute the duplicate
and missing numbers?
"""


def find_dupe_and_missing(arr):
	"""
	My gut on this is to make a
	second array of zeroes equal to
	the size of the array. 
	Iterating through the initial array once,
	updating the memo array[val] += 1.
	If the updated value is now 2, that's our dupe.
	Then, I do an index check on the memo array for a zero,
	and that's the value of the missing one.
	"""
	arrlen = len(arr)
	memo_arr = [0]*arrlen
	dupe = None
	for el in arr:
		memo_arr[el] += 1
		if memo_arr[el] == 2:
			dupe = el
	missing = memo_arr.index(0)
	return (dupe,missing)


if __name__ == "__main__":
	arr = [0,2,2,3,4,5]
	dupe,missing = find_dupe_and_missing(arr)
	assert(dupe == 2)
	assert(missing == 1)
