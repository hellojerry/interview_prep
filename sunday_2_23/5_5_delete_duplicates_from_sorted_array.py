"""
Write a program which takes
as an input a sorted array and updates it
so that all duplicates have been removed and the remaining
elements have been shifted left to fill the emptied indices.
Return the number of remaining valid elements.

ex:

arr: [2,3,5,5,7,11,11,11,13]
out: [2,3,5,7,11,13,None,None,None]


arr: [2,2,2,3,4,5,6,6,7,7]
out: [2,3,4,5,6,7,None,None,None,None]

"""

def remove_duplicates(arr):
	"""
	If current is None, scan until non-none is found or ptr exhausted (return), swap non-none with current
	if prior == Current, swap next value with current index, and make next value None
	if prior is not current, continue normally.
	"""

	arrlen = len(arr)
	if arrlen < 2:
		return arr
	prior = arr[0]
	ptr = 1
	none_ct = 0
	while ptr < arrlen - 1:
		current = arr[ptr]
		if current is None:
			memo_ptr = ptr + 1
			while memo_ptr <= arrlen:
				if memo_ptr == arrlen:
					return arrlen - none_ct
				if arr[memo_ptr] is not None:
					arr[memo_ptr], arr[ptr] = arr[ptr],arr[memo_ptr]
					break
				memo_ptr += 1
			continue
		if prior == current:
			arr[ptr],arr[ptr+1] = arr[ptr+1],None
			none_ct += 1
		else:
			prior = current
			ptr += 1
	return arrlen - none_ct


if __name__ == "__main__":
	a1= [2,3,5,5,7,11,11,11,13]
	e1= [2,3,5,7,11,13,None,None,None]
	assert(len(a1) == len(e1))
	res = remove_duplicates(a1)
	assert(a1 == e1)
	assert(res == 6)
	a2= [2,2,2,3,4,5,6,6,7,7]
	e2= [2,3,4,5,6,7,None,None,None,None]
	res = remove_duplicates(a2)
	assert(len(a2) == len(e2))
	assert(a2 == e2)
	assert(res == 6)

	a3 = [1,1,1,2,3,4,5,6,7,8,8,8,9,9,9]
	e3 = [1,2,3,4,5,6,7,8,9,None,None,None,None,None,None]
	res = remove_duplicates(a3)
	assert(a3 == e3)
	assert(res == 9)
