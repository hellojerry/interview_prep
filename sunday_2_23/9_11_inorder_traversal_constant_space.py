"""
The normal implementation of an inorder traversal
using recursion uses o(n) stack space, where N
is the height of the tree.
Write a nonrecursive program for computing the inorder
traversal of a binary tree. Nodes HAVE parent references.

symmetric:
			314
		6                6
		  2            2
		    3         3

"""



class TreeNode(object):

	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right

	def __str__(self):
		return "TreeNode(%d)" % self.val

	def __repr__(self):
		return "TreeNode(%d)" % self.val

def inorder_basic(root,out):
	if not root:
		return None
	inorder_basic(root.left,out)
	out.append(root)
	inorder_basic(root.right,out)


def inorder_constant(tree):
	"""
	Note that this is only not constant space
	because we're using the out param to test the results.
	"""
	current = root
	out = []
	while current:
		if not current.left:
			out.append(current)
			current = current.right
		else:
			pre = current.left
			while pre.right and pre.right != current:
				pre = pre.right
			if pre.right is None:
				pre.right = current
				current = current.left
			else:
				pre.right = None
				out.append(current)
				current = current.right
	return out



if __name__ == "__main__":
	n3l = TreeNode(3)
	n2l = TreeNode(2, None, n3l)
	n3l.parent = n2l
	n6l = TreeNode(6,None, n2l)
	n2l.parent = n6l
	n3r = TreeNode(3)
	n2r = TreeNode(2,n3r)
	n3r.parent = n2r
	n6r = TreeNode(6,n2r)
	n2r.parent = n6r
	root = TreeNode(314,n6l,n6r)
	arr = []
	inorder_basic(root,arr)
	res = inorder_constant(root)
	assert(res == arr)
