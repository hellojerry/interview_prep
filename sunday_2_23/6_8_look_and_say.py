"""
The look-and-say sequence starts with 1.
Subsequent numbers are derived by describing
the previous number in terms of consecutive digits.
Specifically, to generate an entry of the sequence
from the previous entry, read off the digits of the previous
entry, counting the number of digits in groups of the same digit.

Ex:
N = 8

sequence: [1,11,21,1211,111221,312211,13112221, 1113213211]
expected: "1113213211"
"""

def look_and_say(num):

	cur = "1"
	count = 1
	while count < num:
		sub_ct = 0
		
