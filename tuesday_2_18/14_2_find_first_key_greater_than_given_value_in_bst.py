"""
Write a program that takes as input a BST
and a value, and returns the first key that
would appear in an inorder traversal which
is greater than the input value.

			19
		7		    43
	   3	   11	      23          47
         2  5        17       	37           53
                    13        29   41
                               31

For this btree, with an input of 23, the result is 29

Remember, an inorder traversal is left, center, right.
Upon reaching 23, left none, 23, traverse right,
29 returned before 37.
"""



class TreeNode(object):

	def __init__(self,value,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(value) == int)
		self.value = value
		self.left = left
		self.right = right

	def __repr__(self):
		return "TreeNode(%d)" % self.value

	def __str__(self):
		return "TreeNode(%d)" % self.value

def get_target_node(root,K):
	"""
	So it's not required that a value matches an existing node.
	To traverse a bst, we check the current value.
	if supplied > current, go left, otherwise go right.
	"""
	if not root:
		return None
	if root.value > K:
		return get_target_node(root.left,K)
	elif root.value < K:
		return get_target_node(root.right,K)
	else:
		return root

def inorder_first_eligible(root,K):

	if root.left:
		return root.left
	return root
	

def get_first_greater_inorder_2(root,K):
	"""
	The hint is saying do a binary search while
	keeping some additional state.

	My first thought is to traverse to the location,
	and then from there, do an inorder search.

	I don't think that this is the right way to do it,
	mostly because it presumes the existence of a key
	with the value K.
	"""
	target_node = get_target_node(root,K)
	first_eligible = inorder_first_eligible(target_node.right,K)
	return first_eligible

def get_first_greater_inorder(root, K):
	root, first_so_far = root, None
	while root:
		if root.value > K:
			first_so_far, root = root, root.left
		else:
			root = root.right
	return first_so_far


if __name__ == "__main__":
	n2 = TreeNode(2)
	n5 = TreeNode(5)
	n3 = TreeNode(3,n2,n5)
	n13 = TreeNode(13)
	n17 = TreeNode(17,n13)
	n11 = TreeNode(11,None,n17)
	n7 = TreeNode(7,n3,n11)

	n31 = TreeNode(31)
	n29 = TreeNode(29,None,n31)
	n41 = TreeNode(41)
	n37 = TreeNode(37,n29, n41)
	n23 = TreeNode(23,None,n37)

	n53 = TreeNode(53)
	n47 = TreeNode(47,None,n53)
	n43 = TreeNode(43,n23,n47)
	root = TreeNode(19,n7,n43)
	res = get_first_greater_inorder(root,23)
	assert(res == n29)
