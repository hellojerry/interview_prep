"""
Suppose you are given two sorted arrays of integer. 
If one array has enough empty entries at its end,
it can be used to store the combined entries of the two
arrays in sorted order. 

ex:
	a = [3,13,17,None,None,None,None,None]
	b = [3,7,11,19]
	result = [3,3,7,11,13,17,19,None]

Write a program which takes as input two sorted arrays of integers
and updates the first to the combined entries of the two arrays
in sorted order. Assume the first array has enough empty entries at its
end to hold the result.
"""

def merge_two_sorted_arrays(arr_1,arr_2):
	"""
	Ok, first part is determine which array is larger.
	That is the destination array.

	1. get tail index of existing values on larger array.
	2. get final index eligible to be swapped on larger array (tail idx + len(smaller))
	iterate fat tail index pointer downwards 
	with shorter end pointer.
	put the larger value at the final index, 
	decrement index by one, rinse and repeat

	My solution is a bit different from the book's but
	it's fine.
	"""
	arr_1_len, arr_2_len = len(arr_1), len(arr_2)
	if arr_2_len > arr_1_len:
		## for sanity's sake let's just assume arr 1 is bigger
		arr_1, arr_2 = arr_2, arr_1
		arr_1_len, arr_2_len = arr_2_len, arr_1_len
	tail_existing_index = 0
	while arr_1[tail_existing_index] is not None:
		tail_existing_index += 1
	tail_existing_index -= 1
	final_idx = arr_2_len + tail_existing_index
	short_tail_idx = arr_2_len - 1
	while tail_existing_index >= 0 and short_tail_idx >= 0:
		left_val = arr_1[tail_existing_index]
		right_val = arr_2[short_tail_idx]
		if left_val == right_val:
			## decrement both pointers, decrement final idx by two
			arr_1[final_idx] = left_val
			final_idx -= 1
			tail_existing_index -= 1
			arr_1[final_idx] = right_val
			short_tail_idx -= 1
			final_idx -= 1
		elif left_val < right_val:
			## for this, only swap from the short one
			arr_1[final_idx] = right_val
			final_idx -= 1
			short_tail_idx -= 1
		else:
			arr_1[final_idx] = left_val
			final_idx -= 1
			tail_existing_index -= 1
	return arr_1



if __name__ == "__main__":
	a = [3,13,17,None,None,None,None,None]
	b = [3,7,11,19]
	expected = [3,3,7,11,13,17,19,None]
	res = merge_two_sorted_arrays(a,b)
	assert(res == expected)
