"""
Many algorithms require as a subroutine the computation
of the kth largest element of an array. The first largest element
is simply the largest element. The nth largest element is the smallest element
where N is the length of the array.

Design an algorithm for computing the kth largest element in an array.


example:

A = [3,2,1,5,4]
K = 3
result: 3

Hint: use divide and conquer in conjunction with randomization

"""
import heapq, random

def find_kth_largest_basic(arr, K):
	"""
	This solves it, I don't think it's in the spirit of the question though.
	"""
	return sorted(arr)[-K]


def find_kth_largest_heap(arr,K):
	"""
	So I could do this in o(n) time by iterating through the array
	and pushing it to a max heap, then getting nlargest and the last value from there.
	I also don't think this is in the spirit of the question either.
	I also think it might be worst in terms of complexity.
	"""
	heap = heapq.heapify(arr)
	return heapq.nlargest(K,arr)[-1]

def partition_around_value(left,right,arr,pivot_idx):
	pivot = arr[pivot_idx]
	new_pivot_idx = left
	arr[pivot_idx], arr[right] = arr[right],arr[pivot_idx]
	for i in range(left,right):
		if arr[i] > pivot:
			arr[i], arr[new_pivot_idx] = arr[new_pivot_idx],arr[i]
			new_pivot_idx += 1
	arr[right],arr[new_pivot_idx] = arr[new_pivot_idx], arr[right]
	return new_pivot_idx

def find_kth_largest_quicksort(arr,K):
	"""
	So the goal here is to do this in o(N) time. Somehow a quicksort works a bit better. 
	"""
	left, right = 0, len(arr) - 1
	while left <= right:
		pivot_idx = random.randint(left,right)
		new_pivot_idx = partition_around_value(left,right,arr,pivot_idx)
		if new_pivot_idx == K - 1:
			return arr[new_pivot_idx]
		elif new_pivot_idx > K - 1:
			right = new_pivot_idx - 1
		else:
			left = new_pivot_idx + 1
	



if __name__ == "__main__":
	K = 3
	arr = [3,2,1,5,4]
	res = find_kth_largest_basic(arr,K)
	assert(res == 3)
	res = find_kth_largest_heap(arr,K)
	assert(res == 3)
	res = find_kth_largest_quicksort(arr,K)
	assert(res == 3)
