"""
Given two nodes in a binary tree, design an algorithm that computes their LCA.
Assume that each node has a parent pointer.

			10
		8		9
            6       7         4     12
		  3         2         11

ex: Node(3) and Node(6) -> LCA = Node(8)
ex: Node(2) and Node(9) -> LCA = Node(9)
ex: Node(6) and Node(12) -> LCA = Node(10)

"""


class TreeNode(object):

	def __init__(self,value,left=None,right=None,parent=None):
		for i in [left,right,parent]:
			assert(i is None or type(i) == TreeNode)
		assert(type(value) == int)
		self.value = value
		self.left = left
		self.right = right
		self.parent = parent

	def __repr__(self):
		return "TreeNode(%d)" % self.value

	def __str__(self):
		return "TreeNode(%d)" % self.value


def get_lca_1(first_node,second_node):
	"""
	So my first thought here is to keep fetching the parent nodes
	until there's no parent, pushing each node to an array (a separate one
	for each). Then, after that, i iterate backward over both arrays. The first
	time they don't match, I know to break and retain the prior match and return it.


	"""
	first_path, second_path = [],[]
	first_cur,second_cur = first_node, second_node
	while first_cur:
		first_path.append(first_cur)
		first_cur = first_cur.parent

	while second_cur:
		second_path.append(second_cur)
		second_cur = second_cur.parent

	first_path_len,second_path_len = len(first_path),len(second_path)
	first_path_idx, second_path_idx = first_path_len-1, second_path_len - 1
	current_match = None
	while first_path_idx >= 0 and second_path_idx >= 0:
		first_val = first_path[first_path_idx]
		second_val = second_path[second_path_idx]
		if second_val is first_val:
			current_match = second_val
		else:
			return current_match
		first_path_idx -= 1
		second_path_idx -= 1
	return current_match
	
def get_lca_2(first_node,second_node):
	"""
	Another way to do this is to ascend upwards from one node to the root
	storing each value in a hash table. Then, for the second node,
	return as soon as you find a match in the hash table while ascending to root.
	"""


	first_path = {}
	first_cur = first_node
	while first_cur:
		first_path[first_cur.value] = first_cur
		first_cur = first_cur.parent

	second_cur = second_node
	while second_cur:
		check = first_path.get(second_cur.value, None)
		if check is not None:
			return check
		second_cur = second_cur.parent

	return None

def get_depth(node):

	depth = 0
	cur = node
	while cur:
		depth += 1
		cur = cur.parent
	return depth


def get_lca(first_node, second_node):
	"""
	The third way to do this is to get the depths of both of the nodes
	by ascending to the parent, then moving the deeper node to the equal
	depth of the shallower node, then advancing up one by one and returning on the first path.
	This is the same complexity as the other two in terms of time, but takes up less space.
	"""

	first_depth, second_depth = get_depth(first_node), get_depth(second_node)
	if second_depth > first_depth:
		first_node,second_node = second_node,first_node
		first_depth,second_depth = second_depth,first_depth

	## now we have the first node as deeper

	while first_depth > second_depth:
		first_node = first_node.parent
		first_depth -= 1

	## now, both are equal
	

	while first_node and second_node:
		if first_node is second_node:
			return first_node
		first_node = first_node.parent
		second_node = second_node.parent
	return first_node



if __name__ == "__main__":
	n6 = TreeNode(6)
	n3 = TreeNode(3)
	n7 = TreeNode(7,n3)
	n8 = TreeNode(8,n6,n7)
	n7.parent = n8
	n6.parent = n8
	n3.parent = n7

	n2 = TreeNode(2)
	n11 = TreeNode(11)
	n4 = TreeNode(4,n2)
	n12 = TreeNode(12,None,n11)
	n9 = TreeNode(9,n4)
	n2.parent = n4
	n4.parent = n9
	n11.parent = n12
	n12.parent = n9

	root = TreeNode(10,n8, n9)
	n8.parent = root
	n9.parent = root


	round_1_res = get_lca(n3,n6)
	round_1_expected = n8
	assert(round_1_expected == round_1_res)
	round_2_res = get_lca(n2,n9)
	round_2_expected = n9
	assert(round_2_expected == round_2_res)
	round_3_res = get_lca(n6,n12)
	round_3_expected = root
	assert(round_3_expected == round_3_res)
