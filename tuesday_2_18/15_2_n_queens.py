"""
Write a program which returns all distinct
non-attacking placements of N queens on an NxN 
chessboard.
"""


def is_valid_placement(occupied_row,occupied_col,col_array):
	for target_row in range(occupied_row):
		target_col = col_array[target_row]
		if target_col == occupied_col:
			return False
		if target_row == occupied_row:
			return False
		if abs(occupied_col - target_col) == abs(occupied_row - target_row):
			return False
	return True


def eight_queens(row,col_array, limit,acc):
	"""
	first part:
		diagonal = matching col distance and matching row distance
	"""
	if limit < 4:
		return False
	if row == limit:
		copied = col_array.copy()
		acc.append(list(zip(range(limit),copied)))
	else:
		for col in range(limit):
			if is_valid_placement(row,col,col_array):
				col_array[row] = col
				eight_queens(row+1,col_array,limit,acc)

if __name__ == "__main__":
	limit = 8
	col_array = list(range(8))
	acc = []
	eight_queens(0,col_array,limit,acc)
	print(len(acc))
