"""
The international standard book number (ISBN)
is a unique commercial book identifier.

It is a string of length 10. The first 9
characters are digits; the last character is
a check character. The check character is the sum of the first 
9 digits modulo 11, with 10 represented by "X".

Create a cache for looking up prices of books
identified by their ISBN. For the purpose
of this exercise, treat ISBNs and prices as positive integers.
You must implement lookup, insert, and erase methods.
Use the LRU policy for cache eviction.

Insert: if an ISBN is already present, insert 
should not update the price, but should update that
ISBN to be the most recently used entry.

Lookup: Given an ISBN, return the corresponding price.
If the element is not present, return -1.
If the ISBN is present, update that entry
to be the most recently used ISBN.

Erase: Remove the specified ISBN and corresponding
value from the case. Return True if the ISBN was present,
otherwise, return False.

The other way to do this is with an ordereddict,
which is a TreeMap... an err... a btree

"""

class LRUCache2(object):
	"""
	This isn't too bad. We have a hashmap
	with K: ISBN, and then we need some sort
	of array or queue to hold the LRU
	"""

	def __init__(self, capacity):
		assert(type(capacity) == int and capacity > 0)
		self.capacity = capacity
		self.dict = {}
		self.arr = []

	def __str__(self):
		return str(self.dict)

	def __repr__(self):
		return str(self.dict)

	def delete(self,key):
		"""
		Erase: Remove the specified ISBN and 
		corresponding value from the case. 
		Return True if the ISBN was present,
		otherwise, return False.
		"""
		exists = self.dict.get(key,None)
		if exists is None:
			return False
		else:
			idx = self.arr.index(key)
			self.arr.pop(idx)
			self.dict.pop(key)
			return True

	def get(self,key):
		"""
		Lookup: Given an ISBN, 
		return the corresponding price.
		If the element is not present, return -1.
		If the ISBN is present, update that entry
		to be the most recently used ISBN.
		"""
		exists = self.dict.get(key,None)
		if exists is None:
			return -1
		else:
			idx = self.arr.index(key)
			self.arr.pop(idx)
			self.arr.append(key)
			return exists

	def insert(self, key,val):
		"""
		Insert: if an ISBN is already present, insert 
		should not update the price, but should update that
		ISBN to be the most recently used entry.
		"""
		existing = self.dict.get(key,None)
		arrlen = len(self.arr)
		if existing is None:
			if arrlen == self.capacity:
				to_pop = self.arr[0]
				self.dict.pop(to_pop)
				self.arr.pop(0)
				self.arr.append(key)
				self.dict[key] = val
			else:
				self.arr.append(key)
				self.dict[key] = val
		else:
			print("here")
			idx = self.arr.index(key)
			self.arr.pop(idx)
			self.arr.append(key)




if __name__ == "__main__":
	capacity = 5
	pair_1 = ("a",1)
	pair_2 = ("b",2)
	pair_3 = ("c",3)
	pair_4 = ("d",4)
	pair_5 = ("e",5)
	pair_6 = ("f",6)
	cache = LRUCache(capacity)
	for i in [pair_1,pair_2,pair_3,pair_4,pair_5,pair_6]:
		cache.insert(*i)
	print(cache)
	expected_dict = {
		"b":2,
		"c":3,
		"d":4,
		"e":5,
		"f":6
	}
	expected_arr = ["b","c","d","e","f"]
	assert(cache.dict == expected_dict)
	assert(cache.arr == expected_arr)
	res = cache.delete("a")
	assert(res == False)
	r2 = cache.delete("c")
	assert(r2 == True)
	expected_dict.pop("c")
	assert(cache.dict == expected_dict)
	assert(cache.arr == ["b","d","e","f"])

	r3 = cache.get("a")
	assert(r3 == -1)
	r4 = cache.get("b")
	assert(r4 == 2)
	assert(cache.arr == ["d","e","f","b"])
	cache.insert(*("f",9))
	assert(cache.arr == ["d","e","b","f"])
	assert(cache.dict ==expected_dict)





