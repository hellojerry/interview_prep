"""
Write a program which takes as input an array and finds the distance between
a closest pair of equal entries. 

ex:

arr = ["All","work","and","no","play","makes","for","no","work","no","fun","and","no","results"]
output: 2 (distance between second and third instances of "no")
"""


def find_closest_distance(arr):
	"""
	So I think the way to do this in a single pass is as follows:
	1. push the value to a dict, appending the index of the value to the list
	2. If the list length >= 2, compare the last and second to last diff with the memoized min distance
	if it's shorter than the memoized min distance, update the memoized value. return it when finished.
	We could improve the storage on this by just memoizing the
	most recent distance and doing a comparison there.
	I think we're good to go though.
	"""
	memoized_distance = float("inf")
	accumulator = {}

	for index, word in enumerate(arr):
		existing = accumulator.get(word, None)
		if not existing:
			accumulator[word] = [index]
		else:
			existing.append(index)
			if len(existing) >= 2:
				diff = existing[-1] - existing[-2]
				if diff < memoized_distance:
					memoized_distance = diff
	return memoized_distance

if __name__ == "__main__":
	arr = ["All","work","and","no","play","makes","for","no","work","no","fun","and","no","results"]
	res = find_closest_distance(arr)
	assert(res == 2)
