"""
Consider a coordinate system for the milky way galaxy, in which earth
is at (0,0,0). Model stars at points, and assume distances are in light years.
The milky way galaxy consists of approximately 10**12 stars, and their coordinates
are stored in a file.
How would you compute the K closest stars to Earth?

Hint: Suppose you kow the K closest stars in the first N stars. If the (n+1)th star is to be added to the
set of K closest stars, which element in that set should be evicted?

"""


####
"""
I have no idea how to answer this question - it's best if I read the prep component
at the beginning of the chapter. Heaps are apparently something I'll need to study in more
depth.

How do we model this to begin with?

i guess we should model things with an array of objects. Let's just generate ten to start with.


"""
import random, heapq

class Star(object):

	def __init__(self, name, height,width,depth):
		assert(type(name) == str)
		for i in [height,width,depth]:
			assert(type(i) == int)
		self.name = name
		self.height = height
		self.width = width
		self.depth = depth
		### I don't think this is the proper way to calculate the distance, but whatever.
		### the point of the question isn't to know trigonometry.
		self.earth_distance = abs(self.height) + abs(self.width) + abs(self.depth)

	def __str__(self):
		return "Star(%s,%d,%d,%d)" % (self.name,self.height,self.width,self.depth)

	def __repr__(self):
		return "Star(%s,%d,%d,%d)" % (self.name,self.height,self.width,self.depth)

	def __lt__(self, obj):
		assert(type(obj) == Star)
		return self.earth_distance < obj.earth_distance
		


def find_nearest_k_stars(stars,k):
	"""
	ok, this isn't too bad, this is just a matter of learning the library.
	For this, we want to make a max-heap based on the distance. 
	for each star, we push to the heap. if the heap length is greater than k+1,
	we pop a value off (which would be the maximum value).
	From there, we iterate over N largest and return those.
	"""
	max_heap = []
	heapq.heapify(max_heap)
	for star in stars:
		heapq.heappush(max_heap, (-star.earth_distance, star))
		if len(max_heap) >= k + 1:
			heapq.heappop(max_heap)
	return [i[1] for i in heapq.nlargest(k, max_heap)]


if __name__ == "__main__":
	urf = Star("Earth", 0,0,0)
	stars = []
	for i in range(1,10):
		star = Star(str(i), i, -i, i**2)
		stars.append(star)
	first_3 = stars[:3]
	random.shuffle(stars)
	res = find_nearest_k_stars(stars,3)
	assert(res == first_3)


	stars = []
	for i in range(1,1000):
		star = Star(str(i), i, -i, i**2)
		stars.append(star)
	first_5 = stars[:5]
	random.shuffle(stars)
	res = find_nearest_k_stars(stars,5)
	assert(res == first_5)
