"""
Write a program which takes a nonnegative integer and returns the largest integer
whose square is less than or equal to the given integer. 

For example, if the input is 16, return 4. 

If the input is 300, return 17, since 17**2 = 289 and 18**2 == 324

Hint: look out for a corner case.

"""

import math

def find_largest_root_1(initial_val):
	"""
	So my first instinct is to do math.sqrt. If we have an integer match, return it.
	Otherwise, do int(sqrt) and int(sqrt)+1, square both, do an absolute value comparison,
	and return the smaller.

	This answers the question correctly, but I don't think it's in the spirit of the question.
	"""

	square_root = math.sqrt(initial_val)
	sqrt_as_int = int(square_root)
	if sqrt_as_int == square_root:
		return square_root
	below = sqrt_as_int**2
	above = (sqrt_as_int+1)**2

	if abs(above - initial_val) < abs(below - initial_val):
		return sqrt_as_int+1
	return sqrt_as_int

def find_largest_root(K):
	"""
	The other route to this is a bsearch of sorts.
	we start at left = 0, right = K
		
	"""
	left, right = 0, K

	while left <= right:
		midpoint = int((left+right)/2)
		midpoint_squared = midpoint**2
		if midpoint_squared == K:
			return midpoint
		elif midpoint_squared < K:
			left = midpoint+1
		else:
			right = midpoint - 1
	return left - 1
		


if __name__ == "__main__":
	case_1 = find_largest_root(16)
	assert(case_1 == 4)
	case_2 = find_largest_root(300)
	assert(case_2 == 17)
	case_3 = find_largest_root(909)
	case_3_check = find_largest_root_1(909)
	assert(case_3 == case_3_check)
