"""
Given a set of non-overlapping intervals,
insert a new interval into the intervals
(merge if necessary). Intervals are sorted according
to their start time.

ex1: [[1,3],[6,9]] to_insert: [2,5]
out: [[1,5],[6,9]]

ex2: [[1,2],[3,5],[6,7],[8,10],[12,16]]
to_insert: [4,8]
out: [[1,2],[3,10],[12,16]]
(because 4,8 overlaps with the middle three, inclusive)
"""

def merge_intervals_b(arr,to_insert):
	"""
	So we can expect the input is sorted by leading time, which 
	would be my first step. 
	This seems like a matter of iterating over the array,
	with the first test finding a left side overlap, and then continuing iteration
	until the right side is closed out. first step let's try this by making a new array
	This is too sloppy.
	"""
	out = []
	left_found,right_found = False,False
	for pair in arr:
		print(pair,out)
		if not left_found:
			if to_insert[0] <= pair[1]:
				left_found = True
				print("0")
			
			out.append(pair)
		elif not right_found:
			if to_insert[1] < pair[0]:
				out[-1][1] = to_insert[1]
				print("b")
				out.append(pair)
			elif to_insert[1] >= pair[0]:
				if to_insert[1] > pair[1]:
					print("c")
					continue
				else:
					out[-1][1] = pair[1]
					right_found = True
					print("d")
			else:
				print("e")
				out.append(pair)
		else:
			print("f")
			out.append(pair)
	if not right_found:
		print("here")
		out[-1][1] = to_insert[1]
	return out
		
def merge_intervals(arr,to_insert):
	"""
	I don't know what i'm stumbling on here,
	except i'm going about it in the wrong way.
	It can't be this complicated.
	Let's try this again, very slowly. 
	"""
	ptr = 0
	l_inserted = False
	arrlen = len(arr)
	out = []
	while not l_inserted and ptr < arrlen:
		el = arr[ptr]
		if to_insert[1] < el[0]:
			out.append(to_insert)
			out.append(el)
			l_inserted = True
		elif to_insert[1] < el[1]:
			if not to_insert[0] >= el[0]:
				el[0] = to_insert[0]
			out.append(el)
			l_inserted = True
		elif to_insert[0] <= el[1]:
			if to_insert[0] < el[0]:
				el[0] = to_insert[0]
			el[1] = to_insert[1]
			out.append(el)
			l_inserted = True
		else:
			out.append(el)
		ptr+=1
	while ptr < arrlen:
		el = arr[ptr]
		if to_insert[1] >= el[0]:
			if to_insert[1] > el[1]:
				out[-1][1] = to_insert[1]
			else:
				out[-1][1] = el[1]
		else:
			out.append(el)
		ptr += 1
	if not l_inserted:
		out.append(to_insert)
	return out

if __name__ == "__main__":
	arr_1 = [[1,3],[6,9]]
	to_insert_1 = [2,5]
	expected_1 = [[1,5],[6,9]]
	res1 = merge_intervals(arr_1,to_insert_1)
	print("result",res1)
	print("expected",expected_1)
	assert(res1 == expected_1)
	arr_2 = [[1,2],[3,5],[6,7],[8,10],[12,16]]
	to_insert_2 = [4,8]
	expected_2 = [[1,2],[3,10],[12,16]]
	res2 = merge_intervals(arr_2,to_insert_2)
	print("result",res2)
	print("expected",expected_2)
	assert(res2 == expected_2)
	arr_3 = [[1,5]]
	to_insert_3 = [2,7]
	expected_3 = [[1,7]]
	res3 = merge_intervals(arr_3,to_insert_3)
	print(res3)
	assert(res3 == expected_3)
	arr_4 = [[1,5]]
	to_insert_4 = [2,3]
	expected_4 = [[1,5]]
	res4 = merge_intervals(arr_4,to_insert_4)
	print("result",res4)
	print("expected",expected_4)
	assert(res4 == expected_4)
	arr_5 = [[1,5]]
	to_insert_5 = [6,8]
	expected_5 = [[1,5],[6,8]]
	res5 = merge_intervals(arr_5,to_insert_5)
	print("result",res5)
	print("expected",expected_5)
	assert(res5 == expected_5)
	arr_6 = [[1,5]]
	to_insert_6 = [0,5]
	expected_6 = [[0,5]]
	res6 = merge_intervals(arr_6,to_insert_6)
	assert(res6 == expected_6)
