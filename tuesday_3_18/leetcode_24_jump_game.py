"""
Given an array of non-negative integers, you are initially
positioned at the first index of the array.

Each element in the array represents your maximum jump length:

ex: [2,3,1,1,4]
out: true
explanation: jump 1 step from index 0 to 1, then
three steps to the last index

ex: [3,2,1,0,4]
out: false
explanation: you get bogged down at 3 no matter what.
"""

def jump_game_2(arr):
	"""
	So one tactic I could take here is scan for the zeroes
	and then.. nah.
	Doing this recursively would work; i could take an index,
	scan forward on the remainder in a loop, rinse and repeat,
	gradually decreasing the input array. The problem is that 
	it has exponential complexity.
	If we're going to go with a matrix solution, we need to figure
	out what to store. 
	I have an array length I need to satisfy, and each value has 1->N
	available steps. Could I accomplish this with a nested for-loop?
	Let's try to solve for when it DOESN't work. In the second example
	ex: [3,2,1,0,4], all initial paths lead to idx 3, which is zero.
	"""
	arrlen = len(arr)
	remaining = arrlen
	legals = []
	for i in arr:
		if i > 0:
			legals.append(True)
		else:
			legals.append(False)
	for ind, el in enumerate(arr):
		for j in range(el,0,-1):
			if ind+j >= arrlen:
				return True
			if legals[ind+j] == False:
				continue
			else:
				return jump_game(arr[ind+1:])
		return False
	return True

def jump_game_2(arr):
	"""
	Let's try a different approach - we start from the far right,
	and scan to the leftmost point that can reach it.
	From there, we scan again to the leftmost point again, until we reach the beginning
	note quite with this one
	"""
	arrlen = len(arr)
	if arrlen <= 1:
		return True
	far_right = arrlen - 2
	furthest_left = None
	while far_right >= 0:
		## means we've gone too far
		if arr[far_right] + far_right < arrlen-1:
			break
		else:
			furthest_left = far_right
		far_right -= 1
	if furthest_left is None:
		return False
	return jump_game(arr[:furthest_left])

def jump_game(arr):
	"""
	Let's try avoiding recursion and just do a simple loop. 
	We scan from right to left, going to the leftmost available point.
	Our termination 
	"""

	arrlen = len(arr)
	if arrlen <= 1:
		return True
	right_ptr = arrlen - 1
	for i in range(right_ptr,-1,-1):
		if arr[i] + i >= right_ptr:
			right_ptr = i
	return right_ptr == 0

if __name__ == "__main__":
	arr = [3,2,1,0,4]
	res = jump_game(arr)
	print(res)
	arr = [2,3,1,1,4]
	res = jump_game(arr)
	print(res)
	arr = [2,3,1,1,4,2,3,1,1,4,2,3,1,1,4]
	res = jump_game(arr)
	print(res)
	arr = [0]
	res = jump_game(arr)
	print(res)
	arr = [0,2,3]
	res = jump_game(arr)
	print(res)
