"""
Given a btree, find it's maximum depth.

ex:
	3
     9     20
         15   7

res: 3
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def max_depth_bfs(root):
	"""
	One way to do this is a level order traversal - 
	we just store all the known levels, and return the tail.
	"""
	if not root:
		return 0
	queue = [(root,1)]
	max_depth = 0
	while queue:
		node,level = queue.pop(0)
		new_level = level + 1
		if node.left:
			queue.append([node.left,new_level])
		if node.right:
			queue.append([node.right,new_level])
		max_depth = max(max_depth,level)

	return max_depth

def max_depth_dfs(root,current_level):
	"""
	Other way to do this i think is just doing
	an inorder traversal, returning the current level
	"""
	if not root:
		return current_level
	current_level += 1
	depth = max(max_depth_dfs(root.left,current_level),
			max_depth_dfs(root.right,current_level))

	return depth

if __name__ == "__main__":
	n9 = TreeNode(9)
	n15 = TreeNode(15)
	n7 = TreeNode(7)
	n20 = TreeNode(20,n15,n7)
	root = TreeNode(3,n9,n20)
	out = max_depth_bfs(root)
	print(out)
	out = max_depth_dfs(root,0)
	print(out)
