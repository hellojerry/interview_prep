"""
given a btree, return the level order
traversal of its nodes (left to right,
level by level).

ex:
	3
     9     20
         15   7

res: [[3],[9,20],[15,7]]
"""
class TreeNode(object):
	def __init__(self,val,left=None,right=None):
		for i in [left,right]:
			assert(i is None or type(i) == TreeNode)
		assert(type(val) == int)
		self.val = val
		self.left = left
		self.right = right
	def __str__(self):
		return "TreeNode(%d)" % self.val
	def __repr__(self):
		return "TreeNode(%d)" % self.val

def level_order(root):
	if not root:
		return []
	current_level = 0
	queue = [(root,current_level)]
	out = []
	while queue:
		node,level = queue.pop(0)
		new_level = level + 1
		if node.left:
			queue.append([node.left,new_level])
		if node.right:
			queue.append([node.right,new_level])
		if len(out) == 0:
			out.append([node.val,level])
		else:
			if out[-1][-1] == level:
				out[-1].insert(-1,node.val)
			else:
				out[-1].pop()
				out.append([node.val,level])
	out[-1].pop()
	return out
if __name__ == "__main__":
	n9 = TreeNode(9)
	n15 = TreeNode(15)
	n7 = TreeNode(7)
	n20 = TreeNode(20,n15,n7)
	root = TreeNode(3,n9,n20)
	out = level_order(root)
	print(out)
