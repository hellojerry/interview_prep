"""
Given a string, find the longest palindromic substring.


ex: babad
out: bab
(aba is also valid)

ex: cbbd
out: bb
"""

def longest_palindromic_substring(my_str):
	"""
	My thought with this is to start with
	the first letter and store it as the longest.
	We retain a left pointer and right pointer.
	we break when right pointer hits the tail.
	I feel like this is nearly n2 in complexity.
	There might be another solution though
	"""
	strlen = len(my_str)
	if strlen == 1:
		return my_str[0]
	if strlen == 0:
		return ""
	longest = my_str[0]
	left_ptr, right_ptr = 0,1
	while left_ptr < strlen:
		while right_ptr < strlen:
			candidate = my_str[left_ptr:right_ptr+1]
			if candidate == candidate[::-1]:
				if len(candidate) > len(longest):
					longest = candidate
			right_ptr += 1
		left_ptr += 1
		right_ptr = left_ptr + 1
	return longest

if __name__ == "__main__":
	a1 = "babad"
	res = longest_palindromic_substring(a1)
	print(res)
	a2 = "cbbd"
	res = longest_palindromic_substring(a2)
	print(res)
	a3 = "racecar"
	res = longest_palindromic_substring(a3)
	print(res)
