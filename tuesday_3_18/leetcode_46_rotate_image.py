"""
Rotate a 2d array clockwise 90 degrees


a = [
	[1,2,3,4,5],
	[6,7,8,9,10],
	[11,12,13,14,15],
	[16,17,18,19,20]
	[21,22,23,24,25]
]
res = [
	[21,16,11,6,1],
	[22,17,12,7,2],
	[23,18,13,8,3],
	[24,19,14,9,4],
	[25,20,15,10,5],
]
"""
from pprint import pprint
def rotate_clockwise(matrix):
	"""
	[0,1] = [3,0],
	[1,4] = [0,1],
	[4,3] = [1,4],
	[3,0] = [4,3]
	"""
	rows = len(matrix)
	cols = len(matrix[0])
	m = matrix
	for r in range(int(rows/2)):
		for c in range(r,rows - r - 1):
			d1 = rows - r - 1
			d2 = cols - c - 1
			(m[r][c],m[c][d1],
			m[d1][d2],m[d2][r]) = (
			m[d2][r],m[r][c],
			m[c][d1],m[d1][d2])
			pprint(matrix)
		

if __name__ == "__main__":
	a = [
		[1,2,3,4,5],
		[6,7,8,9,10],
		[11,12,13,14,15],
		[16,17,18,19,20],
		[21,22,23,24,25]
	]
	rotate_clockwise(a)
	print(a)
	res = [
		[21,16,11,6,1],
		[22,17,12,7,2],
		[23,18,13,8,3],
		[24,19,14,9,4],
		[25,20,15,10,5],
	]
	assert(a == res)
