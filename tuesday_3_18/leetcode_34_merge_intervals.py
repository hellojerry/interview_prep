"""
Given a collection of intervals,
merge all overlapping intervals

ex: [[1,3],[2,6],[8,10],[15,18]]
out: [[1,6],[8,10],[15,18]]

ex: [[1,4],[1,5]]
out: [[1,5]]
"""

def merge_intervals(arr):
	"""
	I'm going to assume this is sorted.
	If it's not, the fix is simple.
	we check the current with prior.
	if prior.end >= current.start,
	prior.end = current.end
	else: append
	"""
	arrlen = len(arr)
	if arrlen <= 1:
		return arr
	arr.sort(key=lambda x: x[0])
	out = [arr[0]]
	for i in range(1,arrlen):
		el = arr[i]
		if el[0] <= out[-1][1]:
			if out[-1][0] <= el[0] and out[-1][1] >= el[1]:
				continue
			out[-1][1] = el[1]
			
		else:
			print("a",el,out)
			out.append(el)
	return out

if __name__ == "__main__":	
	a =  [[1,3],[2,6],[8,10],[15,18]]
	expected =  [[1,6],[8,10],[15,18]]
	res = merge_intervals(a)
	print(expected == res)
	print(res)	
	a =  [[1,4],[1,5]]
	expected =  [[1,5]]
	res = merge_intervals(a)
	print(expected == res)
	a = [[1,4],[0,4]]
	expected = [[0,4]]
	res = merge_intervals(a)
	print(res == expected)
	a = [[1,4],[0,1]]
	expected = [[0,4]]
	res = merge_intervals(a)
	print(res == expected)
	a= [[1,4],[2,3]]
	expected= [[1,4]]
	res = merge_intervals(a)
	print(res == expected)
