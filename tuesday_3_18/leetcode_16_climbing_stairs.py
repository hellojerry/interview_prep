"""
you are climbing a staircase.
You can take 1 or 2 steps.
How many distinct combinations are there?

example: N=2
res: 2 (1 and 1, 2)
"""

def count_stairs_recursive(remaining):
	"""
	This solves it btw, but leetcode is retarded
	"""
	if remaining == 0:
		return 1
	if remaining < 0:
		return 0
	return count_stairs_recursive(remaining-1) + count_stairs_recursive(remaining-2)


def count_stairs(matrix,remaining):
	"""
	The other way to do this is with an array or a matrix.
	"""
	if remaining == 0:
		return 1
	if remaining < 0:
		return 0
	if matrix[remaining] == -1:
		
		with_two = count_stairs(matrix,remaining-2)
		with_one = count_stairs(matrix,remaining-1)
		matrix[remaining] = with_two + with_one
	
	return matrix[remaining]

def kickoff(remaining):
	matrix = [-1 for i in range(remaining+1)]
	return count_stairs(matrix,remaining)
	

if __name__ == "__main__":
	r1 = kickoff(2)
	print(r1)
	r2 = kickoff(3)
	print(r2)
	r3 = kickoff(38)
	print(r3)
