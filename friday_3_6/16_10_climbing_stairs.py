"""
You are climbing stairs. You can advance 1 to K steps
at a time. the destination is exactly N steps up. 

For example:

N=4, K=2 has five possible combinations:

- four single steps
- two singles then a double
- a single then a double then a single
- a double then two singles
- two doubles

in other words

[[1,1,1,1],[1,1,2],[1,2,1],[2,1,1],[2,2]]
How many ways are there in which you can take the last step?
I think this is a misleading question, it should just
be how many different routes can you take.
"""

def stair_climber_basic(remaining,avail):
	"""
	So the recursive solution would be something
	like 
	"""
	if remaining < 0:
		return 0
	if remaining == 0:
		return 1
	out = 0
	for val in avail:
		out += stair_climber_basic(remaining - val, avail)
	return out

def stair_climber_r(matrix,avail,remaining):
	"""
	First solution probably has some sort of complexity problem.
	Since this is dynamic programming, we'll want to use a 
	matrix or an array
	"""
	if remaining == 0:
		return 1
	if remaining < 0:
		return 0

	if matrix[remaining] == 0:
		out = 0
		for i in avail:
			out += stair_climber_r(matrix,avail,remaining-i)
		matrix[remaining] = out 
	return matrix[remaining]

def stair_climber(N,K):
	choices = list(range(1,K+1))
	matrix = [0]*(N+1)
	res = stair_climber_r(matrix,choices,N)
	return res

def check(N,K):
	def compute_number_of_ways_to_h(h):
		if h <= 1:
			return 1
		if number_of_ways_to_h[h] == 0:
			number_of_ways_to_h[h] = sum(
				compute_number_of_ways_to_h(h-i)
				for i in range(1,
				min(K,h) + 1))
		return number_of_ways_to_h[h]
	number_of_ways_to_h = [0]*(N+1)
	return compute_number_of_ways_to_h(N)


if __name__ == "__main__":
	r1 = stair_climber_basic(4,[1,2])
	a1 = stair_climber(4,2)
	c1 =check(4,2)
	c2 = check(15,5)
	r2 = stair_climber(15,5)
	a2 = stair_climber_basic(15,list(range(1,6)))
	assert(r1 == c1 == a1)
	assert(r2 == c2 == a2)
