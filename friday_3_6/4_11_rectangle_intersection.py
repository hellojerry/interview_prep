"""
Write a program which tests if two rectangles have a nonempty
intersection. If the intersection is non-empty, 
return the rectangle formed by their intersection.

Hint: Think of the X and Y dimensions independently.
"""

from collections import namedtuple
Rectangle = namedtuple("Rectangle",[
		"name","left","top",
		"bottom","right"])

def get_width_overlap(A,B):
	## swap so A is leftmost.
	if A.left > B.left:
		A,B = B,A
	width_a = A.right - B.left
	width_b = B.right - B.left
	width_overlap = 0
	if A.right < B.right and A.left > B.left:
		width_overlap = width_a
	elif A.right > B.right and A.left < B.left:
		width_overlap = width_b
	elif A.right == B.left or B.right == A.left:
		width_overlap = 0
	elif A.right < B.left:
		width_overlap = 0
	elif A.right > B.left:
		width_overlap = A.right - B.left
	elif A.left < B.right:
		width_overlap = B.right - A.left
	else:
		width_overlap = 0
	return width_overlap

def get_height_overlap(A,B):
	## swap so A is bottom-most
	if A.bottom > B.bottom:
		A,B = B,A
	height_a = A.top - A.bottom
	height_b = B.top - B.bottom
	## height overlap block
	## if A.top
	height_overlap = 0
	## this is for when we have a total enclosure of A by B
	## on the Y axis.
	if A.top < B.top and A.bottom > B.bottom:
		height_overlap = height_a
	elif A.top > B.top and A.bottom < B.bottom:
		height_overlap = height_b
	elif A.top == B.bottom or B.top == A.bottom:
		height_overlap = 0
	elif A.top < B.bottom:
		height_overlap = 0
	elif A.top > B.bottom:
		height_overlap = A.top - B.bottom
	elif A.bottom < B.top:
		height_overlap = B.top - A.bottom
	else:
		height_overlap = 0
	return height_overlap

def is_inside(A,B):
	"""
	So how do we determine if there's an overlap
	on just the height basis?
	"""

	height_overlap = get_height_overlap(A,B)
	width_overlap = get_width_overlap(A,B)
	return height_overlap * width_overlap

if __name__ == "__main__":
	## case 1: rectangle A's top right corner is inside rectangle B's bottom left.
	## should account for all cases of corner overlap.
	A = Rectangle("A",0,5,0,5)
	B = Rectangle("B",3,6,3,6)
	assert(is_inside(A,B) == 4)
	assert(is_inside(B,A) == 4)
	## Case 2: rectangle A is taller and thinner than B, and is overlayed on B's middle
	## such the the bottom of A is lower than B bottom and top of A is higher than A top.
	## while B is wider to the left and right of A.
	A = Rectangle("A",2,5,2,5)
	B = Rectangle("B",0,4,3,6)
	assert(is_inside(A,B) == 3)
	assert(is_inside(B,A) == 3)
	## Case 3: Rectangle A is wholly inside rectangle B.
	A = Rectangle("A",1,2,1,2)
	B = Rectangle("B",0,4,0,4)
	assert(is_inside(A,B) == 1)
	assert(is_inside(B,A) == 1)
	## Case 4: Rectangle A's right tab is inside of rectangle B, and rectangle B
	## is taller than rectangle A. 
	A = Rectangle("A",1,4,2,4)
	B = Rectangle("B",3,5,1,5)
	assert(is_inside(A,B) == 2)
	assert(is_inside(B,A) == 2)

	## Case 5: Rectangle A's top tab is inside of Rectangle B, and rectangle B
	## is wider than rectangle A.
	A = Rectangle("A",2,4,1,4)
	B = Rectangle("B",1,5,2,5)
	assert(is_inside(A,B) == 4)
	assert(is_inside(B,A) == 4)

	## case 6: rectangle A and B are of equal height, and rectangle A's right tab 
	## is inside of rectangle B.
	A = Rectangle("A",1,4,2,4)
	B = Rectangle("B",3,4,2,6)
	assert(is_inside(A,B) == 2)
	assert(is_inside(B,A) == 2) 

	## case 7: no overlap, adjacent left-right
	A = Rectangle("A",1,3,1,3)
	B = Rectangle("B",3,3,1,5)
	assert(is_inside(A,B) == 0)
	assert(is_inside(B,A) == 0)

	## case 8: no overlap, adjacent top-bottom
	A = Rectangle("A",2,2,1,3)
	B = Rectangle("B",2,3,2,3)
	assert(is_inside(A,B) == 0)
	assert(is_inside(B,A) == 0)
	## case 9: no overlap, no adjacency, same plane
	A = Rectangle("A",1,3,2,2)
	B = Rectangle("B",4,3,2,5)
	assert(is_inside(A,B) == 0)
	assert(is_inside(B,A) == 0)

	## case 10: no overlap, no adjacency, diagonal
	A = Rectangle("A",1,2,1,2)
	B = Rectangle("B",3,3,2,4)
	assert(is_inside(A,B) == 0)
	assert(is_inside(B,A) == 0)
