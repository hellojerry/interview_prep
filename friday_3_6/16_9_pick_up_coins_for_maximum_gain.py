"""
In the pick-up-coins game, an even number of coins
are placed in a line. Two players take turns at choosing
one coin each - they can only choose from the two coins
at the ends of the line. The game ends when all the coins
have been picked up. The player whose coins have the higher total
value wins. A player cannot pass his turn.


ex:

	[25,5,10,5,10,5,10,25,1,25,1,25,1,25,5,10]

Design an efficient algorithm for computing the maximum
total value for the starting player in the pick up coins game.
"""



def maximum_money_initial(coins_arr):
	"""
	My initial general idea was right - the mistake
	I made was assuming the opponent didn't make the optimal
	decision. 
	"""
	arrlen = len(coins_arr)
	if arrlen == 0:
		return 0
	max_left = coins_arr[0] + min(
			maximum_money_initial(coins_arr[2:]),
			maximum_money_initial(coins_arr[1:-1])
		)
	max_right = coins_arr[-1] + min(
			maximum_money_initial(coins_arr[:-2]),
			maximum_money_initial(coins_arr[1:-1])
			)
	return max(max_left,max_right)

def maximum_money_solver(matrix,left_idx,right_idx,coins_arr):
	"""
	Initial solution's issue might be nasty from
	a complexity perspective. Same principle though - 
	We want the current value, plus the minimum of the remaining
	array
	"""
	if left_idx > right_idx:
		return 0
	if matrix[left_idx][right_idx] == 0:
		with_left = coins_arr[left_idx] + min(
			maximum_money_solver(matrix,left_idx+2,
					right_idx,coins_arr),
			maximum_money_solver(matrix,left_idx+1,
					right_idx-1,coins_arr)
			)

		with_right = coins_arr[right_idx] + min(
			maximum_money_solver(matrix,left_idx+1,
					right_idx-1,coins_arr),
			maximum_money_solver(matrix,left_idx,
					right_idx-2,coins_arr)

		)
		matrix[left_idx][right_idx] = max(with_left,
						with_right)


	return matrix[left_idx][right_idx]

def maximum_money(coins_arr):
	arrlen = len(coins_arr)
	matrix = [[0]*arrlen for i in coins_arr]
	return maximum_money_solver(matrix,0,arrlen-1,coins_arr)


def check(coins):

	def compute_maximum_revenue_for_range(a,b):
		if a > b:
			return 0
		if maximum_revenue_for_range[a][b] == 0:
			max_revenue_a = coins[a] + min(
				compute_maximum_revenue_for_range(
					a+2,b),
				compute_maximum_revenue_for_range(
					a+1,b-1)
			)
			max_revenue_b = coins[b] + min(
				compute_maximum_revenue_for_range(
					a+1,b-1),
				compute_maximum_revenue_for_range(
					a,b-2)
			)
			maximum_revenue_for_range[a][b] = max(
				max_revenue_a,max_revenue_b)
		return maximum_revenue_for_range[a][b]

	maximum_revenue_for_range = [[0]*len(coins) for _ in coins]
	return compute_maximum_revenue_for_range(0, len(coins)-1)


if __name__ == "__main__":
	arr = [25,5,10,5,10,5,10,25,1,25,1,25,1,25,5,10]
	r1 = maximum_money_initial(arr)
	c1 = check(arr)
	b1 = maximum_money(arr)
	assert(r1 == c1 == b1)
	arr2 = [5,25,10,1]
	r2 = maximum_money_initial(arr2)
	c2 = check(arr2)
	b2 = maximum_money(arr2)
	assert(r2 == c2 == b2)
