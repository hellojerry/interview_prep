"""
A string is said to be palindromic if it reads the same backwards
and forwards. A decomposition of a string is a set of strings whose
concatenation is the string. for example, "611116" is palindromic,
and "611","11"6 is one decomposition for it.

Compute all palindromic decompositions of a given string. For example,
if the string is "0204451881", then the decomposition "020,"44","5","1881"
is palindromic, as is "020","44","5","1","88","1". However,
"02044","5","1881" is not a palindromic decomposition.

Hint: focus on the first string in a palindromic decomposition.

test case: 02044

res: ["0","2","0","4","4"], ["020","44"],["0","2","0","44"], ["020","4","4"]

"""


def directed_compositions(my_str,offset,partial_partition,result):
	"""
	I don't understand this one. It's a little tricky to visualize.
	"""
	if offset == len(my_str):
		result.append(list(partial_partition))
		return
	for i in range(offset+1, len(my_str)+1):
		prefix = my_str[offset:i]
		if prefix == prefix[::-1]:
			directed_compositions(my_str, i,partial_partition + [prefix],result)
def kickoff(my_str):
	res = []
	directed_compositions(my_str,0, [],res)
	print(res)

if __name__ == "__main__":
	kickoff("02044")
