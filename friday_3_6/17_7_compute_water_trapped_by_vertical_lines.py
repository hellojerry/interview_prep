"""
Write a program which takes as input an integer array
and returns the pair of entries that trap the maximum amount of water.



                      |
                    | |
                | | | |                 |
	      | | | | |     |   |       |
	  |   | | | | | |   |   | |   | |
	| | | | | | | | | | | | | | | | | |
       [1,2,1,3,4,4,5,6,2,1,3,1,3,2,1,2,4,1]

maximum amount here is between idx 4 and idx 16, with a total of 4*12 = 48
"""
from collections import namedtuple
result_type = namedtuple("Result", ["value","left_idx","right_idx"])
def get_maximum(arr):
	"""
	I think what we can do here is
	1. get area trapped between 0 and len-1
	2. move left pointer forward:
		if a new max:
			move left forward
		else:
			move the smaller value pointer forward	
	"""

	current_max = 0
	arrlen = len(arr)
	current_max_left,current_max_right = 0, arrlen-1
	left_ptr,right_ptr = 0, arrlen-1
	while left_ptr < right_ptr:
		left_val,right_val = arr[left_ptr],arr[right_ptr]
		area = min(left_val,right_val) * (right_ptr - left_ptr)
		if area > current_max:
			current_max = area
			current_max_left,current_max_right = left_ptr,right_ptr
			left_ptr += 1
		else:
			if left_val > right_val:
				right_ptr -= 1
			else:
				left_ptr += 1


	return result_type(current_max,current_max_left,current_max_right)
if __name__ == "__main__":
	arr = [1,2,1,3,4,4,5,6,2,1,3,1,3,2,1,2,4,1]
	res = get_maximum(arr)
	assert(res == result_type(48,4,16))
