"""
Consider the problem of laying out text using a 
fixed width font. Each line can hold no more than a fixed number 
of characters. Words on a line are to be separated by exactly
one blank. Therefor, we may be left with whitespace at the end of a line
since the next word will not fit in the remaining space.

Define the messiness of the end-of-line whitespace as follows.
The messiness of a single line ending with B characters is B**2.
The total messiness of a sequence of lines is the sum of the messiness
of all the lines. A sequence of words can be split across lines
in different ways with different messiness. 

Ex: (both lines are 36 characters)

A: (messiness of 206)
I_have_inserted_a_large_number_of___
new_examples_from_the_papers_for_the
Mathematical_Tripos_during_the_last_
twenty_years,_which_should_be_useful
to_Cambridge_students.______________

B: (messiness of 82)

I_have_inserted_a_large_number______
of_new_examples_from_the_papers_____
for_the_Mathematical_Tripos_during__
the_last_twenty_years,_which_should_
be_useful_to_Cambridge_students.____

Given a string of words separated by single blanks,
decompose the text into lines such that no word is split across lines
and the messiness of the decomposition is minimized. Each line
can hold no more than a specified number of characters.

Hint: focus on the last word and the last line.
"""

def pretty(text,max_length):
	"""
	The strategy with all these things seems to boil down to using
	a matrix or an array. 
	"""
	words = text.split(" ")
	


if __name__ == "__main__":
	text = "I have inserted a large number of new examples from the papers for the Mathematical Tripos during the last twenty years, which should be useful to Cambridge students."
