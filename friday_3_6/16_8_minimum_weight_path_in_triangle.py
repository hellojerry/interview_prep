"""
A sequence of integer arrays in which the Nth array consists
of N entries naturally corresponds to a triangle of numbers.

		2
	     4     4
           8    5    6
        4    2     6    2
      1    5    2    3    4
This could also be represented as

arr = [
	[2],
	[4,4],
	[8,5,6],
	[4,2,6,2],
	[1,5,2,3,4]
]

Define a path in the triangle to be a sequence of entries in the triangle
in which adjacent entries in the sequence correspond to entries that
are adjacent in the triangle. The path must start at the top,
descend the triangle continuously, and end with an entry on the bottom row.
The weight of a path is the sum of the entries.

Write a program that finds the minimum weight path in a triangle.
The minimum weight in the above example is 15 (2,4,5,2,2), aka
[0][0], [1][1], [2][1], [3][1], [4][2]

Hint: what property does the prefix of a minimum weight have?
"""

def get_minimum_weight(matrix,cur_path,proposed_row,proposed_col):
	"""
	My thought here is that we can compute left and right, take the minimum,
	and recurse downwards returning the minimum.
	if we start at 0,0, then left is [1][0], and right is [1][1]
	the new column can either be equal to the current column, or the current column +1
	"""
	if proposed_row >= len(matrix):
		## we want to exit here... probably with a current total
		return sum(cur_path)

	new_row = proposed_row + 1
	cur_res = sum(cur_path)
	with_proposed = cur_path + [matrix[proposed_row][proposed_col]]
	left_res = get_minimum_weight(matrix, with_proposed, new_row, proposed_col)
	right_res =get_minimum_weight(matrix, with_proposed, new_row, proposed_col+1)
	return min(left_res,right_res)

if __name__ == "__main__":
	arr = [
		[2],
		[4,4],
		[8,5,6],
		[4,2,6,2],
		[1,5,2,3,4]
	]
	assert(get_minimum_weight(arr,[],0,0) == 15)
	
	


