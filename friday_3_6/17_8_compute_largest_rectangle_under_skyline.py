"""
You are given a sequence of adjacent buildings. Each has a width
and a height. Compute the area of the largest rectangle under the skyline

I can't tell if this problem is saying that the widths are always the same
(i.e. all are 2) or if the widths are heterogeneous. The solution seems
to presuppose that the buildings are all width 1, as does the example.


	        |     | |
	      | |     | | |
	  |   | |     | | |
	  |   | | |   | | |     |
	  | | | | | | | | | |   |
	| | | | | | | | | | | | |
	1,4,2,5,6,3,2,6,6,5,2,1,3

The maximum area in this example is from idx 2 -> 11 (inclusive), which
is 2*10 == 20


	    |
	    | |
            | |
	|   | | |
	| | | | | 
	2 1 5 4 2 => 8


	  | |
	| | |   
	| | | |       | |
	| | | | | | | | | |
        3 4 4 2 1 1 1 2 2 1 -> 10

	  | |
	| | |   
	| | | |       | 
	| | | | | | | | 
        3 4 5 2 1 1 1 2 -> 9

"""

def get_max_area(arr):
	"""
	I think we want the combination of the highest minimum height
	* width. How do we get the lowest between two points without 
	a computational explosion? 
	I suppose the effect of finding the minimum interceding gets weaker
	as we move the pointers inward.
	The problem is in the pointer moving strategy. 
	hmm. must've tricked myself in debugging.
	""" 
	arrlen = len(arr)
	best_left,best_right = 0,0
	max_area = 0
	left_ptr,right_ptr = 0, arrlen - 1
	while left_ptr < right_ptr:
		left_val,right_val = arr[left_ptr],arr[right_ptr]
		min_level = min(arr[left_ptr:right_ptr+1])
		dist = right_ptr - left_ptr
		area = min_level * (dist+1)
		if area > max_area:
			max_area = area
			best_left,best_right = left_ptr,right_ptr
		if left_val > right_val:
			right_ptr -= 1
		else:
			left_ptr += 1
	return max_area

if __name__ == "__main__":
	res = get_max_area([1,4,2,5,6,3,2,6,6,5,2,1,3])
	assert(res == 20)
	assert(get_max_area([2,1,5,4,2]) == 8)
	assert(get_max_area([3,4,4,2,1,1,1,2,2,1]) == 10)
	assert(get_max_area([3,4,5,2,1,1,1,2]) == 9)
