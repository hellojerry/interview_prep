"""
Write a program which returns all distinct binary trees
with a specified number of nodes. For example,
if the number of nodes is specified to be three,
there are five possible binary trees:

	A       A         A       A   A
         B       B      B   C    B   B
          C     C               C     C
"""
from collections import namedtuple

BTNode = namedtuple("BTNode",["val","left","right"])

def generate_btrees(ct):
	"""
	This problem is shit because it's going to be a pain to
	generate a testable return value. I don't think this is a good question.
	"""
	if ct == 0:
		return [None]
	out = []
	for i in range(ct):
		right_nodes = ct - 1 - i
		left_tree = generate_btrees(i)
		right_tree = generate_btrees(right_nodes)
		out += [BTNode(0,left,right) for left in left_tree for right in right_tree]
	return out

if __name__ == "__main__":
	res = generate_btrees(3)
	print(res)
